<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#"> Về chúng tôi </a>
        <span class="item">The new MB</span>
      </div>
    </div>
</div>
<section   class="sec banner-heading-1 banner-heading-1-2 next-shadow "   >
    <div class="container height2">
        <div class="divtext ">
        <h1 class=" efch-2 ef-img-l" >Chào MB! <br>Chào sự thay đổi diệu kỳ!</h1>
        <p  class=" efch-3 ef-img-l ">Tròn 25 năm thành lập, đây là thời điểm đẹp để MB tự tin và sẵn sàng cho một sự đổi thay diệu kỳ trên hành trình trở thành Ngân hàng thông minh và thuận tiện nhất.</p>
        </div>
    </div>
    <img class="br lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-15-2.jpg" src="">
</section>


<section class="sec-tb sec-img-text group-ef lazy-hidden">
  <div class="container">
    <div class="row center">
      <div class="col-lg-6">
        <div class="img  efch-2 ef-img-t ">
          <img class=" lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/img-5.jpg" >
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext ">
          <h2 class="ht  efch-1 ef-tx-t ">Thương hiệu <br> của chúng tôi</h2>
           
          <p class="efch-2 ef-tx-t">Để đi đầu trong một thế giới luôn chuyển động và thay đổi từng ngày như hiện nay, MB đã thực hiện một hành trình chuyển đổi, khởi đầu bằng sự chuyển đổi nhận diện thương hiệu mới: cung cấp dịch vụ ngân hàng hiện đại mang đến cho khách hàng và doanh nghiệp một tương lai ngập tràn triển vọng.</p>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec-tb sec-img-text bg-gray-2 group-ef lazy-hidden">
  <div class="container">
    <div class="row center end">
      <div class="col-lg-6">
        <div class="img  efch-2 ef-img-t ">
          <img class=" lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/img-6.png" >
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext  ">
          <h2 class="ht  efch-1 ef-tx-t ">Câu chuyện <br> của chúng tôi</h2>
           
          <p class="efch-2 ef-tx-t">Tự hào với nền tảng vững vàng được hình thành qua hơn 1/4 thế kỷ, MB đang vươn mình với những bước đi táo bạo để chuyển đổi thành một Ngân hàng số toàn diện, hiện đại, sẵn sàng cho thời đại công nghệ số, lấy khách hàng là trung tâm của mọi sự thay đổi. Chúng tôi tin rằng, nếu đồng hành cùng một Ngân hàng thông minh, thuận tiện và tin cậy, khách hàng của chúng tôi sẽ có thêm thời gian và cơ hội để gặt hái những thành công.</p>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec-tb sec-img-svg-3 group-ef lazy-hidden">
  <div class="container"  >
  
    <div class="row equalHeight list-item">
      <?php 
      $a_5_1 = ['img-7-1.svg','img-7-2.svg','img-7-3.svg'];
      $a_5_2 = ['Đơn giản và Thuận tiện nhờ Đổi mới Công nghệ','Kết nối bạn với những cơ hội tốt hơn.','Chỉ dẫn tin cậy, giúp bạn làm chủ tương lai vững bền'];

      for($i=1;$i<=3;$i++) { ?>
      <div class="col-sm-6 col-md-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/<?php echo $a_5_1[$i-1]; ?>" >
          </div>
          <div class="divtext">
            <h4 class="title equal"><?php echo $a_5_2[$i-1]; ?></h4>          
            <div class="desc">Là Ngân hàng thông minh, chúng tôi cam kết không ngừng đổi mới để cung cấp những giải pháp sáng tạo và an toàn, song hành cùng nhịp sống năng động của bạn.</div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<section  class=" sec-ab-2-5 sec-b" >
  <div class="container">
    <div class="row row-logo center">
      <div class="col-md-5">
        <div class="divtext">
          <h2 class="">Logo MB</h2>
          <p> <b>Logo mới của MB giữ lại hình ảnh ngôi sao trong logo trước đây như một di sản và là yếu tố cốt lõi mang bản sắc của MB, nhưng được phát triển thành một biểu tượng hiện đại hơn, thân thiện hơn, không ngừng chuyển động về phía trước - thể hiện cam kết mang đến dịch vụ ngân hàng thông minh tới khách hàng.</b>  </p>          
        </div>  
        
      </div>
      <div class="col-md-7">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/img-9.png" >
          </div>        
      </div>

    </div>

    <div class="list-5 equalHeight row list-item" >
        <?php
        for($i=1;$i<=3;$i++) {?>
          <div class="col-md-4">
              <div class="item efch-<?php echo $i+1; ?> ef-img-l ">
                <div class="divtext">
                  <h4 class="title equal line2">Biểu tượng ngôi sao lấy cảm hứng từ ngôi sao trên quốc kỳ Việt Nam </h4>
                  <div class="desc ">Kế thừa từ ngôi sao trong logo trước đây, như một di sản và là yếu tố cốt lõi mang bản sắc của MB, nhưng được phát triển và cấu thành bởi những yếu tố đồ họa đại diện cho văn hóa, công nghệ, tinh thần kết nối </div>
                </div>

              </div>
          </div>                  
        <?php } ?>
      </div>  
  </div>
</section>

<section class=" sec-tb bg-gray-2">
  <div class="container"  >
    <div class="bgvideo  lazy-hidden">
      <div class="entry-head text-center">
        <h2 class="ht">Logo MB - sự đổi thay mới</h2>
      </div>

      <div class="single_video  tRes_16_9 max750" data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0"> 
        <img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="assets/images/img-8.jpg" src="" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
      </div>
    </div>

  </div>
</section>


<?php include 'include/index-bottom.php';?>