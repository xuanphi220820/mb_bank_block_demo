<?php include 'include/index-top.php';?>
<section   class="sec banner-heading-1 lazy-hidden next-shadow" data-lazy-type="bg" data-lazy-src="assets/images/heading-15.jpg" >
    <div class="container">
        <div class="divtext top35">
          <h1 class=" efch-1 ef-img-l" >Đoàn kết lãnh đạo</h1>
          <div class="desc cl1 efch-2 ef-img-l">Lorem ipsum dolor sit amet</div>
        </div>
    </div>
</section>

<main id="main"  class="sec-tb " >
  <div class="container">
    <div class="sec-b">
      <h2 class="text-center h1">Hội đồng quản trị</h2>
      <div class="list-8 row list-item" >
          <?php
          $imgtile = ['ÔNG LÊ HỮU ĐỨC',
          'ÔNG LƯU TRUNG THÁI',
          'BÀ VŨ THỊ HẢI PHƯỢNG',
          'ÔNG ĐỖ MINH PHƯƠNG',
          'ÔNG LÊ VIẾT HẢI',
          'ÔNG KIỀU ĐẶNG HÙNG',
          'ÔNG NGÔ MINH THUẤN',
          'BÀ NGUYỄN THỊ NGỌC',
          'BÀ NGUYỄN THỊ THỦY',
          'ÔNG TRẦN TRUNG TÍN',
          'BÀ VŨ THÁI HUYỀN'];
          $imgposition = ['Chủ tịch HĐQT',
          'Phó Chủ tịch HĐQT',
          'Phó Chủ tịch HĐQT',
          'Phó Chủ tịch HĐQT',
          'Phó Chủ tịch HĐQT',
          'Thành viên HĐQT',
          'Thành viên HĐQT',
          'Thành viên HĐQT',
          'Thành viên HĐQT',
          'Thành viên HĐQT',
          'Thành viên HĐQT'];
          $img = ['Quantri1','Quantri2','Quantri3','Quantri4','Quantri5','Quantri6','Quantri7','Quantri8','Quantri9','Quantri10','Quantri11'];
          for($i=1;$i<=11;$i++) {?>
            <div class="col-md-3">
                <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l  btnModal"  data-modal="lanhdaoModal">
                  <div class="img tRes">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mblanhdao/<?php echo $img[$i-1]?>.jpg">
                  </div>
                  <div class="divtext">
                    <h4 class="title line2"><?php echo $imgtile[$i-1] ?> </h4>
                    <div class="desc line2"><?php echo $imgposition[$i-1] ?></div>
                  </div>
                </a>
            </div>                  
          <?php } ?>
      </div>
      <?php //include '_module/pagination.php';?>
    </div>

    <div class="sec-b">
      <h2 class="text-center h1">Ban kiểm soát</h2>
      <div class="list-8 row list-item" >
          <?php
          $img = ['kiemsoat1.jpg','kiemsoat2.jpg','kiemsoat3.jpg','kiemsoat4.jpg'];
          $imgtile = ['BÀ LÊ THỊ LỢI',
          'ÔNG ĐỖ VĂN HƯNG',
          'BÀ PHẠM THU NGỌC',
          'BÀ ĐỖ THỊ TUYẾT MAI'];
          $imgpositon = ['Trưởng Ban Kiểm soát',
          'Thành viên Ban Kiểm soát',
          'Thành viên Ban Kiểm soát',
          'Thành viên Ban Kiểm soát'];
          for($i=1;$i<=4;$i++) {?>
            <div class="col-md-3">
                <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l  btnModal"  data-modal="lanhdaoModal">
                  <div class="img tRes">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mblanhdao/<?php echo $img[$i-1]?>">
                  </div>
                  <div class="divtext">
                    <h4 class="title line2"><?php echo $imgtile[$i-1] ?></h4>
                    <div class="desc line2"><?php echo $imgposition[$i-1] ?></div>
                  </div>
                </a>
            </div>                  
          <?php } ?>
      </div>
    </div>
    <div >
      <h2 class="text-center h1">BAN ĐIỀU HÀNH</h2>
      <div class="list-8 row list-item" >
          <?php
          $img = ['dieuhanh1.jpg','dieuhanh2.jpg','dieuhanh3.jpg','dieuhanh4.jpg','dieuhanh5.jpg',
          'dieuhanh6.jpg','dieuhanh7.jpg','dieuhanh8.jpg','dieuhanh9.jpg','dieuhanh10.jpg','dieuhanh11.jpg',];
          $imgtile = ['ÔNG LƯU TRUNG THÁI',
          'BÀ NGUYỄN THỊ AN BÌNH',
          'BÀ NGUYỄN MINH CHÂU',
          'BÀ PHẠM THỊ TRUNG HÀ',
          'ÔNG TRẦN MINH ĐẠT',
          'ÔNG UÔNG ĐÔNG HƯNG',
          'ÔNG LÊ HẢI',
          'ÔNG HÀ TRỌNG KHIÊM',
          'ÔNG LÊ QUỐC MINH',
          'ÔNG LÊ XUÂN VŨ',
          'BÀ TRẦN THỊ BẢO QUẾ'];
          $imgposition = ['Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Thành viên Ban Điều Hành',
          'Phó Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Phó Tổng Giám đốc',
          'Thành viên Ban Điều Hành',
          'Thành viên Ban Điều Hành'];
          for($i=1;$i<=11;$i++) {?>
            <div class="col-md-3">
                <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l  btnModal"  data-modal="lanhdaoModal">
                  <div class="img tRes">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mblanhdao/<?php echo $img[$i-1]?>">
                  </div>
                  <div class="divtext">
                    <h4 class="title line2"><?php echo $imgtile[$i-1] ?></h4>
                    <div class="desc line2"><?php echo $imgposition[$i-1] ?></div>
                  </div>
                </a>
            </div>                  
          <?php } ?>
      </div>
    </div>    


  </div>      
</main>


<div id="lanhdaoModal" class="myModal lanhdaoModal "> 
    <div class="container max950">
        <div class="contentModal">
          <div class="logo"><a href="./"  > <img src="assets/images/logo-blue.svg" alt=""></a></div>
          <div class="row bottom">
            <div class="col-md-5">
              <div class="img tRes"><img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mblanhdao/dieuhanh1.jpg">  </div>
            </div>
            <div class="col-md-7">
              <div class="divtext">
                <h2 class="title ">ÔNG LƯU TRUNG THÁI </h2>
                <h3 class="position ">Phó Chủ tịch HĐQT </h3>
                <div class="desc ">
                  Ông Lưu Trung Thái tốt nghiệp Thạc sỹ Quản trị Kinh doanh – Đại học Hawaii – Mỹ. Ông Thái đã có gần 22 năm gắn bó với MB và nhiều năm liền nắm giữ các vị trí quan trọng của MB như Phụ trách Tín dụng Hội sở, Giám đốc Chi nhánh, Giám đốc Nhân sự, Phó Tổng Giám đốc. Từ năm 2011 đến 04/2014, ông được giao trọng trách Chủ tịch HĐQT kiêm Tổng Giám đốc Công ty CP Chứng khoán MB (MBS) và để lại nhiều dấu ấn mạnh mẽ trong quá trình tái cơ cấu công ty. Ngày 24/04/2013, Ông được tín nhiệm bầu làm thành viên HĐQT MB nhiệm kỳ 2009 - 2014 và chính thức được giao nhiệm vụ Phó Chủ tịch HĐQT từ tháng 09/2013. Năm 2017, ông được giao trọng trách là Phó Chủ tịch HĐQT kiêm Tổng giám đốc MB. Với những đóng góp của mình, tại ĐHĐCĐ nhiệm kỳ 2019 – 2024, ông tiếp tục được HĐQT giới thiệu và trúng cử vị trí Phó Chủ tịch HĐQT kiêm Tổng giám đốc MB 
                </div>
              </div>                    
            </div>
          </div>
        </div>
    </div>
</div>


<?php include 'include/index-bottom.php';?>