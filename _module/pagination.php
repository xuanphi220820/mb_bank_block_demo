<div class="pages">
	<ul class="page-numbers">
		<li><a class="prev page-numbers" href="#"><i class="icon-arrow-2 ix"></i></a></li>
		<li><span aria-current="page" class="page-numbers current">1</span></li>
		<li><a class="page-numbers" href="#">2</a></li>
		<li><span class="page-numbers">...</span></li>
		<li><a class="page-numbers" href="#">8</a></li>
		<li><a class="page-numbers" href="#">9</a></li>
		<li><a class="next page-numbers" href="#"><i class="icon-arrow-2"></i></a></li>
	</ul>	
</div>