<?php include 'include/index-top.php';?>
<section   class="sec banner-heading-1 lazy-hidden next-shadow" data-lazy-type="bg" data-lazy-src="assets/images/heading-15-1.jpg" >
    <div class="container">
        <div class="divtext width2">
        <h1 class=" efch-2 ef-img-l" >Tầm nhìn, sứ mệnh</h1>
        <p  class=" efch-3 ef-img-l cl1">Trở thành một Ngân hàng thuận tiện nhất  với Khách hàng</p>
        <p  class=" efch-4 ef-img-l cl1">Vì sự phát triển của đất nước, vì lợi ích của khách hàng</p>
        </div>
    </div>
</section>

<section  class="sec-tb " >
    <div class="container">
        <div class="entry-head text-center">
            <h2 class="ht efch-1 ef-img-l">Lịch sử hình thành</h2>
        </div>
        <p class="text-center fs16">Trải qua gần 25 năm xây dựng và trưởng thành, MB ngày càng phát triển lớn mạnh, trở thành một tập đoàn tài chính đa năng với ngân hàng mẹ MB tại Việt Nam & nước ngoài (Lào, Campuchia) và các công ty thành viên (trong lĩnh vực chứng khoán, bảo hiểm, tài chính tiêu dùng, quản lý quỹ, quản lý tài sản, bảo hiểm nhân thọ). Với các mặt hoạt động kinh doanh hiệu quả, MB đã khẳng định được thương hiệu, uy tín trong ngành dịch vụ tài chính tại Việt Nam. MB có các hoạt động dịch vụ và sản phẩm đa dạng trên nền tảng quản trị rủi ro vượt trội, hạ tầng CNTT hiện đại, phát triển mạnh mẽ mở rộng hoạt động trên các phân khúc thị trường mới bên cạnh thị trường truyền thống của một NHTM. Sau hơn 25 năm xây dựng và trưởng thành, hiện nay MB được đánh giá là một định chế tài chính vững vàng, tin cậy, phát triển an toàn bền vững, có uy tín cao.</p>

        <br><br><br> 
        <div class="max950">
          <div class="timeline ">
            <?php
            $tit = ['NĂM 2017 - NAY','NĂM 2010 - 2016','NĂM 2005 - 2009','NĂM 1994 - 2004'];
            $con = [
              'Đây là năm mở đầu quan trọng của giai đoạn chiến lược mới 2017 - 2021, trong đó MB định hướng tầm nhìn "Trở thành ngân hàng thuận tiện nhất" với mục tiêu đến năm 2021 sẽ nằm trong Top 5 hệ thống Ngân hàng Việt Nam hiệu quả kinh doanh và an toàn',
              'Năm 2010 là bước ngoặc quan trọng đưa MB ghi dấu ấn trở thành 1 trong những ngân hàng hàng đầu Việt Nam sau này.',
              'Giai đoạn này đánh dấu bước chuyển mình quan trọng, tạo nền tản quan trọng để vươn lên phát triển mạnh mẽ trong những năm tiếp theo.',
              'Từ ý tưởng ban đầu là xây dựng 1 tổ chức tính dụng phục dụ doanh nghiệp quân đội trong nhiệm vụ sản xuất kinh doanh thời kỳ tiền hội nhập'];
            $img = ['lichsu-1.jpg','lichsu-2.jpg','lichsu-3.jpg','lichsu-4.jpg'];
            for($i=1;$i<=4;$i++) {  ?>            
            <div class="group-ef lazy-hidden  item <?php if($i%2==0) echo 'item-2'; else echo 'item-1'; ?>">
              <div class="row grid-space-80 center <?php if($i%2==0) echo 'end ';  ?>">
                <div class="col-md-6 efch-1 ef-img-t">
                  <div class="img"><img  class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/mbhistory/<?php echo $img[$i-1] ?>" ></div>
                </div>
                <div class="col-md-6 ">
                  <span class="circle"></span>
                  <div class="divtext efch-2 ef-tx-t">
                    <h2 class="title"><?php echo $tit[$i - 1]?></h2>
                    <div class="desc"><?php echo $con[$i - 1]?></div>
                    
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>
        </div>

         
    </div>
</section>

<?php include 'include/index-bottom.php';?>