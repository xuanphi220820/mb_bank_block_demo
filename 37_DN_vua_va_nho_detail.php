<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
	<div class="container">
		<div class="breadcrumbs">
			<a class="item" href="#">Home</a>
			<a class="item" href="#">Doanh nghiệp</a>
			<span class="item">Doanh nghiệp vừa & nhỏ SME</span>
		</div>
	</div>
</div>

<section class="banner-img-1 next-shadow">
	<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/banner.jpg" src="assets/images/doanhnghiep/dnnho/banner.jpg">
</section>

<section class="sec-tb">
	<div class="container">
		<div class="max750">
			<h1 class="text-center">Nhu cầu của doanh nghiệp</h1>
			<div class="menuicon  owl-carousel s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$img = ['bank.svg','money-2.svg','key.svg','lai-suat.svg','dich-vu-khac.svg','money-1.svg','bank.svg','money-2.svg','key.svg','lai-suat.svg'];
				$a_h1 = [
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh',
					'Tài trợ & Phát sinh',
					'Sản phẩm ngoại hối & thị trường vốn',
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh'
				];
				$tab = ['#tab1','#tab2','#tab3','#tab4','#tab5','#tab6','#tab7'];
				for ($i = 1; $i <= 10; $i++) { ?>
					<div class="item">
						<a href="<?php echo $tab[$i-1] ?> " class="link scrollspy">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="title"><?php echo $a_h1[$i - 1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<section id="tab1" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Chuyển tiền quốc tế online',
				'Khấu chi dựa trên dòng tiền',
				'Cho vay mua ô tô đi lại',
				'Chuyển tiền quốc tế online',
				'Khấu chi dựa trên dòng tiền',
				'Cho vay mua ô tô đi lại'
			];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/<?php echo $img[$i - 1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
						<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt iste quas enim et ullam. Porro nesciunt explicabo, obcaecati, quasi dolores rem unde dolor illo ipsa. Repellat reprehenderit doloribus non illum.</div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab2" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Ngân hàng số MBBIZ</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Sms Banking',
				'Internet Baking (eMB)',
				'i-Transfer (luân chuyển hồ trên eMB)',
				'Sms Banking',
				'Internet Baking (eMB)',
				'i-Transfer (luân chuyển hồ trên eMB)'
			];
			$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-4.jpg','img-5.jpg','img-6.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/<?php echo $img[$i - 1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab4" class=" sec-b sec-cauhoi ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht ">Cho vay & bảo lãnh</h2>
		</div>
		<div class="accodion accodion-1">
			<?php
			$ques = [
				'Cho vay vốn lưu động',
				'Cho vay trung, dài hạn & dự án',
				'Dịch vụ thanh toán'
			];
			for ($i = 1; $i <= 3; $i++) {
				?>
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_2_<?php echo $i; ?>">
					<label class="accodion-title" for="chck_2_<?php echo $i; ?>"><span><?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content">
						<div class="inner">
							<p>Ngân hàng MBBank không giới hạn số tiền vay tối đa với KH, số tiền vay tối đa này phụ thuộc vào tình hình tài chính và khả năng trả nợ của bạn.</p>
							<p>Thời gian vay tối đa của sản phẩm tùy thuộc dự án liên kết với ngân hàng nhưng tối đa 20 năm </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
						</div>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<section id="tab3" class=" sec-b sec-cauhoi ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht ">Tài khoản và dịch vụ</h2>
		</div>
		<div class="accodion accodion-1">
			<?php
			$ques = [
				'Tài khoản và gói thanh toán',
				'Tiền gửi có kỳ hạn',
				'Thẻ doanh nghiệp',
				'Dịch vụ thanh toán',
				'Dịch vụ khác'
			];
			for ($i = 1; $i <= 5; $i++) {
				?>
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if ($i == 1) echo 'checked'; ?>>
					<label class="accodion-title" for="chck_1_<?php echo $i; ?>"><span><?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content">
						<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
							<?php
							$a_h1 = ['Tài khoản thanh toán','Tài khoản kí quỹ','Tài khoản vốn chuyên dùng'];
							$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-4.jpg','img-5.jpg','img-6.jpg'];
							for ($j = 1; $j <= 10; $j++) { ?>
								<a href="#" class="item efch-<?php echo $j + 1; ?> ef-img-l equal">
									<div class="img tRes_71">
										<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/<?php echo $img[$j - 1] ?>">
									</div>
									<div class="divtext">
										<h4 class="title line2"><?php echo $a_h1[$j-1] ?></h4>
										<div class="desc line2 cl3">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
									</div>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<section id="tab5" class=" sec-b sec-cauhoi ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht ">Tài trợ thương mại</h2>
		</div>
		<div class="accodion accodion-1">
			<?php
			$ques = [
				'Xuất khẩu',
				'Nhập khẩu',
				'Nội địa'
			];
			for ($i = 1; $i <= 3; $i++) {
				?>
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_3_<?php echo $i; ?>">
					<label class="accodion-title" for="chck_3_<?php echo $i; ?>"><span><?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content">
						<div class="inner">
							<p>Ngân hàng MBBank không giới hạn số tiền vay tối đa với KH, số tiền vay tối đa này phụ thuộc vào tình hình tài chính và khả năng trả nợ của bạn.</p>
							<p>Thời gian vay tối đa của sản phẩm tùy thuộc dự án liên kết với ngân hàng nhưng tối đa 20 năm </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
						</div>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<section id="tab6" class=" sec-b sec-cauhoi ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht ">Sản phẩm ngoại hối và thị trường vốn</h2>
		</div>
		<div class="accodion accodion-1">
			<?php
			$ques = [
				'Ngoại hối',
				'Phát sinh tiền tệ',
				'Phát sinh hàng hóa'
			];
			for ($i = 1; $i <= 3; $i++) {
				?>
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_4_<?php echo $i; ?>">
					<label class="accodion-title" for="chck_4_<?php echo $i; ?>"><span><?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content">
						<div class="inner">
							<p>Ngân hàng MBBank không giới hạn số tiền vay tối đa với KH, số tiền vay tối đa này phụ thuộc vào tình hình tài chính và khả năng trả nợ của bạn.</p>
							<p>Thời gian vay tối đa của sản phẩm tùy thuộc dự án liên kết với ngân hàng nhưng tối đa 20 năm </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
						</div>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<!-- <section class="sec-tb">
	<div class="container">
		<div class="row list-item flex">
			<div class="col-md-4">
				<img src="assets/images/care-logo-lg.png" alt="" class="img loaded">
			</div>
			<div class="col-md-5">
				<p class="desc">	Thiếu nội dung yêu cầu viết thêm nội dung cho 3 dòng. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Fugiat aliquam voluptatem minima? Obcaecati voluptate, nobis voluptatibus quis quam eaque saepe perspiciatis odit, sapiente iusto optio accusamus in ullam repellendus quidem!</p>
			</div>
			<div class="col-md-3">
				<a href="https://smecare.com.vn/" class="btn lg text-normal">Khám phá ngay</a>
			</div>
		</div>
	</div>
</section> -->

<section class="sec-b sec-img-text group-ef lazy-hidden">
	<div class="container"  >
		<div class="row center">
			<div class="col-lg-6">
				<div class="img tRes_66 efch-2 ef-img-r ">
					<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/sme.jpg" src="assets/images/doanhnghiep/dnnho/sme.jpg">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="divtext entry-content">
					<h2 class="ht  efch-1 ef-tx-t ">SME CARE  BY MB</h2>

					<div class="desc mb-30">Thiếu nội dung yêu cầu viết thêm nội dung cho 3 dòng. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo.</div>
					<a class="btn lg" href="#">ĐĂNG KÝ NGAY</a>	
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/index-bottom.php'; ?>