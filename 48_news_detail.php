<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section  class="banner-heading-3 next-shadow" >
	<div class="container">
		<div class="divtext">
			<div class="max750">
				<h1 class=" " >App MB Bank – MIỄN PHÍ CHUYỂN TIỀN – MIỄN LO DỊCH BỆNH</h1>
			</div>
		</div>
	</div>
	<!-- <img class="img br lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-10.svg" src=""> -->
</section>

<main id="main"  class="sec-b page-news-detail" >
	<div class="container">
		<div class=" max750">
			<div class="top-heading">
				<div class="date">01/ 12/ 2019</div>
				
				<ul class="blog-item-social ">
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
				</ul>					
			</div>

			<div class="entry-content">
				<p class="b">Bạn ngại nhất mỗi khi chuyển tiền phải đến các điểm giao dịch chờ đợi xếp hàng để làm thủ tục? Không cần xếp hàng tại các cây ATM, không lo mất phí giao dịch, không ngại đối thoại trực tiếp giữa mùa nCov… Giờ đây với App ngân hàng MBBank, mọi giao dịch chuyển tiền đều có thể thực hiện mọi lúc mọi nơi, nhanh trong chớp mắt với nhiều hình thức chuyển tiền khác nhau.</p>
				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/tintuc/detail/img-1.jpg" src="assets/images/tintuc/detail/img-1.jpg">
					<figcaption> Ông Lê Xuân Vũ và Ông Nguyễn Đức Huy tại Lễ trao giải</figcaption>
				</figure>
				
				<p>App MBBank là app ngân hàng số dành cho khách hàng duy nhất tại Việt Nam xuất sắc đạt “Danh hiệu Sao Khuê 2019”. Kể từ khi ra mắt, App MBBank là giải pháp tài chính tối ưu với các tính năng: mở tài khoản, đăng ký mở thẻ Visa, rút tiền, thanh toán, tiết kiệm hay vay vốn, rút tiền mặt, đặt vé máy bay, thanh toán QR Code bằng App … Mới đây, Ngân hàng TMCP Quân Đội (MB) tiếp tục cho ra mắt tính năng chuyển tiền nhanh trên ứng dụng App MBBank.</p>
				<p>Thay vì phải xếp hàng tại các ngân hàng để làm thủ tục chuyển tiền, giờ đây, tất cả đã trở nên dễ dàng hơn bao giờ hết. Khi sử dụng App MBBank, khách hàng của MB sẽ thực hiện việc chuyển tiền trên điện thoại vô cùng dễ dàng và nhanh chóng, an toàn mọi lúc mọi nơi với nhiều hình thức chuyển tiền khác nhau. Đáng nói nhất là tính năng chuyển tiền nhanh trên ứng dụng App MBBank. Điểm nổi trội của tính năng này là người nhận có thể ngay lập tức nhận được tiền sau chỉ một vài giây, thay vì việc phải chờ đợi hàng giờ đồng hồ như trước đây.</p>
				
				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/tintuc/detail/img-2.jpg" src="assets/images/tintuc/detail/img-1.jpg">
					<figcaption> Nhân viên MB tư vấn sản phẩm dịch vụ tới khách hàng tịa Lễ trao giải</figcaption>
				</figure>
				<p>Tiện ích “chuyển tiền trong MB qua số điện thoại” cho phép khách hàng chuyển tiền thông qua số điện thoại người nhận đã được đăng ký dịch vụ eBanking tại MB. Chỉ cần nhập số điện thoại người nhận hoặc chọn từ danh bạ điện thoại sau đó nhập số tiền, nội dung chuyển tiền, khách hàng sẽ nhận được một mã xác thực giao dịch và nhập mã bảo mật OTP. Công đoạn chuyển tiền kết thúc nhanh chóng, dễ dàng.</p>
				<p>Với hình thức “chuyển qua số tài khoản ngân hàng”, từ màn hình trang chủ, khách hàng chọn “chuyển đến tài khoản ngân hàng”. Sau khi chọn ngân hàng nhận và nhập các thông tin theo yêu cầu, xác nhận giao dịch, nhập OTP, bạn đã hoàn tất công đoạn chuyển tiền nhanh chóng qua tài khoản ngân hàng với 3 tiện ích nổi bật: Đến tài khoản thanh toán MB với hạn mức giao dịch từ 500 triệu - 2 tỷ đồng; Đến tài khoản thanh toán khác MB của gần 80 ngân hàng khác với hạn mức giao dịch 300 triệu đồng và chuyển tiền vào tài khoản thụ hưởng khác MB qua Citad với hạn mức giao dịch 500 triệu - 2 tỷ đồng.</p>
				<p>Ngoài hình thức chuyển tiền trong MB qua số điện thoại hay qua số tài khoản ngân hàng, App MBBank còn cho phép khách hàng chuyển tiền thông qua số thẻ ATM hoặc chuyển đến tài khoản công ty chứng khoán MBS. Tất cả giao dịch được thực hiện nhanh chóng, có mã bảo mật an toàn, đáp ứng nhu cầu chuyển tiền nhanh chóng, thuận tiện và bảo mật của khách hàng.</p>
				<p>Với hình thức “chuyển qua số tài khoản ngân hàng”, từ màn hình trang chủ, khách hàng chọn “chuyển đến tài khoản ngân hàng”. Sau khi chọn ngân hàng nhận và nhập các thông tin theo yêu cầu, xác nhận giao dịch, nhập OTP, bạn đã hoàn tất công đoạn chuyển tiền nhanh chóng qua tài khoản ngân hàng với 3 tiện ích nổi bật: Đến tài khoản thanh toán MB với hạn mức giao dịch từ 500 triệu - 2 tỷ đồng; Đến tài khoản thanh toán khác MB của gần 80 ngân hàng khác với hạn mức giao dịch 300 triệu đồng và chuyển tiền vào tài khoản thụ hưởng khác MB qua Citad với hạn mức giao dịch 500 triệu - 2 tỷ đồng.</p>
				<p>Ngoài hình thức chuyển tiền trong MB qua số điện thoại hay qua số tài khoản ngân hàng, App MBBank còn cho phép khách hàng chuyển tiền thông qua số thẻ ATM hoặc chuyển đến tài khoản công ty chứng khoán MBS. Tất cả giao dịch được thực hiện nhanh chóng, có mã bảo mật an toàn, đáp ứng nhu cầu chuyển tiền nhanh chóng, thuận tiện và bảo mật của khách hàng.</p>

				<p>Khách hàng có thể tải App MBBank và trải nghiệm tại:</p>
				<p>- Link tải tại App Store: <a href="#" class="b cl1">https://apple.co/2AqB7ZM</a></p>
				<p>- Link tải tại Google Play: <a href="#" class="b cl1">https://bit.ly/2v5ZsyP</a></p>
				<p>Hoặc liên hệ MB247: <span class="cl1">1900 545 426 / 024 3767 4050</span> để được tư vấn.</p>
			</div>
			<br>
			<div class="tags">
				<h2>Nội dung liên quan</h2>
				<a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
				<a class="tag" href="#">Ngân hàng đầu tư</a>
				<a class="tag" href="#">Quản lý dòng tiền</a>
			</div>					
		</div>
	</div>    	
</main>
</section>



<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Tin bài liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          '"Quán quân" công bố thông tin năm 2017 đang thuộc …',
          'MB khuyến cáo khách hàng bảo mật thông tin ngân hàng điện tử'
          ];
        $img = ['tintuc-1.jpg','tintuc-3.jpg'];
        for($i=1;$i<=2;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/tintuc/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh…</div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>


<?php include 'include/index-bottom.php';?>