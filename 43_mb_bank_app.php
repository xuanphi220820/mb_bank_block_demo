<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <span class="item">Gói sản phẩm gia đình tôi yêu</span>
      </div>
    </div>
</div>

<section class=" banner-heading-1 lazy-hidden group-ef next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">App MB Bank</h1>
      <div class="efch-3 ef-img-l desc cl1">Trải nghiệm vượt trội cùng app MB Bank</div>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-4.jpg">
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Tính năng</a></li>
        <li><a href="#tab3" class="scrollspy">Hướng dẫn</a></li>
        <li><a href="#tab4" class="scrollspy">Hạn mức<!--  --></a></li>
        <li><a href="#tab5" class="scrollspy">Bạn chưa biết</a></li>
        <li><a href="#tab6" class="scrollspy">Khuyến mãi</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php'; ?>

<section id="tab2" class="sec-t sec-img-svg group-ef lazy-hidden">
  <div class="container">
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">Tính năng</h2>
    </div>
    <div class="menuicon  owl-carousel   s-nav nav-2" data-res="3,3,3,2" paramowl="margin=0">
      <?php
      $img = ['atm.svg','bank.svg','calendar.svg','key.svg','tai-lieu.svg','atm-2.svg','atm.svg','calendar.svg','tai-lieu.svg','bank.svg'];
      $title =  [
        'Mở tài khoản online ',
        'Thông tin tài khoản',
        'Vay online',
        'Thanh toán tiện ích',
        'Tiết kiệm online',
        'Rút tiền ATM',
        'Rút tiền ATM',
        'Thanh toán tiện ích',
        'Rút tiền ATM',
        'Rút tiền ATM'
      ];
      $desc =   [
        'Mở mới ngay tài khoản MB online - Chuyển tiền miễn phí trọn đời qua số điện thoại, tài khoản ngân hàng, thẻ ATM',
        'Cập nhật liên tục toàn bộ thông tin tài khoản, thẻ tiết kiệm',
        'Vay siêu nhanh từ thẻ tín dụng',
        'Hóa đơn điện, nước, nạp thẻ điện thoại, internet, truyền hình, mua vé xem phim, vé may bay…',
        'Mở tiết kiệm số, truy vấn và tất toán, tính lãi tiết kiệm',
        'Rút tiền miễn phí không cần thẻ',
        'Mở tiết kiệm số, truy vấn và tất toán, tính lãi tiết kiệm',
        'Cập nhật liên tục toàn bộ thông tin tài khoản, thẻ tiết kiệm',
        'Hóa đơn điện, nước, nạp thẻ điện thoại, internet, truyền hình, mua vé xem phim, vé may bay…',
        'Rút tiền miễn phí không cần thẻ'
      ];
      for ($i = 0; $i <= 10; $i++) { ?>
        <div class="item <?php if ($i == 1) echo 'active'; ?>">
          <a href="#" class="item">
            <div class="img">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[2*$i] ?>">
            </div>
            <div class="divtext">
              <h4 class="title"><?php echo $title[2*$i] ?></h4>          
              <div class="desc"><?php echo $desc[2*$i] ?></div>
            </div>  
          </a>
          <a href="#" class="item">
            <div class="img">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[2*$i+1] ?>">
            </div>
            <div class="divtext">
              <h4 class="title text-normal"><?php echo $title[2*$i+1] ?></h4>          
              <div class="desc"><?php echo $desc[2*$i+1] ?></div>
            </div>  
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab3" class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container">
    <div class="row center end">
      <div class="col-lg-6">

        <div class="single_video  tRes_16_9 " data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/video.jpg" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
        </div>

      </div>
      <div class="col-lg-6">
        <div class="divtext ">
          <h2 class="ht ">Hướng dẫn sử dụng</h2>
          <div class="desc max555 mb-20">Cùng lắng nghe những chia sẻ của chị Nguyễn Thị B đã sử dụng dịch vụ vay của MB Bank </div>
          <div class="mb-30 cl1 fs17 w6">
            <a href="#">Hướng dẫn đăng ký <i class="icon-arrow-db"></i></a>
          </div>
          <a class="btn lg" href="#">Toàn bộ tính năng</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class=" sec-tb sec-cauhoi ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht ">Câu hỏi thường gặp</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>      
    <div class="accodion accodion-1">
      <?php
      $ques = [
        'Hạn mức giao dịch chuyển tiền',
        'Những ai được sử dụng dịch vụ?',
        'Đăng ký App ngân hàng MB Bank bằng cách nào?',
        'Tôi có thể chuyển tiền vào các ngày thứ 7, chủ nhật, ngày lễ không?'
      ];
      for($i=1;$i<=4;$i++) {
        ?>
        <div class="accodion-tab ">
          <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
          <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span>
            <?php echo $ques[$i-1] ?><span class="triangle" ><i class="icon-plus"></i></span> </label>
            <div class="accodion-content entry-content" >
              <div class="table-responsive">
                <table class="table table-full">
                  <tr>
                    <th>Nội dung</th>
                    <th>Tiêu chí</th>
                  </tr>
                  <tr>
                    <td>Đến tài khoản thanh toán khác MB</td>
                    <td>Chuyển tiền vào tài khoản thu hưởng của 40 ngân hàng khác MB. Người thụ hưởng nhận tiền ngay sau khi kết thúc giao dịch. Hạn mức giao dịch 300 triệu đồng</td>
                  </tr>
                  <tr>
                    <td>Đến số thẻ ATM</td>
                    <td>Chuyển tiền vào tài khoản thu hưởng của 40 ngân hàng khác MB. Người thụ hưởng nhận tiền ngay sau khi kết thúc giao dịch. Hạn mức giao dịch 300 triệu đồng</td>
                  </tr>
                  <tr>
                    <td>Đến tài khoản MBS</td>
                    <td>Chuyển tiền vào tài khoản công ty chứng khoán MBS</td>
                  </tr>
                  <tr>
                    <td>Liên ngân hàng</td>
                    <td>Chuyển tiền vào tài khoản thu hưởng hàng khác MB qua Citad. Người thụ hưởng nhận tiền sau 1-2 tiếng. Hạn mức giao dịch 500 triệu - 2 tỷ đồng</td>
                  </tr>
                  <tr>
                    <td>Đến tài khoản thanh toán MB</td>
                    <td>Chuyển tiền vào tài khoản thụ hưởng mở tại MB. Hạn mức giao dịch từ  500 triệu - 2 tỷ đồng</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <?php
        } ?>
      </div>           
    </div>
  </section>


  <section id="tab6" class="sec-t " >
    <div class="container">
      <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
        <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
      </div>
      <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
        <?php
        $a_h1 = [
          'Thông báo danh sách khách hàng trúng thưởng CT',
          'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
          'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
          'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
          'Thông báo danh sách khách hàng trúng thưởng CT',
          'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
          'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
          'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
        ];
        $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
        for($i=1;$i<=8;$i++) {?>
          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
            <div class="img tRes_71">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
            </div>
            <div class="divtext">
              <div class="date">01/ 12/ 2019</div>
              <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
            </div>
          </a>
        <?php } ?>
      </div>          
    </div>
  </section>

  <section id="tab5" class="sec-b">
    <div class="container">
      <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Có thể bạn chưa biết ?</h2>
      </div>
      <div class="list-5 row list-item">
        <?php
        $a_h1_2 = [
                    'Nhận và chuyển tiền trong 1 giây với App MBBank',
                    'Ứng dụng Mbbank- rút tiền ATM không cần thẻ',
                    'Vay online siêu nhanh trên ứng dụng App MB Bank'
                  ];
        $desc = [
                  'Thay vì phải xếp hàng tại các ngân hàng để làm thủ tục chuyển tiền, giờ đây, tất cả đã trở nên dễ dàng hơn bao giờ hết khi sử dụng App MBBank',
                  'Rút tiền tại ATM qua App MBBank không chỉ tiện lợi khi bạn quên ví, quên thẻ mà còn có thể chuyển tiền cho người thân vô cùng nhanh chóng và bảo mật.',
                  'Chỉ bằng vài cú chạm trên App MBBank, bất kỳ ở đâu và bất kỳ khi nào khách hàng đều có thể thực hiện giao dịch vay vốn một cách nhanh chóng và an toàn mà không cần giấy tờ, không cần tài sản bảo đảm.'
                ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg'];
        for ($i = 1; $i <= 3; $i++) { ?>
          <div class="col-md-4">
            <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
              <div class="img tRes_71">
                <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/taikhoan/<?php echo $img[$i-1] ?>">
              </div>
              <div class="divtext">
                <h4 class="title"><?php echo $a_h1_2[$i-1]; ?></h4>
                <div class="desc line4"><?php echo $desc[$i-1]; ?></div>
              </div>
            </a>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>

  <?php include '_block/tu_van.php'; ?>

  <?php include 'include/index-bottom.php'; ?>