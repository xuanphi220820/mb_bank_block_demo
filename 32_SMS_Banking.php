<?php include 'include/index-top.php';?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <a class="item" href="#">Dịch vụ số</a>
        <span class="item">SMS Banking</span>
      </div>
    </div>
</div>


<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >SMS Banking</h1>
        <div class="efch-3 ef-img-l desc cl1 b">Trải nghiệm siêu vượt trội</div>
        </div>
        <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-7.jpg">
    </div>
    
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Tính năng</a></li>
        <li><a href="#tab3" class="scrollspy">Điều kiện</a></li>
        <li><a href="#tab3" class="scrollspy">Hướng dẫn</a></li>
        <li><a href="#tab5" class="scrollspy">Ưu đãi</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php';?>


<section id="tab2" class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">TÍNH NĂNG</h2>
    </div>    
    <div class="row list-item">
      <?php
      $a_h1_2 = ['SMS','Tỉ giá','lãi suất thanh toán','ATM','lãi suất tiết kiệm','Số dư tài khoản','sao kê','chi nhánh','đối tượng khách hàng','hình thức bảo mật','hạn mức và biểu phí'];
      $img = ['ung-dung-2.svg','money-4.svg','bieu-do.svg','atm-2.svg','save-money.svg','atm.svg','tai-lieu.svg','bank.svg','money-3.svg','policy.svg','dich-vu-khac.svg'];
      $desc = [
                'Nhận thông báo số dư tự động bằng SMS ngay sau khi phát sinh',
                'Truy vấn thông tin tỉ giá',
                'Truy vấn lãi suất tiền gửi thanh toán',
                'Truy vấn các địa điểm đặt máy ATM của MB trên toàn quốc',
                'Truy vấn lãi suất tiền gửi tiết kiệm',
                'Truy vấn tài khoản thanh toán',
                'Khách hàng có thể sao kê tối đa là 5 giao dịch tài khoản gần nhất',
                'Truy vấn địa điểm các chi nhánh phòng giao dịch của MB',
                'Những đối tượng khách hàng có làm tài khoản thanh toán tại MB',
                'Bảo mật các số điện thoại đăng ký sử dụng dịch vụ của khách hàng',
                'Hạn mức và biểu phí duy trì dịch vụ 12.000/ tháng (chưa VAT)',
              ];
      for($i=1;$i<=11;$i++) { ?>
      <div class="col-sm-6 col-md-6 col-lg-3 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_h1_2[$i - 1] ?></h4>          
            <div class="desc"><?php echo $desc[$i - 1] ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab3" class=" sec-b   ">
  <div class="container"  >
        <div class="max950">  

      <div class="row list-item">
        <?php for($i=1;$i<=2;$i++) { ?>
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Điều kiện đăng ký và sử dụng</h4>
            <div class="widget-content entry-content">
              <ul>
                <li>Có đầy đủ năng lực pháp luật dân sự và năng lực hành vi dân sự</li>
                <li>Khách hàng là cá nhân/ tổ chức mở TKTT tại MB</li>
                <li>Khách hàng sử dụng thuê bao di động của một trong các mạng viễn thông tại Việt Nam</li>
              </ul>
              <p>Tham khảo điều khoản, điều kiện của dịch vụ <a href="#">TẠI ĐÂY</a></p>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      </div>  
    
  </div>
</section>

<section id="tab5"  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
            <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $a_h1 = [
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
                    ];
            $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
            for($i=1;$i<=8;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>