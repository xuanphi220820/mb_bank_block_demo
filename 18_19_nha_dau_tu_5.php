<?php include 'include/index-top.php'; ?>
<div class="entry-breadcrumb">
  <div class="container">
    <div class="breadcrumbs">
      <a class="item" href="#">Home</a>
      <a class="item" href="#"> Nhà đầu tư  </a>
      <span class="item">Công bố thông tin khác</span>
    </div>
  </div>
</div>
<section class="sec banner-heading-1 next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Tài liệu khác</h1>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-14_4.jpg">
</section>
<section class="sec-tb sec-h-1 lazy-hidden group-ef">
  <div class="container">
    <!-- <div class="entry-head text-center">
      <h2 class="ht efch-1 ef-img-t">Nhà đầu tư</h2>
    </div> -->
    <div class="menuicon">
      <?php
      $a_h1_2 = ['Thông báo', 'Báo cáo tài chính', 'Đại hội cổ đông', 'Báo cáo thường niên', ' Tài liệu nhà đầu tư', 'Tài liệu khác', 'Điều lệ NHTM'];
      $link = ['#', '14_nha_dau_tu_1.php', '15_nha_dau_tu_2.php', '16_nha_dau_tu_3.php', '17_nha_dau_tu_4.php', '18_19_nha_dau_tu_5.php', '#'];
      $img = ['thong-bao.svg', 'bieu-do.svg', 'meeting.svg', 'Tai-lieu-3.svg', 'tai-lieu-ndt.svg', 'mess.svg', 'policy.svg'];
      for ($i = 1; $i <= 7; $i++) { ?>
        <div class="item   efch-<?php echo $i + 2; ?> ef-img-t <?php if ($i == 6) echo 'active'; ?>">
          <a href="./<?php echo $link[$i - 1] ?>" class="link">
            <div class="img">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i - 1] ?>">
            </div>
            <div class="title"><?php echo $a_h1_2[$i - 1]; ?></div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>
<main id="main" class="sec-b">
  <div class="container">
    <div class="cttab-v3   ">
      <div class="tab-menu max750">
        <div class="active"><span>Hỏi đáp</span></div>
        <div><span>Báo cáo xếp hạng tín niệm</span></div>
        <div><span>Tài liệu - Biểu mẫu</span></div>
      </div>
      <div class="tab-content">
        <div class="active">
          <div class="tab-inner">
            <div class="max750 mb-40">
              <div class="inline-table w100">
                <h3 class="ctext">Bạn gặp phải khó khăn?</h3>
                <div class="c100">
                  <form role="search" method="get" class="searchform input h50" action="#">
                    <input type="text" placeholder="Hãy cho chúng tôi biết khó khăn của bạn" value="" name="s" class="textinput">
                    <button type="submit" class="searchbutton"><i class="icon-search-2"> </i></button>
                  </form>
                </div>
              </div>
            </div>

            <div class="accodion accodion-1">
              <section class=" sec-b sec-cauhoi ">
                <div class="container"  >    
                  <div class="accodion accodion-1">
                    <?php
                    $question = ['1. Tôi phải làm những thủ tục gì khi nhận thừa kế cổ phiếu từ người thân?',
                    '2. Khi tôi không lưu ký cổ phiếu MBB các quyền lợi của cổ đông có bị ảnh hưởng không?',
                    '3. Tôi muốn biết những đối tượng nào phải thực hiện các thủ tục công bố thông tin, báo cáo cơ quan nhà nước khi thực hiện giao dịch cổ phiếu MBB',
                    '4. Tôi là cổ đông của MB và tôi muốn thực hiện điều chỉnh thông tin thì cần liên hệ với bộ phận nào?',
                    '5. Khi tôi không lưu ký cổ phiếu MBB các quyền lợi của cổ đông có bị ảnh hưởng không?',
                    '6. Tôi muốn biết những đối tượng nào phải thực hiện các thủ tục công bố thông tin, báo cáo cơ quan nhà nước khi thực hiện giao dịch cổ phiếu MBB',
                    '7. Tôi là cổ đông của MB và tôi muốn thực hiện điều chỉnh thông tin thì cần liên hệ với bộ phận nào?'];
                    for($i=1;$i<=7;$i++) {
                      ?>
                      <div class="accodion-tab ">
                        <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                        <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span> <?php echo $question[$i-1] ?> </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                        <div class="accodion-content entry-content" >
                          <div class="inner">
                            <p>Quý cổ đông làm thủ tục quyền sở hữu do thừa kế tại công ty chứng khoán nơi cổ đông mở tài khoản nếu cổ đông đã lưu ký cổ phiếu.<br>
                              <br>Trường hợp chưa lưu ký, cổ đông thực hiện theo quy định tại Điều 28 Quy chế hoạt động đăng ký chứng khoán (Ban hành kèm theo Quyết định số 22/QĐ-VSD ngày 13 tháng 3 năm 2015 của Tổng giám đốc Trung tâm Lưu ký chứng khoán Việt Nam) <br>
                              <br>Chi tiết vui lòng xem tại ĐÂY</p>
                            </div>
                          </div>
                        </div>
                        <?php
                      } ?>
                    </div>
                    <div class="text-center">
                      <button class="btn lg">xem thêm</button>
                    </div>            
                  </div>
                </section>
              </div>



              <?php include '_module/pagination.php'; ?>
            </div>
          </div>
          <div>
            <div class="tab-inner">
              2
            </div>
          </div>
          <div>
            <div class="tab-inner">

            <!-- <div class="accodion accodion-2">
                <?php
                for ($j = 1; $j <= 3; $j++) {
                ?>
                <div class="accodion-tab ">
                    <input type="radio" id="chck_2_<?php echo $j; ?>" <?php if ($j == 1) echo 'checked'; ?> name="chck_2" >
                    <label class="accodion-title h2" for="chck_2_<?php echo $j; ?>" ><span> 2019 </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                    <div class="accodion-content entry-content" >
                        <div class="inner">
                          <ul class="list-download">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>                        
                            <li> 
                              <span class="title"><i class="icon-t14"></i> Quy chế tổ chức và hoạt động của Ban kiểm soát MB năm 2019</span> 
                              <span class="data">4.28 MB</span> 
                              <span class="down"><a   href="#"><i class="icon-arrow-6 ib"></i>  </a></span>
                            </li>
                            <?php } ?>                        
                          </ul>
                        </div>
                    </div>
                </div>
                <?php
                } ?>
              </div>  -->
              <div class="row grid-space-60 mb-30">
                <div class="col-md-6">
                  <ul class="list-download ">
                    <?php
                    $con = ['Quy chế tổ chức và hoạt động của Ban kiểm soát MB năm 2019',
                    'Quy chế Quản trị Nội Bộ',
                    'Quy chế Tổ chức và hoạt động HĐQT',
                    'Quy chế làm việc tại Đại hội đồng cổ đông nhiệm kỳ 2019 - 2024',
                    'Mẫu biểu đề cử nhân sự dự kiến bầu TV HĐQT, BKS nhiệm kỳ 2019 - 2024'];
                    for ($i = 1; $i <= 5; $i++) { ?>
                      <li>
                        <span class="title"><i class="icon-t14"></i> <?php echo $con[$i-1]?></span>
                        <span class="down"><a href="#"><i class="icon-arrow-6 ib"></i> </a></span>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="col-md-6">
                  <ul class="list-download ">
                    <?php
                    $con = ['Văn bản chuyển quyền sở hữu do thừa kế',
                    'Phụ lục Thông báo giao dịch quyền mua cổ phiếu',
                    'Phụ lục Thông báo giao dịch cổ phiếu',
                    'Phụ lục Báo cáo về thay đổi sở hữu của cổ đông lớn',
                    'Phụ lục Báo cáo về sở hữu của cổ đông lớn'];
                    for ($i = 1; $i <= 5; $i++) { ?>
                      <li>
                        <span class="title"><i class="icon-t14"></i> <?php echo $con[$i-1]?></span>
                        <span class="down"><a href="#"><i class="icon-arrow-6 ib"></i> </a></span>
                      </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
              <?php include '_module/pagination.php'; ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </main>


  <?php include 'include/index-bottom.php'; ?>