<?php include 'include/index-top.php';?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <a class="item" href="#">Dịch vụ số</a>
        <span class="item">MB Bank Plus</span>
      </div>
    </div>
</div>

<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >MB BankPlus</h1>
        <div class="efch-3 ef-img-l desc cl1 b">Trải nghiệm vượt trội</div>
        </div>
        <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-8.jpg">
    </div>
    
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Tính năng</a></li>
        <li><a href="#tab3" class="scrollspy">Điều kiện</a></li>
        <li><a href="#tab4" class="scrollspy">Bảo mật</a></li>
        <li><a href="#tab5" class="scrollspy">Biểu phí</a></li>
        <li><a href="#tab6" class="scrollspy">Hỏi đáp</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php';?>


<section id="tab2" class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">TÍNH NĂNG</h2>
    </div>    
    <div class="row list-item">
      <?php
      $a_h1_2 = [
                  'SMS',
                  'Tỉ giá',
                  'thanh toán cước',
                  'Thanh toán hóa đơn',
                  'gửi tiết kiệm',
                  'chuyển tiền',
                  'Đối tượng',
                  'loại tiên tệ'
                ];
      $img = ['ung-dung-2.svg','money-4.svg','bieu-do.svg','tai-lieu.svg','save-money.svg','dich-vu-khac.svg','money-3.svg','money-4.svg'];
      $desc = [
                'Truy vấn số dư, lịch sử giao dịch, thông tin khuyến mại',
                'Chuyển tiền trong và ngoài hệ thống MB (SĐT, số thẻ, STK)',
                'Thanh toán cước viễn thông tất cả các mạng (trả trước, trả sau)',
                'Thanh toán hóa đơn dịch vụ: điện, nước, học phí, game, thuế bảo hiểm',
                'Khách hàng gửi tiết kiệm ngay trên điện thoại',
                'Khách hàng có thể nộp/rút/chuyển tiền mặt qua MB Bankplus',
                'Khách hàng là cá nhân mở TKTT tại MB hoặc sử dụng thẻ trả trước do MB',
                'Loại tiền tệ sử dụng VND'
              ];

      for($i=1;$i<=8;$i++) { ?>
      <div class="col-sm-6 col-md-6 col-lg-3 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_h1_2[$i - 1] ?></h4>          
            <div class="desc"><?php echo $desc[$i - 1] ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>


<section id="tab3" class=" sec-b   ">
  <div class="container"  >
        <div class="max950">  

      <div class="row list-item">
        <?php for($i=1;$i<=2;$i++) { ?>
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Điều kiện đăng ký và sử dụng</h4>
            <div class="widget-content entry-content">
              <ul>
                <li>Có đầy đủ năng lực pháp luật dân sự và năng lực hành vi dân sự</li>
                <li>Khách hàng là cá nhân/ tổ chức mở TKTT tại MB</li>
                <li>Khách hàng sử dụng thuê bao di động của một trong các mạng viễn thông tại Việt Nam</li>
              </ul>
              <p>Tham khảo điều khoản, điều kiện của dịch vụ <a href="#">TẠI ĐÂY</a></p>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      </div>  
    
  </div>

</section>


<section id="tab4" class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
    <div class="row center">
      <div class="col-lg-6">
        <div class="img tRes_66 efch-2 ef-img-r ">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/smsbanking/baomat.jpg" src="https://via.placeholder.com/10x6">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">Biện pháp bảo mật</h2>

          <p>Thông qua Số điện thoại và mật khẩu (PIN).</p>
          <p><span class="b">•  Số điện thoại:</span> Khách hàng phải xác nhận việc truy cập dịch vụ và thực hiện giao dịch bởi số điện thoại đăng ký sử dụng dịch vụ BankPlus.</p>
          <p>Với mỗi giao dịch vấn tin số dư tài khoản, thanh toán và chuyển khoản: KH cần nhập đúng PIN dịch vụ để thực hiện giao dịch</p>

        </div>
      </div>
    </div>
  </div>
</section>

<section id="tab5" class=" sec-tb  ">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht ">HẠN MỨC VÀ BIỂU PHÍ</h2>
    </div>      
  <div class="row list-item">
    <div class="col-md-4">
      <div class="widget-default">
        <h4 class="widget-title">Hạn mức chuyển tiền</h4>
        <div class="widget-content entry-content">
          <p>Tối thiểu 10.000 VNĐ/giao dịch.</p>
        </div>
      </div>
    </div>    
    <?php for($i=1;$i<=2;$i++) { ?>
    <div class="col-md-4">
      <div class="widget-default">
        <h4 class="widget-title">Hạn mức chuyển tiền tối đa</h4>
        <div class="widget-content entry-content">
          <div class="toggleAutoHeight" data-height="200" data-more="+ Xem thêm" data-less="- Thu gọn" data-i="" >
            <div class="tgh-content">
              <div class="tgh-first">
                <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              </div>                
              <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
              <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
              <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
              <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              <?php if($i==1) { ?>
              <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
              <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
              <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
              <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  </div>
</section>
 
<section id="tab6" class=" sec-b sec-cauhoi ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht ">Câu hỏi thường gặp</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>      
    <div class="accodion accodion-1">
        <?php
        $ques = [
                  'Dịch vụ MB BankPlus là gì?',
                  'Làm thế nào để đăng ký dịch vụ MB BankPlus',
                  'Sử dụng dịch vụ MB BankPlus để thực hiện các giao dịch tài chính có đảm bảo an toàn không?',
                  ' So với BankPlus của những ngân hàng khác, MB BankPlus của MB có gì khác biệt?'
                ];
        for($i=1;$i<=4;$i++) {
        ?>
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span><?php echo $ques[$i-1] ?><span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content entry-content" >
                    <div class="inner">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. Mauris malesuada nisi sit amet augue accumsan tincidunt. Maecenas tincidunt, velit ac porttitor pulvinar.</p>
                </div>
            </div>
        <?php
        } ?>
    </div>           
  </div>
</section>

<section  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
            <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $a_h1 = [
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
                    ];
            $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
            for($i=1;$i<=8;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>