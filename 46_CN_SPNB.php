<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <span class="item">Vay siêu nhanh</span>
      </div>
    </div>
</div>

<section class=" banner-heading-1 lazy-hidden group-ef next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Vay siêu nhanh</h1>
    </div>
    <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-6.jpg">
  </div>

</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Hướng dẫn</a></li>
        <li><a href="#tab3" class="scrollspy">Điều kiện vay vốn</a></li>
        <li><a href="#tab4" class="scrollspy">Hạn mức vay & Lãi suất</a></li>
        <li><a href="#tab5" class="scrollspy">Phương thức cho vay</a></li>
        <li><a href="#tab6" class="scrollspy">Hỏi đáp</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php'; ?>

<section class="sec-t sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">TÍNH NĂNG</h2>
    </div>    
    <div class="row list-item">
      <?php
      $a_h1_2 = [
        'Giao dịch online',
        'Vay vốn đơn giản',
        'Mức cho vay',
        'Chủ động trả vốn',
        'Xử lý hồ sơ'
      ];
      $img = ['ung-dung-2.svg','money-4.svg','money-4.svg','ung-dung-3.svg','Giay-to.svg'];
      $desc = [
        'Thực hiện hoàn toàn online, không cần đến Ngân hàng',
        'Hồ sơ vay vốn đơn giản',
        'Tối đa 75%  hạn mức thẻ tín dụng ; thời gian vay: 20 tháng',
        'Khách hàng chủ động trả Gốc, lãi online',
        'Trải nghiệm vay vốn hiện đại, giao diện thân thiện <br> với tốc độ xử lý nhanh'
      ];

      for($i=1;$i<=5;$i++) { ?>
        <?php if ($i <= 3): ?>
          <div class="col-sm-6 col-md-6 col-lg-4 efch-<?php echo $i+1; ?> ef-img-t ">
            <div class="item">
              <div class="img ">
                <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
              </div>
              <div class="divtext">
                <h4 class="title"><?php echo $a_h1_2[$i - 1] ?></h4>          
                <div class="desc"><?php echo $desc[$i - 1] ?></div>
              </div>   
            </div>     
          </div>
        <?php endif ?>
        <?php if ($i > 3): ?>
          <div class="col-sm-6 col-md-6 col-lg-6 efch-<?php echo $i+1; ?> ef-img-t ">
            <div class="item">
              <div class="img ">
                <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
              </div>
              <div class="divtext">
                <h4 class="title"><?php echo $a_h1_2[$i - 1] ?></h4>          
                <div class="desc"><?php echo $desc[$i - 1] ?></div>
              </div>   
            </div>     
          </div>
        <?php endif ?>
      <?php } ?>
    </div>
  </div>
</section>  

<section id="tab3" class=" sec-t">
  <div class="container">
    <div class="entry-head text-center">
      <h2 class="ht ">Điều kiện vay vốn</h2>
    </div>

    <div class="max950">

      <div class="row list-item">
        <?php for ($i = 1; $i <= 2; $i++) { ?>
          <div class="col-md-6">
            <div class="widget-default">
              <h4 class="widget-title">Gói eMB Advance</h4>
              <div class="widget-content entry-content">
                <div>Cho phép khách hàng thực hiện cả các dịch vụ tài chính và phi tài chính, bao gồm:</div>
                <div>- Các tính năng của gói Basic;</div>
                <div>- Tiết kiệm số</div>
                <div>- Chuyển tiền</div>
                <ul>
                  <li>Chuyển tiền giữa các tài khoản của Khách hàng…</li>
                  <li>Chuyển tiền liên ngân hàng</li>
                  <li>Chuyển tiền nội bộ</li>
                  <li>Chuyển tiền nội bộ theo lô</li>
                  <li>Chuyển tiền liên ngân hàng theo lô</li>
                  <li>Chuyển khoản qua thẻ</li>
                  <li>Chuyển khoản qua MBS</li>
                </ul>
              </div>
            </div>
          </div>
        <?php } ?>

      </div>
    </div>
  </div>
</section>

<?php
$a_tb4_1 = ['Hạn mức chuyển tiền', 'Hạn mức chuyển tiền tối đa', 'Phí duy trì', 'Phí duy trì'];
$a_tb4_2 = [
  '<p>Tối thiểu 10.000 VNĐ/giao dịch</p>
  <p>Áp dụng cho mỗi lần giao dịch thành công</p>',
  '<p>•   Bankplus eco: 20 triệu đồng/giao dịch và 50 triệu đồng/ngày.</p>
  <p>•   Bankplus Pro: 100 triệu đồng/giao dịch </p>',
  '<p>•   Gói Bankplus Eco: online 11.000﻿ VNĐ</p>
  <p>•   Gói Bankplus Pro: 22.000 VNĐ</p>',
  '<p>•   Bankplus agent: 100 triệu đồng/giao dịch… và 500 triệu đồng/ngày.</p>'
];
?>
<section id="tab4" class="sec-t">
  <div class="container">
    <div class="entry-head text-center">
      <h2 class="ht  ">Hạn mức vay và lãi suất vay </h2>
    </div>
    <div class="table-responsive">
      <table class="table table-full table-lai-xuat">
        <tr>
          <th>Nội dung</th>
          <th>Tiêu chí</th>
        </tr>
        <?php
        for ($i = 1; $i <= 3; $i++) {
          ?>
          <tr>
            <td>Hạn mức vay</td>
            <td>Tối đa bằng 75% Hạn mức thẻ tín dụng và không vượt quá giá trị hạn mức khả dụng của còn lại của thẻ</td>
          </tr>
          <?php
        } ?>
      </table>
    </div>
  </div>
</section>

<section id="tab5" class="sec-t sec-img-text group-ef lazy-hidden">
  <div class="container">
    <div class="row center">
      <div class="col-lg-6">
        <div class="img tRes_66 efch-2 ef-img-r ">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/vaysieunhanh/phuongthucchovay.jpg" src="https://via.placeholder.com/10x6">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">Phương thức cho vay và trả nợ</h2>

          <p><strong>Phương thức cho vay: </strong>Cho vay theo món</p>
          <p><strong>Trả nợ tiền vay: </strong>Gốc, lãi trả hàng tháng</p>
          <p><strong>Phương thức trả nợ:</strong></p>
          <p>•  Hệ thống tự động trích nợ tài khoản thanh toán vào ngày thanh toán, 
          •  Khách hàng có thể chủ động thanh toán chuyển khoản, nộp tiền tại Quầy và các kênh khác theo thông báo của MB từng thời kỳ.</p>
          <p><strong>Thẻ Visa/ Vin ID: </strong>nhắc nợ ngày <strong>05</strong> hàng tháng, thanh toán ngày <strong>20</strong> hàng tháng</p>
          <p><strong>Thẻ Thè JCB: </strong>nhắc nợ ngày <strong>08</strong> hàng tháng, thanh toán ngày <strong>23</strong> hàng tháng</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="tab6" class=" sec-t sec-cauhoi ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht ">Câu hỏi thường gặp</h2>
      <a class="viewall" href="./22_FAQ.php">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>      
    <div class="accodion accodion-1">
        <?php
        $ques = [
                  'Điều kiện để đăng ký tham gia gói vay siêu nhanh trên App MBBank ?',
                  'Đăng ký "Vay siêu nhanh trên App MBBank" có mất phí không?',
                  'Hạn mức tôi được cấp là bao nhiêu?'
                ];
        for($i=1;$i<=3;$i++) {
        ?>
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span><?php echo $ques[$i-1] ?><span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content entry-content" >
                    <div class="inner">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. Mauris malesuada nisi sit amet augue accumsan tincidunt. Maecenas tincidunt, velit ac porttitor pulvinar.</p>
                    </div>
                </div>
            </div>
        <?php
        } ?>
    </div>           
  </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['khamphathem-1.jpg','khamphathem-2.jpg','khamphathem-3.jpg','khamphathem-4.jpg'];
        $link = ['','35_MB_Visa.php','','43_mb_bank_app.php'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="./<?php echo $link[$i-1] ?>" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/vaysieunhanh/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php'; ?>

<?php include 'include/index-bottom.php'; ?>