<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">MB Kết nối</span>
      </div>
    </div>
</div>

<section class="banner-img-1 next-shadow">
	<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/heading-1.jpg" src="assets/images/ketnoi/banner.png">
</section>


<main id="main" class="sec-t ">
	<div class="container">
		<div class="max750">
			<h1 class="text-center">MB kết nối</h1>
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$img = ['money-1.svg','family.svg','car.svg','travel.svg','save-money.svg','the-1.svg','money-1.svg','giadinh.svg','car.svg'];
				$title = 	[
								'Nổi bật',
								'Gia đình',
								'Nhà & Xe',
								'Du lịch',
								'Bảo hiểm',
								'Tiết kiệm & Đầu tư',
								'Du lịch',
								'Bảo hiểm',
								'Tiết kiệm & Đầu tư'
							];
				for ($i = 1; $i <= 10; $i++) { ?>
					<div class="item <?php if ($i == 1) echo 'active'; ?>">
						<a href="#" class="link">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>">
							</div>
							<div class="title"><?php echo $title[$i-1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</main>

<section class="sec-b sec-blog-2">
	<div class="container">
		<h2 class="">Chọn MB - Chọn trải nghiệm</h2>
		<div class="row list-item">
			<div class="col-lg-4">
				<a href="#" class="item-banner  tRes ">
					<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/img-1.png">
				</a>
			</div>
			<div class="col-lg-8">
				<div class="list-1-1  mb-30 ">


					<a href="#" class="item  tRes_39 ">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/img-2.png">
						<div class="divtext">
							<h4 class="title line2">Mẹo du lịch Nhật Bản siêu tiết kiệm</h4>
						</div>
					</a>

				</div>
				<div class="list-5 row list-item">
					<?php
					$img = ['img-3.png','img-4.png'];
					$title = 	[
									'Ngân hàng số - Triển vọng và phát triển trong tương lai',
									'Giải pháp quản lý tài chính cho gia đình trẻ'
								];
					for ($i = 1; $i <= 2; $i++) { ?>
						<div class="col-md-6">
							<a href="./41_ket_noi_detail.php" class="item efch-<?php echo $i + 1; ?> ef-img-l ">
								<div class="img tRes_51">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/<?php echo $img[$i-1] ?>">
								</div>
								<div class="divtext">
									<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="sec-b">
    <div class="container">
      <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Nổi bật</h2>
        <p class="desc cl5">Những thông tin mới nhất từ MB giúp kế hoạch tài chính của bạn vững vàng hơn</p>
      </div>
      <div class="list-5 row list-item">
        <?php
        $a_h1_2 = [
                    'App MBBank: Miễn phí chuyển tiền - Miễn lo dịch bệnh',
                    'Thế hệ không ngừng kết nối cần gì?',
                    'Vay nhanh Online: Cứu nguy kịp thời cho những ngày ví rỗng'
                  ];
        $img = ['nb-1.png','nb-2.png','nb-3.png'];
        $desc = [
        			'Không cần xếp hàng tại các cây ATM, không lo mất phí giao dịch, không ngại đối thoại…',
        			'Khi những đứa trẻ đầu tiên của thế hệ Z bước sang tuổi 23, nhu cầu, sở thích của họ hoàn toàn thay đổi; đặc biệt là cách họ kết nối với thế giới xung quanh.',
        			'Chỉ cần vài cú chạm trên App MBBank, bất kỳ ở đâu và bất kỳ lúc nào, bạn cũng có thể..'
        		];
        for ($i = 1; $i <= 3; $i++) { ?>
          <div class="col-md-4">
            <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
              <div class="img tRes_71">
                <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/<?php echo $img[$i-1] ?>">
              </div>
              <div class="divtext">
              	<div class="date">01/12/2019</div>
                <h4 class="title"><?php echo $a_h1_2[$i-1]; ?></h4>
                <div class="desc line4"><?php echo $desc[$i-1]; ?></div>
              </div>
            </a>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>


<section class="sec-b sec-h-4">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Hoạt động của MB và Doanh nghiệp</h2>
			<a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>
		<div class="row list-item">
			<div class="col-lg-6 list-1">
				<a href="#" class="item  tRes_56 video">
					<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/video-1.png">
					<div class="divtext">
						<div class="date">01/ 12/ 2019</div>
						<h4 class="title line2">Mẹo du lịch Nhật Bản siêu tiết kiệm</h4>
					</div>
				</a>
			</div>
			<div class="col-lg-6">
				<div class="list-6-1">
					<?php
					$img = ['video-2.png','video-3.png'];
					$title = [
								'SME CARE BY MB - ĐI CÙNG DOANH NGHIỆP',
								'MBBANK - TOUCHING YOUR LIFE'
							];
					$desc = [
								'Hỗ trợ toàn diện - Đưa doanh nghiệp vừa và nhỏ lên một tầm cao mới',
								'Cuộc thi sản xuất video content "Touching Your Life" với tổng giải thưởng hơn 1 tỷ đồng'
							];
					for ($i = 1; $i <= 2; $i++) { ?>
						<a href="#" class="item item-inline-table">
							<div class="img tRes_56 video cl">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/<?php echo $img[$i-1] ?>">
							</div>
							<div class="divtext">
								<div class="date">01/ 12/ 2019</div>
								<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
								<div class="desc line3"><?php echo $desc[$i-1] ?></div>
							</div>
						</a>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/index-bottom.php'; ?>