<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Thẻ</a>
        <span class="item">Chương trình trả góp lãi suất 0%</span>
      </div>
    </div>
</div>

<section class=" banner-heading-1 lazy-hidden group-ef next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Thẻ tín dụng MB</h1>
      <div class="efch-3 ef-img-l desc b cl1">Cho cuộc sống giản đơn</div>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r mw100" data-lazy-type="image" data-lazy-src="assets/images/canhan/the/banner.png">
</section>

<section class="sec-tb">
  <div class="container">
    <div class="accodion accodion-2">
      <?php
      $ques = [
                'Mua sắm',
                'Chăm sóc sức khỏe',
                'Giáo dục',
                'Du lịch'
              ];
      for($j=1;$j<=4;$j++) {
        ?>
        <div class="accodion-tab ">
          <input type="radio" id="chck_1_<?php echo $j; ?>" <?php if($j==1) echo 'checked'; ?> name="chck_1" >
          <label class="accodion-title h2" for="chck_1_<?php echo $j; ?>" ><span><?php echo $ques[$j-1] ?></span><span class="triangle" ><i class="icon-plus"></i></span> </label>
          <div class="accodion-content entry-content" >
            <div class="inner">
              <p>Chương trình Trả góp lãi suất 0% với thẻ tín dụng MB:</p>
              <p> • Áp dụng với thẻ tín dụng MB Visa và MB JCB Sakura</p>
              <p> • Áp dụng với mặt hàng có giá trị tối thiểu 3 triệu VNĐ trở lên </p>
              <p> • Kỳ hạn trả góp: 3, 6, 9, 12, 24 tháng. Tùy thuộc vào quy định của các đơn vị liên kết với MB </p>
              <p> • Hệ thống đơn vị liên kết trả góp lãi suất <span class="cl1 b">0%</span> liên tục được cập nhật <a href="#" class="b cl1">tại đây</a> </p>
              <p class="b">•  Tất cả các đơn vị đều áp dụng Phí trả góp <span class="cl1">0%</span> ( Trừ đơn vị Trả góp Điện máy Halo 92 : 0,6%/tháng)</p>
              <p class="b"> • Danh sách chi tiết hơn 1000 điểm liên kết trả góp được cập nhật 
                <a href="#">Tại đây</a></p>

              <div class="table-responsive">
                <table class="table table-full table-card">
                  <tr>
                    <th>Nhãn hàng</th>
                    <th>Kỳ hạn</th>
                    <th>Địa chỉ áp dụng</th>
                    <th>Sản phẩm áp dụng</th>
                  </tr>
                  <?php
                  $img = ['Logo-FPT.png','Logo-dienmayxanh.png','Logo-tgdd.png','Logo-thienhoa.png','Logo-Media.png','Logo-didongviet.png','Logo-viettelstore.png'];
                  $title =  [
                              'Công Ty Cổ Phần Bán Lẻ Kỹ Thuật Số FPT',
                              'Công Ty Cổ Phần Điện máy xanh',
                              'Công Ty Cổ Phần Thế giới di động',
                              'Điện máy Thiên Hoà',
                              'Điện máy - Máy tính - mobile',
                              'Điện thoại - Phụ kiện',
                              'Viettel'
                            ];
                  $link = [
                           'http://fptshop.com.vn/',
                           'http://dienmayxanh.com /',
                           'http://thegioididong.com/',
                           'http://mediamart.vn/',
                           'http://didongviet.vn/',
                           'http://viettelstore.vn/'
                          ];
                  for($i=1;$i<=5;$i++) {
                    ?>                            
                    <tr>
                      <td>
                        <img class=" loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/canhan/the/<?php echo $img[$i-1] ?>" src="assets/images/canhan/the/<?php echo $img[$i-1] ?>">
                        <p class="b mg-0"><?php echo $title[$i-1] ?></p>
                        <a href="#" class="cl1"><?php echo $link[$i-1] ?></a>
                      </td>
                      <td><span class="b">3, 6, 9, 12 tháng <br> Đến 12/03/2020</span></td>
                      <td>Áp dụng trên tất cả địa chỉ cảu FPT shop trên toàn quốc</td>
                      <td>Toàn bộ sản phẩm của cửa hàng<td>
                      </tr>
                      <?php
                    } ?>
                  </table>
                </div>
                <?php include '_module/pagination.php'?>
              </div>
            </div>
          </div>
          <?php
        } ?>
      </div>
    </div>
  </section>

  <?php include '_block/tu_van.php'; ?>






  <?php include 'include/index-bottom.php'; ?>