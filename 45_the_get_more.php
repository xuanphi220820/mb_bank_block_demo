<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Thẻ</a>
        <span class="item">Chương trình ưu đãi dành cho chủ thẻ MB</span>
      </div>
    </div>
</div>

<section class=" banner-heading-1 lazy-hidden group-ef next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Tận hưởng <br> 200 điểm ưu đãi</h1>
      <div class="efch-3 ef-img-l desc cl1">Dành cho chủ thẻ MB</div>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r mw100" data-lazy-type="image" data-lazy-src="assets/images/canhan/the-more/banner.png">
</section>

<section class="sec-tb">
  <div class="container">
  <div class="accodion accodion-2">
                <?php
                $ques = [
                          'Ẩm thực',
                          'Du lịch',
                          'Mua sắm',
                          'Chăm sóc sức khỏe',
                          'Giáo dục',
                          'Giải trí'
                        ];
                for($j=1;$j<=6;$j++) {
                ?>
                <div class="accodion-tab ">
                    <input type="radio" id="chck_1_<?php echo $j; ?>" <?php if($j==1) echo 'checked'; ?> name="chck_1" >
                    <label class="accodion-title h2" for="chck_1_<?php echo $j; ?>" ><span><?php echo $ques[$j-1] ?></span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                    <div class="accodion-content entry-content" >
                        <div class="inner">
                          <div class="table-responsive">
                            <table class="table table-full table-get-more">
                              <tr>
                                <th>Nhãn hàng</th>
                                <th>Địa điểm</th>
                                <th>Ưu đãi</th>
                              </tr>
                              <?php
                              $img = ['nhahang-1.jpg','nhahang-2.jpg','nhahang-3.jpg','nhahang-4.jpg','nhahang-5.jpg','nhahang-6.jpg'];
                              $title =  [
                                          'Nhà hàng hải sản san hô',
                                          'Home Mộc ',
                                          'Home hội an',
                                          'Ngon villa đà nẵng',
                                          'Ngon villa sài gòn',
                                          'Chi Hoa Vietnamese Cuisine'
                                        ];
                              $desc =   [
                                          '58 Lý Thường Kiệt, Trần Hưng Đạo, Hoàn Kiếm, Hà Nội',
                                          '31 Vân Hồ, Lê Đại Hành, Hai Bà Trưng, Hoàn Kiếm, Hà Nội',
                                          '112 Nguyễn Thái Học, Minh An, Ancient Town, Quảng Nam',
                                          '90 Lê Quang Đạo, Ngũ Hành Sơn, Đà Nẵng',
                                          '31A Lê Thánh Tôn, Bến Nghé, Quận 1, Hồ Chí Minh',
                                          '31A Lê Thánh Tôn, Bến Nghé, Quận 1, Hồ Chí Minh'
                                        ];
                              $discount = [
                                            'Giảm 10%',
                                            'Giảm 20% (tối đa 500.000 VNĐ)',
                                            'Giảm 20% (tối đa 500.000 VNĐ)',
                                            'Giảm 20% (tối đa 500.000 VNĐ)',
                                            'Giảm 20% (tối đa 500.000 VNĐ)',
                                            'Giảm 20% (tối đa 500.000 VNĐ)'
                                          ];
                              for($i=1;$i<=5;$i++) {
                              ?>                            
                              <tr>
                                <td>
                                  <img class=" loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/canhan/the-more/<?php echo $img[$i-1] ?>" src="assets/images/canhan/the-more/<?php echo $img[$i-1] ?>">
                                </td>
                                <td>
                                  <p class="uppercase b mb-10"><?php echo $title[$i-1] ?></p>
                                  <p><?php echo $desc[$i-1] ?></p>
                                </td>
                                <td>
                                  <p class="uppercase cl1 b"><?php echo $discount[$i-1] ?></p>
                                  <p>Thời gian: 31/03/2020</p>
                                  <p>Áp dụng: Thẻ tín dụng <span class="b">MB JBC Sakura</span></p>
                                </td>
                              </tr>
                              <?php
                              } ?>
                            </table>
                          </div>
                          <?php include '_module/pagination.php'?>
                        </div>
                    </div>
                </div>
                <?php
                } ?>
            </div>
</div>
</section>

<?php include '_block/tu_van.php'; ?>






<?php include 'include/index-bottom.php'; ?>