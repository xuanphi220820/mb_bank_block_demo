<?php include 'include/index-top.php'; ?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Tư vấn hổ trợ</span>
      </div>
    </div>
</div>
<section class="sec banner-heading-1 next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Trung tâm hỗ trợ</h1>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/faq/banner.png">
</section>

<main id="main" class="sec-tb">
  <div class="container">
    <h1 class="text-center">Bạn hỏi, MB trả lời</h1>
    <div class="cttab-v3   ">
      <div class="tab-menu">
        <div class="active"><span>Câu hỏi thường gặp</span></div>
        <div><span>Khách hàng cá nhân</span></div>
        <div><span>Khách hàng doanh nghiệp</span></div>
        <div><span>Khách hàng cá nhân cap cấp</span></div>
      </div>
      <div class="tab-content">
        <div class="active">
          <div class="tab-inner">
            <div class="flex-bw">
              <div class="sl">
                <h3 class="ctext">Sản phẩm/ Dịch vụ</h3>
                <select class="select">
                  <option>Tiền gửi</option>
                  <option>2030</option>
                </select>
              </div>
              <div class="sl">
                <h3 class="ctext">Chi tiết sản phẩm/ dịch vụ</h3>
                <select class="select">
                  <option>Tiên gửi có kì hạn</option>
                  <option>Báo cáo tài chính hợp nhất</option>
                </select>
              </div>
            </div>
            <section class=" sec-b sec-cauhoi ">
              <!-- <div class="entry-head text-center">
                  <h2 class="ht ">Câu hỏi thường gặp</h2>
                </div> -->
              <div class="accodion accodion-1">
                <?php
                $ques = [
                          'Tôi là chiến sĩ đang trong thời gian thực hiện nghĩa vụ, khi tham gia Tiết kiệm lập nghiệp tôi được hưởng những lợi ích gì?',
                          'Khi gửi tiết kiệm tại Ngân hàng, khoản tiền gửi của tôi có được MB mua bảo hiểm không?'
                        ];
                for ($i = 1; $i <= 2; $i++) {
                ?>
                  <div class="accodion-tab ">
                    <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if ($i == 1) echo 'checked'; ?>>
                    <label class="accodion-title" for="chck_1_<?php echo $i; ?>"><span> <?php echo $i; ?>. <?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
                    <div class="accodion-content entry-content">
                      <div class="inner">
                        <p>Đáp 1: Khi tham gia Tiết kiệp lập nghiệp, chiến sĩ sẽ nhận được các lợi ích:</p>
                        <p>Khoản tiết kiệm được đảm bảo an toàn và bảo mật tuyệt đối</p>
                        <p>Thuận tiện linh hoạt:</p>
                        <p>- Được giới thiệu và đăng ký mở tiết kiệm ngay tại đơn vị</p>
                        <p>- Gửi tiền dễ dàng, mọi lúc mọi nơi: qua Đơn vị quản lý/ đặt lệnh chuyển tiền tự động định kỳ/ chủ động nộp tiền/chuyển khoản tại quầy giao dịch MB,  chuyển khoản qua eMB, ATM, BankPlus, MB.Plus</p>
                        <p>Hoàn toàn chủ động trong kế hoạch chi tiêu và tiết kiệm</p>
                        <p>Miễn phí mở tài khoản thanh toán, phát hành thẻ quân nhân … (ưu đãi theo Chính sách MB từng thời kỳ)</p>
                      </div>
                    </div>
                  </div>
                <?php
                } ?>
              </div>
              <div class="text-center">
                <button class="btn lg">xem thêm</button>
              </div>
            </section>
          </div>
        </div>
        <div>
          <div class="tab-inner">
            2
          </div>
        </div>

      </div>
    </div>
  </div>
</main>

<?php include '_module/pagination.php' ?>
<?php include 'include/index-bottom.php'; ?>