<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
	<div class="container">
		<div class="breadcrumbs">
			<a class="item" href="#">Trang chủ</a>
			<span class="item">Tin tức</span>
		</div>
	</div>
</div>

<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
	<div class="container">
		<div class="divtext ">
			<h1 class=" efch-1 ef-img-l" >Tin tức <br> hoạt động nổi bật</h1>
		</div>

	</div>
	<img class="img tr lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/tintuc/banner.png">
</section>

<section  class="sec-tb" >
	<div class="container">
		<div class="entry-head text-center">
			<h2 class="ht efch-1 ef-img-t h1">Tin tức MB</h2>
		</div>
		<div class="menuicon">
			<?php
			$icon = ['policy.svg','magazine.svg','bieu-do.svg','meeting.svg'];
			$a_h1_2 = [
				'Tin MB',
				'MB với báo chí',
				'Tài chính ngân hàng',
				'Trách nhiệm xã hội'];

			$id = 	[
						'#news1',
						'#news2',
						'#news3',
						'#news4'
					];
				for($i=1;$i<=4;$i++) {?>
					<div class="item   efch-<?php echo $i+2; ?> ef-img-t <?php if($i==1) echo 'active'; ?>">
						<a href="<?php echo $id[$i-1] ?>" class="link scrollspy">
							<div class="img">
								<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $icon[$i-1] ?>">
							</div>
							<div class="title"><?php echo $a_h1_2[$i-1]; ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>

	<section id="news1" class="sec-tb">
		<div class="container">
			<div class="entry-head">
				<h2 class="ht efch-1 ef-img-l">Tin MB</h2>
				<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
			</div>
			<div class="list-5 row list-item" > 
				<?php
				$img = ['tintuc-1.jpg','tintuc-2.jpg','tintuc-3.jpg','tintuc-4.jpg','tintuc-5.jpg','tintuc-3.jpg'];
				$title = 	[
					'"Quán quân" công bố thông tin năm 2017 đang thuộc về nhóm cổ …',
					'MB liên tiếp được vinh danh 2 giải thưởng “Ngân hàng số tiêu …',
					'MB khuyến cáo khách hàng bảo mật thông tin ngân hàng điện tử',
					'Cảnh báo lừa đảo về việc yêu cầu cung cấp OTP',
					'Hội nhập và tâm thế người cầm lái MB bank',
					'Thông báo thay đổi nhận diện thương hiệu mới'
				];
				for($i=1;$i<=3;$i++) {?>
					<div class="col-md-4">
						<a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l ">
							<div class="img tRes_71">
								<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/tintuc/<?php echo $img[$i-1] ?>">
							</div>
							<div class="divtext">
								<div class="date">01/ 12/ 2019</div>
								<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
								<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh…</div>
							</div>
						</a>
					</div>				          
				<?php } ?>
			</div>

		</div>
	</section>

	<section id="news2" class="sec-tb sec-h-4">
		<div class="container">
			<div class="entry-head">
				<h2 class="ht efch-1 ef-img-l">MB với báo chí</h2>
				<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
			</div>

			<div class="row list-item">
				<div class="col-lg-8 ">

					<div class="list-5 row ">
						<?php
						$img = ['tintuc-1.jpg','tintuc-2.jpg'];
						$a_h1 =	[
							'"Quán quân" công bố thông tin năm 2017 đang thuộc về nhóm cổ …',
							'MB liên tiếp được vinh danh 2 giải thưởng “Ngân hàng số tiêu …'
						];
						for ($i = 1; $i <= 2; $i++) { ?>
							<div class="col-md-6">
								<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
									<div class="img tRes_71">
										<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/tintuc/<?php echo $img[$i - 1] ?>">
									</div>
									<div class="divtext">
										<div class="date">01/ 12/ 2019</div>
										<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
										<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh…</div>
									</div>
								</a>
							</div>
						<?php } ?>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="list-6">
						<?php
						$img = ['bc-1.jpg','bc-2.jpg','bc-3.jpg'];
						$a_h1 =	[
							'Cho vay mua, xây dựng, sữa chữa nhà, đất',
							'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
							'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'
						];
						for ($i = 1; $i <= 3; $i++) { ?>
							<a href="#" class="item item-inline-table">
								<div class="img">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/tintuc/<?php echo $img[$i - 1] ?>">
								</div>
								<div class="divtext">
									<h4 class="title line4"><?php echo $a_h1[$i - 1] ?></h4>
								</div>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="news3" class="sec-tb sec-h-4__" >
		<div class="container">
			<h2 class="">Tin tức tài chính ngân hàng</h2>
			<p class="cl5">Những thông tin mới nhất từ MB giúp kế hoạch tài chính của bạn vững vàng hơn</p>
			<div class="list-5 list-5-1 row list-item" >
				<?php
				$title = 	[
					'Thông báo danh sách Khách hàng trúng thưởng CT vay mua nhà…',
					'Thông báo danh sách Khách hàng trúng thưởng CT vay mua nhà…',
					'Kiểm soát tài chính doanh nghiệp một cách dễ dàng và an toàn',
					'Phương án quản trị doanh nghiệp theo đúng định hướng',
					'Phương án quản trị doanh nghiệp theo đúng định hướng',
					'Thu hút nhân tài, chế độ khen thưởng ở các doanh nghiệp'
				];
				for($i=1;$i<=3;$i++) {?>
					<div class="col-md-4">
						<a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
							<div class="divtext">
								<div class="date">01/ 12/ 2019</div>
								<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
								<div class="desc r_f_1_3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. </div>
								<span class="more cl1">Tìm hiểu thêm</span>
							</div>

						</a>
					</div>				          
				<?php } ?>
			</div>	
		</div>
	</section>
	
	<section id="news4" class="sec-tb sec-h-4__" >
		<div class="container">
			<h2 class="">Trách nhiệm xã hội</h2>
			<p class="cl5">Những thông tin mới nhất từ MB giúp kế hoạch tài chính của bạn vững vàng hơn</p>
			<div class="list-5 list-5-1 row list-item" >
				<?php
				$title = 	[
					'Thông báo danh sách Khách hàng trúng thưởng CT vay mua nhà…',
					'Thông báo danh sách Khách hàng trúng thưởng CT vay mua nhà…',
					'Kiểm soát tài chính doanh nghiệp một cách dễ dàng và an toàn',
					'Phương án quản trị doanh nghiệp theo đúng định hướng',
					'Phương án quản trị doanh nghiệp theo đúng định hướng',
					'Thu hút nhân tài, chế độ khen thưởng ở các doanh nghiệp'
				];
				for($i=1;$i<=3;$i++) {?>
					<div class="col-md-4">
						<a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
							<div class="divtext">
								<div class="date">01/ 12/ 2019</div>
								<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
								<div class="desc r_f_1_3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. </div>
								<span class="more cl1">Tìm hiểu thêm</span>
							</div>

						</a>
					</div>				          
				<?php } ?>
			</div>	
		</div>
	</section>




	<?php include 'include/index-bottom.php';?>