<?php include 'include/index-top.php'; ?>
<?php include '_module/breadcrumb.php'; ?>
<section class="sec-tb">
	<div class="container">
		<div class="text-center">
			<h1>Đăng ký dịch vụ trực tuyến</h1>
			<p class="desc max750">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore atque porro rem labore, earum, eveniet suscipit cum dolores, nesciunt perferendis ratione asperiores consequatur eligendi saepe eum! Impedit sunt dolore ad?</p>
		</div>
	</div>
</section>
<section class="online-signup">
	<div class="container">
		<div class="max950">
			<div class="flex-bw">
				<div class="step">
					<a href="#" class="b">01</a>
				</div>
				<div class="step">
					<a href="#" class="b active">02</a>
				</div>
				<div class="step">
					<a href="#" class="b">03</a>
				</div>
				<div class="step">
					<a href="#" class="b">04</a>
				</div>
				<div class="step-line"></div>
			</div>
		</div>
	</div>
</section>
<section class=" sec-tb ">
	<div class="container">
		<div class="max750">
			<form class="row list-item form-contact">
				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 fs24">Bước 2/4: Thông tin dịch vụ</h3>
					</div>
				</div>

				<div class="col-lg-12">
					<div class="text-center">
						<h3 class="ctext mg-0 pt-10 pb-10 bg-1">Thông tin dịch vụ đăng ký</h3>
					</div>
				</div>

				<div class="col-lg-6">
					<label class="radio b fs16"><input type="radio" name="check1" checked=""><span></span>Mở tài khoản</label>
				</div>

				<div class="col-lg-6">
					<label class="radio b fs16"><input type="radio" name="check1"><span></span>SMS Banking</label>
				</div>

				<div class="col-lg-6">
					<label class="radio b fs16"><input type="radio" name="check1"><span></span>eMB ngân hàng điện tử</label>
				</div>

				<div class="col-lg-6">
					<label class="radio b fs16"><input type="radio" name="check1"><span></span>Thẻ ATM Active Pl</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Số điện thoại nhận SMS (*)">
					</label>
				</div>
				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Tên đăng nhập ebanking (*)">
					</label>
				</div>

				<div class="col-12 text-center">
					<a class="btn" href="<?php if (1 == 1): ?>
							./25_dang_ky_online_3.php
						<?php else: ?>
							#
                  		<?php endif ?> ">Tiếp tục</a>
				</div>
			</form>
		</div>

	</div>
</section>

<?php include 'include/index-bottom.php'; ?>