<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >Gia đình tôi yêu</h1>
        <div class="efch-3 ef-img-l desc">Tặng 1 triệu cho mỗi con  <br> Miễn phí trọn đời cho bố mẹ</div>
        </div>
        <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-6.jpg">
    </div>
    
</section>


<?php include '_block/block_3.php';?>
<?php include '_block/block_4.php';?>


<section class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">Lợi ích của bạn</h2>
    </div>    
    <div class="row list-item">
      <?php 
      $a_5_1 = ['other/Fast.svg','other/Fast.svg'];
      $a_5_2 = ['MỨC CHO VAY','Thời hạn CHO VAY','Thủ tục đơn giản','PHƯƠNG THỨC TRẢ NỢ','MỨC CHO VAY','Thời hạn CHO VAY','Thủ tục đơn giản','PHƯƠNG THỨC TRẢ NỢ '];
      $a_5_3 = ['Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Lãi trả định kỳ hoặc cuối kỳ tính theo niên kim cố định','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Lãi trả định kỳ hoặc cuối kỳ tính theo niên kim cố định'];
      for($i=1;$i<=4;$i++) { ?>
      <div class="col-sm-6 col-md-6 col-lg-3 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/Fast.svg" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
            <div class="desc"><?php echo $a_5_3[$i-1]; ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>



<section class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
    <div class="row center">
      <div class="col-lg-6">
        <div class="img tRes_66 efch-2 ef-img-r ">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/1000x600" src="https://via.placeholder.com/10x6">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">Hướng dẫn Đăng ký</h2>
           
          <p ><strong>Cách 1</strong> Đăng ký trực tiếp trên App Bank </p>
          <p ><strong>Cách 2</strong> Đăng ký nhận thông tin trực tiếp từ đội ngũ chăm sóc khách hàng của MB Bank</p>
          <a class="btn lg" href="#">ĐĂNG KÝ NGAY</a>  </p>

        </div>
      </div>
    </div>
  </div>
</section>
 
 <?php include '_block/cau_hoi.php';?>


<section  class="sec-b " >
    <div class="container">
        <div class="entry-head text-center">
            <h2 class="ht efch-1 ef-img-l">Ưu đãi</h2>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            for($i=1;$i<=10;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $i; ?> Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head text-center">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>