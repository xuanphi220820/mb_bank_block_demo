<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Mbbank PC</title>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel='stylesheet' href='assets/css/main.css' type='text/css' media='all' />
	<link rel='stylesheet' href='assets/css/bonus.css' type='text/css' media='all' />
	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- META FOR FACEBOOK -->
	<meta property="og:site_name" content="Propzy" />
	<meta property="og:url" itemprop="url" content="http://localhost" />
	<meta property="og:image" itemprop="thumbnailUrl" content="xxx" />
	<meta property="og:title" content="Home" itemprop="headline" />
	<meta property="og:description" content="" itemprop="description" />
	<!-- END META FOR FACEBOOK -->

</head>

<body class="mb-priority">
	<div id="wrapper">
		<span class="menu-btn overlay"> </span>
		<div id="panel">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<!-- <div class="form-search-header">
							<button><i class="icon-search-2"></i></button>
							<input id="search" type="text" placeholder="Nhập từ khóa cần tìm kiếm .......">
						</div> -->
					</div>
					<div class="col-md-8">
						<ul class="menu line text-right">
							<li>
								<button class="search-sg"><i class="icon-search-2"></i></button> 
								<input id="search" type="text" placeholder="Tìm kiếm">
							</li>
							<li> <a href="./05_Ve_MB_3.php">Về MB</a> </li>
							<li> <a href="./13_nha_dau_tu.php">Nhà đầu tư</a> </li>
							<li> <a href="https://tuyendung.mbbank.com.vn/TinTuc">Tuyển dụng</a> </li>
							<li> <a href="./51_lien_he.php">Liên hệ</a> </li>
							<li>
								<div class="dropdown language">
									<div class="title"> <span><img src="assets/images/flags/vn.png" alt=""></span> <i class="icon-arrow-2 ib"></i> </div>
									<div class="content">
										<div class="inner">
											<ul class="menu">
												<li class="lang-en"><a href="#" hreflang="en" title="English (en)"><img src="assets/images/flags/gb.png" alt=""> <span>English</span></a></li>
												<li class="lang-vi active"><a href="#" hreflang="vi" title="Tiếng Việt (vi)"><img src="assets/images/flags/vn.png" alt=""> <span>Tiếng Việt</span></a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>

				</div>
			</div>
			<div id="search-sg">
				<div class="container">
					<a href="./01_index.php" id="logo"> <img src="assets/images/logo-blue.svg" alt=""></a>
					<div class="suggest">
						<div class="sg kw">
							<span class="b cl1">Từ khóa phổ biến</span>
							<div class="key">
								<a href="#">vay vốn</a>
								<a href="#">ngân hàng số</a>
								<a href="#">thẻ tín dụng quốc tế</a>
								<a href="#">tiền hửi thanh toán</a>
							</div>
						</div>
						<div class="sg sv">
							<span class="b cl1">Dịch vụ gợi ý</span>
							<div class="tags">
								<a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
								<a class="tag" href="#">Ngân hàng đầu tư</a>
								<a class="tag" href="#">Quản lý dòng tiền</a>
							</div>
						</div>
					</div>
					<span class="icon-close close-sg"></span>
				</div>
			</div>
		</div>
		<header id="header" class="fixe bg8" role="banner">
			<div class="container">
				<a href="./01_index.php" id="logo"> <img src="assets/images/doanhnghiep/priority/logo.png" alt=""></a>
				<div class="wrap-menu-header ">
					<ul class="menu-top-header " data-style="1">
						<li class="children">
							<a href="29_SP_NoiBat.php"><span>Khách hàng cá nhân</span></a>
						</li>
						<li class="children">
							<a href="#"><span>Khách hàng tổ chức</span></a>
							<ul>
								<li><a href="37_doanh_nghiep.php">Doanh nghiệp vừa và nhỏ</a></li>
								<li><a href="./38_DN_Lon.php">Doanh nghiệp lớn</a></li>
								<li><a href="./36_doanh_nghiep_tai_chinh.php">Định chế tài chính</a></li>
							</ul>		
						</li>
						<li class="active children">
							<a href="#"><span>Khách hàng ưu tiên</span></a>
							<ul>
								<li><a href="39_dau_tu.php">MB Priority</a></li>
								<li><a href="https://smecare.com.vn/">MB Private</a></li>
							</ul>
						</li>
						<li><a href="#"><span>MB kết nối</span></a></li>
						<li class="highlight children e-banking">
							<a href="./49_login.php"><span>Ebanking</span></a>
							<ul>
								<li><a href="49_login.php">Cá Nhân</a></li>
								<li><a href="50_login-2.php">Doanh nghiệp</a></li>
							</ul>
						</li>

					</ul>
					<ul  class="menu-top-header e-banking">
						<li class="highlight children">
							<a href="./49_login.php"><span>Ebanking</span></a>
							<ul>
								<li><a href="49_login.php">Cá Nhân</a></li>
								<li><a href="50_login-2.php">Doanh nghiệp</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="group-header">
					<div class="item ilang">
						<div class="dropdown language">
							<div class="title"> <span><img src="assets/images/flags/vn.png" alt=""></span> <i class="icon-arrow-2 ib"></i> </div>
							<div class="content">
								<div class="inner">
									<ul class="menu">
										<li class="lang-en"><a href="#" hreflang="en" title="English (en)"><img src="assets/images/flags/gb.png" alt=""> <span>English</span></a></li>
										<li class="lang-vi active"><a href="#" hreflang="vi" title="Tiếng Việt (vi)"><img src="assets/images/flags/vn.png" alt=""> <span>Tiếng Việt</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="item imenu"><span class="menu-btn x"><span></span></span> </div>

				</div>
			</div>
		</header>

		<div class="wrap-menu-mb">
			<div class="wrapul main">
				<div class="inner">
					<ul class="menu">
						<?php include 'include/mainmenu.php'; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="entry-breadcrumb">
			<div class="container">
				<div class="breadcrumbs">
					<a class="item" href="#">Home</a>
					<span class="item cl8">Khách hàng ưu tiên</span>
				</div>
			</div>
		</div>

		<section class="banner-img-1 next-shadow">
			<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/heading-1.jpg" src="assets/images/doanhnghiep/priority/banner.png">
		</section>

		<section  class="sec-tb sec-h-1 lazy-hidden group-ef" >
			<div class="container">
				<div class="entry-head text-center">
					<h2 class="ht efch-1 ef-img-t">Hãy để MB hỗ trợ bạn ngay</h2>
				</div>
				<div class="menuicon">
					<?php
					$img = ['ico-1.svg','ico-2.svg','ico-3.svg','ico-4.svg','ico-5.svg'];
					$a_h1_2 = [
						'Giới thiệu',
						'Giao dịch',
						'Đầu tư',
						'Bảo vệ',
						'Đăc quyền'
					];
					for($i=1;$i<=5;$i++) {?>
						<div class="item  efch-<?php echo $i+2; ?> ef-img-t">
							<a href="#" class="link priority <?php if ($i == 1) echo "active-priority" ?>">
								<div class="img">
									<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/icon/<?php echo $img[$i-1] ?>">
								</div>
								<div class="title cl3"><?php echo $a_h1_2[$i-1]; ?></div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<section  class="sec-tb sec-h-2 bg-gray lazy-hidden group-ef" >
			<div class="container">
				<div class="entry-head">
					<h2 class="ht efch-1 ef-img-t">Sản phẩm nổi bật</h2>
				</div>
				<div class="row list-item list-1">
					<?php
					$a_h2_1 = ['Gói dịch vụ','Sản phẩm'];
					$a_h2_2 = ['Thảm đỏ','đầu tư'];
					$img = ['img-1.png','img-2.png'];
					for($i=1;$i<=2;$i++) {?>
						<div class="col-md-6 efch-<?php echo $i+1; ?> ef-img-t">
							<a href="#" class="item tRes_66">
								<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/<?php echo $img[$i-1] ?>">

								<div class="divtext">
									<div class="desc"><?php echo $a_h2_1[$i-1]; ?></div>
									<h4 class="title"><?php echo $a_h2_2[$i-1]; ?></h4>	          		
								</div>
							</a>
						</div>
					<?php } ?>
				</div>		
				<div class="row list-item list-2">
					<?php
					$icon = ['ung-dung-2.svg','family.svg','money-1.svg'];
					$a_h1_2 = ['Tài khoản số đẹp','Tiết kiệm cho con','Vay siêu nhanh'];

					for($i=1;$i<=3;$i++) {?>
						<div class="col-lg-3 col-6 efch-<?php echo $i+3; ?> ef-img-t">
							<a href="#" class="item">
								<div class="img">
									<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/icon/<?php echo $icon[$i-1] ?>">
								</div>
								<h4 class="title cl8"><?php echo $a_h1_2[$i-1]; ?></h4>
								<div class="desc cl8">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
							</a>
						</div>
					<?php } ?>
					<div class="col-lg-3 col-6 efch-7 ef-img-t">
						<div class="item item-2 bg8">
							<h5 class="title c-white">Truy cập nhanh</h5>
							<a class="btn btn-3 radius-8 bg9 bd9" href="#"><i><img src="assets/images/doanhnghiep/priority/tcn-1.svg" alt=""></i>Mở quỹ đầu tư</a>
							<a class="btn btn-3 radius-8 bg9 bd9" href="#"><i><img src="assets/images/doanhnghiep/priority/tcn-2.svg" alt=""></i>Thẻ MB VIP</a>
							<a class="btn btn-3 radius-8 bg9 bd9" href="#"><i><img src="assets/images/doanhnghiep/priority/tcn-3.svg" alt=""></i>Tiền gửi</a>
						</div>
					</div>	    	
				</div>
			</div>
		</section>

		<section  class="sec-b" >
			<div class="container">
				<div class="entry-head">
					<h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
					<a class="viewall cl8" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
				</div>
				<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
					<?php
					$a_h1 = [
						'Thông báo danh sách khách hàng trúng thưởng CT',
						'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
						'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
						'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
						'Thông báo danh sách khách hàng trúng thưởng CT',
						'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
						'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
						'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
					];
					$img = ['khuyenmai-1.png','khuyenmai-2.png','khuyenmai-1.png','khuyenmai-2.png','khuyenmai-1.png','khuyenmai-2.png','khuyenmai-1.png','khuyenmai-2.png'];
					for($i=1;$i<=8;$i++) {?>
						<a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
							<div class="img tRes_71">
								<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/<?php echo $img[$i-1] ?>">
							</div>
							<div class="divtext">
								<div class="date">01/ 12/ 2019</div>
								<h4 class="title line2 cl8"><?php echo $a_h1[$i-1]; ?></h4>
							</div>
						</a>
					<?php } ?>
				</div>          
			</div>
		</section>
		<section class="sec-b sec-h-4">
			<div class="container">
				<div class="entry-head">
					<h2 class="ht efch-1 ef-img-l">MB kết nối</h2>
					<a class="viewall cl8" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
				</div>

				<div class="row list-item">
					<div class="col-lg-8 ">

						<div class="list-5 row ">
							<?php
							$img = ['kn-1.png','kn-2.png'];
							$a_h1 =	[
								'MB ra mắt hệ sinh thái số dành cho khách hàng doanh nghiệp',
								'Doanh nghiệp vừa và nhỏ được quảng cáo miễn phí với gói sản phẩm "SWE ..."'
							];
							for ($i = 1; $i <= 2; $i++) { ?>
								<div class="col-md-6">
									<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
										<div class="img tRes_71">
											<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/<?php echo $img[$i - 1] ?>">
										</div>
										<div class="divtext">
											<div class="date">01/ 12/ 2019</div>
											<h4 class="title line2 cl8"><?php echo $a_h1[$i - 1] ?></h4>
											<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
										</div>
									</a>
								</div>
							<?php } ?>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="list-6">
							<?php
							$img = ['kn-3.png','kn-4.png','kn-5.png'];
							$a_h1 =	[
								'Cho vay mua, xây dựng, sữa chữa nhà, đất',
								'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
								'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'
							];
							for ($i = 1; $i <= 3; $i++) { ?>
								<a href="#" class="item item-inline-table">
									<div class="img">
										<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/priority/<?php echo $img[$i - 1] ?>">
									</div>
									<div class="divtext">
										<h4 class="title line4 cl8"><?php echo $a_h1[$i - 1] ?></h4>
									</div>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="sec-cta">
			<div class="container">
				<div class="row   center">
					<div class="col-4 " >
						<a class="item" href="#">
							<span class="img"><img src="assets/images/svg/t1.svg" alt=""></span>
							<div class="divtext">
								<h4 class="title">Gọi ngay</h4>
								<div class="desc"> 1900 545426 - (84-24) 3767 4050 <br> (Từ nước ngoài)</div>
							</div>
						</a>
					</div>
					<div class="col-4 " >
						<a class="item" href="#">
							<span class="img"><img src="assets/images/svg/t2.svg" alt=""></span>
							<div class="divtext">
								<h4 class="title">Gửi email</h4>
								<div class="desc">mb247@mbbank.com.vn</div>
							</div>
						</a>
					</div>
					<div class="col-4 " >
						<a class="item" href="./52_diem_atm.php">
							<span class="img"><img src="assets/images/svg/t3.svg" alt=""></span>
							<div class="divtext">
								<h4 class="title">Tìm ATm & Điểm GD</h4>
								<div class="desc">Tìm điểm giao dịch & ATM gần bạn</div>
							</div>
						</a>
					</div>		  		
				</div>
			</div>
		</section>
		<section class="sec-download-pc group-ef lazy-hidden bg8">
			<div class="container">
				<div class="row">
					<div class="col-md-6   efch-2 ef-img-r" >
						<p class="stitle">Đăng ký nhận thông tin khuyến mãi</p>
						<form role="search" method="get" class="searchform " action="">
							<div><input type="text" placeholder="Nhập email để nhận thông tin" value="" name="s" class="input"></div>
							<button type="submit" class="btn btn-2">Đăng ký</button>
						</form>
					</div>
					<div class="col-md-6   efch-3 ef-img-r" >

						<div class="wapp">
							<span class="code"><img src="assets/images/code.png" alt=""></span>
							<div class="app">
								<p class="stitle">Hãy tải app ngay hôm nay</p>
								<a href="#"><img src="assets/images/btt-google.svg" alt=""></a> &nbsp;
								<a href="#"><img src="assets/images/btt-chplay.svg" alt=""></a>		
							</div>
						</div>	
					</div>
				</div>
			</div>
		</section>
		<div id="footer-pc" class=" group-ef lazy-hidden">
			<div class="container">
				<div class="row grid-space-10">
					<div class="col-lg-5 col-sm-12 efch-1 ef-img-t">
						<div class="widget widget-info">
							<div><a href="./" class="logo"> <img src="assets/images/logo-blue.svg" alt=""></a></div>
							<h3>Ngân hàng TMCP Quân Đội</h3>
							<p>Toà nhà MBBank - Hội sở 21 Cát Linh, Đống Đa, Hà Nội</p>
							<p>Email: mb247@mbbank.com.vn</p>
							<p class="w6">  Swift code: MSCBVNVX</p>
							<ul class="blog-item-social ">
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
							</ul>							
						</div>					
					</div>

					<div class="col-lg-7">
						<div class="row">
							<div class="col-md-4 col-6    efch-2 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Cá nhân</h4>
									<ul class="menu">
										<li><a href="#">Ngân hàng số</a></li>
										<li><a href="#">Tiết kiệm</a></li>
										<li><a href="#">Cho vay</a></li>
										<li><a href="#">Thẻ</a></li>
										<li><a href="#">Chuyển tiền thanh toán</a></li>
										<li><a href="#">Bảo hiểm nhân thọ</a></li>
										<li><a href="#">Sản phẩm khác</a></li>
									</ul>
								</div>
							</div>		
							<div class="col-md-4 col-6    efch-2 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Doanh nghiệp</h4>
									<ul class="menu">
										<li><a href="#">Ngân hàng số</a></li>
										<li><a href="#">Tài khoản và dịch vụ</a></li>
										<li><a href="#">Tiền gửi</a></li>
										<li><a href="#">Tín dụng và bảo lãnh</a></li>
										<li><a href="#">Tài trợ thương mại</a></li>
										<li><a href="#">Thị trường tiền tệ và vốn</a></li>
										<li><a href="#">Dịch vụ khác</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-4 col-6    efch-3 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Khách hàng cao cấp</h4>
									<ul class="menu">
										<li><a href="#">Private</a></li>
										<li><a href="#">Priorities</a></li>
									</ul>
								</div>
								<div class="widget">
									<h4 class="widget-title">Về MBBank</h4>
									<ul class="menu">
										<li><a href="#">Về chúng tôi</a></li>
										<li><a href="#">Nhà đầu tư</a></li>
										<li><a href="#">Nghề nghiệp</a></li>
										<li><a href="#">Liên hệ</a></li>
									</ul>
								</div>							

							</div>												
						</div>
					</div>	

				</div>
				<div class="line"></div>
				<div class="row grid-space-10">
					<div class="col-lg-6 col-md-7 efch-5 ef-img-t">
						<ul class="menu line">
							<li>  <a href="#">Điều khoản sử dụng</a>  </li>
							<li>  <a href="#">Bảo mật thông tin khách hàng</a>  </li>
						</ul>
					</div>
					<div class="col-lg-6 col-md-5 efch-6 ef-img-t">
						<div class="copyright">2019 © Copyright MBbank. All rights reserved.</div>
					</div>				
				</div>
			</div>

			<a id="back-top" class="back-top-1" href="#" ><i class="icon-arrow-db it"></i> <span>TOP</span></a>
		</div> <!--End #footer PC-->

		<section class="sec-download-mb">

			<div class="wform">
				<p class="stitle">Đăng ký nhận thông tin khuyến mãi</p>

				<form role="search" method="get" class="searchform " action="">
					<div class="aaa"><input type="text" placeholder="Nhập email để nhận thông tin" value="" name="s" class="input"></div>


					<button type="submit" class="btn btn-2">Đăng ký</button>
				</form>
			</div>

			<div class="wdownload">
				<span class="stitle">Tải app ngay</span>&nbsp;
				<a href="#"><img src="assets/images/btt-google-mb.svg" alt=""></a> &nbsp;
				<a href="#"><img src="assets/images/btt-chplay-mb.svg" alt=""></a>	
			</div>
		</section>
		<div id="footer-mb" class=" group-ef lazy-hidden">
			<div class="container">

				<div class="widget widget-info">
					<div><a href="./" class="logo"> <img src="assets/images/logo-blue.svg" alt=""></a></div>
					<h3>Ngân hàng TMCP Quân Đội</h3>
					<p>Toà nhà MBBank - Hội sở 21 Cát Linh, Đống Đa, Hà Nội</p>
					<p>Email: mb247@mbbank.com.vn</p>
					<p>Swift code: MSCBVNVX</p>
					<p class="w6">Hãy gọi cho chúng tôi để được tư vấn 24/7</p>
					<div class="call">
						<a class="phone" href="#"><i class="icon-phone-1"></i> 1900 545426</a>

						<ul class="blog-item-social ">
							<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
							<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
							<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
							<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
						</ul>						
					</div>
				</div>					


				<div class="accodion accodion-0">
					<div class="accodion-tab ">
						<input type="checkbox" id="chck_mf"  >
						<label class="accodion-title" for="chck_mf" ><span> Mở rộng </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
						<div class="accodion-content" >
							<div class="inner">

								<div class="row grid-space-10">
									<div class="col-md-3 col-6  efch-2 ef-img-t">
										<div class="widget">
											<h4 class="widget-title">Cá nhân</h4>
											<ul class="menu">
												<li><a href="#">Ngân hàng số</a></li>
												<li><a href="#">Tiết kiệm</a></li>
												<li><a href="#">Cho vay</a></li>
												<li><a href="#">Thẻ</a></li>
												<li><a href="#">Chuyển tiền thanh toán</a></li>
												<li><a href="#">Bảo hiểm nhân thọ</a></li>
												<li><a href="#">Sản phẩm khác</a></li>
											</ul>
										</div>
									</div>		
									<div class="col-md-3 col-6  efch-2 ef-img-t">
										<div class="widget">
											<h4 class="widget-title">Doanh nghiệp</h4>
											<ul class="menu">
												<li><a href="#">Ngân hàng số</a></li>
												<li><a href="#">Tài khoản và dịch vụ</a></li>
												<li><a href="#">Tiền gửi</a></li>
												<li><a href="#">Tín dụng và bảo lãnh</a></li>
												<li><a href="#">Tài trợ thương mại</a></li>
												<li><a href="#">Thị trường tiền tệ và vốn</a></li>
												<li><a href="#">Dịch vụ khác</a></li>
											</ul>
										</div>
									</div>
									<div class="col-md-3 col-6  efch-3 ef-img-t">
										<div class="widget">
											<h4 class="widget-title">Khách hàng cao cấp</h4>
											<ul class="menu">
												<li><a href="#">Private</a></li>
												<li><a href="#">Priorities</a></li>
											</ul>
										</div>
										<div class="widget">
											<h4 class="widget-title">Liên hệ</h4>
											<ul class="menu">
												<li><a href="#">Chat trực tuyến</a></li>
												<li><a href="#">Tìm điểm giao dịch</a></li>
												<li><a href="#">Tổng đài 24/7</a></li>
											</ul>
										</div>							
									</div>
									<div class="col-md-3 col-6  efch-4 ef-img-t">
										<div class="widget">
											<h4 class="widget-title">Về MBBank</h4>
											<ul class="menu">
												<li><a href="#">Về chúng tôi</a></li>
												<li><a href="#">Nhà đầu tư</a></li>
												<li><a href="#">Nghề nghiệp</a></li>
												<li><a href="#">Liên hệ</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

			<div class="menu-footer-mb">
				<div class="row">
					<div class="col-3">
						<a class="item " href="#">
							<span class="img">	<img src="assets/images/svg/home.svg" alt="">	</span>
							<span class="name">Trang chủ</span>
						</a>
					</div>	
					<div class="col-3" >
						<a class="item" href="#">
							<span class="img"><img src="assets/images/svg/folder.svg" alt=""></span>
							<span class="name">Sản phẩm</span>
						</a>
					</div>	
					<div class="col-3">
						<a class="item " href="#">
							<span class="img"><img src="assets/images/svg/MB.svg" alt=""></span>
							<span class="name">MB++</span>
						</a>
					</div>	
					<div class="col-3">
						<a class="item " href="#">
							<span class="img"><img src="assets/images/svg/giadinh.svg" alt=""></span>
							<span class="name">Gia đình</span>
						</a>
					</div>	
					<div class="col-3">
						<a class="item " href="#">
							<span class="img"><img src="assets/images/svg/tienich.svg" alt=""></span>
							<span class="name">Tiện ích</span>
						</a>
					</div>	
				</div>
			</div>		
		</div> <!--End #footer MB-->
	</div> <!--End #wrapper-->
	<script type='text/javascript' src='assets/js/owl.carousel.min.js'></script> 
	<script type='text/javascript' src='assets/js/script.js'></script>
	<script type='text/javascript' src='assets/js/bonus.js'></script>

</body></html>
<i class=""></i>
