<?php include 'include/index-top.php';?>
<section   class="sec banner-heading-1 lazy-hidden next-shadow" data-lazy-type="bg" data-lazy-src="assets/images/mb3/MB3banner.jpg" >
    <div class="container">
        <div class="divtext">
        <h1 class=" efch-2 ef-img-l" >Chào <MB!></MB!><br>Chào sự thay đổi diệu kỳ!</h1>
        </div>
    </div>
</section>


<section class="sec-tb sec-img-text group-ef">
  <div class="container">
    <div class="row center ">
      <div class="col-lg-6">
        <div class="single_video  tRes_60 radius-8" data-id="k4Skm7OFczs" data-video="autoplay=1&amp;controls=1&amp;mute=0"> 
          <img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="assets/images/mb3/Bitmap.png" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext ">
          <h2 class="ht ">Vững vàng - Thông minh Tự tin - Kết nối</h2>
          <div class="desc ">
          <p>MB là một định chế vững về tài chính, mạnh về quản lý, minh bạch về thông tin, thuận tiện và tiên phong trong cung cấp dịch vụ để thực hiện được sứ mệnh của mình, là một tổ chức, một đối tác Vững vàng, tin cậy.</p>
          <p>Vững vàng – Thông minh. Tự tin – Kết nối. Chúng tôi nỗ lực thực hiện sứ mệnh đó để mỗi khách hàng đến với MB đều làm chủ hành trình của mình và thành công bền vững.</p>
          <div>Hãy cùng MB khám phá và mở ra những cơ hội mới trong cuộc đời bạn, gia đình bạn...  </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="sec-tb bg-gray    ">
  <div class="container">

    <div class="entry-head ">
        <h2 class="ht efch-1 ef-img-l">Về chúng tôi</h2>
    </div>    
    <div class="list-10 row list-item" >
        <?php

        $a_ab_3 = ['Lịch sử <br> hình thành','Đội ngũ <br> lãnh đạo'];

        for($i=1;$i<=2;$i++) {?>
            <div class="col-sm-6">
                <a href="<?php if ($i == 1): ?>
						        ./08_ve_mb_lich_su.php
					        <?php else: ?>
						        ./07_lanh_dao.php
                  <?php endif ?> " 
                  class="item item-<?php echo $i; ?> tRes_55 " >
                  <div class="divtext">
                    <h3 class="title "> <?php echo $a_ab_3[$i-1]; ?></h3>
                    <i class="icon-arrow-db"></i>
                  </div>
                </a>
            </div>       
        <?php } ?>           


    </div>
  </div>
</section>





<section class="sec-tb sec-ab-4 ">
  <div class="container">

    <div class="entry-head ">
        <h2 class="ht efch-1 ef-img-l">CON SỐ NỔI BẬT</h2>
    </div>    
    <div class="  row grid-space-20 " >
      <div class="col-lg-8 col-sm-12  ">
          <div class="item item-1  " >
            <div class="img tRes_92">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/MB31.png">
            </div>
            <div class="divtext">
              <div class="t1">Top 500 </div>
              <div class="t2">Ngân hàng mạnh nhất <br>Châu Á – Thái Bình Dương năm 2018</div>
            </div>
          </div>
      </div>         
      <div class="col-lg-4 col-sm-6">
          <div class="item item-2  " >
            <div class="img tRes_92">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/MB32.png">
            </div>
            <div class="divtext">
              <div class="t1">ROE <br>19,41% </div>
              <div class="t2">Top đầu về chỉ tiêu  <br>hiệu quả</div>
            </div>
          </div>
      </div>  
      <div class="col-lg-4 col-sm-6">
          <div class="item item-3  " >
            <div class="img tRes_92">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/img-ab-9.jpg">
            </div>
            <div class="divtext">
              <div class="t1">The New<br>MB</div>
            </div>
          </div>
      </div>  
      <div class="col-lg-4 col-sm-6">
          <div class="item item-4  " >
            <div class="img tRes_92">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/MB34.png">
            </div>
            <div class="divtext">
              <div class="t1">3 triệu <span class="t3">Active user</span> </div>
              <div class="t2">Sử dụng dịch vụ trong  <br>hệ sinh thái số</div>
            </div>
          </div>
      </div>  
      <div class="col-lg-4 col-sm-6">
          <div class="item item-5  " >
            <div class="img tRes_92">
              <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/MB35.png">
            </div>
            <div class="divtext">
              <div class="t1">Top 500 </div>
              <div class="t2">Giá trị thương  <br>hiệu năm 2018</div>
            </div>
          </div>
      </div>  

    </div>
  </div>
</section>




<section class="sec-b sec-img-svg-2 group-ef lazy-hidden">
  <div class="container"  >
  
    <div class="row list-item">
      <?php 
      $svg = ['MB3icon1.svg','MB3icon2.svg','MB3icon3.svg'];
      $a_5_2 = ['Hoạt động','Tin tức','Giải thưởng'];
      $link = ['#','07_ve_mb_2.php','10_ve_mb_thanhtich.php'];

      for($i=1;$i<=3;$i++) { ?>
      <div class="col-sm-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <a class="item" href="./<?php echo $link[$i - 1] ?>">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/<?php echo $svg[$i-1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
          </div>   
        </a>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khách hàng nghĩ gì về MB</h2>
        </div>
        <div class="owl-carousel s-nav nav-2 list-11 owl-flex " data-res="3,3,2,1" paramowl="margin=0">
            <?php
            $avatar = ['MB3-avatar2.png','MB3-avatar1.png','MB3-avatar3.png','MB3-avatar1.png','MB3-avatar1.png','MB3-avatar1.png'];
            for($i=1;$i<=6;$i++) {?>
              <div   class="item efch-<?php echo $i+1; ?> ef-img-l ">
                <div class="ic"><i class="icon-quote"></i></div>
                <div class="inline-table">
                  <div class="ctext ">
                      <span class="img tRes"><img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/<?php echo $avatar[$i-1]?>"></span>
                  </div>
                  <div class="c100">
                    <h5 class="title">Ola Turner</h5>
                    <div class="cl6">Giám đốc</div>                    
                  </div>
                </div>
                <div class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. Mauris malesuada nisi sit amet augue accumsan tincidunt. </div>
              </div>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">CÔNG TY THÀNH VIÊN</h2>
        </div>
        <div class="owl-carousel s-nav nav-2 list-9 owl-flex " data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $logo = ['MB3-logo1.png','MB3-logo2.png','MB3-logo3.png','MB3-logo4.png','MB3-logo1.png','MB3-logo1.png','MB3-logo1.png','MB3-logo1.png'];
            $con = ['MB sở hữu 100% vốn điều lệ, tầm nhìn trở thành top 3 công ty AMC thuộc ngân hàng TMCP hoạt động hiệu quả nhất Việt Nam',
            'MB sở hữu 50% vốn góp, tầm nhìn trở thành top 5 công ty tài chính tiêu dùng hàng đầu thị trường về quy mô và hiệu quả hoạt động',
            'MB sở hữu hơn 90% cổ phần, nhiều năm liên tục ở vị trí top 3 công ty quản lý quỹ về hiệu quả hoạt động',
            'MB sở hữu gần 80% cổ phần, hiện là "Công ty quản lý đầu tư tốt nhất Việt Nam" và "Dịch vụ ngân hàng đầu tư tốt nhất Việt Nam " do Tạp chí World Finance…'];
            for($i=1;$i<=8;$i++) {?>
              <div   class="item efch-<?php echo $i+1; ?> ef-img-l ">
                <div class="img ">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mb3/<?php echo $logo[$i-1]?>">
                </div>
                <div class="desc"><?php echo $con[$i-1]?></div>
              </div>
            <?php } ?>
        </div>          
    </div>
</section>

<?php include 'include/index-bottom.php';?>