<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
	<div class="container">
		<div class="breadcrumbs">
			<a class="item" href="#">Home</a>
			<span class="item">Khách hàng tổ chức</span>
		</div>
	</div>
</div>

<section class="banner-img-1 next-shadow">
	<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/heading-1.jpg" src="assets/images/doanhnghiep/dnnho/banner-main.png">
</section>

<section class="sec-tb">
	<div class="container">
		<div class="max750">
			<h1 class="text-center">Nhu cầu doanh nghiệp</h1>
			<div class="menuicon  owl-carousel s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$img = ['bank.svg','money-2.svg','key.svg','lai-suat.svg','dich-vu-khac.svg','money-1.svg','bank.svg','money-2.svg','key.svg','lai-suat.svg'];
				$a_h1 = [
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh',
					'Tài trợ & Phát sinh',
					'Sản phẩm ngoại hối & thị trường vốn',
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh'
				];
				$link = ['#tab1','#tab2','#tab3','#tab4','#tab5','#tab6'];
				for ($i = 1; $i <= 10; $i++) { ?>
					<div class="item <?php if ($i == 1) echo 'active'; ?>">
						<a href="<?php echo $link[$i-1] ?> " class="link scrollspy">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="title"><?php echo $a_h1[$i - 1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<section id="tab1" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1 = [
				'Chuyển tiền quốc tế online',
				'Khấu chi dựa trên dòng tiền',
				'Cho vay mua ô tô đi lại',
				'Chuyển tiền quốc tế online',
				'Khấu chi dựa trên dòng tiền',
				'Cho vay mua ô tô đi lại'
			];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 6; $i++) { ?>
				<div class="col-md-4">
					<a href="./37_DN_vua_va_nho_detail.php" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/<?php echo $img[$i - 1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title"><?php echo $a_h1[$i - 1] ?></h4>
							<div class="desc line4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut velit corporis cumque a sint dolore eos hic quasi quos natus laudantium vitae ipsa tempora veritatis, impedit in, ipsam fuga eius.</div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</section>

<section class="sec-tb care">
	<div class="container">
		<div class="row grid-space-60 mb-30">
			<div class="col-lg-6">
				<div class="text-center">
					<img src="assets/images/care-logo-lg.png" alt="" class="img loaded loaded mb-20">
				</div>
			</div>
			<div class="col-lg-6">
				<h2 class="title">SME CARE BY MB</h2>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. 
				</div>
			</div>
		</div>
		
		<div class="row list-item">
			<?php
			$img_care = ['C.png', 'A.png', 'R.png', 'E.png'];
			// $title_care = ['Capital', 'Advertising', 'Relation', 'Education'];
			$desc_care  = ['Capital - Tài chính', 'Advertising - Truyền thông', 'Relation - Cộng đồng', 'Education - Đào tạo'];
			for ($i = 1; $i <= 4; $i++) { ?>
				<div class="col-md-3">
					<div class="care-box text-center">
						<a href="https://smecare.com.vn/">
								<div class="img_care">
									<img src="assets/images/<?php echo $img_care[$i - 1] ?>" alt="">
								</div>
								<div class="content_care">
									<p class="cl3 fs16"><?php echo $desc_care[$i - 1] ?></p>
								</div>
						</a>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<section class="sec-b sec-h-4">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">MB kết nối</h2>
			<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>

		<div class="row list-item">
			<div class="col-lg-8 ">

				<div class="list-5 row ">
					<?php
					$img = ['MBKN-1.jpg','MBKN-2.jpg'];
					$a_h1 =	[
						'MB ra mắt hệ sinh thái số dành cho khách hàng doanh nghiệp',
						'Doanh nghiệp vừa và nhỏ được quảng cáo miễn phí với gói sản phẩm "SWE ..."'
					];
					for ($i = 1; $i <= 2; $i++) { ?>
						<div class="col-md-6">
							<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
								<div class="img tRes_71">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
								</div>
								<div class="divtext">
									<div class="date">01/ 12/ 2019</div>
									<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
									<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="list-6">
					<?php
					$img = ['MBKN-3.jpg','MBKN-4.jpg','MBKN-5.jpg'];
					$a_h1 =	[
						'Cho vay mua, xây dựng, sữa chữa nhà, đất',
						'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
						'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'
					];
					for ($i = 1; $i <= 3; $i++) { ?>
						<a href="#" class="item item-inline-table">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="divtext">
								<h4 class="title line4"><?php echo $a_h1[$i - 1] ?></h4>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="sec-b sec-tigia sec-h-2">
	<div class="container">
		<div class="row list-item list-2">
			<div class="col-lg-7">
				<div class="cttab-v3 divtigia">
					<div class="tab-menu">
						<div class="tg-tab active">Tỉ giá</div>
						<div class="tg-tab">Lãi suất <span class="cl5 text-normal fs18">(%/ năm)</span></div>
					</div>
					<div class="tab-content">
						<div class="active">
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Mã NT</th>
										<th>Mua tiền mặt</th>
										<th>Mua chuyển khoản</th>
										<th>Bán ra</th>
									</tr>
									<?php
									for ($i = 1; $i <= 5; $i++) { ?>
										<tr>
											<td><img src="assets/images/flags/us.png" alt=""> USD</td>
											<td>23,180</td>
											<td>23,180</td>
											<td>23,180</td>
										</tr>
									<?php } ?>
								</table>
							</div>
						</div>
						<div>
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Kỳ hạn</th>
										<th>USD</th>
										<th>VND</th>
									</tr>
									<tr>
										<td>Không kỳ hạn</td>
										<td>0 %</td>
										<td>0.1 %</td>
									</tr>
									<tr>
										<td>1 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>2 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>3 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>5 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>6 Tháng</td>
										<td>0 %</td>
										<td>5.3 %</td>
									</tr>
									<tr>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-5 efch-7 ef-img-t">
					<div class="item item-2 tg-truycapnhanh">
						<h5 class="uppercase fs24 mb-30">Truy cập nhanh</h5>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t10"></i> Mở tài khoản DN online</a>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t8"></i> Đăng ký vay</a>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t11"></i> Tra cứu thông tin bảo lãnh</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/index-bottom.php'; ?>