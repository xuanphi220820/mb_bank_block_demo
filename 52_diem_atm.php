<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Điểm GD & ATM</span>
      </div>
    </div>
</div>

<main id="main" class="page-diem-atm">
<?php 
$api = 'AIzaSyArRfxyxqmW2NviGCwmUSethLU5Yr5Qbxw'; 
$a_latlng = [
	'15.963290, 108.280804',
	'16.071132, 108.244903',
	'11.565269, 109.023322',
	'16.09332018535926,107.80059814453125',
	'12.618897304044012,108.49273681640625',
	'10.444597722834871,106.3037109375',
	'16.772986930964848,106.962890625',
	'15.971891580928968,108.006591796875',
	'13.742053062720373,108.863525390625',
	'20.838277806058926,105.831298828125',
	'21.412162229725396,105.150146484375',
	'19.425153718960146,104.534912109375',
	'17.64402202787271,106.2158203125',
	'11.759814674441913,106.6552734375'
];
$a_cl = [
	'1',
	'2',
	'3',
	'1',
	'2',
	'3',
	'1',
	'2',
	'3',
	'1',
	'2',
	'3',
	'1',
	'2'		
];
?>

	<div class="wrap-list-map">
		<div class="row grid-space-0">
			<div class="col-md-4 ">
				<div class="ajax-content-map">
					<ul class="menu row grid-space-0">
						<li class="col-6 active"><span class="item">Chi nhánh</span></li>
						<li class="col-6 "><span  class="item" >ATM</span></li>
					</ul>

					<div   class="form-search-focus mb-20">
			            <input type="text" placeholder="Địa điểm">
			            <button><i class="icon-search-2"></i></button>
			        </div>


				    <div class="mb-20">
				    	<div>Tỉnh/ Thành phố</div>
				        <select title="Chọn TP" name="" class="selectpicker"  data-live-search="true" data-hide-disabled="true" data-actions-box="true" data-count-selected-text="Đã chọn" data-selected-text-format="count>1">
				          <?php for($i=1;$i<=7;$i++){
				            echo '<option value="'.$i.'">Thành phố '.$i.'</option>';
				          }?>
				        </select>           
				    </div>

				    <div class="mb-20">
				    	<div>Quận/Huyện</div>
				        <select title="Chọn quận/ huyện" name="" class="selectpicker"  data-live-search="true" data-hide-disabled="true" data-actions-box="true" data-count-selected-text="Đã chọn" data-selected-text-format="count>1">
				          <?php for($i=1;$i<=7;$i++){
				            echo '<option value="'.$i.'">Quận '.$i.'</option>';
				          }?>
				        </select>           
				    </div>

				    <div   class=" map-list-store "   >
				    	<?php for($i=1;$i<=14;$i++){  ?>
						<div class="item color-<?php echo $a_cl[$i-1]; ?>"  >
							<div class="location"   data-latlng="<?php echo $a_latlng[$i-1]; ?>">
								<h5 class="title">Transaction Office Republic <?php echo $i; ?></h5>
								<div class="address">298, Đường Cộng Hòa, Phường 13, Quận Tân Bình, Hồ Chí Minh</div>   
								<div class="htmlInfoWindow hidden">
									<a class="infoWindow" target="_blank" href="#">
						            <div class="divtext">
						            <h5 class="title">Transaction Office Republic <?php echo $i; ?></h5>
						            <div class="address"><i class="icon-map"></i>298, Đường Cộng Hòa, Phường 13, Quận Tân Bình, Hồ Chí Minh</div>            
						        	</div>
						        	</a>
						    </div>  
							</div>
						</div>
						<?php } ?>
					</div> 	
				</div>			
			</div>
			<div class="col-md-8">
				<div id="map" ></div>
			</div>
		</div>
	</div>


	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api; ?>"         type="text/javascript"></script>
	<script >
	(function($){



            $('.ajax-content-map ul.menu li').each(function () {
                $(this).click(function(){
                	if(!$(this).hasClass('active')){
                    	$(this).toggleClass('active').siblings().removeClass('active');
                    }
                }); 
            }); 		

	    function bindInfoWindow(marker, map, infowindow, strDescription, latlng) {
	        google.maps.event.addListener(marker, 'click', function () {
	            currentMark = this;
	            //marker.setAnimation(google.maps.Animation.BOUNCE);

	            var latlng2 = latlng.split(",");
	            var pos = {
	                lat: Number(latlng2[0]),
	                lng: Number(latlng2[1])
	            };
	            
	            //this.setIcon(icon2);  change icon on click
	            infowindow.setContent(strDescription);
	            infowindow.setPosition(pos);
	            infowindow.open(map);
	            map.setCenter(pos);   

	        });
	    }

	    /*-------------------------------------------*/
	    function initialize_load_map() {

	        var infowindow = new google.maps.InfoWindow({
	            pixelOffset: new google.maps.Size(0,-20),
	            maxWidth: 350,
	        });

	        map = new google.maps.Map(document.getElementById("map"), {});

	        //close the map if the user zoom
	        google.maps.event.addListener(map,'zoom_changed',function(){
	           //map.clearMarkers();
	           if (infowindow) {
	                infowindow.close();
	            }
	        });
	        var latlngbounds = new google.maps.LatLngBounds();
	        var LatLngList = [];
	        var markers = [];

	        $('.map-list-store .item').each(function() {
	            var thisa = $(this);
	            var latlng = $(this).find("div.location").data("latlng");
	            var img = $(this).data("icon");
	            var latlng2 = latlng.split(",");
	            var lat = Number(latlng2[0]);
	            var lng = Number(latlng2[1]);

	            var details = $(this).find('.htmlInfoWindow').html();

	            var icon = {
	                url: img, // url
	                scaledSize: new google.maps.Size(30, 30), // scaled size
	            };
	            if(img){
	                var marker = new google.maps.Marker({
	                    position: { lat: lat,lng: lng },
	                    map: map,
	                    icon: icon
	                });
	            }else{
	                var marker = new google.maps.Marker({
	                    position: { lat: lat,lng: lng },
	                    map: map,
	                    icon: 'assets/images/pin.png',
	                });                  
	            }            
	         
	            markers.push(marker);

	            LatLngList.push(new google.maps.LatLng(lat, lng));
	            bindInfoWindow(marker, map, infowindow, details,latlng);

	            $(this).find("div.location").click(function(e) {
	                e.preventDefault();
	                var latlng = $(this).data("latlng");
	                latlng = latlng.split(",");
	                var pos = {
	                    lat: Number(latlng[0]),
	                    lng: Number(latlng[1])
	                };
	                infowindow.setPosition(pos);
	                infowindow.setContent(details);
	                infowindow.open(map);
	                map.setCenter(pos);
	            });

	        });

	        LatLngList.forEach(function(latlng) {
	            latlngbounds.extend(latlng);
	        });
	        map.setCenter(latlngbounds.getCenter());
	        map.setZoom(6);
	        map.setOptions({
	            styles:'[]'
	        });
	    };

	    initialize_load_map(); 

	})(jQuery);
	  
	</script>


</main> <!-- #main -->
<script type='text/javascript' src='assets/js/bootstrap.min.js'></script> 
<script type="text/javascript" src="assets/js/bootstrap-select/bootstrap-select.min.js"></script>   
<?php include 'include/index-bottom.php';?>





