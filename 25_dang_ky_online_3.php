<?php include 'include/index-top.php'; ?>
<?php include '_module/breadcrumb.php'; ?>
<section class="sec-tb">
	<div class="container">
		<div class="text-center">
			<h1>Đăng ký dịch vụ trực tuyến</h1>
			<p class="desc max750">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore atque porro rem labore, earum, eveniet suscipit cum dolores, nesciunt perferendis ratione asperiores consequatur eligendi saepe eum! Impedit sunt dolore ad?</p>
		</div>
	</div>
</section>
<section class="online-signup">
	<div class="container">
		<div class="max950">
			<div class="flex-bw">
				<div class="step">
					<a href="#" class="b">01</a>
				</div>
				<div class="step">
					<a href="#" class="b">02</a>
				</div>
				<div class="step">
					<a href="#" class="b active">03</a>
				</div>
				<div class="step">
					<a href="#" class="b">04</a>
				</div>
				<div class="step-line"></div>
			</div>
		</div>
	</div>
</section>
<section class=" sec-tb ">
	<div class="container">
		<div class="max750">
			<form class="row list-item form-contact">
				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 fs24">Bước 3/4: Chi nhánh xác thực thông tin</h3>
						<p class="desc cl5">Vui lòng chọn chi nhánh xác thực dịch vụ</p>
					</div>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Tỉnh thành phố (*)">
					</label>
				</div>

				<div class="col-lg-7">
					<label class="block">
						<input class="input" placeholder="Chi nhánh (*)">
					</label>
				</div>
				<div class="col-lg-3">
					<label class="block">
						<input class="input" placeholder="Mã xác thực (*)">
					</label>
				</div>

				<div class="col-lg-2">
					<label class="block">
						<img src="assets/images/captchar.png" alt="">
					</label>
				</div>

				<div class="col-12 text-center">
						<a class="btn" href="<?php if (1 == 1): ?>
							./25_dang_ky_online_4.php
						<?php else: ?>
							#
                  		<?php endif ?> ">Tiếp tục</a>
				</div>
			</form>
		</div>

	</div>
</section>

<?php include 'include/index-bottom.php'; ?>