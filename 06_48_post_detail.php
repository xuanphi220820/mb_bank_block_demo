<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#"> Về MB  </a>
        <span class="item">Tin tức</span>
      </div>
    </div>
</div>
<section  class="banner-heading-3 next-shadow" >
	<div class="container">
		<div class="divtext">
			<div class="max750">
				<h1 class=" " >MB liên tiếp được vinh danh 2 giải thưởng “Ngân hàng số tiêu biểu” và “Ngân hàng cộng đồng tiêu biểu” năm 2018</h1>
			</div>
		</div>
	</div>
	<!-- <img class="img br lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-10.svg" src=""> -->
</section>

<main id="main"  class="sec-b page-news-detail" >
	<div class="container">
		<div class=" max750">
			<div class="top-heading">
				<div class="date">01/ 12/ 2019</div>
				
				<ul class="blog-item-social ">
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
				</ul>					
			</div>

			<div class="entry-content">
				<p> <b>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”. </b></p>
				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/mbpostdetail/mbpostdetail1.png" src="https://via.placeholder.com/75x30">
					<figcaption style = 'vertical-align :middle;'> Ông Lê Xuân Vũ và Ông Nguyễn Đức Huy tại Lễ trao giải</figcaption>
				</figure>
				
				<p>VOBA 2018 là giải thưởng nhằm tìm kiếm và tôn vinh các ngân hàng có những hoạt động xuất sắc và đóng góp tiêu biểu cho sự phát triển của ngành Ngân hàng nói riêng và lĩnh vực tài chính nói chung trong năm. Các giải thưởng của VOBA 2018 được xét duyệt dựa trên nhiều tiêu chí khắt khe và được đánh giá một cách khách quan, độc lập bởi các chuyên gia đại diện các cơ quan Chính phủ, bộ, ngành, hiệp hội trong lĩnh vực tài chính, ngân hàng. Quá trình xét duyệt, đánh giá được thực hiện với quy trình minh bạch, nghiêm túc, kỹ lưỡng, trải qua ba vòng: Vòng hồ sơ, vòng phỏng vấn trực tiếp và vòng bình chọn.<br>
					<br>
					Hiện nay trên thị trường, MB là đơn vị tiên phong ứng dụng công nghệ thông tin (CNTT) phục vụ hoạt động kinh doanh. Từ năm 2017, MB đẩy mạnh phát triển và ra mắt nhiều ứng dụng ngân hàng số bắt kịp xu hướng mới của thế giới như: MB Facebook Fanpage, App Ngân hàng MBBank … với nhiều tính năng và tiện ích vượt trội cho người dùng.
				</p>
				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/mbpostdetail/mbpostdetail2.png" src="https://via.placeholder.com/75x30">
					<figcaption> Nhân viên MB tư vấn sản phẩm dịch vụ tới khách hàng tịa Lễ trao giải</figcaption>
				</figure>

				<p> <br>Bên cạnh đó, MB triển khai cải tiến mô hình Ngân hàng cộng đồng, nâng cấp hạ tầng công nghệ hỗ trợ quản lý, vận hành, phát triển kinh doanh toàn hàng mà điển hình là hệ thống BPM, PD, SmartRM… Tất cả được thực hiện trên nền tảng công nghệ số, đảm bảo đơn giản, nhanh chóng, bảo mật, an toàn.<br>
					<br>Bên cạnh hoạt động kinh doanh, MB luôn quan tâm và tổ chức nhiều hoạt động thể hiện trách nhiệm cao với cộng đồng, trong các lĩnh vực phát triển giáo dục; Chăm sóc sức khỏe; Bảo vệ môi trường; Phát triển hạ tầng giao thông; Phát triển nông thôn; Bảo vệ và phát triển trẻ em; Đền ơn đáp nghĩa. Ngoài ra, MB còn xây dựng nhiều chương trình, sản phẩm ngân hàng nhằm hỗ trợ kịp thời về mặt tài chính cho số đông những người có thu nhập thấp, hoàn cảnh khó khăn, hộ sản xuất kinh doanh nhỏ lẻ.<br>
					<br>“Ngân hàng số tiêu biểu” và “Ngân hàng cộng đồng tiêu biểu” năm 2018 là những ghi nhận tự hào mà cộng đồng tài chính - ngân hàng Việt Nam dành cho MB sau hơn 24 năm phát triển, đặc biệt trong bối cảnh ngân hàng đang có những bước chuyển mình mạnh mẽ, hướng tới giai đoạn phát triển mới.
				</p>
			</div>
			<br>

			<div class="tags">
				<h2>Nội dung liên quan</h2>
				<a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
				<a class="tag" href="#">Ngân hàng đầu tư</a>
				<a class="tag" href="#">Quản lý dòng tiền</a>
			</div>					
    	</div>
	</div>    	
</main>
</section>


<section  class="sec-tb" >
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-t">Tin bài liên quan</h2>
		</div>
		<div class="list-7  list-item row" >
			<?php
			$img = ['mbpostdetail3.png','mbpostdetail4.png',];
		    for($i=1;$i<=2;$i++) {?>
		    	<div class="col-md-6">
		          <a href="#" class="item item-inline-table">
		          	<div class="img">
		          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mbpostdetail/<?php echo $img[$i-1]?>">
		          	</div>
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
		          		<div class="desc line3">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
		          	</div>
		          </a>
	        	</div>
	    	<?php } ?>
	    </div>
	</div>	
</section>


<?php include 'include/index-bottom.php';?>