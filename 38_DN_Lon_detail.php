<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
	<div class="container">
		<div class="breadcrumbs">
			<a class="item" href="#">Home</a>
			<a class="item" href="#">Doanh nghiệp</a>
			<span class="item">Doanh nghiệp lớn</span>
		</div>
	</div>
</div>

<section class="banner-img-1 next-shadow">
	<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/banner-1.jpg" src="assets/images/doanhnghiep/dnlon/banner-1.jpg">
</section>

<section class="sec-tb">
	<div class="container">
		<div class="max750">
			<h1 class="text-center">Nhu cầu của doanh nghiệp</h1>
			<div class="menuicon  owl-carousel s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$img = ['bank.svg','money-2.svg','key.svg','lai-suat.svg','dich-vu-khac.svg','money-1.svg','bank.svg','money-2.svg','key.svg','lai-suat.svg'];
				$a_h1 = [
					'Sản phẩm nổi bật',
					'Ngân hàng đầu tư',
					'Tín dụng',
					'Dịch vụ thu hộ hóa đơn',
					'Thanh toán quốc tế & Tài trợ thương mại',
					'Sản phẩm ngoại hối & phát sinh',
					'Tài trợ & Phát sinh',
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh'
				];
				$link = ['#tab1','#tab2','#tab3','#tab4','#tab5','#tab6'];
				for ($i = 1; $i <= 10; $i++) { ?>
					<div class="item">
						<a href="<?php echo $link[$i-1]?> " class="link scrollspy">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="title"><?php echo $a_h1[$i - 1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<section id="tab1" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Ngân hầng đầu tư',
				'Trái phiếu doanh nghiệp G-Bond',
				'Dịch vụ quản lý dòng tiền dành KHDN',
				'Ngân hầng đầu tư',
				'Trái phiếu doanh nghiệp G-Bond',
				'Dịch vụ quản lý dòng tiền dành KHDN'
			];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg','img-5.jpg','img-6.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab2" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Ngân hàng đầu tư</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN',
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN'
			];
			$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab7" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Giải pháp tài chính may do</h2>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1 = [
				'Giải pháp dịch vụ may do',
				'Giải pháp dịch vụ may do',
				'Cho vay mua ô tô đi lại',
				'Chuyển tiền quốc tế online',
				'Khấu chi dựa trên dòng tiền',
				'Cho vay mua ô tô đi lại'
			];
			$img = ['img-10.jpg','img-9.jpg','img-3.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 2; $i++) { ?>
				<div class="col-md-4">
					<a href="./37_DN_vua_va_nho_detail.php" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i - 1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title"><?php echo $a_h1[$i - 1] ?></h4>
							<div class="desc line4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut velit corporis cumque a sint dolore eos hic quasi quos natus laudantium vitae ipsa tempora veritatis, impedit in, ipsam fuga eius.</div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab3" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Dịch vụ thu hộ</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Dịch vụ thu tiền BHXH',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ thi hộ tiền điện',
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN'
			];
			$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab4" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Tín dụng</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Cho vay trung dài hạn',
				'Cho vay ngắn hạn',
				'Cho vay ngắn hạn theo nghành',
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN'
			];
			$img = ['img-3.jpg','img-5.jpg','img-6.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab4" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Thanh toán quốc tế và tài trợ thương mại</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Tài trợ nhập khẩu',
				'Tài trợ xuất khẩu',
				'Thanh toán quốc tế',
				'Cho vay ngắn hạn',
				'Cho vay ngắn hạn theo nghành',
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN'
			];
			$img = ['img-11.jpg','img-12.jpg','img-6.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab6" class=" sec-b sec-cauhoi ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht ">Sản phẩm ngoại hối và phái sinh</h2>
		</div>
		<div class="accodion accodion-1">
			<?php
			$ques = [
				'Giao dịch ngoại hối',
				'Sản phẩm phái sinh hàng hóa',
				'Sản phẩm phái sinh tiền tệ',
				'Dịch vụ thanh toán',
				'Dịch vụ khác'
			];
			for ($i = 1; $i <= 3; $i++) {
				?>
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if ($i == 1) echo 'checked'; ?>>
					<label class="accodion-title" for="chck_1_<?php echo $i; ?>"><span><?php echo $ques[$i-1] ?></span> <span class="triangle"><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content">
						<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
							<?php
							$a_h1 = ['Giao dịch giao ngay','Giao dịch kỳ hạn','Giao dịch hoán đổi'];
							$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-4.jpg','img-5.jpg','img-6.jpg'];
							for ($j = 1; $j <= 10; $j++) { ?>
								<a href="#" class="item efch-<?php echo $j + 1; ?> ef-img-l equal">
									<div class="img tRes_71">
										<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnnho/<?php echo $img[$j - 1] ?>">
									</div>
									<div class="divtext">
										<h4 class="title line2"><?php echo $a_h1[$j-1] ?></h4>
										<div class="desc line2 cl3">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
									</div>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php
			} ?>
		</div>
	</div>
</section>

<section id="tab" class="sec-tb sec-img-svg group-ef lazy-hidden">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht  efch-1 ef-img-t">Why MB?</h2>
		</div>
		<div class="max950">
			<div class="row  list-item grid-space-60">
				<?php
				$img = ['giai-thuong.svg', 'meeting.svg', 'thong-bao.svg'];
				$a_5_2 = ['Giải thưởng', 'Sự kiện <br> (Hợp tác toàn diện)', 'Về MB'];
				$link = ['10_ve_mb_thanhtich.php','#','05_Ve_MB_3.php'];
				for ($i = 1; $i <= 3; $i++) { ?>
					<div class="col-sm-4 efch-<?php echo $i + 1; ?> ef-img-t ">
						<a href="./<?php echo $link[$i-1] ?>">
							<div class="item">
								<div class="img ">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i-1] ?>" src="https://via.placeholder.com/6x4">
								</div>
								<div class="divtext">
									<h4 class="title"><?php echo $a_5_2[$i - 1]; ?></h4>
								</div>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>


<?php include 'include/index-bottom.php'; ?>