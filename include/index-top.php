<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title>Mbbank PC</title>
  <link rel="shortcut icon" href="images/favicon.ico">
  <link rel='stylesheet' href='assets/css/main.css' type='text/css' media='all' />
  <link rel='stylesheet' href='assets/css/bonus.css' type='text/css' media='all' />
  <script type="text/javascript" src="assets/js/jquery.js"></script>

  <!-- META FOR FACEBOOK -->
  <meta property="og:site_name" content="Propzy" />
  <meta property="og:url" itemprop="url" content="http://localhost" />
  <meta property="og:image" itemprop="thumbnailUrl" content="xxx" />
  <meta property="og:title" content="Home" itemprop="headline" />
  <meta property="og:description" content="" itemprop="description" />
  <!-- END META FOR FACEBOOK -->

</head>

<body>
  <div id="wrapper">
    <span class="menu-btn overlay"> </span>
    <div id="panel">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <!-- <div class="form-search-header">
              <button><i class="icon-search-2"></i></button>
              <input id="search" type="text" placeholder="Nhập từ khóa cần tìm kiếm .......">
            </div> -->
          </div>
          <div class="col-md-8">
            <ul class="menu line text-right">
              <li>
                <form class="form-search-hd">
                  <button type="submit" class="search-sg"><i class="icon-search-2"></i></button> 
                  <input id="search" type="text" placeholder="Tìm kiếm">
                </form>
              </li>
              <li> <a href="./05_Ve_MB_3.php">Về MB</a> </li>
              <li> <a href="./13_nha_dau_tu.php">Nhà đầu tư</a> </li>
              <li> <a href="https://tuyendung.mbbank.com.vn/TinTuc">Tuyển dụng</a> </li>
              <li> <a href="./51_lien_he.php">Liên hệ</a> </li>
              <li>
                <div class="dropdown language">
                  <div class="title"> <span><img src="assets/images/flags/vn.png" alt=""></span> <i class="icon-arrow-2 ib"></i> </div>
                  <div class="content">
                    <div class="inner">
                      <ul class="menu">
                        <li class="lang-en"><a href="#" hreflang="en" title="English (en)"><img src="assets/images/flags/gb.png" alt=""> <span>English</span></a></li>
                        <li class="lang-vi active"><a href="#" hreflang="vi" title="Tiếng Việt (vi)"><img src="assets/images/flags/vn.png" alt=""> <span>Tiếng Việt</span></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>
      <div id="search-sg">
        <div class="container">
          <a href="./01_index.php" id="logo"> <img src="assets/images/logo-blue.svg" alt=""></a>
          <div class="suggest">
            <div class="sg kw">
              <span class="b cl1">Từ khóa phổ biến</span>
              <div class="key">
                <a href="#">vay vốn</a>
                <a href="#">ngân hàng số</a>
                <a href="#">thẻ tín dụng quốc tế</a>
                <a href="#">tiền hửi thanh toán</a>
              </div>
            </div>
            <div class="sg sv">
              <span class="b cl1">Dịch vụ gợi ý</span>
              <div class="tags">
                <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
                <a class="tag" href="#">Ngân hàng đầu tư</a>
                <a class="tag" href="#">Quản lý dòng tiền</a>
              </div>
            </div>
          </div>
          <span class="icon-close close-sg"></span>
        </div>
      </div>
    </div>
    <header id="header" class="fixe" role="banner">
      <div class="container">
        <a href="./01_index.php" id="logo"> <img src="assets/images/logo.svg" alt=""></a>
        <div class="wrap-menu-header ">
          <ul class="menu-top-header " data-style="1">
            <?php include 'include/mainmenu.php'; ?>
          </ul>
          <ul  class="menu-top-header e-banking">
            <li class="highlight children">
              <a href="./49_login.php"><span>Ebanking</span></a>
              <ul>
                <li><a href="49_login.php">Cá Nhân</a></li>
                <li><a href="50_login-2.php">Doanh nghiệp</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="group-header">

          <div class="item ilang">
            <div class="dropdown language">
              <div class="title"> <span><img src="assets/images/flags/vn.png" alt=""></span> <i class="icon-arrow-2 ib"></i> </div>
              <div class="content">
                <div class="inner">
                  <ul class="menu">
                    <li class="lang-en"><a href="#" hreflang="en" title="English (en)"><img src="assets/images/flags/gb.png" alt=""> <span>English</span></a></li>
                    <li class="lang-vi active"><a href="#" hreflang="vi" title="Tiếng Việt (vi)"><img src="assets/images/flags/vn.png" alt=""> <span>Tiếng Việt</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="item imenu"><span class="menu-btn x"><span></span></span> </div>

        </div>
      </div>
    </header>
    <?php include '_block/block_search_result.php'; ?>

    <div class="wrap-menu-mb">
      <div class="wrapul main">
        <div class="inner">
          <form class="form-search-hd">
            <button type="submit" class="search-sg"><i class="icon-search-2"></i></button> 
            <input id="search" type="text" placeholder="Tìm kiếm">
          </form>
          <ul class="menu">
            <?php include 'include/mainmenu.php'; ?>
          </ul>
        </div>
      </div>
    </div>