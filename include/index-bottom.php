	<section class="sec-cta">
		<div class="container">
		  	<div class="row   center">
		  		<div class="col-4 " >
					<a class="item" href="./32_SMS_Banking.php">
						<span class="img"><img src="assets/images/svg/t1.svg" alt=""></span>
						<div class="divtext">
							<h4 class="title">Gọi ngay</h4>
							<div class="desc"> 1900 545426 - (84-24) 3767 4050 <br> (Từ nước ngoài)</div>
						</div>
					</a>
		  		</div>
		  		<div class="col-4 " >
					<a class="item" href="#">
						<span class="img"><img src="assets/images/svg/t2.svg" alt=""></span>
						<div class="divtext">
							<h4 class="title">Gửi email</h4>
							<div class="desc">mb247@mbbank.com.vn</div>
						</div>
					</a>
		  		</div>
		  		<div class="col-4 " >
					<a class="item" href="./52_diem_atm.php">
						<span class="img"><img src="assets/images/svg/t3.svg" alt=""></span>
						<div class="divtext">
							<h4 class="title">Tìm ATm & Điểm GD</h4>
							<div class="desc">Tìm điểm giao dịch & ATM gần bạn</div>
						</div>
					</a>
		  		</div>		  		
		  	</div>
	  	</div>
	</section>
	<section class="sec-download-pc group-ef lazy-hidden">
		<div class="container">
		  	<div class="row">
		  		<div class="col-md-6   efch-2 ef-img-r" >
					<p class="stitle">Đăng ký nhận thông tin khuyến mãi</p>
					<form role="search" method="get" class="searchform " action="">
						<div><input type="text" placeholder="Nhập email để nhận thông tin" value="" name="s" class="input"></div>
					    <button type="submit" class="btn btn-2">Đăng ký</button>
					</form>
		  		</div>
		  		<div class="col-md-6   efch-3 ef-img-r" >

			  		<div class="wapp">
			  			<span class="code"><img src="assets/images/code.png" alt=""></span>
			  			<div class="app">
							<p class="stitle">Hãy tải app ngay hôm nay</p>
							<a href="#"><img src="assets/images/btt-google.svg" alt=""></a> &nbsp;
							<a href="#"><img src="assets/images/btt-chplay.svg" alt=""></a>		
						</div>
						
			  		</div>		  			

		  		</div>



		  	</div>
	  	</div>
	</section>
	<div id="footer-pc" class=" group-ef lazy-hidden">
		<div class="container">
			<div class="row grid-space-10">
				<div class="col-lg-4 col-sm-12 efch-1 ef-img-t">
					<div class="widget widget-info">
						<div><a href="./" class="logo"> <img src="assets/images/logo-blue.svg" alt=""></a></div>
						<h3>Ngân hàng TMCP Quân Đội</h3>
						<p>Toà nhà MBBank - Hội sở 21 Cát Linh, <br> Đống Đa, Hà Nội</p>
						<p>Email: mb247@mbbank.com.vn</p>
						<p class="w6">  Swift code: MSCBVNVX</p>
							<ul class="blog-item-social ">
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
								<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
							</ul>							
					</div>					
				</div>

				<div class="col-lg-8">
					<div class="row">
						<div class="col-md-3 col-6    efch-2 ef-img-t">
							<div class="widget">
								<h4 class="widget-title">Khách hàng cá nhân</h4>
								<ul class="menu">
									<li><a href="#">Ngân hàng số</a></li>
									<li><a href="#">Tài khoản</a></li>
									<li><a href="#">Cho vay</a></li>
									<li><a href="#">Tiết kiệm</a></li>
									<li><a href="#">Thẻ</a></li>
									<li><a href="#">Chuyển tiền</a></li>
									<li><a href="#">Bảo hiểm</a></li>
									<li><a href="https://mbstar.mbbank.com.vn/#/home">MBStar</a></li>
								</ul>
							</div>
						</div>		
						<div class="col-md-3 col-6    efch-2 ef-img-t">
							<div class="widget">
								<h4 class="widget-title">Khách hàng tổ chức</h4>
								<ul class="menu">
									<li><a href="#">Doanh nghiệp vừa & nhỏ</a></li>
									<li><a href="#">Doanh nghiệp lớn</a></li>
									<li><a href="#">Định chế tài chính</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3 col-6    efch-3 ef-img-t">
							<div class="widget">
								<h4 class="widget-title">Khách hàng ưu tiên</h4>
								<ul class="menu">
									<li><a href="#">Private</a></li>
									<li><a href="#">Priority</a></li>
								</ul>
							</div>						
						</div>
						<div class="col-md-3 col-6    efch-3 ef-img-t">
							<div class="widget">
								<h4 class="widget-title">Về MB</h4>
								<ul class="menu">
									<li><a href="#">Về chúng tôi</a></li>
									<li><a href="#">Nhà đầu tư</a></li>
									<li><a href="#">Nghề nghiệp</a></li>
									<li><a href="#">Liên hệ</a></li>
								</ul>
							</div>						
						</div>												
					</div>
				</div>	



			</div>
			<div class="line"></div>
			<div class="row grid-space-10">
				<div class="col-lg-6 col-md-7 efch-5 ef-img-t">
					<ul class="menu line">
				      <li>  <a href="#">Điều khoản sử dụng</a>  </li>
				      <li>  <a href="#">Bảo mật thông tin khách hàng</a>  </li>
				    </ul>
				</div>
				<div class="col-lg-6 col-md-5 efch-6 ef-img-t">
					<div class="copyright">2019 © Copyright MBbank. All rights reserved.</div>
				</div>				
			</div>
		</div>

		<a id="back-top" class="back-top-1" href="#" ><i class="icon-arrow-db it"></i> <span>TOP</span></a>
	</div> <!--End #footer PC-->







	<section class="sec-download-mb">

		<div class="wform">
			<p class="stitle">Đăng ký nhận thông tin khuyến mãi</p>
		
		<form role="search" method="get" class="searchform " action="">
			<div class="aaa"><input type="text" placeholder="Nhập email để nhận thông tin" value="" name="s" class="input"></div>
			
			
		    <button type="submit" class="btn btn-2">Đăng ký</button>
		</form>
		</div>

		<div class="wdownload">
			<span class="stitle">Tải app ngay</span>&nbsp;
					<a href="#"><img src="assets/images/btt-google-mb.svg" alt=""></a> &nbsp;
					<a href="#"><img src="assets/images/btt-chplay-mb.svg" alt=""></a>	
		</div>
	</section>
	<div id="footer-mb" class=" group-ef lazy-hidden">
		<div class="container">

			<div class="widget widget-info">
				<div><a href="./" class="logo"> <img src="assets/images/logo-blue.svg" alt=""></a></div>
				<h3>Ngân hàng TMCP Quân Đội</h3>
				<p>Toà nhà MBBank - Hội sở 21 Cát Linh, Đống Đa, Hà Nội</p>
				<p>Email: mb247@mbbank.com.vn</p>
				<p>Swift code: MSCBVNVX</p>
				<p class="w6">Hãy gọi cho chúng tôi để được tư vấn 24/7</p>
				<div class="call">
					<a class="phone" href="#"><i class="icon-phone-1"></i> 1900 545426</a>
					
					<ul class="blog-item-social ">
						<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
						<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
						<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
						<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
					</ul>						
				</div>
			</div>					


		    <div class="accodion accodion-0">
	            <div class="accodion-tab ">
	                <input type="checkbox" id="chck_mf"  >
	                <label class="accodion-title" for="chck_mf" ><span> Mở rộng </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
	                <div class="accodion-content" >
	                    <div class="inner">

						<div class="row grid-space-10">
							<div class="col-md-3 col-6  efch-2 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Cá nhân</h4>
									<ul class="menu">
										<li><a href="#">Ngân hàng số</a></li>
										<li><a href="#">Tiết kiệm</a></li>
										<li><a href="#">Cho vay</a></li>
										<li><a href="#">Thẻ</a></li>
										<li><a href="#">Chuyển tiền thanh toán</a></li>
										<li><a href="#">Bảo hiểm nhân thọ</a></li>
										<li><a href="#">Sản phẩm khác</a></li>
									</ul>
								</div>
							</div>		
							<div class="col-md-3 col-6  efch-2 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Doanh nghiệp</h4>
									<ul class="menu">
										<li><a href="#">Ngân hàng số</a></li>
										<li><a href="#">Tài khoản và dịch vụ</a></li>
										<li><a href="#">Tiền gửi</a></li>
										<li><a href="#">Tín dụng và bảo lãnh</a></li>
										<li><a href="#">Tài trợ thương mại</a></li>
										<li><a href="#">Thị trường tiền tệ và vốn</a></li>
										<li><a href="#">Dịch vụ khác</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-6  efch-3 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Khách hàng cao cấp</h4>
									<ul class="menu">
										<li><a href="#">Private</a></li>
										<li><a href="#">Priorities</a></li>
									</ul>
								</div>
								<div class="widget">
									<h4 class="widget-title">Liên hệ</h4>
									<ul class="menu">
										<li><a href="#">Chat trực tuyến</a></li>
										<li><a href="#">Tìm điểm giao dịch</a></li>
										<li><a href="#">Tổng đài 24/7</a></li>
									</ul>
								</div>							
							</div>
							<div class="col-md-3 col-6  efch-4 ef-img-t">
								<div class="widget">
									<h4 class="widget-title">Về MBBank</h4>
									<ul class="menu">
										<li><a href="#">Về chúng tôi</a></li>
										<li><a href="#">Nhà đầu tư</a></li>
										<li><a href="#">Nghề nghiệp</a></li>
										<li><a href="#">Liên hệ</a></li>
									</ul>
								</div>
							</div>
						</div>
	                    </div>
	                </div>
	            </div>

		    </div>









		</div>

		<div class="menu-footer-mb">
			<div class="row">
				<div class="col-3">
					<a class="item " href="#">
						<span class="img">	<img src="assets/images/svg/home.svg" alt="">	</span>
						<span class="name">Trang chủ</span>
					</a>
				</div>	
				<div class="col-3" >
					<a class="item" href="#">
						<span class="img"><img src="assets/images/svg/folder.svg" alt=""></span>
						<span class="name">Sản phẩm</span>
					</a>
				</div>	
				<div class="col-3">
					<a class="item " href="#">
						<span class="img"><img src="assets/images/svg/MB.svg" alt=""></span>
						<span class="name">MB++</span>
					</a>
				</div>	
				<div class="col-3">
					<a class="item " href="#">
						<span class="img"><img src="assets/images/svg/giadinh.svg" alt=""></span>
						<span class="name">Gia đình</span>
					</a>
				</div>	
				<div class="col-3">
					<a class="item " href="#">
						<span class="img"><img src="assets/images/svg/tienich.svg" alt=""></span>
						<span class="name">Tiện ích</span>
					</a>
				</div>	
			</div>
		</div>		
	</div> <!--End #footer MB-->





</div> <!--End #wrapper-->





<script type='text/javascript' src='assets/js/owl.carousel.min.js'></script> 
<script type='text/javascript' src='assets/js/script.js'></script>
<script type='text/javascript' src='assets/js/bonus.js'></script>

</body></html>
