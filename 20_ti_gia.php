<?php include 'include/index-top.php'; ?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Tỷ giá ngoại tệ</span>
      </div>
    </div>
</div>
<section class="sec banner-heading-1 next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Tỷ giá ngoại tệ</h1>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-14_6.jpg">
</section>


<section class="sec-tb">
  <div class="container">
    <div class="search tigia mb-30 max950">
      <h3 class="ctext mg-0">Tra cứu tỉ giá theo ngày</h3>
      <input type="date" class="input cl5">
      <button class="btn lg">Tra cứu</button>
    </div>
    <div class="table-responsive">
      <table class="table table-full table-ti-gia">
        <tr>
          <th>Kí hiệu ngoại tệ</th>
          <th>Tên ngoại tệ</th>
          <th>Mua (tiền mặt)</th>
          <th>Mua chuyển khoản</th>
          <th>Bán</th>
        </tr>
        <?php
        for ($i = 1; $i <= 10 ; $i++) {
        ?>
          <tr>
            <td>0<?php echo $i; ?>AUD</td>
            <td><span class="b uppercase">Australian Dollar</span></td>
            <td>15,053.74</td>
            <td>15,053.74</td>
            <td>15,526.35</td>
          </tr>
        <?php
        } ?>
      </table>
    </div>
  </div>
</section>

<?php include '_module/pagination.php' ?>

<?php include 'include/index-bottom.php'; ?>