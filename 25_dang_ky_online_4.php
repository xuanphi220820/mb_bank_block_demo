<?php include 'include/index-top.php'; ?>
<?php include '_module/breadcrumb.php'; ?>
<section class="sec-tb">
	<div class="container">
		<div class="text-center">
			<h1>Đăng ký dịch vụ trực tuyến</h1>
			<p class="desc max750">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore atque porro rem labore, earum, eveniet suscipit cum dolores, nesciunt perferendis ratione asperiores consequatur eligendi saepe eum! Impedit sunt dolore ad?</p>
		</div>
	</div>
</section>
<section class="online-signup">
	<div class="container">
		<div class="max950">
			<div class="flex-bw">
				<div class="step">
					<a href="#" class="b">01</a>
				</div>
				<div class="step">
					<a href="#" class="b">02</a>
				</div>
				<div class="step">
					<a href="#" class="b">03</a>
				</div>
				<div class="step">
					<a href="#" class="b active">04</a>
				</div>
				<div class="step-line"></div>
			</div>
		</div>
	</div>
</section>
<section class=" sec-tb ">
	<div class="container">
		<div class="max750">
			<form class="row list-item form-contact">
				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 fs24">Bước 4/4: Hoàn tất đăng ký trực tuyến</h3>
					</div>
				</div>

				<div class="col-12">
					<div class="text-center">
						<img src="assets/images/ico-dangky.svg" alt="">
					</div>
				</div>

				<div class="col-12">
					<div class="text-center">
						<p class="desc mg-0">Chúc mừng bạn đã đăng ký thành công</p>
						<p class="desc mg-0">Vui lòng đến chi nhánh đã đăng ký để xác thực thông tin tài khoản</p>
					</div>
				</div>

				<div class="col-12 ">
					<div class="text-center">
						<a class="btn" href="01_index.php">Về trang chủ</a>
					</div>
				</div>
			</form>
		</div>

	</div>
</section>

<?php include 'include/index-bottom.php'; ?>