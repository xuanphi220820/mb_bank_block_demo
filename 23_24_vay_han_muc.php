<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#"> Công cụ tính toán  </a>
        <span class="item">Vay hạn mức</span>
      </div>
    </div>
</div>

<main id="main"  class="sec-tb page-vay-von" >
  <div class="container"  >
    <h2 class="ht">Công cụ tính</h2>

    <div  class="cttab-xx  sec-b">
      <div class="w-menu-over">
        <div  class="tab-menu style2">
          <div  class="active"><span>Vay hạn mức</span></div>
          <div ><span>Vay đầu tư hạn mức cố định</span></div>
        </div>
      </div>
      <div class="tab-content">
        <div class="active">
          <div class="tab-inner  ">
            <div class="form-vay-von">
              <div class="row ">
                <div class="col-md-7 ">
                  <div class="inner">
                    <div class="row">
                        <div class="col-md-7">
                        <h5 class="title">Tổng nhu cầu vốn (số tiền vay): <br> <span class="note">(VNĐ)</span></h5>
                      </div>             
                      <div class="col-md-5">
                        <div><input class=" input"  name=""  value="" placeholder="Nhập số tiền (*)"></div>
                        <div class="price">1,000,000,000</div>
                      </div>             
                    </div>  
                    <div class="row">
                        <div class="col-md-7">
                        <h5 class="title">Vốn tự có:<br> <span class="note">(VNĐ)</span></h5>
                      </div>             
                      <div class="col-md-5">
                        <div><input class=" input"  name=""  value="" placeholder="Nhập số tiền (*)"></div>
                        <div class="price">1,000,000,000</div>
                      </div>             
                    </div>  

                    <div class="row">
                        <div class="col-md-7">
                        <h5 class="title">Nhu cầu vay vốn từ MB:<br> <span class="note">(VNĐ)</span></h5>
                      </div>             
                      <div class="col-md-5">
                        <div><input class=" input"  name=""  value="" placeholder="Nhập số tiền (*)"></div>
                        <div class="price">1,000,000,000</div>
                      </div>             
                    </div>  

                    <div class="row">
                        <div class="col-md-7">
                        <h5 class="title">Kỳ hạn vay:</h5>
                      </div>             
                      <div class="col-md-5">
                        <select class="select">
                          <option>12 tháng</option>
                          <option>10 tháng</option>
                        </select>
                      </div>             
                    </div>   
                  </div>                   
                </div>
                <div class="col-md-5">
                  <div class="result">
                    <div class="row">
                        <div class="col-md-6">
                        <h5 class="title">Khoản vay:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t2">1,800,000</span> 
                        <span class="t3">VNĐ</span>
                      </div>             
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                        <h5 class="title">Vốn tự có:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t4">5,000,000</span> 
                        <span class="t3">VNĐ</span>
                      </div>             
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                        <h5 class="title">Kỳ hạn vay:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t5">12 tháng</span> 
                      </div>             
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                        <h5 class="title">Tiền lãi hàng tháng:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t4">1,800,000</span> 
                        <span class="t3">VNĐ</span>
                      </div>             
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                        <h5 class="title">Tiền gốc hàng tháng:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t4">1,800,000</span> 
                        <span class="t3">VNĐ</span>
                      </div>             
                    </div> 

                    <div class="total row">
                        <div class="col-md-6">
                        <h5 class="title">Tổng tiền phải trả:</h5>
                      </div>             
                      <div class="col-md-6">
                        <span class="t2">18,700,000</span> 
                        <span class="t3">VNĐ</span>
                      </div>             
                    </div> 

                  </div>
                  
                </div>
              </div>

              <p class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</p>
              <a class="btn">Xem bảng</a>
            </div>

          </div>
        </div>
        <div >
          <div class="tab-inner">
            2
          </div>
        </div>


      </div>
    </div> <!-- end tab-->

    <div class="sec-b">
      <div class="accodion accodion-3">
        <div class="accodion-tab ">
            <input type="checkbox" id="chck_1_1" >
            <label class="accodion-title uppercase" for="chck_1_1" ><span> Xem bảng tính trả tiền hàng tháng</span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
            <div class="accodion-content entry-content" >
                <div class="inner">
                  <div class="table-responsive">
                    <table class="table table-full">
                      <tr>
                        <th width="50">STT</th>
                        <th>Kỳ trả nợ</th>
                        <th>Lãi trả/ tháng</th>
                        <th>Tối thiểu</th>
                        <th>Tối đa</th>
                      </tr>
                      <?php for($i=1;$i<=6;$i++) { ?>
                      <tr>
                        <td>01</td>
                        <td>13/04/2020</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                      </tr>
                      <?php } ?>
                    </table>
                  </div>
                </div>
            </div>
        </div>

      </div>
    </div>

    <div class="sec-b sec-img-svg-4">
      <h2 class="ht">Công cụ tính khác</h2>
          <div class="row list-item">
            <?php 
            $a_5_2 = ['Vay mua nhà đất','Vay mua ô tô','Vay mua nhà dự án','Vay mua nhà','Vay mua nhà','Vay mua nhà'];
            $link = [];

            for($i=1;$i<=6;$i++) { ?>
            <div class="col-sm-4 efch-<?php echo $i+1; ?> ef-img-t ">
              <a class="item" href="<?php echo $link[$i-1] ?>">
                <div class="img ">
                  <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/tinh-toan.svg" src="https://via.placeholder.com/6x4">
                </div>
                <div class="divtext">
                  <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
                </div>   
              </a>     
            </div>
            <?php } ?>
          </div>
    </div>




  </div>

  <script>
  (function($){
  $(document).ready(function(){



  });
  })(jQuery);
  </script>

</main>
<?php include 'include/index-bottom.php';?>