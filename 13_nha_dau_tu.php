<?php include 'include/index-top.php'; ?>

<section class="  sec-dt-1 lazy-hidden " data-lazy-type="bg" data-lazy-src="assets/images/bg-1.jpg">
  <div class="container">

    <div class="row center">
      <div class="col-md-6">
        <div class="img"><img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/chutich.png"></div>
      </div>
      <div class="col-md-6">
        <blockquote>MB quyết tâm hoàn thành tốt nhiệm vụ và kế hoạch năm 2019 với tinh thần Chiến quyết liệt, tạo cách biệt, tiếp tục theo đuổi tầm nhìn chiến lược trở thành ngân hàng thuận tiện nhất</blockquote>
        <div class="text-center">
          <h2 class="mg-0 chutich">Ông Lê Hữu Đức</h2>
          <p class="desc cl6 fs18">Chủ tịch HĐQT</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec-tb sec-h-1 lazy-hidden group-ef">
  <div class="container">
    <!-- <div class="entry-head text-center">
      <h2 class="ht efch-1 ef-img-t">Nhà đầu tư</h2>
    </div> -->
    <div class="menuicon">
      <?php
      $a_h1_2 = ['Thông báo', 'Báo cáo tài chính', 'Đại hội cổ đông', 'Báo cáo thường niên', ' Tài liệu nhà đầu tư', 'Tài liệu khác', 'Điều lệ NHTM'];
      $link = ['#', '14_nha_dau_tu_1.php', '15_nha_dau_tu_2.php', '16_nha_dau_tu_3.php', '17_nha_dau_tu_4.php', '18_19_nha_dau_tu_5.php', '#'];
      $img = ['thong-bao.svg', 'bieu-do.svg', 'meeting.svg', 'Tai-lieu-3.svg', 'tai-lieu-ndt.svg', 'mess.svg', 'policy.svg'];
      for ($i = 1; $i <= 7; $i++) { ?>
        <div class="item   efch-<?php echo $i + 2; ?> ef-img-t <?php if ($i == 1) echo 'active'; ?>">
          <a href="./<?php echo $link[$i - 1] ?>" class="link">
            <div class="img">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i - 1] ?>">
            </div>
            <div class="title"><?php echo $a_h1_2[$i - 1]; ?></div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>
<section class="sec-b">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-t">Cập nhật giao dịch MB</h2>
    </div>
    <div class="list-5 row list-item">
      <?php
      $img = ['img-1.jpg','img-2.jpg'];
      for ($i = 1; $i <= 2; $i++) { ?>
        <div class="col-md-4">
          <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l ">
            <div class="img tRes_71">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/nhadautu/<?php echo $img[$i - 1] ?>">
            </div>
            <div class="divtext">
              <div class="date">01/ 12/ 2019</div>
              <h4 class="title line2"><?php echo $i; ?> Báo cáo kết quả giao dịch cổ phiếu của người nội bộ</h4>
              <div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit... </div>
            </div>
          </a>
        </div>
      <?php } ?>
      <div class="col-md-4">
        <div class="widget-ndt pd-15">
          <div><a href="./"> <img height="50" src="assets/images/logo-blue.svg" alt=""></a></div>

          <div class="numbs">

            <span class="t1"><i class="icon-t13"></i> 20,900</span>
            <span class="t2">+100 <br>+0.2%</span>
          </div>

          <p class="desc">Ngày cập nhật 27/02/2020</p>

          <ul class="list">
            <li><span class="t1">Sàn</span> <span class="t2">HOSE <span class="t3">(MBB)</span> </span></li>
            <li><span class="t1">KLGD</span> <span class="t2">722.680 <span class="t3">(CP)</span> </span></li>
            <li><span class="t1">GTGD</span> <span class="t2">33.862,00 <span class="t3">(Triệu VND)</span> </span></li>
            <li><span class="t1">Vốn hoá</span> <span class="t2">189.437,05 <span class="t3">(Tỷ VND)</span> </span></li>
            <li><span class="t1">KLCP</span> <span class="t2">4.022.018.040 <span class="t3">(CP)</span> </span></li>

            <li><a href="https://thongtinthitruong.mbbank.com.vn/tong-quan.html" class="fs13 cl1">Thông tin Giao dịch, Tài chính, Tin tức & Sự kiện <i class="icon-arrow-db"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
</section>

<section class="sec-tb  bg-gray ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-t">Thông tin thị trường</h2>
    </div>
    <div><img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/bieu-do.png"></div>


  </div>
</section>

<section class="sec-b sec-h-4__">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">Thông báo từ MB</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>
    <div class="list-5 list-5-1 row list-item">
      <?php
      $con = ['MB triển khai giao dịch bán cổ phiếu quỹ',
      'Báo cáo kết quả giao dịch cổ phiếu của người nội bộ',
      'Quyết định thay đổi niêm yết'];
      for ($i = 1; $i <= 3; $i++) { ?>
        <div class="col-md-4">
          <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
            <div class="divtext">
              <div class="date">01/ 12/ 2019</div>
              <h4 class="title line2"><?php echo $con[$i-1]?></h4>
              <div class="desc r_f_1_3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem </div>
              <span class="more cl1">Tìm hiểu thêm</span>
            </div>

          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section class="sec-b sec-h-4">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">MB với báo chí</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>

    <div class="row list-item">
      <div class="col-lg-8 ">

        <div class="list-5 row ">
          <?php
          $img = ['01','02'];
          $con = ['Quán quân công bố thông tin năm 2017 đang thuộc về nhóm cổ …',
            'MB liên tiếp được vinh danh 2 giải thưởng “Ngân hàng số tiêu …'];
          for ($i = 1; $i <= 2; $i++) { ?>
            <div class="col-md-6">
              <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
                <div class="img tRes_71">
                  <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/nhadautu/mbbaochi/<?php echo $img[$i - 1] ?>.jpg">
                </div>
                <div class="divtext">
                  <div class="date">01/ 12/ 2019</div>
                  <h4 class="title line2"><?php echo $con[$i-1]?></h4>
                  <div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem </div>
                </div>
              </a>
            </div>
          <?php } ?>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="list-6">
          <?php
          $img = ['03','04','05'];
          $con = ['Cho vay mua, xây dựng, sửa chữa nhà, đất',
            'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
            'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'];
          for ($i = 1; $i <= 3; $i++) { ?>
            <a href="#" class="item item-inline-table">
              <div class="img">
                <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/nhadautu/mbbaochi/<?php echo $img[$i - 1] ?>.jpg">
              </div>
              <div class="divtext">
                <h4 class="title line4"><?php echo $con[$i-1]?></h4>
              </div>
            </a>

          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec-tb ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">Họp cổ đông</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>
    <div class=" boxwidget box-download-2">
      <h2 class="widget-title">2019</h2>
      <div class="row grid-space-60">
        <div class="col-lg-6">
          <div class="single_video  tRes_16_9" data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/nhadautu/video-thumb.jpg" src="assets/images/nhadautu/video-thumb.jpg" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="list-download ">
            <?php
            $con = ['Nghị quyết sửa đổi, bổ sung Quy chế tổ chức hoạt động của BKS',
            'Nghị quyết sửa đổi, bổ sung Quy chế tổ chức hoạt động của HĐQT, Quy chế…',
            'Nghị quyết về việc bầu Chủ tịch HĐQT và các Phó Chủ tịch HĐQT nhiệm kỳ 2019 -…',
            'trình phương án sử dụng Vốn chủ sở hữu 2019',
            'trình phương án sử dụng Vốn chủ sở hữu 2019'];
            for ($i = 1; $i <= 5; $i++) { ?>
              <li>
                <span class="title"><i class="icon-t14"></i> <?php echo $con[$i-1]?></span>
                <span class="down"><a href="#"><i class="icon-arrow-6 ib"></i> </a></span>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>



<section class="sec-b sec-dt-2">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">Báo cáo tài chính</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class=" boxwidget">
          <h3 class="widget-title">Năm 2018</h3>
          <ul class="list-download ">
            <?php
            $con = ['Báo cáo tài chính hợp nhất Quý 3 năm 2018',
            'Báo cáo tài chính riêng lẻ Quý 3 năm 2018',
            'Báo cáo tài chính hợp nhất Quý 2 năm 2018',
            'Báo cáo tài chính riêng lẻ Quý 2 năm 2018',
            'Báo cáo tài chính hợp nhất Quý 1 năm 2018',
            'Báo cáo tài chính hợp nhất Quý 3 năm 2018',
            'Báo cáo tài chính riêng lẻ Quý 3 năm 2018',
            'Báo cáo tài chính hợp nhất Quý 2 năm 2018',
            'Báo cáo tài chính riêng lẻ Quý 2 năm 2018',
            'Báo cáo tài chính hợp nhất Quý 1 năm 2018'];
            for ($i = 1; $i <= 10; $i++) { ?>
              <li>
                <span class="title"><i class="icon-t14"></i> <?php echo $con[$i-1]?></span>
                <span class="down"><a href="#"><i class="icon-arrow-6 ib"></i> </a></span>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <div class=" boxwidget">
          <h3 class="widget-title">Năm 2019</h3>
          <ul class="list-download ">
            <?php 
            $con = ['Báo cáo tài chính hợp nhất Quý 3 năm 2019',
            'Báo cáo tài chính riêng lẻ Quý 3 năm 2019',
            'Báo cáo tài chính hợp nhất Quý 2 năm 2019',
            'Báo cáo tài chính riêng lẻ Quý 2 năm 2019',
            'Báo cáo tài chính hợp nhất Quý 1 năm 2019',
            'Báo cáo tài chính hợp nhất Quý 3 năm 2019',
            'Báo cáo tài chính riêng lẻ Quý 3 năm 2019',
            'Báo cáo tài chính hợp nhất Quý 2 năm 2019',
            'Báo cáo tài chính riêng lẻ Quý 2 năm 2019',
            'Báo cáo tài chính hợp nhất Quý 1 năm 2019'];
            for ($i = 1; $i < 10; $i++) { ?>
              <li>
                <span class="title"><i class="icon-t14"></i><?php echo $con[$i-1]?></span>
                <span class="down"><a href="#"><i class="icon-arrow-6 ib"></i> </a></span>
              </li>
            <?php } ?>
          </ul>

        </div>
      </div>
    </div>

  </div>
</section>

<section class="sec-b ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">CÔNG TY THÀNH VIÊN</h2>
    </div>
    <div class="owl-carousel s-nav nav-2 list-9 owl-flex " data-res="4,3,2,1" paramowl="margin=0">
      <?php
      $logo = ['MB3-logo1.png','MB3-logo2.png','MB3-logo3.png','MB3-logo4.png','MB3-logo1.png','MB3-logo1.png','MB3-logo1.png','MB3-logo1.png'];
      $con = ['MB sở hữu 100% vốn điều lệ, tầm nhìn trở thành top 3 công ty AMC thuộc ngân hàng TMCP hoạt động hiệu quả nhất Việt Nam',
            'MB sở hữu 50% vốn góp, tầm nhìn trở thành top 5 công ty tài chính tiêu dùng hàng đầu thị trường về quy mô và hiệu quả hoạt động',
            'MB sở hữu hơn 90% cổ phần, nhiều năm liên tục ở vị trí top 3 công ty quản lý quỹ về hiệu quả hoạt động',
            'MB sở hữu gần 80% cổ phần, hiện là "Công ty quản lý đầu tư tốt nhất Việt Nam" và "Dịch vụ ngân hàng đầu tư tốt nhất Việt Nam " do Tạp chí World Finance…'];
      for ($i = 1; $i <= 10; $i++) { ?>
        <div class="item efch-<?php echo $i + 1; ?> ef-img-l ">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/mb3/<?php echo $logo[$i-1]?>">
          </div>
          <div class="desc"><?php echo $con[$i-1]?></div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<?php include 'include/index-bottom.php'; ?>