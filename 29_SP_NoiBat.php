<?php include 'include/index-top.php'; ?>
<section class="banner-img-1 next-shadow">
	<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/banner.jpg" src="">
</section>

<main id="main" class="sec-tb ">
	<div class="container">
		<h1 class="text-center">Nhu cầu của bạn là gì</h1>
		<div class="max750">
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$a_h1_2 = ['Ngân hàng số','Tài khoản','Cho vay','Tiết kiệm','Thẻ','Chuyển tiền','Chuyển tiền'];
				$link = ['30_sp_ngan_hang.php','31_CN-TaiKhoan.php','#','#','35_MB_Visa.php','#','#'];
				$img = ['bank.svg','user.svg','money-2.svg','save-money.svg','the-1.svg','toan-cau.svg','toan-cau.svg'];
				for ($i = 1; $i <= 7; $i++) { ?>
					<div class="item <?php if ($i == 1) echo 'active'; ?>">
						<a href="./<?php echo $link[$i - 1] ?>" class="link">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>">
							</div>
							<div class="title"><?php echo $a_h1_2[$i-1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1_2 = 	[
							'App MBBank',
							'Gia đình tôi yêu',
							'Vay siêu nhanh trên App MBBank',
							'Vay nhà đất, nhà dự án',
							'Thẻ tín dụng Quốc tế MB Visa',
							'Thẻ tín dụng Quốc tế MB JCB',
							'Thẻ tín dụng MB Visa Infinite',
							'Cho vay du học',
							'Cho vay mua ô tô',
							'Tiết kiệm dân cư',
							'Chuyển tiền du học',
							'Chuyển tiền định cư',
							'Bảo hiểm nhân thọ MBAL',
							'Bảo hiểm phi nhân thọ MIC'
						];
			$img = ['spnb-1.jpg','spnb-2.jpg','spnb-3.jpg','spnb-4.jpg','spnb-5.jpg','spnb-6.jpg','spnb-7.jpg','spnb-8.jpg','spnb-9.jpg','spnb-7.jpg','spnb-8.jpg','spnb-9.jpg','spnb-7.jpg','spnb-8.jpg',];
			$link = [
						'43_mb_bank_app.php',
						'42_CN_SPNB.php',
						'46_CN_SPNB.php',
						'35_MB_Visa.php',
						'',
						'',
						'',
						'',
						''
					];
			for ($i = 1; $i <= 14; $i++) { ?>
				<div class="col-md-4">
					<a href="<?php echo $link[$i-1] ?> " class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/spnb/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title"><?php echo $a_h1_2[$i-1]; ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<?php //include '_module/pagination.php';
		?>
	</div>
</main>

<?php include 'include/index-bottom.php'; ?>