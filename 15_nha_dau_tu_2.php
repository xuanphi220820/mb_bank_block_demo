<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#"> Nhà đầu tư  </a>
        <span class="item">Tài liệu đại hội cổ đông</span>
      </div>
    </div>
</div>
<section   class="sec banner-heading-1 next-shadow"  >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >Tài liệu đại hội cổ đông</h1>
        </div>
    </div>
    <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-14_2.jpg">
</section>
<section class="sec-tb sec-h-1 lazy-hidden group-ef">
  <div class="container">
    <!-- <div class="entry-head text-center">
      <h2 class="ht efch-1 ef-img-t">Nhà đầu tư</h2>
    </div> -->
    <div class="menuicon">
      <?php
      $a_h1_2 = ['Thông báo', 'Báo cáo tài chính', 'Đại hội cổ đông', 'Báo cáo thường niên', ' Tài liệu nhà đầu tư', 'Tài liệu khác', 'Điều lệ NHTM'];
      $link = ['#', '14_nha_dau_tu_1.php', '15_nha_dau_tu_2.php', '16_nha_dau_tu_3.php', '17_nha_dau_tu_4.php', '18_19_nha_dau_tu_5.php', '#'];
      $img = ['thong-bao.svg', 'bieu-do.svg', 'meeting.svg', 'Tai-lieu-3.svg', 'tai-lieu-ndt.svg', 'mess.svg', 'policy.svg'];
      for ($i = 1; $i <= 7; $i++) { ?>
        <div class="item   efch-<?php echo $i + 2; ?> ef-img-t <?php if ($i == 3) echo 'active'; ?>">
          <a href="./<?php echo $link[$i - 1] ?>" class="link">
            <div class="img">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i - 1] ?>">
            </div>
            <div class="title"><?php echo $a_h1_2[$i - 1]; ?></div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>
<main id="main" class="sec-b">
  <div class="container">
    <div class="filter-category text-center sec-b">
      <select class="select">
        <option>2020</option>
        <option>2030</option>
      </select>
      <select class="select">
        <option>Báo cáo tài chính hợp nhất</option>
        <option>Báo cáo tài chính hợp nhất</option>
      </select>     
    </div>  

    <?php for($j=1;$j<=1;$j++) {?>  
      <div class="boxwidget box-download-2 mb-30">
        <h2 class="widget-title">2019</h2>
        <div class="row grid-space-60">
          <div class="col-lg-6">
        <div class="single_video  tRes_60" data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0"> 
          <img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="assets/images/nhadautu/investor_daihoicodong.png" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
        </div>              
          </div>  
          <div class="col-lg-6">
                  <ul class="list-download ">
            <?php
            $con = ['Nghị quyết sửa đổi, bổ sung Quy chế tổ chức hoạt động của BKS',
            'Nghị quyết sửa đổi, bổ sung Quy chế tổ chức hoạt động của HĐQT, Quy chế…',
            'Nghị quyết về việc bầu Chủ tịch HĐQT và các Phó Chủ tịch HĐQT nhiệm kỳ 2019 -…',
            'trình phương án sử dụng Vốn chủ sở hữu 2019',
            'trình phương án sử dụng Vốn chủ sở hữu 2019'];
            for($i=1;$i<=5;$i++) {?>                        
                <li> 
                  <span class="title"><i class="icon-t14"></i> <?php echo $con[$i-1]?> </span> 
                  <span class="down"><a   href="#"><i class="icon-arrow-6 ib"></i>  </a></span>
                </li>
            <?php } ?>                        
                  </ul>             
          </div>  
        </div>
      </div>
    <?php } ?> 


    <!-- <?php include '_module/pagination.php';?>  -->
  </div>
</main>


<?php include 'include/index-bottom.php';?>