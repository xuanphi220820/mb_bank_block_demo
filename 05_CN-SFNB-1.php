<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >App MB Bank</h1>
        <div class="efch-3 ef-img-l desc">Trải nghiệm vượt trội cùng</div>
        </div>
    </div>
    <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-4.jpg">
</section>


<?php include '_block/block_3.php';?>
<?php include '_block/block_4.php';?>
<?php include '_block/text_imgsvg_3.php';?>


<section class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
  	<div class="row center end">
  		<div class="col-lg-6">
  			<div class="img tRes_66 efch-2 ef-img-r ">
  				<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/1000x600" src="https://via.placeholder.com/10x6">
  			</div>
  		</div>
  		<div class="col-lg-6">
  			<div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">PHƯƠNG THỨC TRẢ NỢ LINH HOẠT</h2>
			    <ul class="efch-3 ef-tx-t">
			    	<li>Gốc trả định kỳ/cuối kỳ; </li>
			    	<li>Lãi trả định kỳ hoặc cuối kỳ tính theo niên kim cố định/ theo dư nợ ban đầu/ theo dư nợ giảm dần;</li>
			    </ul> 	

  			</div>
  		</div>
  	</div>
  </div>
</section>
 
 <?php include '_block/cau_hoi.php';?>

<section class=" sec-b ">
  <div class="container"  >
		<div class=" b-tl-1">
		  <div class="inner">
		      <h2 class="ht">Hạn mức & biểu phí</h2>
		      <div class="desc">Để xem danh sách tất cả các loại phí hiện có, xin vui lòng tải về biểu phí mới nhất, bạn hãy nhấp vào nút bên cạnh để tải về.</div>
		    </div>
		    <a class="btn lg btn-2" href="#">xem và tải về</a>
		</div>
  </div>
</section>

<section  class="sec-b " >
    <div class="container">
        <div class="entry-head text-center">
            <h2 class="ht efch-1 ef-img-l">Ưu đãi</h2>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            for($i=1;$i<=10;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $i; ?> Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>


<section  class="sec-tb bg-gray" >
    <div class="container"  >
        <div class="max750">
            <h2 class="ht text-center">Sản phẩm liên quan</h2>
            <ul class="cols-2 link2">
                <li><a href="#">Tặng sổ tiết kiệm cho con</a></li>
                <li><a href="#">Tặng sổ tiết kiệm cho con</a></li>
                <li><a href="#">Tặng sổ tiết kiệm cho con</a></li>
                <li><a href="#">Tặng sổ tiết kiệm cho con</a></li>
                <li><a href="#">Tặng sổ tiết kiệm cho con</a></li>
            </ul>
            <div class="tags">
                <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
                <a class="tag" href="#">Ngân hàng đầu tư</a>
                <a class="tag" href="#">Quản lý dòng tiền</a>
            </div>  
        </div>
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>