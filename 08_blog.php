<?php include 'include/index-top.php';?>
<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext ">
        <h1 class=" efch-1 ef-img-l" >Đoàn kết lãnh đạo</h1>
        </div>
        
    </div>
    <img class="img tr lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-11.jpg">
</section>
<main id="main"  class="sec-t " >
	<div class="container">
		<h1 class="text-center">Tips hay</h1>
		<div class="menuicon  owl-carousel   s-nav nav-2" data-res="8,4,3,2" paramowl="margin=0">
	    <?php
	    for($i=1;$i<=10;$i++) {?>
	    <div class="item <?php if($i==1) echo 'active'; ?>">
          <a href="#" class="link">
          	<div class="img">
          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/svg/other/ask.svg">
          	</div>
          	<div class="title">Ngân hàng số <?php echo $i; ?></div>
          </a>
        </div>
    	<?php } ?>
    	</div>
	</div>    	
</main>


<section class="sec-b">
	<div class="container">
			<h2 class="">Nổi bật</h2>
			<p>Những thông tin mới nhất từ MB giúp kế hoạch tài chính của bạn vững vàng hơn</p>
			<div class="list-5 row list-item" >
			    <?php
			    for($i=1;$i<=3;$i++) {?>
			    	<div class="col-md-4">
			          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l ">
			          	<div class="img tRes_71">
			          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
			          	</div>
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2"><?php echo $i; ?> Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn</h4>
		          		<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
		          	</div>
			          </a>
			    	</div>				          
		    	<?php } ?>
			</div>
			<div class="list-5 row list-item" >
			    <?php
			    for($i=1;$i<=4;$i++) {?>
			    	<div class="col-md-3">
			          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l ">
			          	<div class="img tRes_71">
			          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
			          	</div>
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2"><?php echo $i; ?> Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn</h4>
		          	</div>
			          </a>
			    	</div>				          
		    	<?php } ?>
			</div>
	</div>
</section>

<section class="sec-b sec-blog-2">
	<div class="container">
		<h2 class="">Hoạt động của MB và Doanh nghiệp</h2>
		<div class="row list-item">
			<div class="col-lg-4">
	          <a href="#" class="item-banner  tRes ">
	          	<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/350x600">
	          </a>					
			</div>
			<div class="col-lg-8">
				<div class="list-1-1  mb-30 " >


		          <a href="#" class="item  tRes_39 ">
		          	<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
		          	<div class="divtext">
		          		<h4 class="title line2">Mẹo du lịch Nhật Bản siêu tiết kiệm</h4>
		          	</div>
		          </a>

				</div>						
				<div class="list-5 row list-item" >
				    <?php
				    for($i=1;$i<=2;$i++) {?>
				    	<div class="col-md-6">
				          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l ">
				          	<div class="img tRes_51">
				          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
				          	</div>
			          	<div class="divtext">
			          		<h4 class="title line2"><?php echo $i; ?> Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn</h4>
			          	</div>
				          </a>
				    	</div>				          
			    	<?php } ?>
				</div>					
			</div>
		</div>
	</div>
</section>


<section  class="sec-b sec-h-4" >
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Hoạt động của MB và Doanh nghiệp</h2>
		</div>
		<div class="row list-item">
			<div class="col-lg-6 list-1">
		          <a href="#" class="item  tRes_56 video">
		          	<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2">Mẹo du lịch Nhật Bản siêu tiết kiệm</h4>
		          	</div>
		          </a>
			</div>
			<div class="col-lg-6">
				<div class="list-6-1" >
				    <?php
				    for($i=1;$i<=2;$i++) {?>
			          <a href="#" class="item item-inline-table">
			          	<div class="img tRes_56 video cl">
			          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
			          	</div>
			          	<div class="divtext">
			          		<div class="date">01/ 12/ 2019</div>
			          		<h4 class="title line2"><?php echo $i; ?> Liveshow “Khi ta 25”</h4>
			          		<div class="desc line3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
			          	</div>
			          </a>
			          
			    	<?php } ?>
		    	</div>	
			</div>
		</div>
	</div>
</section>

<?php include 'include/index-bottom.php';?>