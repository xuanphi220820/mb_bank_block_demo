<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section   class="sec banner-heading-1 lazy-hidden next-shadow" data-lazy-type="bg" data-lazy-src="assets/images/mbcup/giaithuongbanner.png" >
    <div class="container">
        <div class="divtext top35">
          <h1 class=" efch-1 ef-img-l" >Giải thưởng</h1>
        </div>
    </div>
</section>
<main id="main"  class="sec-tb " >
  <div class="container">
    <h1 class="text-center">Giải thưởng & Thành tích</h1>
    <div class=" sec-b filter-category text-center">
      <select class="select">
        <option>Năm</option>
        <option>2015</option>
        <option>2016</option>
        <option>2017</option>
        <option>2018</option>
        <option>2019</option>
        <option>2020</option>
        <option>2021</option>
      </select>
    </div>


    <div class="list-12 row list-item" >
        <?php
        $img = ['giaithuong1.png','giaithuong2.png','giaithuong3.png','giaithuong4.png','giaithuong5.png','giaithuong6.png'];
        $con = ['MB là ngân hàng dẫn đầu tốc độ tăng trưởng doanh số chi tiêu thẻ tín dụng',
        'Nhận giải thưởng của Asian Banker, MBBank khẳng định vị thế hàng đầu trên thị trường phái sinh',
        'MB vinh dự nhận 4 giải thưởng danh giá từ tổ chức thẻ quốc tế Nhật Bản JCB',
        'MB vinh dự là một trong bốn ngân hàng đạt thương hiệu quốc gia năm 2018',
        'MB lọt Top 5 Ngân hàng thương mại Việt Nam uy tín năm 2019',
        'App MBBank là App ngân hàng số duy nhất cho khách hàng tại Việt Nam đạt danh hiệu “Sao Khuê 2019”'];
        for($i=1;$i<=6;$i++) {?>
          <div class="col-md-4">
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img ">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/mbcup/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <div class="cl6">Ngành ngân hàng</div>
                  <h4 class="title"><?php echo $con[$i-1] ?></h4>
                </div>
              </a>
          </div>                  
        <?php } ?>
    </div>

    <?php include '_module/pagination.php';?>






  </div>      
</main>

<?php include 'include/index-bottom.php';?>