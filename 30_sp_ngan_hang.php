<?php include 'include/index-top.php';?>
<section  class="banner-img-1 next-shadow" >
	<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/banner.jpg" src="">
</section>

<section class="sec-tb">
	<div class="container">
		<h1 class="text-center">Nhu cầu của bạn là gì</h1>
		<div class="max750">
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$a_h1_2 = ['Ngân hàng số','Tài khoản','Cho vay','Tiết kiệm','Thẻ','Chuyển tiền','Chuyển tiền'];
				$link = ['#','31_CN-TaiKhoan.php','#','#','#','#','#'];
				$img = ['bank.svg','user.svg','money-2.svg','save-money.svg','the-1.svg','toan-cau.svg','toan-cau.svg'];
				for ($i = 1; $i <= 7; $i++) { ?>
					<div class="item <?php if ($i == 1) echo 'active'; ?>">
						<a href="./<?php echo $link[$i - 1] ?>" class="link">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>">
							</div>
							<div class="title"><?php echo $a_h1_2[$i-1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<section class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,2,1" paramowl="margin=0">
			<?php
			$img = ['img-1','img-2','img-3','img-1','img-2','img-3'];
			$tit = ['Tài khoản thanh toán','Gói dịch vụ gia đình tôi yêu','Vay siêu nhanh từ thẻ tín dụng (Nhóm SPNB)','Tài khoản thanh toán','Gói dịch vụ gia đình tôi yêu','Vay siêu nhanh từ thẻ tín dụng (Nhóm SPNB)'];
			$con = ['Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			'Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			'Sản phẩm Vay siêu nhanh từ thẻ tín dụng: Là sản phẩm cho vay mục đích tiêu dùng cá nhân dành cho Khách hàng cá nhân có thẻ tín dụng do MB phát hành...',
			'Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			'Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			'Sản phẩm Vay siêu nhanh từ thẻ tín dụng: Là sản phẩm cho vay mục đích tiêu dùng cá nhân dành cho Khách hàng cá nhân có thẻ tín dụng do MB phát hành...'];
			for ($i = 1; $i <= 7; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/spnb/<?php echo $img[$i - 1] ?>.jpg">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $tit[$i-1] ?></h4>
						<div class="desc line2"><?php echo $con[$i-1] ?></div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
			<?php
			$img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
			$tit = ['Thông báo danh sách Khách hàng trúng thưởng CT …',
			'Những lưu ý khi chi tiêu thanh toán khi đi du lịch …',
			'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
			'Những lưu ý khi chi tiêu thanh toán khi đi du lịch …'];
			// $con = ['Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			// 'Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng.',
			// 'Sản phẩm Vay siêu nhanh từ thẻ tín dụng: Là sản phẩm cho vay mục đích tiêu dùng cá nhân dành cho Khách hàng cá nhân có thẻ tín dụng do MB phát hành...'];
			for ($i = 1; $i <= 8; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $tit[$i-1] ?></h4>
						<!-- <div class="desc line2"><?php echo $con[$i-1] ?></div> -->
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section class=" sec-tb bg10">
	<div class="container"  >
		<div class="entry-head text-center">
			<h2 class="ht ">Hướng dẫn sử dụng</h2>
			<div class="desc max500 mb-20">Cùng lắng nghe những chia sẻ của chị Nguyễn Thị B đã sử dụng dịch vụ vay của MB Bank. Cùng lắng nghe những chia sẻ của chị Nguyễn Thị B đã sử dụng dịch vụ vay của MB Bank. Cùng lắng nghe những chia sẻ của chị Nguyễn Thị B đã sử dụng dịch vụ vay của MB Bank</div>
			<a href="#" class="cl1 fs17 w6">Hướng dẫn đăng ký <i class="icon-arrow-1"></i><i class="icon-arrow-1"></i></a>  
		</div>

		<div class="single_video  tRes_16_9 max750 mb-30" data-id="k4Skm7OFczs" data-video="autoplay=1&controls=1&mute=0"> 
			<img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="assets/images/canhan/spnb/video.jpg" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
		</div>
		<div class="text-center">
			<a href="#" class="btn lg">Tải app ngay</a>
		</div>
	</div>
</section>

<section class="sec-tb sec-h-3 other-pd">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm khác</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 p-lr-50" data-res="4,3,2,1" paramowl="margin=20">
			<?php
			$a_h1 = ['Family Banking','Vay online','Tiết kiệm online','Đầu tư online','Family Banking','Vay online','Tiết kiệm online','Đầu tư online'];
			$img = ['meeting.svg','dich-vu-khac.svg','save-money.svg','bieu-do.svg','meeting.svg','dich-vu-khac.svg','save-money.svg','bieu-do.svg'];
			$link = ['./42_CN_SPNB.php','#','#'];
			for ($i = 1; $i <= 8; $i++) { ?>
				<a href="<?php echo $link[$i-1]?>" class="item efch-<?php echo $i + 1; ?> ef-img-l text-left">
					<div class="img mb-10">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2 fs24"><?php 	echo $a_h1[$i - 1] ?></h4>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">MB Kết nối</h2>
			<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1_2 = 	[
							'Tương lai cho con bắt đầu từ sự đầu tư của cha mẹ',
							'Gia đình - Nền tảng của cuộc sống',
							'Làm sao để cha mẹ có quỹ tích lũy khi con trưởng thành'
						];
			$img = ['mbketnoi-1.jpg','mbketnoi-2.jpg','mbketnoi-3.jpg'];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-md-4">
					<a href="<?php echo $link[$i-1] ?> " class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title"><?php echo $a_h1_2[$i-1]; ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</section>

<?php include 'include/index-bottom.php';?>