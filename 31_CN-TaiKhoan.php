<?php include 'include/index-top.php'; ?>
<section class="banner-img-1 next-shadow">
	<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/taikhoan/banner.jpg" src="">
</section>

<section class="sec-tb">
	<div class="container">
		<h1 class="text-center">Nhu cầu của bạn là gì</h1>
		<div class="max750">
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$a_h1_2 = ['Ngân hàng số','Tài khoản','Cho vay','Tiết kiệm','Thẻ','Chuyển tiền','Chuyển tiền'];
				$link = ['30_sp_ngan_hang.php','31_CN-TaiKhoan.php','#','#','#','#','#'];
				$img = ['bank.svg','user.svg','money-2.svg','save-money.svg','the-1.svg','toan-cau.svg','toan-cau.svg'];
				for ($i = 1; $i <= 7; $i++) { ?>
					<div class="item <?php if ($i == 2) echo 'active'; ?>">
						<a href="./<?php echo $link[$i - 1] ?>" class="link">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>">
							</div>
							<div class="title"><?php echo $a_h1_2[$i-1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<main id="main" class="sec-tb ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1 = [
					'Tài khoản thanh toán',
					'Gói dịch vụ gia đình tôi yêu',
					'Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn'
					];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-md-4">
					<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l ">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/taikhoan/<?php echo $img[$i - 1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
							<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Tài khoản thanh toán</h2>
		</div>
		<div class="list-7  list-item row">
			<?php
			$a_h1 = [
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum',
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum'
					];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
			for ($i = 1; $i <= 4; $i++) { ?>
				<div class="col-md-6">
					<a href="#" class="item item-inline-table">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Tài khoản số đẹp</h2>
		</div>
		<div class="list-7  list-item row">
			<?php
			$a_h1 = [
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum',
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum'
					];
			$img = ['img-8.jpg','img-9.jpg','img-10.jpg'];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-md-6">
					<a href="#" class="item item-inline-table">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/taikhoan/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Tài khoản MB Vip</h2>
		</div>
		<div class="list-7  list-item row">
			<?php
			$a_h1 = [
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum',
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum'
					];
			$img = ['img-4.jpg','img-5.jpg','family-banking.jpg'];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-md-6">
					<a href="#" class="item item-inline-table">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Gói Combo tài khoản</h2>
		</div>
		<div class="list-7  list-item row">
			<?php
			$a_h1 = [
					'Thẻ tín dụng Quốc tế MB JCB',
					'Đặc quyền cho chủ thẻ MB Visa Platinum'
					];
			$img = ['img-8.jpg','img-9.jpg'];
			for ($i = 1; $i <= 2; $i++) { ?>
				<div class="col-md-6">
					<a href="#" class="item item-inline-table">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/taikhoan/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<?php //include '_module/pagination.php';
		?>
	</div>
</main>

<?php include 'include/index-bottom.php'; ?>