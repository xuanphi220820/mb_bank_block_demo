<?php include 'include/index-top.php';?>
<section  class="sec-banner-detail next-shadow" >
	<div class="tRes_30"><img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/1440x380" src="https://via.placeholder.com/140x38"></div>
</section>

<main id="main"  class="sec-tb " >
	<div class="container">
		<h1 class="text-center">Hãy để MB hỗ trợ bạn ngay</h1>
		<div class="sec-b">
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="8,4,3,2" paramowl="margin=0">
		    <?php
		    for($i=1;$i<=10;$i++) {?>
		    <div class="item <?php if($i==1) echo 'active'; ?>">
	          <a href="#" class="link">
	          	<div class="img">
	          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/svg/other/ask.svg">
	          	</div>
	          	<div class="title">Ngân hàng số <?php echo $i; ?></div>
	          </a>
	        </div>
	    	<?php } ?>
	    	</div>
    	</div>

		<div class="list-5 row list-item" >
		    <?php
		    for($i=1;$i<=6;$i++) {?>
		    	<div class="col-md-4">
		          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
		          	<div class="img tRes_71">
		          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
		          	</div>
		          	<div class="divtext">
		          		<h4 class="title"><?php echo $i; ?> Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn</h4>
		          		<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
		          	</div>
		          </a>
		    	</div>				          
	    	<?php } ?>
		</div>

		<?php include '_module/pagination.php';?>



		<section  class="sec-tb" >
			<h2 class="ht">DỊCH VỤ SỐ</h2>
			<div class="list-7  list-item row" >
			    <?php
			    for($i=1;$i<=4;$i++) {?>
			    	<div class="col-md-6">
			          <a href="#" class="item item-inline-table">
			          	<div class="img">
			          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
			          	</div>
			          	<div class="divtext">
			          		<h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
			          		<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
			          	</div>
			          </a>
		        	</div>
		    	<?php } ?>
	    	</div>	
		</section>

		<section  class="sec-tb" >
			<h2 class="ht">VAY ONLINE</h2>
			<div class="list-7  list-item row" >
			    <?php
			    for($i=1;$i<=3;$i++) {?>
			    	<div class="col-md-6">
			          <a href="#" class="item item-inline-table">
			          	<div class="img">
			          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
			          	</div>
			          	<div class="divtext">
			          		<h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
			          		<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
			          	</div>
			          </a>
		        	</div>
		    	<?php } ?>
	    	</div>	
		</section>

		<section  class="sec-tb" >
			<h2 class="ht">TÍN DỤNG</h2>

	      <div  class="cttab-v3   ">
	        <div  class="tab-menu">
	          <div  class="active"><span>Cho vay trung dài hạn</span></div>
	          <div  ><span>Cho vay ngắn hạn</span></div>
	          <div ><span>Cho vay ngắn hạn theo ngành</span></div>
	          <div ><span>Bảo lãnh trong bán, cho thuê mua nhà ở HTTTL</span></div>
	        </div>
	        <div class="tab-content">
	          <div class="active">
	            <div class="tab-inner">
					<div class="list-7  list-item row" >
					    <?php
					    for($i=1;$i<=3;$i++) {?>
					    	<div class="col-md-6">
					          <a href="#" class="item item-inline-table">
					          	<div class="img">
					          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
					          	</div>
					          	<div class="divtext">
					          		<h4 class="title line2"><?php echo $i; ?> Cho vay VND lãi suất linh hoạt</h4>
					          		<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					          	</div>
					          </a>
				        	</div>
				    	<?php } ?>
			    	</div>	
	            </div>
	          </div>
	          <div >
	            <div class="tab-inner">
	              2
	            </div>
	          </div>
	          <div>
	            <div class="tab-inner">
	              3
	            </div>
	          </div>
	          <div>
	            <div class="tab-inner">
	              4
	            </div>
	          </div>

	        </div>
	      </div>


		</section>

		<div class="sec-tb">
		<div class="filter-category">
			<select class="select">
				<option>2020</option>
				<option>2030</option>
			</select>
			<select class="select">
				<option>Báo cáo tài chính hợp nhất</option>
				<option>Báo cáo tài chính hợp nhất</option>
			</select>			
		</div>
		</div>



        <div class="accodion accodion-2">
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_2" checked >
                <label class="accodion-title h2" for="chck_2" ><span> 2019 </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content" >
                    <div class="inner">
	                    <ul class="list-download">
		    				<?php for($i=1;$i<=5;$i++) {?>	                    	
	                    	<li> 
	                    		<span class="title"><i class="icon-date-2"></i> Quy chế tổ chức và hoạt động của Ban kiểm soát MB năm 2019</span> 
	                    		<span class="data">4.28 MB</span> 
	                    		<span class="down"><a   href="#"><i class="icon-arrow-6 ib"></i>	</a></span>
	                    	</li>
	    					<?php } ?>	                    	
	                    </ul>

	                    <?php include '_module/pagination.php';?>
                    </div>
                </div>
            </div>
        </div>

        <div class="boxwidget box-download-2">
        	<h2 class="widget-title">2019</h2>
        	<div class="row grid-space-60">
        		<div class="col-lg-6">
					<div class="single_video  tRes_16_9" data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0"> 
					  <img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="https://via.placeholder.com/1600x900" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
					</div>        			
        		</div>	
        		<div class="col-lg-6">
                    <ul class="list-download ">
	    				<?php for($i=1;$i<=5;$i++) {?>	                    	
                    	<li> 
                    		<span class="title"><i class="icon-date-2"></i> Quy chế tổ chức và hoạt động của Ban kiểm soát MB năm 2019</span> 
                    		<span class="down"><a   href="#"><i class="icon-arrow-6 ib"></i>	</a></span>
                    	</li>
    					<?php } ?>	                    	
                    </ul>        			
        		</div>	
        	</div>
        	<br><br>
        	<?php include '_module/pagination.php';?>
        </div>




	</div>    	
</main>

<?php include 'include/index-bottom.php';?>