// JavaScript Document
(function($) {
    $(document).ready(function() {
        $('#search').focusin(function() {
            $('#search-sg').fadeIn();

            $('#search').css("width","200px");
            $('#search').attr('placeholder','Nhập từ khóa cần tìm kiếm.....');
        });
        $('#search').focusout(function() {
            $('#search-sg').fadeOut();
            $('#search').css("width","70px");
            $('#search').attr('placeholder','Tìm kiếm');
        });
        $('.close-sg').click(function() {
            $('#search-sg').css("display", "none");
            $('#search-result').fadeOut();
            $('body').removeClass("fixed-screen");
        });


        $('.form-search-hd').submit(function(e){
            e.preventDefault();
            $('#search-result').show();
            $('body').addClass("fixed-screen").removeClass('showMenu');
        });
    });

})(jQuery);