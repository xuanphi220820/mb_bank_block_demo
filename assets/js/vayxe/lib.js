!function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
}(function(e) {
    "use strict";
    function t(e) {
        return e && (0 === e.offsetWidth || 0 === e.offsetHeight || !1 === e.open)
    }
    function i(e) {
        for (var i = [], n = e.parentNode; t(n); )
            i.push(n),
            n = n.parentNode;
        return i
    }
    function n(e, t) {
        function n(e) {
            void 0 !== e.open && (e.open = !e.open)
        }
        var o = i(e)
          , s = o.length
          , a = []
          , r = e[t];
        if (s) {
            for (var l = 0; s > l; l++)
                a[l] = o[l].style.cssText,
                o[l].style.display = "block",
                o[l].style.height = "0",
                o[l].style.overflow = "hidden",
                o[l].style.visibility = "hidden",
                n(o[l]);
            r = e[t];
            for (var d = 0; s > d; d++)
                o[d].style.cssText = a[d],
                n(o[d])
        }
        return r
    }
    function o(t, i) {
        if (this.$window = e(window),
        this.$document = e(document),
        this.$element = e(t),
        this.options = e.extend({}, l, i),
        this.polyfill = this.options.polyfill,
        this.onInit = this.options.onInit,
        this.onSlide = this.options.onSlide,
        this.onSlideEnd = this.options.onSlideEnd,
        this.polyfill && r)
            return !1;
        this.identifier = "js-" + s + "-" + a++,
        this.startEvent = this.options.startEvent.join("." + this.identifier + " ") + "." + this.identifier,
        this.moveEvent = this.options.moveEvent.join("." + this.identifier + " ") + "." + this.identifier,
        this.endEvent = this.options.endEvent.join("." + this.identifier + " ") + "." + this.identifier,
        this.toFixed = (this.step + "").replace(".", "").length - 1,
        this.$fill = e('<div class="' + this.options.fillClass + '" />'),
        this.$handle = e('<div class="' + this.options.handleClass + '" />'),
        this.$range = e('<div class="' + this.options.rangeClass + '" id="' + this.identifier + '" />').insertAfter(this.$element).prepend(this.$fill, this.$handle),
        this.$element.css({
            position: "absolute",
            width: "1px",
            height: "1px",
            overflow: "hidden",
            opacity: "0"
        }),
        this.handleDown = e.proxy(this.handleDown, this),
        this.handleMove = e.proxy(this.handleMove, this),
        this.handleEnd = e.proxy(this.handleEnd, this),
        this.init();
        var n = this;
        this.$window.on("resize." + this.identifier, function(e, t) {
            return t = t || 100,
            function() {
                if (!e.debouncing) {
                    var i = Array.prototype.slice.apply(arguments);
                    e.lastReturnVal = e.apply(window, i),
                    e.debouncing = !0
                }
                return clearTimeout(e.debounceTimeout),
                e.debounceTimeout = setTimeout(function() {
                    e.debouncing = !1
                }, t),
                e.lastReturnVal
            }
        }(function() {
            !function(e, t) {
                var i = Array.prototype.slice.call(arguments, 2);
                setTimeout(function() {
                    return e.apply(null, i)
                }, t)
            }(function() {
                n.update()
            }, 300)
        }, 20)),
        this.$document.on(this.startEvent, "#" + this.identifier + ":not(." + this.options.disabledClass + ")", this.handleDown),
        this.$element.on("change." + this.identifier, function(e, t) {
            if (!t || t.origin !== n.identifier) {
                var i = e.target.value
                  , o = n.getPositionFromValue(i);
                n.setPosition(o)
            }
        })
    }
    var s = "rangeslider"
      , a = 0
      , r = function() {
        var e = document.createElement("input");
        return e.setAttribute("type", "range"),
        "text" !== e.type
    }()
      , l = {
        polyfill: !0,
        rangeClass: "rangeslider",
        disabledClass: "rangeslider--disabled",
        fillClass: "rangeslider__fill",
        handleClass: "rangeslider__handle",
        startEvent: ["mousedown", "touchstart", "pointerdown"],
        moveEvent: ["mousemove", "touchmove", "pointermove"],
        endEvent: ["mouseup", "touchend", "pointerup"]
    };
    o.prototype.init = function() {
        this.update(!0),
        this.$element[0].value = this.value,
        this.onInit && "function" == typeof this.onInit && this.onInit()
    }
    ,
    o.prototype.update = function(e) {
        (e = e || !1) && (this.min = parseFloat(this.$element[0].getAttribute("min") || 0),
        this.max = parseFloat(this.$element[0].getAttribute("max") || 100),
        this.value = parseFloat(this.$element[0].value || this.min + (this.max - this.min) / 2),
        this.step = parseFloat(this.$element[0].getAttribute("step") || 1)),
        this.handleWidth = n(this.$handle[0], "offsetWidth"),
        this.rangeWidth = n(this.$range[0], "offsetWidth"),
        this.maxHandleX = this.rangeWidth - this.handleWidth,
        this.grabX = this.handleWidth / 2,
        this.position = this.getPositionFromValue(this.value),
        this.$element[0].disabled ? this.$range.addClass(this.options.disabledClass) : this.$range.removeClass(this.options.disabledClass),
        this.setPosition(this.position)
    }
    ,
    o.prototype.handleDown = function(e) {
        if (e.preventDefault(),
        this.$document.on(this.moveEvent, this.handleMove),
        this.$document.on(this.endEvent, this.handleEnd),
        !((" " + e.target.className + " ").replace(/[\n\t]/g, " ").indexOf(this.options.handleClass) > -1)) {
            var t = this.getRelativePosition(e)
              , i = this.$range[0].getBoundingClientRect().left
              , n = this.getPositionFromNode(this.$handle[0]) - i;
            this.setPosition(t - this.grabX),
            t >= n && t < n + this.handleWidth && (this.grabX = t - n)
        }
    }
    ,
    o.prototype.handleMove = function(e) {
        e.preventDefault();
        var t = this.getRelativePosition(e);
        this.setPosition(t - this.grabX)
    }
    ,
    o.prototype.handleEnd = function(e) {
        e.preventDefault(),
        this.$document.off(this.moveEvent, this.handleMove),
        this.$document.off(this.endEvent, this.handleEnd),
        this.$element.trigger("change", {
            origin: this.identifier
        }),
        this.onSlideEnd && "function" == typeof this.onSlideEnd && this.onSlideEnd(this.position, this.value)
    }
    ,
    o.prototype.cap = function(e, t, i) {
        return t > e ? t : e > i ? i : e
    }
    ,
    o.prototype.setPosition = function(e) {
        var t, i, n = void 0 === window.vibV2Slide || null == window.vibV2Slide ? 0 : window.vibV2Slide;
        t = this.getValueFromPosition(this.cap(e, 0, this.maxHandleX)),
        i = this.getPositionFromValue(t),
        this.$fill[0].style.width = i - n + this.grabX + "px",
        this.$handle[0].style.left = i + "px",
        this.setValue(t),
        this.position = i,
        this.value = t,
        this.onSlide && "function" == typeof this.onSlide && this.onSlide(i, t)
    }
    ,
    o.prototype.getPositionFromNode = function(e) {
        for (var t = 0; null !== e; )
            t += e.offsetLeft,
            e = e.offsetParent;
        return t
    }
    ,
    o.prototype.getRelativePosition = function(e) {
        var t = this.$range[0].getBoundingClientRect().left
          , i = 0;
        return void 0 !== e.pageX ? i = e.pageX : void 0 !== e.originalEvent.clientX ? i = e.originalEvent.clientX : e.originalEvent.touches && e.originalEvent.touches[0] && void 0 !== e.originalEvent.touches[0].clientX ? i = e.originalEvent.touches[0].clientX : e.currentPoint && void 0 !== e.currentPoint.x && (i = e.currentPoint.x),
        i - t
    }
    ,
    o.prototype.getPositionFromValue = function(e) {
        return (e - this.min) / (this.max - this.min) * this.maxHandleX
    }
    ,
    o.prototype.getValueFromPosition = function(e) {
        var t, i;
        return t = e / (this.maxHandleX || 1),
        i = this.step * Math.round(t * (this.max - this.min) / this.step) + this.min,
        Number(i.toFixed(this.toFixed))
    }
    ,
    o.prototype.setValue = function(e) {
        e !== this.value && this.$element.val(e).trigger("keyup", {
            origin: this.identifier
        })
    }
    ,
    o.prototype.destroy = function() {
        this.$document.off("." + this.identifier),
        this.$window.off("." + this.identifier),
        this.$element.off("." + this.identifier).removeAttr("style").removeData("plugin_" + s),
        this.$range && this.$range.length && this.$range[0].parentNode.removeChild(this.$range[0])
    }
    ,
    e.fn[s] = function(t) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var n = e(this)
              , a = n.data("plugin_" + s);
            a || n.data("plugin_" + s, a = new o(this,t)),
            "string" == typeof t && a[t].apply(a, i)
        })
    }
});

Number.prototype.formatNumber = function(t) {
    t = t || ".";
    return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + t)
}

var MB = MB || {};
!function() {
    "use strict";
    MB.GLOBAL_CONFIG = {
        vi: {
            pluralWeeks: "tuần",
            singularWeek: "tuần",
            pluralMonths: "tháng",
            singularMonth: "tháng",
            pluralYears: "năm",
            singularYear: "năm",
            rateUnit: "%/năm",
            thousandsSeparator: ".",
            decimalSeparator: ",",
            ordinalSeparator: "thứ",
            ordinalFirst: "",
            ordinalSecond: "",
            ordinalThird: "",
            ordinalMore: "",
            maxInterestRate: "Giá trị lớn nhất là 20"
        },
        en: {
            pluralWeeks: "weeks",
            singularWeek: "week",
            pluralMonths: "months",
            singularMonth: "month",
            pluralYears: "years",
            singularYear: "year",
            rateUnit: "%p.a",
            thousandsSeparator: ",",
            decimalSeparator: ".",
            ordinalSeparator: "",
            ordinalFirst: "st",
            ordinalSecond: "nd",
            ordinalThird: "rd",
            ordinalMore: "th",
            maxInterestRate: "Maximum value is 20"
        }
    }
}();
var MB = MB || {};
!function(t) {
    "use strict";
    function e(t) {
        this.el = document.getElementById(t),
        this.initEvent()
    }
    e.prototype.initEvent = function() {
        var e = this;
        t(this.el).on("click", ".close-btn", function(t) {
            t.preventDefault(),
            e.closeModal()
        })
    }
    ,
    e.prototype.showModal = function() {
        var e = this
          , o = document.body
          , s = document.querySelector("html");
        this.el.classList.remove("hidden"),
        setTimeout(function() {
            o.classList.add("modal-open"),
            e.el.classList.add("show"),
            s.classList.contains("lt-ie9") && t(e.el).animate({
                top: "0"
            }, 500, "linear", function() {})
        }, 200)
    }
    ,
    e.prototype.closeModal = function() {
        var e = this
          , o = document.body
          , s = document.querySelector("html");
        o.classList.remove("modal-open"),
        this.el.classList.remove("show"),
        s.classList.contains("lt-ie9") ? t(this.el).animate({
            top: "100%"
        }, 500, "linear", function() {
            e.el.classList.add("hidden")
        }) : setTimeout(function() {
            e.el.classList.add("hidden")
        }, 500)
    }
    ,
    MB.Modal = e
}(jQuery);
