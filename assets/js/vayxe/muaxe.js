//=mbbank def
// $( '*' ).click( function ( ) { } );
$( 'html' ).css( "-webkit-tap-highlight-color", "rgba(0, 0, 0, 0)" );
var config_minSoTienMuonVay = 100000000;
var config_maxSoTienMuonVay = 10000000000;

var config_minKyHanVayNewCar = 1;
var config_maxKyHanVay = 8;
var config_minKyHanVayOldCar = 1;
var config_minKyHanVay = 6;

var config_minKyHanVay = 1;
var config_maxKyHanVay = 84;

// var config_maxKyHanVay1 = 84;
// var config_maxKyHanVay2 = 72;									

var config_percentNewCar = 0.8;
var config_percentOldCar = 0.75;

var MAXLOAN_DEFAULT = 10000000000;
var MAXLOAN_DUHOC = 10000000000;
var MAXLOAN_LAODONG = 500000000000;

var MAXKYHANVAY_XE = 84;
var MAXKYHANVAY_DUHOC = 120;
var MAXKYHANVAY_LAODONG = 36;
var MAXKYHANVAY_DEFAULT = 84;

var LAISUAT_DEFAULT = 0.116;
var LAISUAT_XE = 0.116;
var LAISUAT_DUHOC = 0.12;
var LAISUAT_LAODONG = 0.15;


var config_laiSuatCoBan = LAISUAT_XE;

var config_laiSuatNewCar = 0.082;
var config_laiSuatOldCar = 0.087;
var config_laiSuatCoSo = 0.084;

var config_plusLaiSuatCoSo = 0.037;

var unitVND = "VNĐ";
var unitYear = "Tháng";
var maxYear = 1;

// vayxe
var MAXPRICE = 3000000000, MINPRICE = 1500000000;


$(document).ready(function(){
	init_Configs();
	updateRangeSlider();
	$("#loan_term_field").val(1);

});

var year = config_maxKyHanVay;
$(document).ready(function(){
	$("#monthly_expenses").parent().find(".min-max-value .max-value").html(formatNum(config_maxSoTienMuonVay) + ' ' + unitVND);
	$("#monthly_expenses").parent().find(".min-max-value .min-value").html(formatNum(config_minSoTienMuonVay) + ' ' + unitVND);

	$("#loan_term").parent().find(".min-max-value .max-value").html(config_maxKyHanVay+ " " + unitYear);
	$("#loan_term").parent().find(".min-max-value .min-value").html(config_minKyHanVayNewCar+ " " + unitYear);

	$("#price_car").keypress(function(e){
		var key = window.event ? event.keyCode : event.which;
		if ( key < 48 || key > 57 ) {
			return false;
		} else {
			return true;
		}
	});

	$("#price_car").keyup(function(e){
		var keycode = e.which || e.keyCode;
		if(keycode != 37 && keycode != 39 && keycode != 8 && keycode != 46 && keycode != 38 && keycode != 40){
			var val = e.currentTarget.value;
			if(val == "")
				return false;
			var price = val.replace(/[\.\,]/g, '');
			$("#price_car").val(formatNum(price));
			$("#price_car").attr("data-value", price);
		}
	});

	$("#price_car").focusout(function(){
		var carP = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));
			config_minSoTienMuonVay = carP * get_MaxPercentChoVay()/100;
			$("#monthly_expenses").parent().find(".min-max-value .max-value").html(formatNum(config_minSoTienMuonVay) + ' ' + unitVND);
			if(isNaN(carP))
				return false;
		
			$(this).val(formatNum(carP.toFixed()));
			updateMonthLyExpensesField(carP);
			updateRangeSlider();
		});

	$("#monthly_expenses_field").parent().addClass("focus");
	$("#loan_term_field").parent().addClass("focus");

	$("#monthly_expenses").change(function(){
		if(isShowButton()) {
			updateRangeSlider();
		}
		showHideButton();
	});

	$("#monthly_expenses_field").focusout(function(){
		
		if($("#price_car").length > 0){
			var carP = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));
			config_minSoTienMuonVay = carP * get_MaxPercentChoVay()/100;
			$("#monthly_expenses_field").attr("data-value", config_minSoTienMuonVay);
		}
		
			
		if(isShowButton())
			updateRangeSlider();
	});

	$("#monthly_expenses_field").keypress(function(){
		if($("#price_car").length > 0){
			var price = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));
			if(isNaN(price) && $("#price_car").val() != "")
				return false;
		}
	});

	$("#loan_term").change(function(){
		updateRangeSlider();
	});

	$("#loan_term_field").focusout(function(){
		updateRangeSlider();
	});

	$("#loan_term_field").keyup(function(){
		if(isNaN($(this).val())){
			$(this).val('');
		}
	});

});

function check(e,value){
    	//Check Charater
	var unicode=e.charCode? e.charCode : e.keyCode;
	if (value.indexOf(".") != -1)if( unicode == 46 )return false;
	if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
 }

function init_Configs(){
	if(typeof MB.loan_type !== "undefined"){
		switch(MB.loan_type) {
			case 'VAY_XE':
				config_laiSuatCoBan =  LAISUAT_XE;
				config_maxKyHanVay = MAXKYHANVAY_XE;
				break;
				
			case 'VAY_DU_HOC':
				config_laiSuatCoBan =  LAISUAT_DUHOC;
				config_maxKyHanVay = MAXKYHANVAY_DUHOC;
				break;
			case 'VAY_LAO_DONG':
				config_laiSuatCoBan =  LAISUAT_LAODONG;
				config_maxKyHanVay = MAXKYHANVAY_LAODONG;

				break;
			default:
				config_laiSuatCoBan =  LAISUAT_DEFAULT;
				config_maxKyHanVay = MAXKYHANVAY_DEFAULT;
				break;
		}
	}
	else{
		return false;
	}
}

function get_MaxThoiGianChoVay() {
	var price = parseInt($('#price_car').attr("data-value"));
	
	if(typeof MB.loan_type !== "undefined"){
		switch(MB.loan_type) {
			case 'VAY_XE':
				if(  price <= MINPRICE){
					return 84;
				}
				else{
					return 72;
				}
			case 'VAY_CAM_CO':
			  return 84;
			case 'VAY_DU_HOC':
				return MAXKYHANVAY_DUHOC;
			case 'VAY_LAO_DONG':
				return MAXKYHANVAY_LAODONG;
			default:
				return 84;
		}
	}
}
// return str to details show
function get_LoanType(){
	if(typeof MB.loan_type !== "undefined"){
		switch(MB.loan_type) {
			case 'VAY_XE':
				return "giá xe"
			case 'VAY_CAM_CO':
				return "GTCG";
			case 'VAY_DU_HOC':
				return "Du học";
			case 'VAY_LAO_DONG':
				return "Lao động";
			default:
				return "";
		}
	}
}

function get_MaxPercentChoVay() {
	var price = parseInt($('#price_car').attr("data-value"));

	if(typeof MB.loan_type !== "undefined"){
		switch(MB.loan_type) {
			case 'VAY_XE':
				if(price <= MINPRICE){
					return 75;
				}
				else if( MINPRICE < price &&  price <= MAXPRICE ){
					return 70;
				}
				else {
					return 55;
				}
			case 'VAY_CAM_CO':
			  return 100;
			case 'VAY_DU_HOC':
				return 100;
			default:
				return 100;
		}
	}
}

function get_MaxPriceValue(){
	if(typeof MB.loan_type !== "undefined"){
		switch(MB.loan_type) {

			case 'VAY_LAO_DONG':
				return MAXLOAN_LAODONG;
			case 'VAY_DU_HOC':
				return MAXLOAN_DUHOC;
			default:
				return MAXLOAN_DEFAULT;
		}
	}
}

	function checkLength(){
		var fieldVal = document.getElementById('loan_term_field').value;
		if(fieldVal <= maxYear){
			return true;
		} else {
			document.getElementById('loan_term_field').value = '';
		}
	}
	
	function updateMonthLyExpensesField(carP) {
		var percent = get_MaxPercentChoVay();

		var price = Math.round(percent*carP/100);
		$("#monthly_expenses_field").val(formatNum(price));
		$("#monthly_expenses_field").attr("data-value", price);
		setTimeout(function(){$("#monthly_expenses_field").trigger("focusout")}, 100);
	}
	
	function updateRangeSlider() {
		// var carP = config_maxSoTienMuonVay;
		var carP = get_MaxPriceValue();
		if($("#price_car").length > 0 && $("#price_car").val() != "")
			carP = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));

		percent = get_MaxPercentChoVay();
		year = get_MaxThoiGianChoVay();

		// #TODO
		$("#loan_term").attr("max", year).rangeslider('update', true);


		$("#loan_term").parent().find(".min-max-value .max-value").html(year + ' ' + unitYear);
		updateMonthLyExpenses(carP, percent);
		showHideButton();
	}
	
	function updateMonthLyExpenses(carP, percent) {
		var value = Math.round(carP*percent/100);
		$("#monthly_expenses").attr("max", value).rangeslider('update', true);
		$("#monthly_expenses").parent().find(".min-max-value .max-value").html(formatNum(value) + ' ' + unitVND);
	}

var laiSuatCoSo = config_laiSuatCoSo;
var KhoanVay = function(thang, laiSuat, duNoConLai, tienGoc, tienLai, tongGocLai) {
	this.thang = thang;
	this.laiSuat = laiSuat;
	this.duNoConLai = duNoConLai;
	this.tienGoc = tienGoc;
	this.tienLai = tienLai;
	this.tongGocLai = tongGocLai;
}

function changeYear(year) {
	updateChiTietKhoanVay(year);
}

function updateChiTietKhoanVay(numbYear) {
	$("#tb_soNam").html("").append("<li>Năm</li>");
	var kyHanVay = parseInt($("#loan_term_field").val().split(" ")[0]);
	for(var i = 1; i <= kyHanVay; i++) {
		if(i == numbYear)
			$("#tb_soNam").append('<li class="active" onclick="changeYear('+i+')">'+i+'</li>');
		else 
			$("#tb_soNam").append('<li onclick="changeYear('+i+')">'+i+'</li>');
	}
	var listChiTiet = initKhoanVay();

	lai = config_laiSuatNewCar*100;
	var soTienVay = parseInt($("#monthly_expenses_field").val().split(" ")[0].replace(/[\,\.]/g, ''));
	$("#tb_year").html("").append('<div class="mb-header-box-table mb-detail-mortgage">'
		+'<div class="mb-fix-row1-table">Tháng</div>'
		+'<div class="mb-right-slider-table">'
		+   '<div class="mb-slider-mobile-table">'
		+      '<div class="mb-colum-table-deposit">Lãi suất tham khảo</div>'
		+      '<div class="mb-colum-table-deposit">Dư nợ còn lại</div>'
		+      '<div class="mb-colum-table-deposit">Tiền lãi hàng tháng</div>'
		+      '<div class="mb-colum-table-deposit">Tiền gốc hàng tháng</div>'
		+      '<div class="mb-colum-table-deposit">Tổng tiền trả hàng tháng</div>'
		+   '</div>'
		+'</div>'
		+'<div class="clearfix"></div>'
		+'</div>')
	if(numbYear <= 1)
		$("#tb_year").append('<div class="mb-title-box-table-deposit">Lãi suất cố định 6 tháng đầu: <span> '+lai.toFixed(1)+'%</span></div>');
	else
		$("#tb_year").append('<div class="mb-title-box-table-deposit">Lãi suất thả nổi từ tháng thứ 7 trở đi: <span> lãi suất cơ sở *+ 3.7%</span></div>');
	if(numbYear == 1) {
		$("#tb_year").append('<div class="mb-line-box-table-deposit mb-detail-mortgage">'
			+    '<div class="mb-fix-row1-table">0</div>'
			+    '<div class="mb-right-slider-table">'
			+       '<div class="mb-slider-mobile-table">'
			+          '<div class="mb-colum-table-deposit">-</div>'
			+          '<div class="mb-colum-table-deposit">'+formatNum(soTienVay)+'</div>'
			+          '<div class="mb-colum-table-deposit">-</div>'
			+          '<div class="mb-colum-table-deposit">-</div>'
			+          '<div class="mb-colum-table-deposit">-</div>'
			+       '</div>'
			+    '</div>'
			+    '<div class="clearfix"></div>'
			+ '</div>');
	}
	var i = 12*(numbYear - 1) + 1;
	for(i; i < (numbYear*12 + 1); i++) {
		var pLaiSuat = listChiTiet[i].laiSuat;
		if(i >= 7)
			pLaiSuat = (listChiTiet[i].laiSuat * 100).toFixed(2);
		else 
			pLaiSuat = (listChiTiet[i].laiSuat * 100).toFixed(1);

		$("#tb_year").append('<div class="mb-line-box-table-deposit mb-detail-mortgage">'
			+    '<div class="mb-fix-row1-table">'+listChiTiet[i].thang+'</div>'
			+    '<div class="mb-right-slider-table">'
			+       '<div class="mb-slider-mobile-table">'
			+          '<div class="mb-colum-table-deposit">'+pLaiSuat+'%</div>'
			+          '<div class="mb-colum-table-deposit">'+formatNum(Math.round(listChiTiet[i].duNoConLai))+'</div>'
			+          '<div class="mb-colum-table-deposit">'+formatNum(Math.round(listChiTiet[i].tienLai))+'</div>'
			+          '<div class="mb-colum-table-deposit">'+formatNum(Math.round(listChiTiet[i].tienGoc))+'</div>'
			+          '<div class="mb-colum-table-deposit">'+formatNum(Math.round(listChiTiet[i].tongGocLai))+'</div>'
			+       '</div>'
			+    '</div>'
			+    '<div class="clearfix"></div>'
			+ '</div>');
		if(i == 6) {
			$("#tb_year").append('<div class="mb-title-box-table-deposit">Lãi suất thả nổi từ tháng thứ 7 trở đi: <span> lãi suất cơ sở *+ 3.7%</span></div>');
		}
	}

	$("#tb_year").append('<div class="mb-line-box-table-deposit">'
		+'<div class="mb-note-line-box-table-deposit">(*) Lãi suất có thể thay đổi theo từng thời điểm, giảm xuống hoặc tăng lên phụ thuộc vào sự thay đổi của thị trường.<br> VIB luôn cập nhật mức lãi suất cơ sở trên website  vib.com.vn<br>Lịch thanh toán gốc và lãi được tạm tính dựa trên giả định 1 tháng có 30 ngày. Nợ gốc và lãi phải trả hàng tháng của Quý khách sẽ căn cứ trên số ngày thực tế, bao gồm trường hợp ngày thanh toán rơi vào ngày nghỉ, ngày lễ và được chuyển sang ngày làm việc kế tiếp. Quý khách vui lòng tham khảo chi tiết tại sao kê tài khoản vay hoặc thông báo bằng email/tin nhắn.'
		+'</div>'
		+'</div>');
	renderYearNav(numbYear);
	func();
}
	
	function initKhoanVay() {
		var soTienMuonVay = $("#monthly_expenses_field").val().split(" ")[0].replace(/[\,\.]/g, '');
		var kyHanVay = $("#loan_term_field").val().split(" ")[0].replace(/[\,\.]/g, '');
		var month = kyHanVay;
		var listChiTiet = [];
		var laiSuatCoBan = config_laiSuatCoBan;

		listChiTiet.push(new KhoanVay(0, 0, soTienMuonVay, 0, 0, 0));
		var tienGoc = soTienMuonVay / month;
		var j = 0;
		for(var i = 1; i <= month; i++) {
			if(i >= 84) {
				laiSuatCoBan = laiSuatCoSo + config_plusLaiSuatCoSo;
			}
			var tienLai = listChiTiet[j].duNoConLai*laiSuatCoBan/12;
			var tongGocLai = tienGoc + tienLai;
			var duNoConLai = listChiTiet[j].duNoConLai - tienGoc;
			var khoanVay = new KhoanVay(i, laiSuatCoBan, duNoConLai, tienGoc, tienLai, tongGocLai);
			listChiTiet.push(khoanVay);
			j++;
		}
		return listChiTiet;
	}
	
	function func() {
		$(document).ready(function() {
			$(window).resize(function() {
				if( w_wins != $( window ).width() ){
					width = $(window).width();
					switch_slide_news(width);			
					w_wins = $( window ).width();
				}
			});
			switch_slide_news(width);
		});
	}
	
	var formatNum = function(value) {
		var lang = window.location.href ;
		var local = 'vi-VN';
		if(lang.includes('/en/')){
			local='en-US';
		}
		var value_=	parseFloat(value);
		var rs=value_.toLocaleString(local);
		return rs;
	}

	var templateBox = '<div class="mb-line-result-calculation result-calculation divtext">'
	+ '<div class="mb-exit-result-calculation" onclick="hideBox(\'{{INDEX}}\')"><i class="icon-popup-exit"></i></div>'
	+ '<div class="mb-table-result-calc">'
	+	'<div class="mb-row1-result-calc">';
	if(MB.loan_type != 'VAY_DU_HOC' && MB.loan_type != 'VAY_LAO_DONG'){
		templateBox = templateBox
		+		'<div class="line-v2-row-result-calc">'
		+			'Ước tính {{LOAI_VAY}}: <span class="price">{{GIATRI}} VNĐ</span>'
		+		'</div>';
	}

	templateBox = templateBox
	+		'<div class="line-v2-row-result-calc">'
	+			'Số tiền vay : <span class="price">{{SOTIENVAY}} VNĐ</span>'
	+		'</div>'
	+		'<div class="line-v2-row-result-calc">'
	+			'Kỳ hạn vay: <span class="price">{{KYHAN}} Tháng</span>'
	+		'</div>'
	+	'</div>'
	+	'<div class="mb-row2-result-calc">'
	+		'<div class="line-v2-row-result-calc">'
	+			'Tiền lãi hàng tháng <span class="price">{{TIENLAI}} VNĐ</span>'
	+		'</div>'
	+		'<div class="line-v2-row-result-calc">'
	+			'Tiền gốc hàng tháng <span class="price">{{TIENGOC}} VNĐ</span>'
	+		'</div>'
	+		'<div class="line-v2-row-result-calc">'
	+			'Tổng tiền trả hàng tháng <span class="price">{{TIENTRA}} VNĐ</span>'
	+		'</div>'
	+	'</div>'
	+	'<div class="mb-row3-result-calc">'
	+		'<div class="link-detail-result-register">'
	+			'<a id="btnCalcLoan" href="#" onClick=calcLoan() data-price="{{SOTIENVAY}}" data-month={{KYHAN}} class="calcLoan link btn">Chi tiết khoản vay</a>'
	+		'</div>'
	+	'</div>'
	+'</div>'
	+'</div>';
	// var templateChooseAnother = '<div class="mb-line-result-calculation not-choose">'
    //                         	+	'<div class="mb-title-result-calc">Chọn phương án khác để so sánh</div>'
    //                     		+'</div>';

    var KetQua = function(loai, giaTri, soTienVay, kyHan, lai, goc, tong) {
    	this.loai = loai;

		this.giaTri = giaTri;
		this.soTienVay = soTienVay;
		this.kyHan = kyHan;
		this.lai = lai;
		this.goc = goc;
		this.tong = tong;
	}
	
	var listKetQua = [];
	
	$(document).ready(function(){
		$(".mb-btn-dk02").addClass("disabled");
	});
	
	function showResult() { 
		var loai = $("input[name=carType]:checked").val();
		var giaTri = 0;
		if(MB.loan_type != 'VAY_DU_HOC' && MB.loan_type != 'VAY_LAO_DONG'){
			giaTri = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));
		}

		var soTienVay = $("#monthly_expenses_field").attr("data-value");
		var kyHan = $("#loan_term_field").attr("data-value");
		var listChiTiet = initKhoanVay();
		var lai = Math.round(listChiTiet[1].tienLai);
		var goc = Math.round(listChiTiet[1].tienGoc);
		var tong = Math.round(listChiTiet[1].tongGocLai);
		var ketqua = new KetQua(loai, giaTri, soTienVay, kyHan, lai, goc, tong);
		listKetQua.push(ketqua);
		// if(listKetQua.length > 3)
		if(listKetQua.length > 1)
			listKetQua.splice(0, 1);
		
		showResultDetail(listKetQua);
		showHideButton();
	}
	
	var gstartSlide = 0;
	var gtotalResults = 0;
	
	function showResultDetail(list) {
		var resultBox = "";
		var j = 0;
		
		for(var i = 0; i < list.length; i++) {
			if(j >= 3)
				break;
			j++;
			if(MB.loan_type != 'VAY_DU_HOC' && MB.loan_type != 'VAY_LAO_DONG'){
				resultBox += templateBox.replace("{{INDEX}}", i)
				.replace("{{LOAI_VAY}}", get_LoanType())
				.replace("{{GIATRI}}", formatNum(list[i].giaTri))
				.replace("{{SOTIENVAY}}", formatNum(list[i].soTienVay))
				.replace("{{KYHAN}}", list[i].kyHan).replace("{{TIENLAI}}", formatNum(list[i].lai))
				.replace("{{TIENGOC}}", formatNum(list[i].goc))
				.replace("{{TIENTRA}}", formatNum(list[i].tong))
				.replace('{{SOTIENVAY}}', list[i].soTienVay)
				.replace('{{KYHAN}}', list[i].kyHan);

			}
			else{
				resultBox += templateBox.replace("{{INDEX}}", i)
				.replace("{{GIATRI}}", formatNum(list[i].giaTri))
				.replace("{{SOTIENVAY}}", formatNum(list[i].soTienVay))
				.replace("{{KYHAN}}", list[i].kyHan).replace("{{TIENLAI}}", formatNum(list[i].lai))
				.replace("{{TIENGOC}}", formatNum(list[i].goc))
				.replace("{{TIENTRA}}", formatNum(list[i].tong))
				.replace('{{SOTIENVAY}}', list[i].soTienVay)
				.replace('{{KYHAN}}', list[i].kyHan);
			}
			
			
		}
		// var _window = $(window);
		// if(j == 1)
		// 	resultBox += templateChooseAnother + templateChooseAnother;
		// else if(j == 2)
		// 	resultBox += templateChooseAnother;
		// $("#parent-box").html("").append(resultBox);
		$("#parent-box").html(resultBox);

		displayFlex();
		gstartSlide = j - 1;
		gtotalResults = list.length;
		// updateMobileSlider(gstartSlide, gtotalResults);
	}
	function hideBox(index) {
		listKetQua.splice(index, 1);
		showResultDetail(listKetQua);
		showHideButton();
	}
	
	function resetField() {
		$("#brand").val('').change();
		$(".dropdown-menu .inner li").removeClass("selected active");
		
		$("#vehicles").val('').change();
		
		$("#price_car").val('');
		$("#price_car").parent().removeClass("focus");
		
		$("#monthly_expenses_field").val(formatNum(config_minSoTienMuonVay));
		$("#monthly_expenses_field").attr("data-value", config_minSoTienMuonVay);
		
		$("#monthly_expenses").parent().find(".min-max-value .max-value").html(formatNum(config_maxSoTienMuonVay) + ' ' + unitVND);
		$("#monthly_expenses").parent().find(".min-max-value .min-value").html(formatNum(config_minSoTienMuonVay) + ' ' + unitVND);

		$("#monthly_expenses").val(config_minSoTienMuonVay).rangeslider('update', true);
		
		$("#loan_term").val(1).rangeslider('update', true);
		$("#loan_term").parent().find(".min-max-value .max-value").html(config_maxKyHanVay+ " " + $("#loan_term").attr('unit'));  
		
		$("#loan_term_field").val(1);
		
		$("#new_car").prop("checked", true);
	}
	
	function isShowButton() {
		// var loai = $("input[name=carType]:checked").val();
		var loai = MB.loan_type;

		var soTienVay = $("#monthly_expenses_field").attr("data-value");
		var kyHan = $("#loan_term_field").attr("data-value");
		
		if($("#price_car").length > 0){
			var giaTri = parseInt($("#price_car").val().replace(/[\.\,]/g, ''));
			if( $("#price_car").val() != ""){
				for(var i = 0; i < listKetQua.length; i++) {
					if(listKetQua[i].loai == loai
						&& listKetQua[i].giaTri == giaTri 
						&& listKetQua[i].soTienVay == soTienVay && listKetQua[i].kyHan == kyHan)
						return false;
				}
				return true;
			}
		}
		else{
			return true;
		}
		
		if(listKetQua.length == 3)
			return false;
		return false;
	}
	
	function showHideButton() {
		if(!isShowButton())
			$(".mb-btn-dk02").addClass("disabled");
		else
			$(".mb-btn-dk02").removeClass("disabled");
	}
	var w_wins = 0;
	$( window ).load( function(){ 
		w_wins = $( window ).width();
	});	
	var slider = null;
	$(document).ready(function() {
		$(window).resize(function(){ 
			if( w_wins != $( window ).width() ){
				resetRangeSlider($("#monthly_expenses"), config_maxSoTienMuonVay, 1000);
				resetRangeSlider($("#loan_term"), config_maxKyHanVay, 1);
				// updateMobileSlider(gstartSlide, gtotalResults);			
				w_wins = $( window ).width();
			}
		});
	});
	var _window = $(window);

function displayFlex() {
	if(listKetQua.length > 0)
		$('.mb-deposit-tool-calculation .mb-result-calculation').css({'display': 'flex'});
	else 
		$('.mb-deposit-tool-calculation .mb-result-calculation').css({'display': 'none'});
}

function resetRangeSlider($input, maxValue, step) {
	if (_window.width() >= 750 ) {
		$input.rangeslider('destroy');
		var $ruler = $('<span class="arrow-left-1" /> <span class="arrow-left-2" />\<span class="arrow-right-1" /> <span class="arrow-right-2" />');
		var splitterLength = maxValue/step-1;
		var splitter = '';
		splitterLength = splitterLength>15?15:splitterLength;
		for(var i=0; i< splitterLength; i++) {
			splitter += '<span class="splitter" style="width: ' + 100/splitterLength + '%" />';
		}
		var $newFill = $('<div class="rangeslider__fill__new">' + splitter + '</div>');
		$input.rangeslider({
			polyfill: false,
			onInit: function() {
				var $fill = this.$range;
				this.$handle.html($ruler);
				$newFill.width(this.$range.width()-22);
				$fill.prepend($newFill);
			},
			onSlideEnd: function() {
				moreThanMax = false;
			}
		});
		if($input.parent().find(".min-max-value .max-value")[0].innerHTML == $input.parent().find(".min-max-value .min-value")[0].innerHTML) {
			$input.parent().find(".rangeslider__fill").css("width", "0px");
		}
	}
}


function converNumber(number)
{
	var strNumber="";
	strNumber= String(number);
	return strNumber.replace(/\d(?=(?:\d{3})+(?!\d))/g, '$&,')
}

function GetTodayDate() {
	var tdate = new Date();
	var dd = tdate.getDate(); //yields day
	var MM = tdate.getMonth(); //yields month
	var yyyy = tdate.getFullYear(); //yields year
	var currentDate= dd + "-" +( MM+1) + "-" + yyyy;
 
	return currentDate;
 }



function calcLoan(summoney=2000000000,percent=8,time=12 )
{
	var $this = $("#btnCalcLoan");
	var price = $this.attr('data-price');
	var month = $this.attr('data-month');
	var percent = 11.6;

	summoney = price;
	time = month;

	var goc=0, lai=0,goc_lai=0, sum_goc=0,sum_lai=0, tempGoc;
	var day,month,year;
	
	var d= new Date();
	day = d.getDate();
	month = d.getMonth() + 1;
	year = d.getFullYear();
	var strDate= day +"/" + ( (month<10)? "0" + month:month )  + "/" + year;
	
	tempGoc = summoney;
	
	var strResult="";
	strResult="<table class=\"tbl-list-repayment table table-full\"  cellpadding=\"0\" cellspacing=\"0\" border=\"1\" >"
	
	strResult +="<tr>"
				+	"<th width=\"20%\" align=\"center\" colspan=\"2\">Kỳ trả nợ</th>"
				+	"<th width=\"20%\" align=\"center\">Số gốc còn lại</th>"
				+	"<th width=\"20%\" align=\"center\">Gốc</th>"
				+	"<th width=\"20%\" align=\"center\">Lãi</th>"
				+	"<th width=\"20%\" align=\"center\">Tổng gốc + Lãi</th>"
				+"</tr>";
	
	strResult +="<tr>"
				+	"<td width=\"15%\" align=\"center\">"+ strDate +"</td>"
				+	"<td width=\"5%\" align=\"center\">0</td>"
				+	"<td width=\"20%\" align=\"center\">"+ converNumber(summoney) +"</td>"
				+	"<td width=\"20%\" align=\"center\"></td>"
				+	"<td width=\"20%\" align=\"center\"></td>"
				+	"<td width=\"20%\" align=\"center\"></td>"
				+"</tr>";
	
	goc = parseInt(summoney)/time;				
	// tính gốc
					
	for(var i = 0; i<time; i++)
	{
		sum_goc = sum_goc + goc;
		lai = parseInt(tempGoc) /12 * percent / 100; // tính lãi
		
		tempGoc = tempGoc-goc; // gốc còn lại theo từng tháng
		goc_lai = goc+lai;
		sum_lai = sum_lai + lai;
		
		// cộng ngày tháng
		if(month == 12)
		{
			month = 1;
			year = year+1;
		}
		else
		{
			month = month + 1;
		}
		strDate= day +"/" +( (month<10)? "0" + month:month ) + "/" + year;
		
		strResult +="<tr>"
				+	"<td width=\"15%\" align=\"center\">"+ strDate +"</td>"
				+	"<td width=\"5%\" align=\"center\">" + (i+1) + "</td>"
				+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(tempGoc)) +"</td>"
				+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(goc)) +"</td>"
				+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(lai)) + "</td>"
				+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(goc_lai)) +"</td>"
				+"</tr>"
	}
	
	strResult +="<tr>"
				+	"<th width=\"15%\" align=\"center\">Tổng</th>"
				+	"<th width=\"5%\" align=\"center\"></th>"
				+	"<th width=\"20%\" align=\"center\"></th>"
				+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_goc)) +"</th>"
				+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_lai)) + "</th>"
				+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_goc + sum_lai)) +"</th>"
				+"</tr>"
	
	strResult +="</table>";
	
	$('#listRepayment').html(strResult);
	$('#listRepayment-link').trigger('click').removeClass('disabled');
	$('html, body').animate({scrollTop: $("#mb-calculator").offset().top - 100}, 1000);

}