<?php include 'include/index-top.php'; ?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Đăng ký trực tuyến</span>
      </div>
    </div>
</div>
<section class="sec-tb">
	<div class="container">
		<div class="text-center">
			<h1>Đăng ký dịch vụ trực tuyến</h1>
			<p class="desc max750">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore atque porro rem labore, earum, eveniet suscipit cum dolores, nesciunt perferendis ratione asperiores consequatur eligendi saepe eum! Impedit sunt dolore ad?</p>
		</div>
	</div>
</section>
<section class="online-signup">
	<div class="container">
		<div class="max950">
			<div class="flex-bw">
				<div class="step">
					<a href="#" class="b active">01</a>
				</div>
				<div class="step">
					<a href="#" class="b">02</a>
				</div>
				<div class="step">
					<a href="#" class="b">03</a>
				</div>
				<div class="step">
					<a href="#" class="b">04</a>
				</div>
				<div class="step-line"></div>
			</div>
		</div>
	</div>
</section>
<section class=" sec-tb ">
	<div class="container">
		<div class="max750">
			<form class="row list-item form-contact">
				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 fs24">Bước 1/4: Thông tin chung</h3>
					</div>
				</div>
				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Họ và tên (*)">
					</label>
				</div>
				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Số điện thoại (*)">
					</label>
				</div>
				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Email (*)">
					</label>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Số CMND (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Ngày cấp cmnd (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Nơi cấp cmnd (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Ngày sinh (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<span class="title">Tình trạng hôn nhân (<span class="require">*</span>)</span>
						<label class="radio "><input type="radio" name="check1" checked=""><span></span>Độc thân</label> &nbsp; &nbsp; &nbsp;
						<label class="radio "><input type="radio" name="check1"><span></span>Đã kết hôn</label>
					</label>
				</div>

				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 pt-10 pb-10 bg-1">Địa chỉ thường trú</h3>
					</div>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Tỉnh/ Thành phố (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Quận/ Huyện (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Phường/ Xã (*)">
					</label>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Địa chỉ thường trú (*)">
					</label>
				</div>

				<div class="col-12">
					<div class="text-center">
						<h3 class="ctext mg-0 pt-10 pb-10 bg-1">Nới ở hiện tại</h3>
					</div>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Tỉnh/ Thành phố (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Quận/ Huyện (*)">
					</label>
				</div>

				<div class="col-lg-6">
					<label class="block">
						<input class="input" placeholder="Phường/ Xã (*)">
					</label>
				</div>

				<div class="col-12">
					<label class="block">
						<input class="input" placeholder="Địa chỉ thường trú (*)">
					</label>
				</div>

				<div class="col-12 text-center">
					<a class="btn" href="<?php if (1 == 1): ?>
							./25_dang_ky_online_2.php
						<?php else: ?>
							#
                  		<?php endif ?> ">Tiếp tục</a>
				</div>
			</form>
		</div>

	</div>
</section>

<?php include 'include/index-bottom.php'; ?>