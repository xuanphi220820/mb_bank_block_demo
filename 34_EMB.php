<?php include 'include/index-top.php';?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <a class="item" href="#">Dịch vụ số</a>
        <span class="item">eMB (internet banking)</span>
      </div>
    </div>
</div>

<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l text-normal" >eMB (Internet Banking)</h1>
        <div class="efch-3 ef-img-l desc cl1 b">Trải nghiệm vượt trội</div>
        </div>
        <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-9.jpg">
    </div>
    
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Tiện ích</a></li>
        <li><a href="#tab3" class="scrollspy">Gói eMB</a></li>
        <li><a href="#tab4" class="scrollspy">OTP</a></li>
        <li><a href="#tab5" class="scrollspy">Điều kiện</a></li>
        <li><a href="#tab6" class="scrollspy">Hướng dẫn</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php';?>


<section id="tab2" class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">TIỆN ÍCH</h2>
    </div>    
    <div class="row list-item">
      <?php
      $img = ['ung-dung-3.svg','money-4.svg','bieu-do.svg'];
      $a_h1 = [
                'Đối tượng khách hàng',
                'Loại tiền tệ',
                'Biện pháp bảo mật'
              ];
      $desc = [
                'Cá nhân và doanh nghiệp',
                'Loại tiền tệ sử dụng VNĐ',
                'Tên đăng nhập, mật khẩu và OTP'
              ];
      for($i=1;$i<=3;$i++) { ?>
      <div class="col-sm-6 col-md-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_h1[$i - 1] ?></h4>          
            <div class="desc"><?php echo $desc[$i - 1] ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab3" class=" sec-b  ">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht ">Gói eMB</h2>
    </div>      
  <div class="row list-item">
    <div class="col-md-4">
      <div class="widget-default">
        <h4 class="widget-title">Hạn mức chuyển tiền</h4>
        <div class="widget-content entry-content">
          <p>Tối thiểu 10.000 VNĐ/giao dịch.</p>
        </div>
      </div>
    </div>    
    <?php for($i=1;$i<=2;$i++) { ?>
    <div class="col-md-4">
      <div class="widget-default">
        <h4 class="widget-title">Hạn mức chuyển tiền tối đa</h4>
        <div class="widget-content entry-content">
          <div class="toggleAutoHeight" data-height="200" data-more="+ Xem thêm" data-less="- Thu gọn" data-i="" >
            <div class="tgh-content">
              <div class="tgh-first">
                <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              </div>                
              <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
              <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
              <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
              <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              <?php if($i==1) { ?>
              <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
              <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
              <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
              <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
              <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  </div>
</section>
 
<section id="tab4" class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
    <div class="row center end">
      <div class="col-lg-6">
        <div class="img tRes_66 efch-2 ef-img-r ">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/emb/otp.jpg" src="https://via.placeholder.com/10x6">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">OTP (One Time Password)</h2>

          <p>Mật khẩu sử dụng một lần, được sinh ra từ các phương thức xác thực là thiết bị Hard Token hoặc phần mềm Mobile Token do khách hàng lựa chọn đăng ký</p>
          <p>•  Hard Token: là thiết bị mua rời.</p>
          <p>•  Mobile Token: Phần mềm cài đặt trên các thiết bị di động (điện thoại di động/ máy tính bảng) chạy các hệ điều hành phổ biến hiện nay như iOS (iPhone, iPad, iPod touch), Android, Windows Mobile, các điện thoại hỗ trợ javaMobile Token: Phần mềm cài đặt trên các thiết bị di động (điện thoại di động/ máy tính bảng) chạy các hệ điều hành phổ biến hiện nay như iOS (iPhone, iPad, iPod touch), Android, Windows Mobile, các điện thoại hỗ trợ java</p>
          <p>Tham khảo điều khoản, điều kiện của dịch vụ <a href="#">TẠI ĐÂY</a></p>
          <p>Tham khảo các câu hỏi thường gặp <a href="#">TẠI ĐÂY</a></p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="tab5" class=" sec-b   ">
  <div class="container"  >
        <div class="max950">  

      <div class="row list-item">
        <?php for($i=1;$i<=2;$i++) { ?>
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Điều kiện đăng ký và sử dụng</h4>
            <div class="widget-content entry-content">
              <ul>
                <li>Có đầy đủ năng lực pháp luật dân sự và năng lực hành vi dân sự</li>
                <li>Khách hàng là cá nhân/ tổ chức mở TKTT tại MB</li>
                <li>Khách hàng sử dụng thuê bao di động của một trong các mạng viễn thông tại Việt Nam</li>
              </ul>
              <p>Tham khảo điều khoản, điều kiện của dịch vụ <a href="#">TẠI ĐÂY</a></p>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      </div>  
    
  </div>

</section>

<section id="tab6"  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
            <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $a_h1 = [
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
                    ];
            $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
            for($i=1;$i<=8;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>