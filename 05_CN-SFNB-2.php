<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >Gia đình tôi yêu</h1>
        <div class="efch-3 ef-img-l desc">Tặng 1 triệu cho mỗi con  <br> Miễn phí trọn đời cho bố mẹ</div>
        </div>
    </div>
    <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-5.jpg">
</section>


<?php include '_block/block_3.php';?>
<?php include '_block/block_4.php';?>


<section class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">Hướng dẫn dăng ký</h2>
    </div>    
    <div class="row list-item">
      <?php 
      $a_5_1 = ['other/Fast.svg','other/Fast.svg'];
      $a_5_2 = ['LỢI ÍCH','Bảo lãnh online','Không cần đăng ký','Xác nhận tại MB','Bảo lãnh online','Không cần đăng ký'];
      $a_5_3 = ['Khách hàng được Tổng cục thuế xác nhận hoàn thiện nghĩa vụ nộp thuế ngay khi trích nợ thành công trên tài khoản tại MB','Cho phép khách hàng có thể gửi các đề nghị và hồ sơ giao dịch phát hành/ sửa đổi/ giải tỏa bảo lãnh qua eMB','Nhà nhập khẩu được MB ứng trước tiền hàng để thanh toán cho Người hưởng lợi. Do đó, thời gian trả chậm tiền đến 360 ngày','Khách hàng được Tổng cục thuế xác nhận hoàn thiện nghĩa vụ nộp thuế ngay khi trích nợ thành công trên tài khoản tại MB','Cho phép khách hàng có thể gửi các đề nghị và hồ sơ giao dịch phát hành/ sửa đổi/ giải tỏa bảo lãnh qua eMB','Nhà nhập khẩu được MB ứng trước tiền hàng để thanh toán cho Người hưởng lợi. Do đó, thời gian trả chậm tiền đến 360 ngày'];
      for($i=1;$i<=3;$i++) { ?>
      <div class="col-sm-6 col-md-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/Fast.svg" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
            <div class="desc"><?php echo $a_5_3[$i-1]; ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section class=" sec-b   ">
  <div class="container"  >
    <div class="max950">  
      <div class="row list-item">
        <?php for($i=1;$i<=2;$i++) { ?>
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Gói eMB Advance</h4>
            <div class="widget-content entry-content">
              <div>Cho phép khách hàng thực hiện cả các dịch vụ tài chính và phi tài chính, bao gồm:</div>
              <div>- Các tính năng của gói Basic;</div>
              <div>- Tiết kiệm số</div>
              <div>- Chuyển tiền</div>
              <ul>
                <li>Chuyển tiền giữa các tài khoản của Khách hàng…</li>
                <li>Chuyển tiền liên ngân hàng</li>
                <li>Chuyển tiền nội bộ</li>
                <li>Chuyển tiền nội bộ theo lô</li>
                <li>Chuyển tiền liên ngân hàng theo lô</li>
                <li>Chuyển khoản qua thẻ</li>
                <li>Chuyển khoản qua MBS</li>
              </ul>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>  
  </div>
</section>


<section class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
    <div class="row center end">
      <div class="col-lg-6">

        <div class="single_video  tRes_16_9 " data-id="2UrWPUAr68A" data-video="autoplay=1&controls=1&mute=0"> 
          <img class="lazy-hidden" data-lazy-type="image"  data-lazy-src="https://via.placeholder.com/1600x900" src="https://via.placeholder.com/10x6" alt=""> <span class="btnvideo"><i class="icon-play"></i></span>
        </div>

      </div>
      <div class="col-lg-6">
        <div class="divtext ">
          <h2 class="ht ">Ý kiến khách hàng</h2>
          <div class="desc max555">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie.</div>
        </div>
      </div>
    </div>
  </div>
</section>

 
 <?php include '_block/cau_hoi.php';?>

<section class=" sec-b ">
  <div class="container"  >
		<div class=" b-tl-1">
		  <div class="inner">
		      <h2 class="ht">Tài liệu & biểu phí</h2>
		      <div class="desc">Để xem danh sách tất cả các loại phí và tài liệu liên quan đến dịch vụ, bạn hãy nhấp vào nút bên cạnh để tải về.</div>
		    </div>
		    <a class="btn lg btn-2" href="#">xem và tải về</a>
		</div>
  </div>
</section>

<section  class="sec-b " >
    <div class="container">
        <div class="entry-head text-center">
            <h2 class="ht efch-1 ef-img-l">Ưu đãi</h2>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            for($i=1;$i<=10;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $i; ?> Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head text-center">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>