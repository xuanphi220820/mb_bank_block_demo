<?php include 'include/index-top.php'; ?>
<section class=" banner-heading-1 home next-shadow">
	<div class="container">
		<div class="divtext">
			<h1 class=" efch-2 ef-img-l line2">Trải nghiệm vượt trội cùng App MB Bank</h1>
			<p class=" efch-3 ef-img-l cl1">Ngân hàng kết nối</p>
			<div class=" efch-4 ef-img-l"><a class="btn" href="./43_mb_bank_app.php">Tìm hiểu thêm</a></div>
		</div>
	</div>
	<img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-0.jpg" src="">
</section>
<section class="sec-tb sec-h-1 lazy-hidden group-ef">
	<div class="container">
		<div class="entry-head text-center">
			<h2 class="ht efch-1 ef-img-t">Hãy để MB hỗ trợ bạn</h2>
		</div>
		<div class="menuicon">
			<?php
			$a_h1_2 = ['Ngân hàng số', 'Đăng ký online', 'Tỷ giá', 'Lãi suất', 'Công cụ', 'Tư vấn & Hỗ trợ'];
			$link = ['30_sp_ngan_hang.php', '25_dang_ky_online_1.php', '	20_ti_gia.php', '20_nha_dau_tu_6.php', 'tho_tool_1.php', '21_FAQ_tab.php'];
			$img = ['bank.svg', 'ung-dung-2.svg', 'money-4.svg', 'bieu-do.svg', 'tool.svg', 'call.svg'];
			for ($i = 1; $i <= 6; $i++) { ?>
				<div class="item  efch-<?php echo $i + 2; ?> ef-img-t">
					<a href="./<?php echo $link[$i - 1] ?>" class="link">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
						</div>
						<div class="title"><?php echo $a_h1_2[$i - 1]; ?></div>
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<section class="sec-tb sec-h-2 bg-gray lazy-hidden group-ef">
	<div class="container">
		<div class="entry-head text-center">
			<h2 class="ht efch-1 ef-img-t">Sản phẩm nổi bật</h2>
		</div>
		<div class="row list-item list-1">
			<?php

			$img = ['Giadinhtoiyeu.png','AppMBbank.png'];
			$a_h2_1 = ['Gói dịch vụ', 'Sản phẩm'];
			$a_h2_2 = ['gia đình tôi yêu', 'App MBBank'];

			for ($i = 1; $i <= 2; $i++) { ?>
				<div class="col-md-6 efch-<?php echo $i + 1; ?> ef-img-t">
					<a href="./42_CN_SPNB.php" class="item tRes_66">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/homepage/<?php echo $img[$i-1] ?>">
						<div class="divtext">
							<div class="desc"><?php echo $a_h2_1[$i - 1]; ?></div>
							<h4 class="title"><?php echo $a_h2_2[$i - 1]; ?></h4>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
		<div class="row list-item list-2">
			<?php
			$a_h2_2 = ['SMS Banking', 'MBBank Plus', 'Vay siêu nhanh'];
			$img = ['ung-dung-2.svg', 'save-money.svg', 'money-1.svg'];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-lg-3 col-6 efch-<?php echo $i + 3; ?> ef-img-t">
					<a href="./32_SMS_Banking.php" class="item">
						<div class="img">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
						</div>
						<h4 class="title"><?php echo $a_h2_2[$i - 1]; ?></h4>
						<div class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
					</a>
				</div>
			<?php } ?>
			<div class="col-lg-3 col-6 efch-7 ef-img-t">
				<div class="item item-2">
					<h5 class="title">Truy cập nhanh</h5>
					<a class="btn btn-3 radius-8" href="#"> <i><img src="assets/images/homepage/icon_the.svg"></img></i> Mở thẻ tín dụng</a>
					<a class="btn btn-3 radius-8" href="#"> <i><img src="assets/images/homepage/icon_savemoney.svg"></img></i> Tài khoản tiết kiệm</a>
					<a class="btn btn-3 radius-8" href="#"> <i><img src="assets/images/homepage/icon_building.svg"></img></i> Vay mua nhà</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">KHuyến mãi</h2>
			<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
			<?php
			$img = ['KM1','KM2','KM3','KM4','KM1','KM2','KM3','KM4'];
			$content = [
				'Thông báo danh sách Khách hàng trúng thưởng CT ...',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch ...',
				'Tại trợ các doanh nghiệp kinh doanh xăng dầu',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch ...',
				'Thông báo danh sách Khách hàng trúng thưởng CT ...',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch ...',
				'Tại trợ các doanh nghiệp kinh doanh xăng dầu',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch ...',
			];
			for ($i = 1; $i <= 8; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/homepage/<?php echo $img[$i-1] ?>.jpg">
					</div>
					<div class="divtext">
						<div class="date">01/ 12/ 2019</div>
						<h4 class="title"><?php echo $content[$i-1]?></h4>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section class="sec-b sec-h-4">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">MB KẾT NỐI</h2>
			<a class="viewall" href="40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>

		<div class="row list-item">
			<div class="col-lg-8 ">

				<div class="list-5 row ">
					<?php
					$img = ['MBKN-1.jpg', 'MBKN-2.jpg'];
					$title = [
								'Thông báo danh sách khách hàng trúng thưởng CT vay mua nhà,...',
								'Ưu đãi lãi suất khi vay mua nhà'
							];
					for ($i = 1; $i <= 2; $i++) { ?>
						<div class="col-md-6">
							<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
								<div class="img tRes_71">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/homepage/<?php echo $img[$i-1] ?>">
								</div>
								<div class="divtext">
									<div class="date">01/ 12/ 2019</div>
									<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
									<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="list-6">
					<?php
					$img = ['MBKN-3.jpg', 'MBKN-4.jpg', 'MBKN-5.jpg'];
					$con = ['Cho vay mua, xây dựng, sửa chữa nhà, đất',
					'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
					'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'];
					for ($i = 1; $i <= 3; $i++) { ?>
						<a href="#" class="item item-inline-table">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/homepage/<?php echo $img[$i-1] ?>">
							</div>
							<div class="divtext">
								<h4 class="title line4"><?php echo $con[$i-1] ?></h4>
							</div>
						</a>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="sec-b sec-tigia sec-h-2">
	<div class="container">
		<div class="row list-item list-2">
			<div class="col-lg-7">
				<div class="cttab-v3 divtigia">
					<div class="tab-menu">
						<div class="tg-tab active">Tỷ giá</div>
						<div class="tg-tab">Lãi suất <span class="cl5 text-normal fs18">(%/ năm)</span></div>
					</div>
					<div class="tab-content">
						<div class="active">
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Mã NT</th>
										<th>Mua tiền mặt</th>
										<th>Mua chuyển khoản</th>
										<th>Bán ra</th>
									</tr>
									<?php
									for ($i = 1; $i <= 5; $i++) { ?>
										<tr>
											<td><img src="assets/images/flags/us.png" alt=""> USD</td>
											<td>23,180</td>
											<td>23,180</td>
											<td>23,180</td>
										</tr>
									<?php } ?>
								</table>
							</div>
						</div>
						<div>
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Kỳ hạn</th>
										<th>USD</th>
										<th>VND</th>
									</tr>
									<tr>
										<td>Không kỳ hạn</td>
										<td>0 %</td>
										<td>0.1 %</td>
									</tr>
									<tr>
										<td>1 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>2 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>3 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>5 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>6 Tháng</td>
										<td>0 %</td>
										<td>5.3 %</td>
									</tr>
									<tr>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-5">
					<div class="divquidoi">
						<h2>Quy đổi ngoại tệ</h2>

						<div>Chuyển từ</div>
						<div class="input-group">
							<span class="input-group-addon none">
								<select class="select">
									<option>VND</option>
									<option>USD</option>
								</select>
							</span>
							<input class="input" placeholder="Số tiền">
						</div>

						<div>Đến</div>
						<div class="input-group">
							<span class="input-group-addon none">
								<select class="select">
									<option>USD</option>
									<option>VND</option>
								</select>
							</span>
							<input class="input" placeholder="Số tiền">
						</div>

						<p>Cập nhật tại thời điểm 13:32 ngày 12/02/2020</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<?php include 'include/index-bottom.php'; ?>