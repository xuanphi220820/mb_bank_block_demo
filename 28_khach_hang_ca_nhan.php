<?php include 'include/index-top.php'; ?>
<section class="banner-img-1 next-shadow">
	<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/banner.jpg" src="">
</section>

<section class="sec-tb ">
	<div class="container">
		<h1 class="text-center">Nhu cầu của bạn là gì</h1>
		<div class="max750">
			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$a_h1_2 = ['Ngân hàng số','Tài khoản','Cho vay','Tiết kiệm','Thẻ','Chuyển tiền','Sản phẩm <br> nổi bật'];
				$link = ['30_sp_ngan_hang.php','31_CN-TaiKhoan.php','#','#','35_MB_Visa.php','#','#'];
				$img = ['bank.svg','user.svg','money-2.svg','save-money.svg','the-1.svg','toan-cau.svg','money-2.svg'];
				for ($i = 1; $i <= 7; $i++) { ?>
					<div class="item ">
						<a href="./<?php echo $link[$i - 1] ?>" class="link">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>">
							</div>
							<div class="title"><?php echo $a_h1_2[$i-1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>			
			<a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>
		<div class="list-5 row list-item">
			<?php
			$a_h1_2 = 	[
				'App MBBank',
				'Gia đình tôi yêu',
				'Vay siêu nhanh trên App MBBank',
				'Vay nhà đất, nhà dự án',
				'Thẻ tín dụng Quốc tế MB Visa',
				'Thẻ tín dụng Quốc tế MB JCB',
				'Thẻ tín dụng MB Visa Infinite',
				'Cho vay du học',
				'Cho vay mua ô tô',
				'Tiết kiệm dân cư',
				'Chuyển tiền du học',
				'Chuyển tiền định cư',
				'Bảo hiểm nhân thọ MBAL',
				'Bảo hiểm phi nhân thọ MIC'
			];
			$img = ['spnb-1.jpg','spnb-2.jpg','spnb-3.jpg','spnb-4.jpg','spnb-5.jpg','spnb-6.jpg','spnb-7.jpg','spnb-8.jpg','spnb-9.jpg','spnb-7.jpg','spnb-8.jpg','spnb-9.jpg','spnb-7.jpg','spnb-8.jpg',];
			$link = [
				'43_mb_bank_app.php',
				'42_CN_SPNB.php',
				'46_CN_SPNB.php',
				'35_MB_Visa.php',
				'',
				'',
				'',
				'',
				''
			];
			for ($i = 1; $i <= 3; $i++) { ?>
				<div class="col-md-4">
					<a href="<?php echo $link[$i-1] ?> " class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
						<div class="img tRes_71">
							<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/spnb/<?php echo $img[$i-1] ?>">
						</div>
						<div class="divtext">
							<h4 class="title"><?php echo $a_h1_2[$i-1]; ?></h4>
							<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>

		<?php //include '_module/pagination.php';
		?>
	</div>
</section>

<section id="tab2" class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head">
      <h2 class="ht  efch-1 ef-img-t">TÍNH NĂNG</h2>
    </div>    
    <div class="row list-item">
      <?php
      $a_h1_2 = [
                  'Bảo hiểm',
                  'MB LOYALTY',
                  'MB PRIORITY'
                ];
      $img = ['family.svg','star.svg','VIP.svg'];
      for($i=1;$i<=3;$i++) { ?>
      <div class="col-sm-6 col-md-4 col-lg-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/<?php echo $img[$i - 1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h2 class="title"><?php echo $a_h1_2[$i - 1] ?></h2>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section class="sec-b sec-h-4">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">MB kết nối</h2>
			<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>

		<div class="row list-item">
			<div class="col-lg-8 ">

				<div class="list-5 row ">
					<?php
					$img = ['MBKN-1.jpg','MBKN-2.jpg'];
					$a_h1 =	[
						'MB ra mắt hệ sinh thái số dành cho khách hàng doanh nghiệp',
						'Doanh nghiệp vừa và nhỏ được quảng cáo miễn phí với gói sản phẩm "SWE ..."'
					];
					for ($i = 1; $i <= 2; $i++) { ?>
						<div class="col-md-6">
							<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
								<div class="img tRes_71">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
								</div>
								<div class="divtext">
									<div class="date">01/ 12/ 2019</div>
									<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
									<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="list-6">
					<?php
					$img = ['MBKN-3.jpg','MBKN-4.jpg','MBKN-5.jpg'];
					$a_h1 =	[
						'Cho vay mua, xây dựng, sữa chữa nhà, đất',
						'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
						'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'
					];
					for ($i = 1; $i <= 3; $i++) { ?>
						<a href="#" class="item item-inline-table">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="divtext">
								<h4 class="title line4"><?php echo $a_h1[$i - 1] ?></h4>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>


<section  class="sec-b " >
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
			<a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Thông báo danh sách khách hàng trúng thưởng CT',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
				'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
				'Thông báo danh sách khách hàng trúng thưởng CT',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
				'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
				'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
			];
			$img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
			for($i=1;$i<=8;$i++) {?>
				<a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
					</div>
					<div class="divtext">
						<div class="date">01/ 12/ 2019</div>
						<h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
					</div>
				</a>
			<?php } ?>
		</div>          
	</div>
</section>

<?php include 'include/index-bottom.php'; ?>