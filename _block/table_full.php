<section class=" sec-tb ">
  <div class="container"  >
  	<div class="table-responsive">
  	<table class="table table-full">
  		<tr>
  			<th>Nội dung</th>
  			<th>Tiêu chí</th>
  			<th>Hạng chuẩn (Classic)</th>
  			<th>Hạng Vàng (Gold)</th>
  			<th>Hạng Bạch kim (Platinum)</th>
  		</tr>
  		<tr>
  			<td>Hạng mức tín dụng thẻ</td>
  			<td></td>
  			<td>20 triệu đồng < HMTD thẻ   ≤ 500 triệu đồng</td>
  			<td>500 triệu đồng  < HMTD thẻ ≤ 02 tỷ đồng</td>
  			<td>500 triệu đồng  < HMTD thẻ ≤ 02 tỷ đồng</td>
  		</tr>
  		<tr>
  			<td></td>
  			<td><strong>HM ứng/rút tiền mặt/ tối đa trong 01 kỳ sao kê</strong></td>
  			<td>30% HMTD thẻ được cấp</td>
  			<td></td>
  			<td></td>
  		</tr>
  		<tr>
  			<td>Hạn mức ứng/rút tiền mặt</td>
  			<td><strong>HM ứng/rút tiền mặt tối đa /1 ngày</strong></td>
  			<td>20% HMTD thẻ được cấp</td>
  			<td></td>
  			<td></td>
  		</tr>
  		<tr>
  			<td></td>
  			<td><strong>HM ứng/rút tiền mặt tối đa/1 giao dịch</strong></td>
  			<td>20 triệu đồng</td>
  			<td>20 triệu đồng</td>
  			<td></td>
  		</tr>
  		<tr>
  			<td></td>
  			<td><strong>Hạn mức chi tiêu tối đa/1 giao dịch</strong></td>
  			<td>100 triệu đồng</td>
  			<td>500 triệu đồng</td>
  			<td>1 tỷ đồng</td>
  		</tr>
  		<tr>
  			<td>Hạn mức chi tiêu</td>
  			<td><strong>Hạn mức chi tiêu tối đa/1 ngày</strong></td>
  			<td>500 triệu đồng</td>
  			<td>2 tỷ đồng</td>
  			<td>5 tỷ đồng</td>
  		</tr>
  	</table>
  	</div>
  </div>
</section>
