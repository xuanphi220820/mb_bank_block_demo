<section class="sec-tb sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">TÍNH NĂNG</h2>
    </div>   
    <div class="max750">
    	<div class="row  list-item">
        <?php 
        $a_5_1 = ['other/Fast.svg','other/Fast.svg'];
        $a_5_2 = ['Hoạt động tài khoản','Tài trợ chuỗi cung ứng'];
        $a_5_3 = ['Truy vấn thông tin số dư tài khoản; truy vấn giao dịch trong ngày; tìm kiểm giao dịch theo điều kiện (loại bút toán, số bút toán, giá trị giao dịch); quản lý tài khoản; sao kê tiết kiệm','Cho phép các doanh nghiệp trung tâm và các doanh nghiệp vệ tinh thuộc chuỗi cung cấp/phân phối của doanh nghiệp trung tâm này có thể kết nối thực hiện giao dịch với nhau qua eMB'];
        for($i=1;$i<=2;$i++) { ?>
    		<div class="col-sm-6 efch-<?php echo $i+1; ?> ef-img-t ">
          <div class="item">
            <div class="img ">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/Fast.svg" src="https://via.placeholder.com/6x4">
            </div>
            <div class="divtext">
              <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
              <div class="desc"><?php echo $a_5_3[$i-1]; ?></div>
            </div>   
          </div>     
    		</div>
        <?php } ?>
    	</div>
    </div> 
  </div>
</section>