<section class="sec-tb sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">LỢI ÍCH</h2>
    </div>    
  	<div class="row list-item">
      <?php 
      $a_5_1 = ['other/Fast.svg','other/Fast.svg'];
      $a_5_2 = ['MỨC CHO VAY','Thời hạn CHO VAY','Thủ tục đơn giản','PHƯƠNG THỨC TRẢ NỢ','MỨC CHO VAY','Thời hạn CHO VAY','Thủ tục đơn giản','PHƯƠNG THỨC TRẢ NỢ '];
      $a_5_3 = ['Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Lãi trả định kỳ hoặc cuối kỳ tính theo niên kim cố định','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Tối đa 80% nhu cầu tài chính của khách hàng','Lãi trả định kỳ hoặc cuối kỳ tính theo niên kim cố định'];
      for($i=1;$i<=8;$i++) { ?>
  		<div class="col-sm-6 col-md-6 col-lg-3 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/Fast.svg" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
            <div class="desc"><?php echo $a_5_3[$i-1]; ?></div>
          </div>   
        </div>     
  		</div>
      <?php } ?>
  	</div>
  </div>
</section>