<section id="tab1" class=" sec-tb sec-ab-1">
  <div class="container"  >
    <div class="row list-item ">
        <div class="col-lg-8">
            <div class="boxwidget">
                <h2 class="widget-title">GIỚI THIỆU</h2>
                <p>
                App MBBank là ứng dụng ngân hàng của MB trên điện thoại di động, cho phép khách hàng thực hiện hầu hết giao dịch tài chính, thanh toán hàng ngày với thao tác đơn giản, thực hiện được mọi lúc, mọi nơi. Là ứng dụng được cài đặt trên điện thoại thông minh nên thường xuyên được nâng cấp,  mang đến trải nghiệm và sự thuận tiện tối ưu nhất cho khách hàng   </p>                       
            </div>    
            <div class="boxwidget-2">
                <p>Nếu bạn muốn có thêm thông tin về sản phẩm Vay siêu nhanh, hãy liên hệ <br>   với chúng tôi qua:</p>   
                <div class="fs16 cl1 b">
                    <div class="row grid-space-60">
                        <div class="col-md-5">
                            <div class="phone-bl4">
                                <span>1900 545426</span>
                                <i class="icon-arrow-1"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="phone-bl4">
                                <span>(84-24) 3767 4050 (quốc tế gọi về)</span>
                                <i class="icon-arrow-1"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                  
        </div>
        <div class="col-lg-4 sidebar">
            <div class="widget widget-tuvan">
                <h3 class="widget-title">Đăng ký tư vấn</h3>
                <div class="widget-content">
                    <form class="form-tuvan ">
                        <input class="input" placeholder="Nhập họ tên">
                        <input class="input" placeholder="Nhập email">
                        <input class="input" placeholder="Nhập số điện thoại">
                        <textarea class="input" placeholder="Lời nhắn"></textarea>
                        <a class="btn full lg ">Đăng ký ngay</a> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
