<div id="search-result">
	<section id="top-search-result">
		<div class="container">
			<a href="./01_index.php" id="logo"> <img src="assets/images/logo-blue.svg" alt=""></a>
			<span class="icon-close close-sg"></span>
			<form action="#" method="post" name="search" class="search-field" autocomplete="off">
				<button type="submit" name="search-submit" id="search-submit" title="" class="icon-search-2"></button>
				<button type="reset" name="cancel-btn" id="cancel-btn" title="Xóa text" class="btn-clear-text icon-close"></button>
				<input type="text" name="search-input" id="search-input" placeholder="Tìm kiếm ..." class="search-input-transparent">
			</form>
		</div>
	</section>

	<section class="sec-tb search-result">
		<div class="container">
			<div class="cttab-v3">
				<div class="tab-menu">
					<div class="active"><span>Tất cả</span></div>
					<div><span>Tin tức</span></div>
					<div><span>Sản phẩm</span></div>
				</div>
				<div class="tab-content">
					<div class="active">
						<div class="total-search">
							<p>Tìm thấy <span class="total">174</span> kết quả</p>
						</div>
						<div class="tab-inner">
							<section class=" sec-b sec-search-list ">
								<?php
								$contents = [
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015)",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015). Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2. Định hạng nhà phát hành nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2018",
										"desc" => "MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s nâng định hạng đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành dài hạn. Theo đó, định hạng của MB BANK tăng 1 bậc đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành của MB BANK. Đồng thời, Moody’s thay đổi triển vọng đối với các định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn từ tích cực lên ổn định ở bậc cao hơn."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng tín nhiệm định kỳ năm 2016 vào tháng 10/2016. Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "MB BANK được Moody’s nâng triển vọng lên Tích cực",
										"desc" => "MB BANK được Moody^rsquo;s nâng triển vọng lên Tích cực. Thông tin chi tiết xin vui lòng liên hệ:. Vui lòng truy cập website http://www.moodys.com để biết thêm chi tiết."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng. Đồng thời, Moody’s giữ nguyên định hạng với Triển vọng chung ở mức Tích cực đối với định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn và Ổn định đối với định hạng tiền gửi ngoại tệ dài hạn."
									]
								]
								?>
								<?php foreach ($contents as $key => $item):?>
									<div class="search-item">
										<a href="#"><h3 class="ctext"><?php echo $item["title"] ?></h3></a>
										<p><?php echo $item["desc"] ?></p>
									</div>
								<?php endforeach;?>
							</section>
							<?php include '_module/pagination.php' ?>
						</div>
					</div>
					<div>
						<div class="total-search">
							<p>Tìm thấy <span class="total">274</span> kết quả</p>
						</div>
						<div class="tab-inner">
							<section class=" sec-b sec-search-list ">
								<?php
								$contents = [
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015)",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015). Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2. Định hạng nhà phát hành nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2018",
										"desc" => "MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s nâng định hạng đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành dài hạn. Theo đó, định hạng của MB BANK tăng 1 bậc đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành của MB BANK. Đồng thời, Moody’s thay đổi triển vọng đối với các định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn từ tích cực lên ổn định ở bậc cao hơn."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng tín nhiệm định kỳ năm 2016 vào tháng 10/2016. Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "MB BANK được Moody’s nâng triển vọng lên Tích cực",
										"desc" => "MB BANK được Moody^rsquo;s nâng triển vọng lên Tích cực. Thông tin chi tiết xin vui lòng liên hệ:. Vui lòng truy cập website http://www.moodys.com để biết thêm chi tiết."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng. Đồng thời, Moody’s giữ nguyên định hạng với Triển vọng chung ở mức Tích cực đối với định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn và Ổn định đối với định hạng tiền gửi ngoại tệ dài hạn."
									]
								]
								?>
								<?php foreach ($contents as $key => $item):?>
									<div class="search-item">
										<a href="#"><h3 class="ctext"><?php echo $item["title"] ?></h3></a>
										<p><?php echo $item["desc"] ?></p>
									</div>
								<?php endforeach;?>
							</section>
							<?php include '_module/pagination.php' ?>
						</div>
					</div>
					<div>
						<div class="total-search">
							<p>Tìm thấy <span class="total">374</span> kết quả</p>
						</div>
						<div class="tab-inner">
							<section class=" sec-b sec-search-list ">
								<?php
								$contents = [
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015)",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK (Tháng 7/2015). Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2. Định hạng nhà phát hành nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2018",
										"desc" => "MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s nâng định hạng đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành dài hạn. Theo đó, định hạng của MB BANK tăng 1 bậc đối với định hạng tiền gửi nội tệ/ngoại tệ dài hạn và định hạng nhà phát hành của MB BANK. Đồng thời, Moody’s thay đổi triển vọng đối với các định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn từ tích cực lên ổn định ở bậc cao hơn."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK tháng 10/2016. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng tín nhiệm định kỳ năm 2016 vào tháng 10/2016. Định hạng tiền gửi nội tệ/ngoại tệ dài hạn B1/B2."
									],
									[
										"title" => "MB BANK được Moody’s nâng triển vọng lên Tích cực",
										"desc" => "MB BANK được Moody^rsquo;s nâng triển vọng lên Tích cực. Thông tin chi tiết xin vui lòng liên hệ:. Vui lòng truy cập website http://www.moodys.com để biết thêm chi tiết."
									],
									[
										"title" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017",
										"desc" => "Kết quả định hạng tín nhiệm theo Moody's của MB BANK năm 2017. MB Ngân hàng quân đội (MB BANK) vừa được Tổ chức Định hạng tín nhiệm toàn cầu Moody^rsquo;s thực hiện việc rà soát và công bố định hạng. Đồng thời, Moody’s giữ nguyên định hạng với Triển vọng chung ở mức Tích cực đối với định hạng tiền gửi nội tệ dài hạn và định hạng nhà phát hành dài hạn và Ổn định đối với định hạng tiền gửi ngoại tệ dài hạn."
									]
								]
								?>
								<?php foreach ($contents as $key => $item):?>
									<div class="search-item">
										<a href="#"><h3 class="ctext"><?php echo $item["title"] ?></h3></a>
										<p><?php echo $item["desc"] ?></p>
									</div>
								<?php endforeach;?>
							</section>
							<?php include '_module/pagination.php' ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>