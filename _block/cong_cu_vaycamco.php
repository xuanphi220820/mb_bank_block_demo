<section id="mb-calculator" class="vay-xe sec-tb sec-cong-cu" >

	<div class="container"  >
	<h2 class="ht">Công cụ tính Vay cầm cố GTCG</h2>

	  <div  class="cttab-v4   ">
	    <div  class="tab-menu">
	      <div  class="menu-item active"><span>Ước tính số tiền vay</span></div>
	      <div id="listRepayment-link" class="menu-item disabled"  ><span>Ước tính số tiền trả hàng tháng</span></div>
		</div>
		
	    <div class="tab-content">
	      <div class="active">
	        <div class="tab-inner sec-cong-cu-1">
                <script>
                    var MB = MB || {};
                    MB.loan_type = 'VAY_CAM_CO';
                </script>
				<?php include '_block/vay_camco.php';?>
			</div>
	      </div>
	      <div class="">
	        <div class="tab-inner">
				<div id="listRepayment" class="table-responsive">&nbsp;</div>
	        </div>
	      </div>
		</div>
		
	  </div>
  </div>
</section>