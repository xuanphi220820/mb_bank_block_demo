<section  class="sec-tb sec-cong-cu p-tool1" >
    <link rel='stylesheet'  href='assets/js/ion.rangeSlider-master/ion.rangeSlider.min.css' type='text/css' media='all' />
    <script src="assets/js/ion.rangeSlider-master/ion.rangeSlider.min.js"></script>
	<link rel='stylesheet'  href='assets/css/th_tool.css' type='text/css' media='all' />

	<div class="container"  >
		<h2 class="ht">Công cụ tính</h2>
		
		<div class="p-tool1__select1">
			<label class="option1">Vay hạn mức
			  <input type="radio" checked="checked" name="radio">
			  <span class="checkmark1"></span>
			</label>

			<label class="option1">Vay đầu tư hạn mức cố định
			  <input type="radio" name="radio">
			  <span class="checkmark1"></span>
			</label>
		</div>

		<div  class="cttab-v4 p-tool1__box1">
		  <div class="sec-cong-cu-1">
      	<div class="row">
      		<div class="col-md-8">
						<div class="box1">
						  <div class="form1-group">
		      			<div class="gtitle">
		      				<span class="title">Tổng nhu cầu vốn (số tiền vay):<br><span class="form1_label1">(VNĐ)</span></span>
		      				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND/ tháng</span>
		      			</div>							
								<span class="price-range-input" data-type="single"  data-min="1000000" data-max="20000000" data-from="1000000" data-step="1000000"></span>
							</div>	
						</div>

						<!-- <div class="group-range-prcie">
        			<div class="gtitle">
        				<span class="title">1. Thu nhập hàng tháng của tôi là:</span>
        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND/ tháng</span>
        			</div>							
							<span class="price-range-input" data-type="single"  data-min="5000000" data-max="100000000" data-from="1000000" data-step="1000000"></span>
						</div>	
						
						<div class="group-range-prcie">
	      			<div class="gtitle">
	      				<span class="title">2. Chi tiêu hàng tháng của tôi là:</span>
	      				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND/ tháng</span>
	      			</div>							
							<span class="price-range-input" data-type="single"  data-min="1000000" data-max="20000000" data-from="1000000" data-step="1000000"></span>
						</div>	
						
						<div class="group-range-prcie">
	      			<div class="gtitle">
	      				<span class="title">3. Kỳ hạn vay:</span>
	      				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> tháng</span>
	      			</div>							
							<span class="price-range-input" data-type="single"  data-min="0" data-max="240" data-from="0" data-step="1"></span>
						</div>	
						
						<div class="group-range-prcie">
	      			<div class="gtitle">
	      				<span class="title">4. Lãi suất:</span>
	      				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> <span class="price">%</span> VND/ tháng</span>
	      			</div>							
							<span class="price-range-input" data-type="single"  data-min="0" data-max="20" data-from="0" data-step="0.5"></span>
						</div> -->	
      		</div>
      		
      		<div class="col-md-4">
      			<div class="result">
      				<div class="divtext">
      				<div>Tổng số tiền bạn có thể vay (VND)</div>
      				<span class="total">0</span>
      				</div>
      			</div>	
      		</div>
      	</div>

      	<div class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</div>
      </div>    
		</div>
	
	  <!-- <div  class="cttab-v4   ">
	    <div  class="tab-menu">
	      <div  class="active"><span>Ước tính số tiền vay</span></div>
	      <div  ><span>Ước tính số tiền trả hàng tháng</span></div>
	    </div>
	    <div class="tab-content">
	      <div class="active">
	        <div class="tab-inner sec-cong-cu-1">

	        	<div class="row">
	        		<div class="col-md-8">
        			
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">1. Thu nhập hàng tháng của tôi là:</span>
		        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND/ tháng</span>
		        			</div>							
							<span class="price-range-input" data-type="single"  data-min="5000000" data-max="100000000" data-from="1000000" data-step="1000000"></span>
						</div>	
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">2. Chi tiêu hàng tháng của tôi là:</span>
		        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND/ tháng</span>
		        			</div>							
							<span class="price-range-input" data-type="single"  data-min="1000000" data-max="20000000" data-from="1000000" data-step="1000000"></span>
						</div>	
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">3. Kỳ hạn vay:</span>
		        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> tháng</span>
		        			</div>							
							<span class="price-range-input" data-type="single"  data-min="0" data-max="240" data-from="0" data-step="1"></span>
						</div>	
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">4. Lãi suất:</span>
		        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> <span class="price">%</span> VND/ tháng</span>
		        			</div>							
							<span class="price-range-input" data-type="single"  data-min="0" data-max="20" data-from="0" data-step="0.5"></span>
						</div>	

	        		</div>
	        		<div class="col-md-4">
	        			<div class="result">
	        				<div class="divtext">
	        				<div>Tổng số tiền bạn có thể vay (VND)</div>
	        				<span class="total">0</span>
	        				</div>
	        			</div>
	        			
	        		</div>
	        	</div>
	        	<div class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</div>
	        </div>
	      </div>
	      <div >
	        <div class="tab-inner">
	          2
	        </div>
	      </div>


	    </div>
	  </div> -->

  </div>

	<script>
	(function($){
	$(document).ready(function(){

	function beginIonRange(e) {   	
	    e.each(function () {
			var $wslide = $(this),
				$range = $(this).find(".price-range-input"),
			    $input = $(this).find(".price-input-1"),
			    $input2 = $(this).find(".price-input-2"),
			    instance,
			    type = parseInt($range.data('type')),
			    grid = parseInt($range.data('grid')),
			    min = parseInt($range.data('min')),
			    max = parseInt($range.data('max')),
			    from = parseInt($range.data('from')),
			    to = parseInt($range.data('to')),
			    step = parseInt($range.data('step')),
			    prefix = parseInt($range.data('prefix')),
			    postfix = parseInt($range.data('postfix'));
			    //if(!type) { type = 'single';}

			$range.ionRangeSlider({
	            type: type,
	            grid: grid,			
			    min: min,
			    max: max,
			    from: from,
			    to: to,
			    step: step,

		        prefix: prefix,
		        postfix: postfix,

			    onStart: function(data) {
			        $input.prop("value", data.from);
			        $input2.prop('value', data.to);
			    },
			    onChange: function(data) {
	
			    	// if(data.from<=min){
			     //    	data.from = '';
			     //    }
			    	if(data.to>=max){
			        	data.to = '';
			        }
			        $input.prop("value", data.from);
			        $input2.prop('value', ((data.to == max) ? max : data.to));
			    }
			});
			instance = $range.data("ionRangeSlider");
			$input.on("change keyup", function() {
			    var val = parseInt($(this).prop("value")),
			    	val2 = parseInt($input2.prop("value"));
			    // validate
			    if (val < min) {
			        val = min;
			        $(this).val(min)
			    } else if (val > val2) { 
			        val = val2;
			        $(this).val(val2)
			    }
			    instance.update({
			        from: val,
			    });

			});
			$input2.on("change keyup", function() {
			    var val = parseInt($(this).prop("value")),
			    	val2 = parseInt($input2.prop("value"));

 				    	
			    // validate
			    if (val < val2) {
			        val = val2;
			        $(this).val(val2)
			    } else if (val > max) {
			        val = max;
			        $(this).val(max)
			    }
			    instance.update({
			        to: val,
			    });
			});
	    }); 
	}
	beginIonRange($('.group-range-prcie'));



	});
	})(jQuery);
	</script>

</section>