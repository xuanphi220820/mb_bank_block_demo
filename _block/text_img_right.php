<section class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
  	<div class="row center end">
  		<div class="col-lg-6">
  			<div class="img tRes_66 efch-2 ef-img-r ">
  				<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/1000x600" src="https://via.placeholder.com/10x6">
  			</div>
  		</div>
  		<div class="col-lg-6">
  			<div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">Điều kiện vay vốn</h2>
			    <ul class="efch-3 ef-tx-t">
			    	<li>Khách hàng có đủ năng lực pháp luật dân sự và năng lực hành vi dân sự.</li>
			    	<li>Khách hàng có độ tuổi từ đủ 18 tuổi đến không quá 70 tuổi tại thời điểm kết thúc khoản vay.</li>
			    	<li>Khách hàng có hộ khẩu/sổ tạm trú (KT3) tại Tỉnh/Thành phố nơi MB có trụ sở.</li>
			    </ul> 	
			    <a class="btn" href="#">ĐĂNG KÝ NGAY</a>	
  			</div>
  		</div>
  	</div>
  </div>
</section>
 