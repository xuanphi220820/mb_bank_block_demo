<section class=" sec-tb sec-pav ">
  <div class="container"  >
  		<div class="entry-head"><h2 class="ht  text-center">Phương án vay</h2></div>
		<form>
			<div class="text-center mb-40">
		        <label class="radio ">
		          <input name="radio" type="radio" checked="">
		          <span></span>
		            Tính toán khoản vay
		        </label>	
		        &nbsp; &nbsp;
		        <label class="radio ">
		          <input name="radio" type="radio">
		          <span></span>
		            Tính lãi tiền gửi
		        </label>			        			
			</div>	
			<div class="row">
				<div class="col-md-8">
					<label class="rowlabel">
						<h6>Tính toán lịch trả nợ</h4>
						<select class="select">
							<option>Tính lịch trả nợ trên dư nợ giảm dần</option>
							<option>Tính lịch trả nợ trên dư nợ giảm dần</option>
						</select>
					</label>
					<label class="rowlabel">
						<h6>Số tiền vay (*)</h4>
						<select class="select">
							<option>Tính lịch trả nợ trên dư nợ giảm dần</option>
							<option>Tính lịch trả nợ trên dư nợ giảm dần</option>
						</select>
					</label>
					<div class="row">
						<div class="col-6">	
							<label class="rowlabel">
								<h6>Thời hạn vay (Month) (*)</h4>
								<select class="select">
									<option>1</option>
									<option>2</option>
								</select>
							</label>	
													
						</div>
						<div class="col-6">		
							<label class="rowlabel">
								<h6>Lãi suất vay  (%/Year ) (*)</h4>
								<select class="select">
									<option>1</option>
									<option>2</option>
								</select>
							</label>								
						</div>						
					</div>		
					<label class="rowlabel">
						<h6>Ngày giải ngân</h4>
						<input class="input" type="date" />
					</label>	

					<div class="text-center">
						<button class="btn lg btn-2">Tính toán <i class="icon-arrow-6"></i>	</button>
					</div>									

				</div>
				<div class="col-md-4">
					<div class="widget-ketqua">
						<h5 class="widget-title">Kết quả</h5>
						<div class="widget-content">
							<p>Tổng số tiền lãi phải trả (VND):</p>
							<p><div class="result kq-1">0 VND</div></p>
							<p>Tổng số tiền gốc và lãi phải trả (VND):</p>
							<p><div class="result kq-2">0 VND</div></p>							
						</div>
					</div>
					<p class="note">Lưu ý: Bảng tính chỉ mang tính chất tham khảo</p>
					
				</div>
			</div>
		</form>			
  </div>
</section>