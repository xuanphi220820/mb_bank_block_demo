<script>
    $ = jQuery;
</script>
<script src="assets/js/vayxe/lib.js"></script>
<script src="assets/js/vayxe/loan-calculator.js"></script>
<script src="assets/js/vayxe/rangeslider.js"></script> 
<script src="assets/js/vayxe/muaxe.js"></script> 
<link rel="stylesheet" href="assets/css/vayxe.css"> 
   
<div class="tool-calculation row">
    <div class="col-md-8">
        <div class="form-control  form-focus mb-40">
            <span class="g-title ph_input">1. Ước tính giá xe (VNĐ)</span>
            <input required type="tel" id="price_car" class="input pull-right value-current field_input" maxlength="14" unit="VNĐ" pattern="integer" data-single="true">
        </div>
        <div id="personal-loan-borrow">
            <div class="range-calculotor clearfix mb-40">
                <div class="group-line">
                    <span class="g-title  ph_input">2. Số tiền muốn vay(VNĐ) (*)</span>
                    <input id="monthly_expenses_field" type="tel" class="input pull-right value-current field_input" unit="VNĐ" maxlength="14" pattern="integer" data-placement="top" data-single="true" data-decimal-separator="," data-thousands-separator="." data-value="100000000">
                </div>

                <div class="mb-toolbar-range-sllider mb-toolbar-range-sllider02">
                    <div class="calculator-wraper">
                        <div class="range-container">
                            <input class="input" id="monthly_expenses" type="range" value="100000000" min="100000000" max="10000000000" unit="VNĐ" name="monthly_expenses" data-placement="top" data-single="true" data-mt-max="false"/>
                            <div class="min-max-value">
                                <span class="min-value"></span>
                                <span class="max-value"></span>
                            </div>
                        </div>
                    </div>
                    <script>
                        var PERSONAL_LOAN_CAL_DATA = {
                                    config: {
                                        single: true,
                                        business: false,
                                        sign: 'VND',
                                        lang: 'vi'
                                    },
                                    data: [{
                                        id: 'prod_personal1_1m',
                                        name: 'Loan secured by valuable paper (term < 12 month)',
                                        borrow: {
                                            monthly_expenses: {
                                                min: 100000000,
                                                max: 10000000000,
                                                unit: 'VNĐ',
                                                step: 1000,
                                                value: 100000000
                                            },
                                    loan_term: { // months
                                        min: 1,
                                        max: 84,
                                        step: 1,
                                        value: 1
                                    }
                                },
                            }]
                        }
                    </script>
                </div>
                
            </div>

            <div class="range-calculotor clearfix mb-40">
                <div class="group-line">
                    <span class="g-title  ph_input">3. Kỳ hạn vay (Tháng)</span>
                    <input id="loan_term_field" type="tel" class="input pull-right value-current field_input" onkeypress="return check(event,value)" oninput="checkLength()" pattern="integer" maxlength="1" data-placement="top" data-single="true" data-value="1">
                </div>
                <div class="mb-toolbar-range-sllider mb-toolbar-range-sllider02">
                    <div class="calculator-wraper">
                        <div class="range-container">
                            <input id="loan_term" class="input range-car-loan" type="range" value="1" min="1" max="84" unit="Tháng" name="loan_term" data-mt-max="false">
                            <div class="min-max-value">
                                <span class="min-value"></span>
                                <span class="max-value"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-btn-dk-congcu">
                <a onclick="showResult();" href="javascript:void(0)" class="showResult btn md mb-btn-dk02 disabled"><span>Xem kết quả</span></a>
            </div>

            <div class="info-price">
                <div id="info_price" class="note">
                    <ul class="">
                        <li>(*)Tùy thuộc vào giá trị xe</li>
                        <li>Tối đa 75% giá trị xe nếu giá trị xe <= 1.5 tỷ</li>
                        <li>Tối đa 70% giá trị xe nếu giá trị xe từ 1.5 tỷ đến 3 tỷ</li>
                        <li>Tối đa 55% giá trị xe nếu giá trị xe > 3 tỷ</li>
                    </ul>
                    <p>
                    (**) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank
                    </p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="mb-result-calculation col-md-4">
        <div id="parent-box" class="mb-result-calculation-slider result">
            <div class="divtext">
                <div>Tổng số tiền bạn có thể vay (VND)</div>
                <span class="total">0</span>
            </div>
        </div>
    </div>
</div>
