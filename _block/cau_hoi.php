<section class=" sec-b sec-cauhoi ">
  <div class="container"  >
  	<div class="entry-head text-center">
  		<h2 class="ht ">Câu hỏi thường gặp</h2>
  	</div>			
    <div class="accodion accodion-1">
        <?php
        $question = ['1. Tôi phải làm những thủ tục gì khi nhận thừa kế cổ phiếu từ người thân?',
        '2. Khi tôi không lưu ký cổ phiếu MBB các quyền lợi của cổ đông có bị ảnh hưởng không?',
        '3. Tôi muốn biết những đối tượng nào phải thực hiện các thủ tục công bố thông tin, báo cáo cơ quan nhà nước khi thực hiện giao dịch cổ phiếu MBB',
        '4. Tôi là cổ đông của MB và tôi muốn thực hiện điều chỉnh thông tin thì cần liên hệ với bộ phận nào?',
        '5. Khi tôi không lưu ký cổ phiếu MBB các quyền lợi của cổ đông có bị ảnh hưởng không?',
        '6. Tôi muốn biết những đối tượng nào phải thực hiện các thủ tục công bố thông tin, báo cáo cơ quan nhà nước khi thực hiện giao dịch cổ phiếu MBB',
        '7. Tôi là cổ đông của MB và tôi muốn thực hiện điều chỉnh thông tin thì cần liên hệ với bộ phận nào?'];
        for($i=1;$i<=7;$i++) {
        ?>
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span> <?php echo $question[$i-1] ?> </span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content entry-content" >
                    <div class="inner">
                    <p>Quý cổ đông làm thủ tục quyền sở hữu do thừa kế tại công ty chứng khoán nơi cổ đông mở tài khoản nếu cổ đông đã lưu ký cổ phiếu.<br>
                    <br>Trường hợp chưa lưu ký, cổ đông thực hiện theo quy định tại Điều 28 Quy chế hoạt động đăng ký chứng khoán (Ban hành kèm theo Quyết định số 22/QĐ-VSD ngày 13 tháng 3 năm 2015 của Tổng giám đốc Trung tâm Lưu ký chứng khoán Việt Nam) <br>
                    <br>Chi tiết vui lòng xem tại ĐÂY</p>
                    </div>
                </div>
            </div>
        <?php
        } ?>
    </div>
	<div class="text-center">
		<button class="btn lg">xem thêm</button>
	</div>            
  </div>
</section>