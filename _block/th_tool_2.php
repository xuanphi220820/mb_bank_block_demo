<section  class="sec-tb sec-cong-cu p-tool1" >
	<!-- <link rel='stylesheet'  href='assets/js/ion.rangeSlider-master/ion.rangeSlider.min.css' type='text/css' media='all' /> -->
	<!-- <script src="assets/js/ion.rangeSlider-master/ion.rangeSlider.min.js"></script> -->
	<link rel='stylesheet'  href='assets/css/th_tool.css' type='text/css' media='all' />
	<!-- <script src="assets/js/auto-numeric/main.js"></script> -->
	<script src="https://unpkg.com/autonumeric@4.5.13/dist/autoNumeric.min.js"></script>
	<div class="container"  >
		<h2 class="ht">Công cụ tính</h2>
		
		<div  class="cttab-xx  sec-b">
			<div class="w-menu-over">
				<div class="p-tool1__select1 p-tool1__select1-js">
					<label class="option1">Vay hạn mức
						<input type="radio" checked="checked" name="radio-loan1" value="1">
						<span class="checkmark1"></span>
					</label>
					<label class="option1">Vay đầu tư hạn mức cố định
						<input type="radio" name="radio-loan1" value="2">
						<span class="checkmark1"></span>
					</label>
				</div>
			</div>

			<div class="tab-content">
				<div class="active">
					<div class="tab-inner  ">
						<div class="form-vay-von">
							<div class="row ">
								<div class="col-md-7 ">
									<div class="inner">
										<div class="row">
											<div class="col-md-7">
												<h5 class="title">Tổng nhu cầu vốn (số tiền vay): <br> <span class="note">(VNĐ)</span></h5>
											</div>
											<div class="col-md-5">
												<div><input id="loan-needs-field" type="tel" class=" input"  name=""  value="0" placeholder="Nhập số tiền (*)"></div>
												<div class="price">1,000,000,000</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-7">
												<h5 class="title">Vốn tự có:<br> <span class="note">(VNĐ)</span></h5>
											</div>
											<div class="col-md-5">
												<div><input id="funds-field" type="tel" class=" input"  name=""  value="0" placeholder="Nhập số tiền (*)"></div>
												<div class="price">1,000,000,000</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-7">
												<h5 class="title">Nhu cầu vay vốn từ MB:<br> <span class="note">(VNĐ)</span></h5>
											</div>
											
											<div class="col-md-5">
												<div><input id="loan-needs-mb-field" type="tel" class=" input"  name=""  value="0" placeholder="Nhập số tiền (*)"></div>
												<div class="price">1,000,000,000</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-7">
												<h5 class="title">Kỳ hạn vay:<br> <span class="note">(THÁNG)</span></h5>
											</div>
											<div class="col-md-5">
												<!-- <select class="select">
													<option>12 tháng</option>
													<option>10 tháng</option>
												</select> -->
												<div><input id="term-field" type="tel" class=" input"  name=""  value="1" placeholder="Nhập số tháng(*)"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="result th-result-js">
										<div class="row">
											<div class="col-md-6">
												<h5 class="title">Khoản vay:</h5>
											</div>
											<div class="col-md-6">
												<span class="t2">0</span>
												<span class="t3">VNĐ</span>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
												<h5 class="title">Vốn tự có:</h5>
											</div>
											<div class="col-md-6">
												<span class="t4">0</span>
												<span class="t3">VNĐ</span>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<h5 class="title">Kỳ hạn vay:</h5>
											</div>
											<div class="col-md-6">
												<span class="t5">1 tháng</span>
											</div>
										</div>
										<!-- <div class="row">
											<div class="col-md-6">
												<h5 class="title">Tiền lãi hàng tháng:</h5>
											</div>
											<div class="col-md-6">
												<span class="t4">0</span>
												<span class="t3">VNĐ</span>
											</div>
										</div> -->
										<div class="row">
											<div class="col-md-6">
												<h5 class="title">Tiền gốc hàng tháng:</h5>
											</div>
											<div class="col-md-6">
												<span class="t4">0</span>
												<span class="t3">VNĐ</span>
											</div>
										</div>
										<!-- <div class="total row">
											<div class="col-md-6">
												<h5 class="title">Tổng tiền phải trả:</h5>
											</div>
											<div class="col-md-6">
												<span class="t2">0</span>
												<span class="t3">VNĐ</span>
											</div>
										</div> -->
									</div>
								</div>
							</div>
							<p class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</p>
							<a class="btn" onclick="showResult()" href="javascript:void(0)">Xem bảng</a>
						</div>
					</div>
				</div>
				<!-- <div >
					<div class="tab-inner">
						2
					</div>
				</div> -->
			</div>
		</div> <!-- end tab-->
		
		<div class="sec-b">
			<div class="accodion accodion-3">
				<div class="accodion-tab ">
					<input type="checkbox" id="chck_1_1" >
					<label class="accodion-title uppercase" for="chck_1_1" ><span> Xem bảng tính trả tiền hàng tháng</span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
					<div class="accodion-content entry-content" >
						<div class="inner">
							<div class="table-responsive th-result-table-js">
								<table class="table table-full">
									<tr>
										<th width="20%" align="center" colspan="2">Kỳ trả nợ</th>
										<th width="20%" align="center">Số gốc còn lại</th>
										<th width="20%" align="center">Gốc</th>
										<th width="20%" align="center">Lãi</th>
										<th width="20%" align="center">Tổng gốc + Lãi</th></tr>
										<?php for($i=1;$i<=6;$i++) { ?>
										<!-- <tr>
											<td>01</td>
											<td>13/04/2020</td>
											<td>0</td>
											<td>0</td>
											<td>0</td>
										</tr> -->
										<?php } ?>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			
		<?php include 'th_list_tools.php'; ?>
	</div>
	<script>
		var MB = MB || {};
		MB.loan_type = 'VAY_SXKD';
	</script>
		
	<script>
		var INTEREST_RATE_YEAR = 12;
		var CONFIG_INTEREST_RATE_YEAR = INTEREST_RATE_YEAR/100;
		var CONFIG_MAX_PERCENT_LOAN_LIMIT = 0.9;
		var CONFIG_MAX_PERCENT_LOAN_INVESTMENT = 0.85;
		var CONFIG_MAX_MONTH_LOAN_LIMIT = 12;
		var CONFIG_MAX_MONTH_LOAN_INVESTMENT = 180;
		jQuery(document).ready(function($){
			var loan_needs_field = new AutoNumeric('#loan-needs-field', { 'allowDecimalPadding': false });
			var funds_field = new AutoNumeric('#funds-field', { 'allowDecimalPadding': false });
			var loan_needs_mb_field = new AutoNumeric('#loan-needs-mb-field', { 'allowDecimalPadding': false });
			/*===========action============*/
			//------------will optimize code-------------
			$("#loan-needs-field").on('change', function() {
				loan_needs_field.set($("#loan-needs-field").prop("value"));
				var loan_needs = Math.round(loan_needs_field.get());
				var user_funds = Math.round(funds_field.get());
				var loan_needs_mb = loan_needs - user_funds;
				loan_needs_mb = calculateLoadNeedsMbFollowOption(loan_needs_mb);
				if(checkValInput(loan_needs)) {
					loan_needs_field.set(loan_needs);
				} else loan_needs_field.set(0);
				checkLoanNeedsMb(loan_needs_mb, funds_field);
			});
			/*-----------------------*/
			$("#funds-field").change(function() {
				funds_field.set($(this).prop("value"));
				var loan_needs = Math.round(loan_needs_field.get());
				var user_funds = Math.round(funds_field.get());
				var loan_needs_mb = loan_needs - user_funds;
				loan_needs_mb = calculateLoadNeedsMbFollowOption(loan_needs_mb);
				if(checkValInput(user_funds)) {
					funds_field.set(user_funds);
				} else funds_field.set(0);
				//check if loan_needs_mb < 0? alert error, else enter loan_needs_mb
				checkLoanNeedsMb(loan_needs_mb, funds_field);
			});
			/*-----------------------*/
			$("#loan-needs-mb-field").change(function() {
				loan_needs_mb_field.set($(this).prop("value"));
				var loan_needs = Math.round(loan_needs_field.get());
				var user_funds = Math.round(funds_field.get());
				var loan_needs_mb = loan_needs - user_funds;
				var val_input = Math.round(loan_needs_mb_field.get());
				loan_needs_mb = calculateLoadNeedsMbFollowOption(loan_needs_mb);
				if(val_input <= loan_needs_mb && val_input >= 0) {
					loan_needs_mb_field.set(val_input);
				} else if(val_input < 0) {
					loan_needs_mb_field.set(0);
				} else if(val_input > loan_needs_mb) {
					loan_needs_mb_field.set(loan_needs_mb);
				}
			});
			/*-----------------------*/
			$("#term-field").change(function() {
				var value = Math.round(parseInt($(this).prop("value")));
				if(value && checkValInput(value)) {
					if(isTabLoanLimit() && value > CONFIG_MAX_MONTH_LOAN_LIMIT) {
						$(this).prop("value", CONFIG_MAX_MONTH_LOAN_LIMIT);
					} else if(!isTabLoanLimit() && value > CONFIG_MAX_MONTH_LOAN_INVESTMENT) {
						$(this).prop("value", CONFIG_MAX_MONTH_LOAN_INVESTMENT);
					}
				} else $(this).prop("value", 1);
			});
			/*-----------------------*/
			$(".p-tool1__select1-js input[name=radio-loan1]").change(function() {
				$("#term-field").trigger('change');
				
				var loan_needs = Math.round(loan_needs_field.get());
				var user_funds = Math.round(funds_field.get());
				var loan_needs_mb = loan_needs - user_funds;
				
				loan_needs_mb = calculateLoadNeedsMbFollowOption(loan_needs_mb);
				if(loan_needs_mb >= 0) {
					loan_needs_mb_field.set(loan_needs_mb);
				} else {
					loan_needs_mb_field.set(0);
				}
			});
			/*-----------------------*/
			/*============function============*/
			var checkValInput = (val) => {
				if(val < 0 || val > 10000000000000) {
					return false;
				}
				return true;
			};
			//------------will optimize code-------------
			var isTabLoanLimit = () => {
				var tab = $(".p-tool1__select1-js input[name=radio-loan1]:checked");
				if(parseInt(tab.val()) === 1) {
					return true;
				}
				return false;
			}
			/*--------------------------------*/
			var checkLoanNeedsMb = (loan_needs_mb, nummeric_field = false, field_zero = false) => {
				if(checkValInput(loan_needs_mb)) {
					loan_needs_mb_field.set(loan_needs_mb);
				} else {
					alert("số tiền vay phải lớn hơn vốn tự có");
					if(nummeric_field) {
						//field_zero.prop("value", 0);
						nummeric_field.set(0);
					}
				}
			}
			/*--------------------------------*/
			function converNumber(number) {
				var strNumber="";
				strNumber= String(number);
				return strNumber.replace(/\d(?=(?:\d{3})+(?!\d))/g, '$&,')
			}
			/*--------------------------------*/
			var calculateLoadNeedsMbFollowOption = (loan_needs_mb) => {
				if(isTabLoanLimit()) {
				loan_needs_mb = Math.round((loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_LIMIT));
				} else {
					loan_needs_mb = Math.round((loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_INVESTMENT));
				}
				return loan_needs_mb;
			}
			/*--------------------------------*/
			var formatNum = function(value) {
				var lang = window.location.href ;
				var local = 'vi-VN';
				if(lang.includes('\en')){
					local='en-US';
				}
				var value_=	parseFloat(value);
				var rs=value_.toLocaleString(local);
				return rs;
			}
			/*--------------------------------*/
			var calcLoan = (summoney=2000000000,percent=CONFIG_INTEREST_RATE_YEAR,time=12 ) => {
				// var $this = $("#btnCalcLoan");
				// var price = $this.attr('data-price');
				// var month = $this.attr('data-month');
				var price = parseInt(Math.round(loan_needs_mb_field.get()));
				var month = parseInt($("#term-field").prop('value'));
				//var percent = 11.6;
				summoney = price;
				time = month;
				var goc=0, lai=0,goc_lai=0, sum_goc=0,sum_lai=0, tempGoc;
				var day,month,year;
				
				var d= new Date();
				day = d.getDate();
				month = d.getMonth() + 1;
				year = d.getFullYear();
				var strDate= day +"/" + ( (month<10)? "0" + month:month )  + "/" + year;
				
				tempGoc = summoney;
				
				var strResult="";
				strResult="<table class=\"table table-full\" >"		
				strResult +="<tr>"
									+	"<th width=\"20%\" align=\"center\" colspan=\"2\">Kỳ trả nợ</th>"
									+	"<th width=\"20%\" align=\"center\">Số gốc còn lại</th>"
									+	"<th width=\"20%\" align=\"center\">Gốc</th>"
									+	"<th width=\"20%\" align=\"center\">Lãi</th>"
									+	"<th width=\"20%\" align=\"center\">Tổng gốc + Lãi</th>"
							+"</tr>";
				strResult +="<tr>"
									+	"<td width=\"15%\" align=\"center\">"+ strDate +"</td>"
									+	"<td width=\"5%\" align=\"center\">0</td>"
									+	"<td width=\"20%\" align=\"center\">"+ converNumber(summoney) +"</td>"
									+	"<td width=\"20%\" align=\"center\"></td>"
									+	"<td width=\"20%\" align=\"center\"></td>"
									+	"<td width=\"20%\" align=\"center\"></td>"
							+"</tr>";
						
				goc = parseInt(summoney)/time;
				// tính gốc

				// console.log(percent);
								
				for(var i = 0; i<time; i++)
				{
					sum_goc = sum_goc + goc;
					lai = parseInt(tempGoc) /12 * percent; // tính lãi giảm dần
					
					//lai = parseInt(goc)/12 * percent; // tính lãi
					tempGoc = tempGoc-goc; // gốc còn lại theo từng tháng
					goc_lai = goc+lai;
					sum_lai = sum_lai + lai;
					//console.log(goc);
					
					// cộng ngày tháng
					if(month == 12)
					{
						month = 1;
						year = year+1;
					}
					else
					{
						month = month + 1;
					}
					strDate= day +"/" +( (month<10)? "0" + month:month ) + "/" + year;
					
					strResult +="<tr>"
									+	"<td width=\"15%\" align=\"center\">"+ strDate +"</td>"
									+	"<td width=\"5%\" align=\"center\">" + (i+1) + "</td>"
									+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(tempGoc)) +"</td>"
									+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(goc)) +"</td>"
									+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(lai)) + "</td>"
									+	"<td width=\"20%\" align=\"center\">"+ converNumber(Math.round(goc_lai)) +"</td>"
							+"</tr>"
				}
						
				strResult +="<tr>"
									+	"<th width=\"15%\" align=\"center\">Tổng</th>"
									+	"<th width=\"5%\" align=\"center\"></th>"
									+	"<th width=\"20%\" align=\"center\"></th>"
									+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_goc)) +"(VNĐ)</th>"
									+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_lai)) + "(VNĐ)</th>"
									+	"<th width=\"20%\" align=\"center\">"+ converNumber(Math.round(sum_goc + sum_lai)) +"(VNĐ)</th>"
							+"</tr>"
				
				strResult +="</table>";
			
				$('.th-result-table-js').html(strResult);			
				$('#chck_1_1').prop('checked', 'checked');
				// $('#listRepayment-link').trigger('click').removeClass('disabled');
				// $('html, body').animate({scrollTop: $("#mb-calculator").offset().top - 100}, 1000);
			}
			/*--------------------------------*/
			showResult = () => {
				var type = isTabLoanLimit();
				var result_table = $('.th-result-js');
				
				var funds = Math.round(funds_field.get());
				var loan_needs_mb = parseInt(Math.round(loan_needs_mb_field.get())); //parseInt($("#loan-needs-mb-js").prop('value'));
				var term_payment = parseInt($("#term-field").prop('value'));
				var default_payment_monthly = Math.round(loan_needs_mb / term_payment);
				var interest_money_monthly = Math.round(default_payment_monthly * CONFIG_INTEREST_RATE_YEAR / 12);
				// var interest_money_monthly = Math.round(loan_needs_mb * CONFIG_INTEREST_RATE_YEAR / 12 / 30 * term_payment);
				var type_name = null;
				var sum = 0;
				for(var i = 1; i <= term_payment; i++) {
					sum += Math.round(interest_money_monthly + default_payment_monthly);
					//console.log(loan_needs_mb);
				}
				if(type) {
					type_name = 'Vay hạn mức';
				} else type_name = 'Vay đầu tư tài sản cố định (TSCĐ)';
				var template = '<h3>{{TYPE_LOAN}}</h3>'
										+	'<div class="row">'
									    + '<div class="col-md-6">'
										+ '<h5 class="title">Khoản vay:</h5>'
									    + '</div>'
									    + '<div class="col-md-6">'
										+ '<span class="t2">{{LOAN}}</span> '
										+ '<span class="t3">VNĐ</span>'
									    + '</div></div>'
									    + '<div class="row">'
										+ '<div class="col-md-6">'
										+ '<h5 class="title">Vốn tự có:</h5>'
										+ '</div>'
										+ '<div class="col-md-6">'
										+ '<span class="t4">{{FUNDS}}</span> '
										+ '<span class="t3">VNĐ</span>'
										+ '</div></div>'
										+ '<div class="row">'
										+ '<div class="col-md-6">'
										+ '<h5 class="title">Kỳ hạn vay:</h5>'
										+ '</div>'
										+ '<div class="col-md-6">'
										+ '<span class="t5">{{TERM}} tháng</span> '
										+ '</div></div>'
										+ '<div class="row">'
										+ '<div class="col-md-6">'
										+ '<h5 class="title">Lãi suất:</h5>'
										+ '</div>'
										+ '<div class="col-md-6">'
										+ '<span class="t4">{{INTEREST_RATE_YEAR}}</span> '
										+ '<span class="t3">%</span>'
										+ '</div></div>'
										+ '<div class="row">'
										+ '<div class="col-md-6">'
										+ '<h5 class="title">Tiền gốc hàng tháng:</h5>'
										+ '</div>'
										+ '<div class="col-md-6">'
										+ '<span class="t4">{{DEFAULT_PAYMENT}}</span> '
										+ '<span class="t3">VNĐ</span>'
										+ '</div></div>';
				var resultBox = template.replace("{{TYPE_LOAN}}", type_name)
						.replace("{{LOAN}}", formatNum(loan_needs_mb))
						.replace("{{FUNDS}}", formatNum(funds))
						.replace("{{TERM}}", formatNum(term_payment))
						.replace("{{INTEREST_RATE_YEAR}}", formatNum(INTEREST_RATE_YEAR))
						.replace("{{DEFAULT_PAYMENT}}", formatNum(default_payment_monthly));
				result_table.html("").append(resultBox);
				calcLoan();
			}
		});
	</script>
</section>
