<section class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">LỢI ÍCH</h2>
    </div>    
  	<div class="row list-item">
      <?php 
      $a_5_1 = ['other/Fast.svg','other/Fast.svg'];
      $a_5_2 = ['LỢI ÍCH','Bảo lãnh online','Không cần đăng ký','Xác nhận tại MB','Bảo lãnh online','Không cần đăng ký'];
      $a_5_3 = ['Khách hàng được Tổng cục thuế xác nhận hoàn thiện nghĩa vụ nộp thuế ngay khi trích nợ thành công trên tài khoản tại MB','Cho phép khách hàng có thể gửi các đề nghị và hồ sơ giao dịch phát hành/ sửa đổi/ giải tỏa bảo lãnh qua eMB','Nhà nhập khẩu được MB ứng trước tiền hàng để thanh toán cho Người hưởng lợi. Do đó, thời gian trả chậm tiền đến 360 ngày','Khách hàng được Tổng cục thuế xác nhận hoàn thiện nghĩa vụ nộp thuế ngay khi trích nợ thành công trên tài khoản tại MB','Cho phép khách hàng có thể gửi các đề nghị và hồ sơ giao dịch phát hành/ sửa đổi/ giải tỏa bảo lãnh qua eMB','Nhà nhập khẩu được MB ứng trước tiền hàng để thanh toán cho Người hưởng lợi. Do đó, thời gian trả chậm tiền đến 360 ngày'];
      for($i=1;$i<=6;$i++) { ?>
  		<div class="col-sm-6 col-md-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/Fast.svg" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>          
            <div class="desc"><?php echo $a_5_3[$i-1]; ?></div>
          </div>   
        </div>     
  		</div>
      <?php } ?>
  	</div>
  </div>
</section>