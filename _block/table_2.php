<section class=" sec-tb   ">
  <div class="container"  >
		  	<div class="entry-head text-center">
		  		<h2 class="ht ">Các gói cước</h2>
		  	</div>	

		  	<div class="max950">	

			<div class="row list-item">
				<?php for($i=1;$i<=4;$i++) { ?>
				<div class="col-md-6">
					<div class="widget-default">
						<h4 class="widget-title">Gói eMB Advance</h4>
						<div class="widget-content entry-content">
							<div>Cho phép khách hàng thực hiện cả các dịch vụ tài chính và phi tài chính, bao gồm:</div>
							<div>- Các tính năng của gói Basic;</div>
							<div>- Tiết kiệm số</div>
							<div>- Chuyển tiền</div>
							<ul>
								<li>Chuyển tiền giữa các tài khoản của Khách hàng…</li>
								<li>Chuyển tiền liên ngân hàng</li>
								<li>Chuyển tiền nội bộ</li>
								<li>Chuyển tiền nội bộ theo lô</li>
								<li>Chuyển tiền liên ngân hàng theo lô</li>
								<li>Chuyển khoản qua thẻ</li>
								<li>Chuyển khoản qua MBS</li>
							</ul>
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
			</div>	
		
  </div>

</section>