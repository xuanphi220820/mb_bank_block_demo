<style>
	.th-sec-pav { margin-bottom: 20px; }
	.th-sec-pav .select1 {
	    border-radius: 8px;
	    /*background-color: #4048DA;*/
	    /*border: none;*/
	    height: 48px;
	    /* width: 100%; */
	    /*color: #fff;*/
	}
	
	.vib-v2-btn-dk-congcu {
		max-width: 50%;
		margin: 20px 0;
	}
	a.vib-v2-btn-dk02 {
    width: 100%;
    max-width: 220px;
    height: 60px;
    font-size: 18px;
    color: #1b1b1b;
    font-weight: 600;
    border-radius: 5px;
    display: block;
    background: #fdb913;
    text-align: center;
    line-height: 60px;
    transition: all .3s ease;
    position: relative;
    overflow: hidden;
    margin: 0 auto;
	}
	a.vib-v2-btn-dk01::before, a.vib-v2-btn-dk02::before {
    background: #1b1b1b;
    width: 100%;
    height: 100%;
    content: "";
    transition: all .3s ease;
    position: absolute;
    top: 0;
    left: -100%;
	}
	a.vib-v2-btn-dk01::before, a.vib-v2-btn-dk02::before {
    background: #9BE6C8;
	}
	a.vib-v2-btn-dk01, a.vib-v2-btn-dk02 {
    background: #8FD4FF;
	}
	.vib-v2-btn-dk-congcu a.vib-v2-btn-dk02 {
    max-width: 100%;
    transition: all ease 0.4s;
	}
	.vib-v2-btn-dk-congcu a.vib-v2-btn-dk02:hover {
    background-color: #9BE6C8;
    color: white;
	}
	a.vib-v2-btn-dk01 span, a.vib-v2-btn-dk02 span {
    position: relative;
  }

	.mbb-result-calculation {
    width: calc(100%);
    display: block;
    float: left;
    padding: 40px 20px;
    /*background: #f2f2f2;*/
	}
	.mbb-result-calculation .mbb-title1 { 
		text-align: center;
		color: #141ED2; }

	.mbb-result-calculation	.line-v2-row-result-calc h4 span { 
		color: #141ED2;
	}
</style>
<section  class="sec-tb sec-cong-cu" >
  <link rel='stylesheet'  href='assets/js/ion.rangeSlider-master/ion.rangeSlider.min.css' type='text/css' media='all' />
  <script src="assets/js/ion.rangeSlider-master/ion.rangeSlider.min.js"></script>
  <script src="https://unpkg.com/autonumeric@4.5.13/dist/autoNumeric.min.js"></script>

	<div class="container"  >
	<h2 class="ht">Công cụ tính</h2>

	  <div  class="cttab-v4   ">
	    <div  class="tab-menu">
	      <div  class="active" data-tab="1"><span>Vay hạn mức</span></div>
	      <div data-tab="2"><span>tab</span></div>
	    </div>
	    <div class="tab-content">
	      <div class="active">
	        <div class="tab-inner sec-cong-cu-1">

	        	<div class="row">
	        		<div class="col-md-8">
        				<div class="dropdown th-sec-pav">
	                        <select class="form-control slrate dropdown-ctrl select1 th-select1-js" name="" id="loanCalculator">
	                            <option value="1">Vay hạn mức</option>
	                            <option value="2">Vay đầu tư tài sản cố định (TSCĐ)</option>
	                        </select>
	                    </div>

        				<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">Tổng nhu cầu vốn (số tiền vay):</span>
		        				<span class="title2"><input id="loan-needs-field-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
		        			</div>							
							<span id="loan-needs-js" class="price-range-input" data-type="single"  data-min="5000000" data-max="100000000" data-from="1000000" data-step="1000000"></span>
						</div>
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">Vốn tự có:</span>
		        				<span class="title2"><input id="funds-field-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
		        			</div>							
							<span id="funds-js" class="price-range-input" data-type="single"  data-min="5000000" data-max="100000000" data-from="1000000" data-step="1000000"></span>
						</div>	
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">Nhu cầu vốn vay tại MB:</span>
		        				<span class="title2"><input id="loan-needs-mb-field-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
		        			</div>							
							<!-- <span id="loan-needs-mb-js" class="price-range-input" data-type="single"  data-min="1000000" data-max="100000000" data-from="1000000" data-step="1000000"></span> -->
							<span id="loan-needs-mb-js" class="price-range-input" data-type="single"  data-min="0" data-max="0" data-from="0" data-step="1000000"></span>
						</div>	
						<div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">Kỳ hạn vay:</span>
		        				<span class="title2"><input id="term-field-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> tháng</span>
		        			</div>							
							<span id="term-js" class="price-range-input" data-type="single"  data-min="1" data-max="12" data-from="0" data-step="1"></span>
						</div>	
						<!-- <div class="group-range-prcie">
		        			<div class="gtitle">
		        				<span class="title">Lãi suất:</span>
		        				<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> <span class="price">%</span> VND/ tháng</span>
		        			</div>							
							<span class="price-range-input" data-type="single"  data-min="0" data-max="20" data-from="0" data-step="0.5"></span>
						</div> -->	

								<div class="vib-v2-btn-dk-congcu">
									<a onclick="showResult()" href="javascript:void(0)" class="vib-v2-btn-dk02"><span>Xem kết quả</span></a>
								</div>
	        		</div>
	        		<div class="col-md-4">
	        			<div class="result th-result-js">
	        				<div class="divtext">
	        				<div>Tổng số tiền bạn có thể vay (VND)</div>
	        				<span class="total">0</span>
	        				</div>
	        			</div>
	        			
	        		</div>
	        	</div>
	        	<div class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</div>
	        </div>
	      </div>
	      <div >
	        <div class="tab-inner">
	          2
	        </div>
	      </div>


	    </div>
	  </div>

  </div>

	<script>
	(function($){
	$(document).ready(function(){

	function beginIonRange(e) {   	
	    e.each(function () {
			var $wslide = $(this),
				$range = $(this).find(".price-range-input"),
			    $input = $(this).find(".price-input-1"),
			    $input2 = $(this).find(".price-input-2"),
			    instance,
			    type = parseInt($range.data('type')),
			    grid = parseInt($range.data('grid')),
			    min = parseInt($range.data('min')),
			    max = parseInt($range.data('max')),
			    from = parseInt($range.data('from')),
			    to = parseInt($range.data('to')),
			    step = parseInt($range.data('step')),
			    prefix = parseInt($range.data('prefix')),
			    postfix = parseInt($range.data('postfix'));
			    //if(!type) { type = 'single';}

			$range.ionRangeSlider({
	            type: type,
	            grid: grid,			
			    min: min,
			    max: max,
			    from: from,
			    to: to,
			    step: step,

		        prefix: prefix,
		        postfix: postfix,

			    onStart: function(data) {
			        $input.prop("value", data.from);
			        $input2.prop('value', data.to);
			    },
			    onChange: function(data) {
					//console.log(data.to);
			    	// if(data.from<=min){
			     //    	data.from = '';
			     //    }
			    	// if(data.to>=max){
			     //    	data.to = '';
			     //    }
			     //    $input.prop("value", data.from);
			     //    $input2.prop('value', ((data.to == max) ? max : data.to));
			    }
			});
			instance = $range.data("ionRangeSlider");
			$input.on("change", function() {
				//console.log(parseInt($(this).prop("value")));
			    var val = parseInt($(this).prop("value")),
			    	val2 = parseInt($input2.prop("value"));
			    // validate
			    // if (val < min) {
			    //     val = min;
			    //     $(this).val(min)
			    // } else if (val > val2) { 
			    //     val = val2;
			    //     $(this).val(val2)
			    // }
			    // instance.update({
			    //     from: val,
			    // });

			    if (val < min) {
			        val = min;
			        $(this).val(min);
			    } else if (val > max) { 
			        val = max;
			        $(this).val(max)
			    }

			});
			$input2.on("change", function() {
			    var val = parseInt($(this).prop("value")),
			    	val2 = parseInt($input2.prop("value"));

 				    	
			    // validate
			    if (val < val2) {
			        val = val2;
			        $(this).val(val2)
			    } else if (val > max) {
			        val = max;
			        $(this).val(max)
			    }
			    instance.update({
			        to: val,
			    });
			});
	    }); 
	}
	beginIonRange($('.group-range-prcie'));



	});
	})(jQuery);
	</script>

	<script>
	//var LOAN_LIMIT = 1;
	//var LOAN_INVESTMENT_FIXED_ASSETS = 2;
	//var CONFIG_MAX_LOAN_NEEDS = 100000000;
	//var CONFIG_MIN_LOAN_NEEDS = 5000000;
	//var CONFIG_MIN_FUNDS = 5000000;
	//var CONFIG_LOAN_NEEDS_MB = null;
	var CONFIG_INTEREST_RATE_YEAR = 0.12;
	var CONFIG_MAX_PERCENT_LOAN_LIMIT = 0.9;
	var CONFIG_MAX_PERCENT_LOAN_INVESTMENT = 0.85;

	var CONFIG_MAX_MONTH_LOAN_LIMIT = 12;
	var CONFIG_MAX_MONTH_LOAN_INVESTMENT = 180;

	jQuery(document).ready(function($){
		//variable use to set format thousand for number of each field
		var value_assets_field = new AutoNumeric('#loan-needs-field-js');
		var funds_field = new AutoNumeric('#funds-field-js');
		var loan_needs_mb_field = new AutoNumeric('#loan-needs-mb-field-js');
		//------------------------

		//------------------------		
		$(".th-select1-js").change(function(e) {
			if(isTabLoanLimit()) {
				$("#term-js").data("ionRangeSlider").update({
	      	max: 12
	    	});	
			} else {
				$("#term-js").data("ionRangeSlider").update({
	      	max: 180
	    	});	
			}
		});
		//------------will optimize code-------------
		$("#loan-needs-js").change(function() {
			var loan_needs = $("#loan-needs-js").prop('value');
			var user_funds = $("#funds-js").prop('value');
			var loan_needs_mb = loan_needs - user_funds;
			
			if(isTabLoanLimit()) {
			  loan_needs_mb = (loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_LIMIT);
			} else loan_needs_mb = (loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_INVESTMENT);

			if(loan_needs_mb < 0) {
				$("#loan-needs-mb-field-js").val($("#loan-needs-mb-js").data('min'));//.prop("value", CONFIG_MIN_LOAN_NEEDS);
				$("#funds-js").data("ionRangeSlider").reset();
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
					from: $("#loan-needs-mb-js").data('min'),
        		});
			} else {
				//console.log('hihihi');
				$("#loan-needs-mb-field-js").prop("value", loan_needs_mb);
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
    			from: loan_needs_mb,
    			max: loan_needs_mb
    		});

    		value_assets_field.set(loan_needs);
    		loan_needs_mb_field.set(loan_needs_mb);
			}
		});
		//------------will optimize code-------------
		$("#loan-needs-field-js").change(function() {
			//var loan_needs = $("#loan-needs-field-js").prop('value');
			var loan_needs = loan_needs_mb_field.get();
			var user_funds = $("#funds-js").prop('value');
			var loan_needs_mb = loan_needs - user_funds;
			
			if(isTabLoanLimit()) {
			  loan_needs_mb = Math.round(loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_LIMIT);
			} else loan_needs_mb = Math.round(loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_INVESTMENT);

			if(loan_needs_mb < 0) {
				$("#loan-needs-mb-field-js").val($("#loan-needs-mb-js").data('min'));
				$("#funds-js").data("ionRangeSlider").reset();
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
					from: $("#loan-needs-mb-js").data('min')
	    	});
			} else {
				$("#loan-needs-js").data("ionRangeSlider").update({
        			from: loan_needs
	    		});
				
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
        			from: loan_needs_mb,
        			max: loan_needs_mb
	    		});

	    		// $("#loan-needs-mb-field-js").prop('value', loan_needs_mb);

	    		value_assets_field.set(loan_needs);
	    		loan_needs_mb_field.set(loan_needs_mb);
			}
		});

		//------------will optimize code-------------
		$("#funds-js").change(function() {
			var loan_needs = $("#loan-needs-js").prop('value');
			var user_funds = $("#funds-js").prop('value');
			var loan_needs_mb = loan_needs - user_funds;
			
			if(isTabLoanLimit()) {
			  loan_needs_mb = (loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_LIMIT);
			} else loan_needs_mb = (loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_INVESTMENT);

			if(loan_needs_mb < 0) {
				$("#loan-needs-mb-field-js").val($("#loan-needs-mb-js").data('min'));//.prop("value", CONFIG_MIN_LOAN_NEEDS);
				$("#funds-js").data("ionRangeSlider").update({
					from: $("#loan-needs-mb-js").data('min'),
       	});
				$("#loan-needs-mb-js").data("ionRangeSlider").reset();

				loan_needs_mb_field.set(0);
			} else {
				//$("#loan-needs-mb-field-js").prop("value", loan_needs_mb);
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
        			from: loan_needs_mb,
        			max: loan_needs_mb
	    		});

				loan_needs_mb_field.set(loan_needs_mb);
  			funds_field.set(user_funds);
			}
		});
		//------------will optimize code-------------
		$("#funds-field-js").change(function() {
			//var loan_needs = $("#loan-needs-field-js").prop('value');
			var loan_needs = value_assets_field.get();
			var user_funds = $("#funds-field-js").prop('value');
			var loan_needs_mb = loan_needs - user_funds;
			
			if(isTabLoanLimit()) {
			  loan_needs_mb = Math.round(loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_LIMIT);
			} else loan_needs_mb = Math.round(loan_needs_mb * CONFIG_MAX_PERCENT_LOAN_INVESTMENT);

			if(loan_needs_mb < 0) {
				$("#loan-needs-mb-field-js").val($("#loan-needs-mb-js").data('min'));
				$("#funds-js").data("ionRangeSlider").update({
					from: $("#loan-needs-mb-js").data('min'),
        		});
				$("#loan-needs-mb-js").data("ionRangeSlider").reset();
			} else {
				$("#funds-js").data("ionRangeSlider").update({
        			from: loan_needs
	    		});
				
				$("#loan-needs-mb-js").data("ionRangeSlider").update({
        			from: loan_needs_mb,
        			max: loan_needs_mb
	    		});

	    		//$("#loan-needs-mb-field-js").prop('value', loan_needs_mb);
	    		loan_needs_mb_field.set(loan_needs_mb);
			}
		});

		//------------will optimize code-------------
		$("#loan-needs-mb-js").change(function() {
			//$("#loan-needs-mb-field-1-js").prop("value", $(this).prop("value"));
			loan_needs_mb_field.set($(this).prop("value"));
			//funds_field.set(user_funds);
			//value_assets_field.set(loan_needs);
		});

		//------------will optimize code-------------
	    function isTabLoanLimit() {
	   	  var tab = $(".th-select1-js option:checked");
	   	  if(parseInt(tab.val()) === 1) {
	   	  	return true;
	   	  }
	   	  return false;
	    }

	    var formatNum = function(value) {
			var lang = window.location.href ;
			var local = 'vi-VN';
			if(lang.includes('\en')){
				local='en-US';
			}
			var value_=	parseFloat(value);
			var rs=value_.toLocaleString(local);
			return rs;
		}

    	showResult = () => {
	    	var type = isTabLoanLimit();
	    	var result_table = $('.th-result-js');
	    	
	    	var funds = parseInt($("#funds-js").prop('value'));
	    	var loan_needs_mb = parseInt($("#loan-needs-mb-js").prop('value'));
	    	var term_payment = parseInt($("#term-js").prop('value'));
	    	var default_payment_monthly = Math.round(loan_needs_mb / term_payment);
	    	var interest_money_monthly = Math.round(loan_needs_mb * CONFIG_INTEREST_RATE_YEAR / 12);
	    	// var interest_money_monthly = Math.round(loan_needs_mb * CONFIG_INTEREST_RATE_YEAR / 12 / 30 * term_payment);
	    	var type_name = null;
	    	var sum = 0; 
	    	for(var i = 1; i <= term_payment; i++) {
	    		sum = Math.round(interest_money_monthly + default_payment_monthly);
	    	}

	    	if(type) {
	    		type_name = 'Vay hạn mức';
	    	} else type_name = 'Vay đầu tư tài sản cố định (TSCĐ)'; 

	    	var template = '<div class="mbb-result-calculation">'
	 						  + '<h3 class="mbb-title1">{{TYPE_LOAN}}<br> Khoản vay {{LOAN}} VNĐ</h3>'
	 						  + '<div class="line-v2-row-result-calc">'
	 						  + '<h4>Vốn tự có: <span>{{FUNDS}} VNĐ</span></h4>'
	 						  + '<h4>Kỳ hạn vay: <span>{{TERM}} THÁNG</span></h4>'
	 						  + '<h4>Tiền lãi hàng tháng: <span>{{INTEREST_MONEY}} VNĐ</span></h4>'
	 						  + '<h4>Tiền gốc hàng tháng: <span>{{DEFAULT_PAYMENT}} VNĐ</span></h4>'
	 						  + '<h4>Tổng tiền phải trả: <span>{{SUM}} VNĐ</span></h4>'
	 						  + '</div>'
							+'</div>';

			var resultBox = template.replace("{{TYPE_LOAN}}", type_name)
						.replace("{{LOAN}}", formatNum(loan_needs_mb))
						.replace("{{FUNDS}}", formatNum(funds))
						.replace("{{TERM}}", formatNum(term_payment))
						.replace("{{INTEREST_MONEY}}", formatNum(interest_money_monthly))
						.replace("{{DEFAULT_PAYMENT}}", formatNum(default_payment_monthly))
						.replace("{{SUM}}", formatNum(sum));

			result_table.html("").append(resultBox);
    	}
	});
	</script>
</section>
