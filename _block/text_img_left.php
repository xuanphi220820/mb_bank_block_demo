<section class="sec-tb sec-img-text group-ef lazy-hidden">
  <div class="container"  >
  	<div class="row center">
  		<div class="col-lg-6">
  			<div class="img tRes_66 efch-2 ef-img-r ">
  				<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/1000x600" src="https://via.placeholder.com/10x6">
  			</div>
  		</div>
  		<div class="col-lg-6">
  			<div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">HỒ SƠ VAY VỐN</h2>
			     
          <p class="fs16"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et.</strong></p>
			    <ul class="efch-3 ef-tx-t">
			    	<li>Đơn đề nghị vay vốn kiêm cam kết trả nợ (theo mẫu của MB)</li>
			    	<li>CMND/Hộ chiếu, Hộ khẩu/Sổ tạm trú (KT3), Đăng ký kết hôn/Giấy xác nhận tình trạng hôn nhân</li>
			    	<li>Các giấy tờ liên quan đến việc mua, xây dựng và sửa chữa nhà đất</li>
			    	<li>Giấy tờ chứng minh nguồn trả nợ</li>
			    </ul> 
			    <a class="btn lg" href="#">ĐĂNG KÝ NGAY</a>	
  			</div>
  		</div>
  	</div>
  </div>
</section>