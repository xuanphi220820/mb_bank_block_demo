<div class="sec-b sec-img-svg-4">
	<h2 class="ht">Công cụ tính khác</h2>
	<div class="row list-item">
		<?php
		$a_5_2 = ['Vay mua nhà đất','Vay sản xuất kinh doanh','Vay tiêu dùng có tài sản bảo đảm',
		'Vay tiêu dùng không có tài sản bảo đảm', 'Cho vay mua ô tô','Cho vay cầm cố giấy tờ có giá'];
		//,'Vay du học', 'Cho vay người lao động đi làm việc ở nước ngoài'];
		$links = ['tho_tool_1.php', 'tho_tool_2.php', 'tho_tool_3.php', 'tho_tool_4.php',
		'tho_tool_5.php', 'tho_tool_6.php'];//, 'tho_tool_7.php', 'tho_tool_8.php'];
		for($i=1;$i<=6;$i++) { ?>
		<div class="col-sm-4 efch-<?php echo $i+1; ?> ef-img-t ">
			<a class="item" href="<?php echo $links[$i - 1]; ?>">
				<div class="img ">
					<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/tinh-toan.svg" src="https://via.placeholder.com/6x4">
				</div>
				<div class="divtext">
					<h4 class="title"><?php echo $a_5_2[$i-1]; ?></h4>
				</div>
			</a>
		</div>
		<?php } ?>
	</div>
</div>
