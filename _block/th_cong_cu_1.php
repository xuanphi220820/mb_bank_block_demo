<style>
	.th-sec-pav { margin-bottom: 20px; }
	.th-sec-pav .select1 {
	border-radius: 8px;
	/*background-color: #4048DA;*/
	/*border: none;*/
	height: 48px;
	/* width: 100%; */
	/*color: #fff;*/
	}
	
	.vib-v2-btn-dk-congcu {
		max-width: 50%;
		margin: 20px 0;
	}
	a.vib-v2-btn-dk02 {
width: 100%;
max-width: 220px;
height: 60px;
font-size: 18px;
color: #1b1b1b;
font-weight: 600;
border-radius: 5px;
display: block;
background: #fdb913;
text-align: center;
line-height: 60px;
transition: all .3s ease;
position: relative;
overflow: hidden;
margin: 0 auto;
	}
	a.vib-v2-btn-dk01::before, a.vib-v2-btn-dk02::before {
background: #1b1b1b;
width: 100%;
height: 100%;
content: "";
transition: all .3s ease;
position: absolute;
top: 0;
left: -100%;
	}
	a.vib-v2-btn-dk01::before, a.vib-v2-btn-dk02::before {
background: #9BE6C8;
	}
	a.vib-v2-btn-dk01, a.vib-v2-btn-dk02 {
background: #8FD4FF;
	}
	.vib-v2-btn-dk-congcu a.vib-v2-btn-dk02 {
max-width: 100%;
transition: all ease 0.4s;
	}
	.vib-v2-btn-dk-congcu a.vib-v2-btn-dk02:hover {
background-color: #9BE6C8;
color: white;
	}
	a.vib-v2-btn-dk01 span, a.vib-v2-btn-dk02 span {
position: relative;
}
	.mbb-result-calculation {
width: calc(100%);
display: block;
float: left;
padding: 40px 20px;
/*background: #f2f2f2;*/
	}
	.mbb-result-calculation .mbb-title1 {
		text-align: center;
		color: #141ED2; }
		.mbb-result-calculation	.line-v2-row-result-calc h4 span {
		color: #141ED2;
	}
</style>
<section  class="sec-tb sec-cong-cu" >
	<link rel='stylesheet'  href='assets/js/ion.rangeSlider-master/ion.rangeSlider.min.css' type='text/css' media='all' />
	<script src="assets/js/ion.rangeSlider-master/ion.rangeSlider.min.js"></script>
	<!-- <script src="assets/js/auto-numeric/AutoNumeric.js"></script> -->
	<!-- <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.5.4"></script> -->
	<!-- ...or -->
	<script src="https://unpkg.com/autonumeric@4.5.13/dist/autoNumeric.min.js"></script>
	
	<div class="container"  >
		<h2 class="ht">Công cụ tính</h2>
		<div  class="cttab-v4   ">
			<div  class="tab-menu">
				<div  class="active" data-tab="1"><span>Cho vay mua nhà đất, nhà dự án</span></div>
				<div data-tab="2"><span>tab</span></div>
			</div>
			<div class="tab-content">
				<div class="active">
					<div class="tab-inner sec-cong-cu-1">
						<div class="row">
							<div class="col-md-8">
								<div class="dropdown th-sec-pav">
									<select class="form-control slrate dropdown-ctrl select1 th-select-tool1-js" name="" id="loanCalculator">
										<option value="1">Cho vay mua nhà đất</option>
										<option value="2">Cho vay mua nhà dự án</option>
										<option value="3">Cho vay xây dựng, sửa chữa nhà</option>
										<option value="4">Cho vay trang bị nội thất nhà</option>
									</select>
								</div>
								<div class="group-range-prcie">
									<div class="gtitle">
										<span class="title">Ước tính giá trị nhà mua:</span>
										<span class="title2"><input type="tel" id="value-assets-field-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
									</div>
									<span id="value-assets-js" class="price-range-input" data-type="single"  data-min="100000000" data-max="10000000000" data-from="100000000" data-step="10000000"></span>
								</div>
								<div class="group-range-prcie">
									<div class="gtitle">
										<span id="th-title-tool" class="title">Ước tính giá trị tài sản thế chấp:</span>
										<span class="title2"><input id="funds-field-1-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
									</div>
									<span id="funds-1-js" class="price-range-input" data-type="single"  data-min="100000000" data-max="10000000000" data-from="100000000" data-step="10000000"></span>
								</div>
								<div class="group-range-prcie">
									<div class="gtitle">
										<span class="title">Số tiền có thể vay:</span>
										<span class="title2"><input id="loan-needs-mb-field-1-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> VND</span>
									</div>
									<!-- <span id="loan-needs-mb-js" class="price-range-input" data-type="single"  data-min="1000000" data-max="100000000" data-from="1000000" data-step="1000000"></span> -->
									<span id="loan-needs-mb-1-js" class="price-range-input" data-type="single"  data-min="50000000" data-max="100000000" data-from="10000000" data-step="10000000"></span>
								</div>
								<div class="group-range-prcie">
									<div class="gtitle">
										<span class="title">Kỳ hạn vay:</span>
										<span class="title2"><input id="term-field-1-js" class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> tháng</span>
									</div>
									<span id="term-1-js" class="price-range-input" data-type="single"  data-min="1" data-max="60" data-from="0" data-step="1"></span>
								</div>
								<!-- <div class="group-range-prcie">
									<div class="gtitle">
										<span class="title">Lãi suất:</span>
										<span class="title2"><input class="price price-input-1"  name="" class="input " value="" placeholder="Từ"> <span class="price">%</span> VND/ tháng</span>
															</div>
										<span class="price-range-input" data-type="single"  data-min="0" data-max="20" data-from="0" data-step="0.5"></span>
								</div> -->
								<div class="vib-v2-btn-dk-congcu">
									<a onclick="showResult()" href="javascript:void(0)" class="vib-v2-btn-dk02"><span>Xem kết quả</span></a>
								</div>
							</div>
							<div class="col-md-4">
								<div class="result th-result-1-js">
									<div class="divtext">
										<div>Tổng số tiền bạn có thể vay (VND)</div>
										<span class="total">0</span>
									</div>
								</div>
								
							</div>
						</div>
						<div class="note">(*) Bảng tính chỉ mang tính tham khảo và không phải là cam kết về khoản vay của MBBank</div>
					</div>
				</div>
				<div >
					<div class="tab-inner">
						2
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	(function($){
	$(document).ready(function(){
		function beginIonRange(e) {
	e.each(function () {
			var $wslide = $(this),
				$range = $(this).find(".price-range-input"),
			$input = $(this).find(".price-input-1"),
			$input2 = $(this).find(".price-input-2"),
			instance,
			type = parseInt($range.data('type')),
			grid = parseInt($range.data('grid')),
			min = parseInt($range.data('min')),
			max = parseInt($range.data('max')),
			from = parseInt($range.data('from')),
			to = parseInt($range.data('to')),
			step = parseInt($range.data('step')),
			prefix = parseInt($range.data('prefix')),
			postfix = parseInt($range.data('postfix'));
			//if(!type) { type = 'single';}
			$range.ionRangeSlider({
	type: type,
				grid: grid,
			min: min,
			max: max,
			from: from,
			to: to,
			step: step,
	prefix: prefix,
	postfix: postfix,
	prettify_enabled: true,
	prettify_separator: ",",
			onStart: function(data) {
			$input.prop("value", data.from);
			$input2.prop('value', data.to);
			},
			onChange: function(data) {
					//console.log(data.to);
				// if(data.from<=min){
				//    	data.from = '';
			//    }
				// if(data.to>=max){
				//    	data.to = '';
			//    }
			//    $input.prop("value", data.from);
			//    $input2.prop('value', ((data.to == max) ? max : data.to));
			}
			});
			instance = $range.data("ionRangeSlider");
			//$input.autoNumeric('init');
			$input.on("change", function() {
				//console.log(parseInt($(this).prop("value")));
			var val = parseInt($(this).prop("value")),
				val2 = parseInt($input2.prop("value"));
			// validate
			// if (val < min) {
			//     val = min;
			//     $(this).val(min)
			// } else if (val > val2) {
			//     val = val2;
			//     $(this).val(val2)
			// }
			// instance.update({
			//     from: val,
			// });
			if (val < min) {
			val = min;
			$(this).val(min);
			} else if (val > max) {
			val = max;
			$(this).val(max)
			}
			});
			$input2.on("change", function() {
			var val = parseInt($(this).prop("value")),
				val2 = parseInt($input2.prop("value"));
					
			// validate
			if (val < val2) {
			val = val2;
			$(this).val(val2)
			} else if (val > max) {
			val = max;
			$(this).val(max)
			}
			instance.update({
			to: val,
			});
			});
	});
	}
	beginIonRange($('.group-range-prcie'));
	});
	})(jQuery);
	</script>
	<script>
	// var LOAN_LIMIT = 1;
	// var LOAN_INVESTMENT_FIXED_ASSETS = 2;
	// var CONFIG_MAX_LOAN_NEEDS = 100000000;
	var CONFIG_MIN_LOAN_NEEDS = 100000000;
	// var CONFIG_MIN_FUNDS = 5000000;
	// var CONFIG_LOAN_NEEDS_MB = null;
	var CONFIG_INTEREST_RATE_YEAR = 0.12;
	var CONFIG_PERCENT_LOAN_HOUSE = 0.8;
	var CONFIG_PERCENT_COLLATERAL = 0.7;
	var TITLE_HOUSE = 'Ước tính giá trị nhà mua';
	var TITLE_LOAN_FIX = 'Ước tính giá trị xây,sửa nhà,trang bị nội thất';
	jQuery(document).ready(function($){
		var value_assets_field = new AutoNumeric('#value-assets-field-js');
		var funds_field = new AutoNumeric('#funds-field-1-js');
		var loan_needs_mb_field = new AutoNumeric('#loan-needs-mb-field-1-js');
		$(".th-select-tool1-js").change(function(e) {
			if(isTabLoanBuyHouse()) {
				$('#th-title-tool').text(TITLE_HOUSE);
			} else {
				$('#th-title-tool').text(TITLE_LOAN_FIX);
			}
		});
		//------------will optimize code-------------
		$("#value-assets-js").change(function() {
			var loan_needs = $("#value-assets-js").prop('value');
			var user_funds = $("#funds-1-js").prop('value');
			var loan_needs_mb = Math.min(Math.round(loan_needs*CONFIG_PERCENT_LOAN_HOUSE), Math.round(user_funds*CONFIG_PERCENT_COLLATERAL));
			// if(loan_needs_mb < 0) {
				// 	$("#loan-needs-mb-field-1-js").val($("#loan-needs-mb-1-js").data('min'));
				// 	$("#funds-1-js").data("ionRangeSlider").reset();
				// 	$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
					// 		from: $("#loan-needs-mb-1-js").data('min'),
		//      		});
			// } else {
				// $("#value-assets-field-js").prop("value", loan_needs);
				
				// $("#loan-needs-mb-field-1-js").prop("value", loan_needs_mb);
				
				$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
			from: loan_needs_mb,
			to: loan_needs_mb,
			max: loan_needs_mb
		});
		//value_assets_field.set(loan_needs);
		//console.log(value_assets_field.get(loan_needs));
		value_assets_field.set(loan_needs);
		loan_needs_mb_field.set(loan_needs_mb);
		//$('#value-assets-field-js').prop('value', );
		//loan_needs_mb_field.get(loan_needs_mb);
		
		//$("#loan-needs-mb-field-1-js").prop("value", loan_needs_mb_field.get(loan_needs_mb));
		//$("#loan-needs-mb-field-1-js").prop("value", loan_needs_mb_field.get(loan_needs_mb));
		//$("#loan-needs-mb-field-1-js").prop("value", loan_needs_mb_field.get(loan_needs_mb));
			//}
			//$('#value-assets-field-js').attr('value', loan_needs);
			// value_assets_field.set(loan_needs);
			// loan_needs_mb_field.set(loan_needs_mb);
		});
		//------------will optimize code-------------
		$("#value-assets-field-js").change(function() {
			var loan_needs = $("#value-assets-field-js").prop('value');
			var user_funds = $("#funds-1-js").prop('value');
			var loan_needs_mb = Math.min(Math.round(loan_needs*CONFIG_PERCENT_LOAN_HOUSE), Math.round(user_funds*CONFIG_PERCENT_COLLATERAL));
			// if(loan_needs_mb < 0) {
				// 	$("#loan-needs-mb-field-1-js").val($("#loan-needs-mb-1-js").data('min'));
				// 	$("#funds-1-js").data("ionRangeSlider").reset();
				// 	$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
					// 		from: $("#loan-needs-mb-1-js").data('min')
		//   	});
			// } else {
				$("#value-assets-js").data("ionRangeSlider").update({
	from: loan_needs
		});
				
				$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
	from: loan_needs_mb,
	//to: loan_needs_mb,
	//min: 5000000,
	max: loan_needs_mb
		});
			$("#loan-needs-mb-field-1-js").prop('value', loan_needs_mb);
			//value_assets_field.set(loan_needs);
			//value_assets_field.get(loan_needs);
			if(loan_needs < CONFIG_MIN_LOAN_NEEDS) { loan_needs = CONFIG_MIN_LOAN_NEEDS; }
			
			//value_assets_field.get(loan_needs);
			value_assets_field.set(loan_needs);
			loan_needs_mb_field.set(loan_needs_mb);
			//loan_needs_mb_field.get(loan_needs_mb);
			// }
		});
		//------------will optimize code-------------
		$("#funds-1-js").change(function() {
			var loan_needs = $("#value-assets-js").prop('value');
			var user_funds = $("#funds-1-js").prop('value');
			var loan_needs_mb = Math.min(Math.round(loan_needs*CONFIG_PERCENT_LOAN_HOUSE), Math.round(user_funds*CONFIG_PERCENT_COLLATERAL));
			$("#loan-needs-mb-field-1-js").prop("value", loan_needs_mb);
			$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
			from: loan_needs_mb,
			max: loan_needs_mb
		});
			loan_needs_mb_field.set(loan_needs_mb);
			funds_field.set(user_funds);
			//value_assets_field.set(loan_needs);
		});
		//------------will optimize code-------------
		$("#funds-field-1-js").change(function() {
			var loan_needs = $("#value-assets-js").prop('value');
			var user_funds = $("#funds-field-1-js").prop('value');
			var loan_needs_mb = Math.min(Math.round(loan_needs*CONFIG_PERCENT_LOAN_HOUSE), Math.round(user_funds*CONFIG_PERCENT_COLLATERAL));
			
			$("#funds-1-js").data("ionRangeSlider").update({
			from: loan_needs
		});
			
			$("#loan-needs-mb-1-js").data("ionRangeSlider").update({
			from: loan_needs_mb,
			max: loan_needs_mb
		});
	$("#loan-needs-mb-field-1-js").prop('value', loan_needs_mb);
	loan_needs_mb_field.set(loan_needs_mb);
	//funds_field.set(user_funds);
		});
		//------------will optimize code-------------
		$("#loan-needs-mb-1-js").change(function() {
			//$("#loan-needs-mb-field-1-js").prop("value", $(this).prop("value"));
			loan_needs_mb_field.set($(this).prop("value"));
			//funds_field.set(user_funds);
			//value_assets_field.set(loan_needs);
		});
		//------------will optimize code-------------
	//$("#value-assets-field-js").autoNumeric('init');
	function isTabLoanBuyHouse() {
		var tab = $(".th-select-tool1-js option:checked");
		if(parseInt(tab.val()) === 1 || parseInt(tab.val()) === 2) {
			return true;
		}
		return false;
	}
	var formatNum = function(value) {
			var lang = window.location.href ;
			var local = 'vi-VN';
			if(lang.includes('\en')){
				local='en-US';
			}
				var value_=	parseFloat(value);
			var rs=value_.toLocaleString(local);
			return rs;
		}
	showResult = () => {
	var type = isTabLoanBuyHouse();
	var result_table = $('.th-result-1-js');
	
	var funds = parseInt($("#funds-1-js").prop('value'));
	var loan_needs_mb = parseInt($("#loan-needs-mb-1-js").prop('value'));
	var term_payment = parseInt($("#term-1-js").prop('value'));
	var default_payment_monthly = Math.round(loan_needs_mb / term_payment);
	var interest_money_monthly = Math.round(loan_needs_mb * CONFIG_INTEREST_RATE_YEAR / 12);
	// var interest_money_monthly = Math.round(loan_needs_mb * CONFIG_INTEREST_RATE_YEAR / 12 / 30 * term_payment);
	var type_name = null;
	var sum = 0;
	for(var i = 1; i <= term_payment; i++) {
		sum = Math.round(interest_money_monthly + default_payment_monthly);
	}
	if(type) {
		type_name = TITLE_HOUSE;
	} else type_name = TITLE_LOAN_FIX;
	var template = '<div class="mbb-result-calculation">'
							+ '<h3 class="mbb-title1">{{TYPE_LOAN}}<br> Khoản vay {{LOAN}} VNĐ</h3>'
							+ '<div class="line-v2-row-result-calc">'
								+ '<h4>Vốn tự có: <span>{{FUNDS}} VNĐ</span></h4>'
								+ '<h4>Kỳ hạn vay: <span>{{TERM}} THÁNG</span></h4>'
								+ '<h4>Tiền lãi hàng tháng: <span>{{INTEREST_MONEY}} VNĐ</span></h4>'
								+ '<h4>Tiền gốc hàng tháng: <span>{{DEFAULT_PAYMENT}} VNĐ</span></h4>'
								+ '<h4>Tổng tiền phải trả: <span>{{SUM}} VNĐ</span></h4>'
							+ '</div>'
						+'</div>';
			var resultBox = template.replace("{{TYPE_LOAN}}", type_name)
					.replace("{{LOAN}}", formatNum(loan_needs_mb))
					.replace("{{FUNDS}}", formatNum(funds))
					.replace("{{TERM}}", formatNum(term_payment))
					.replace("{{INTEREST_MONEY}}", formatNum(interest_money_monthly))
					.replace("{{DEFAULT_PAYMENT}}", formatNum(default_payment_monthly))
					.replace("{{SUM}}", formatNum(sum));
		result_table.html("").append(resultBox);
	};
	});
	</script>
</section>