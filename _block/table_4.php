<?php
$a_tb4_1 = ['Hạn mức chuyển tiền','Hạn mức chuyển tiền tối đa','Phí duy trì','Phí duy trì'];
$a_tb4_2 = [
	'<p>Tối thiểu 10.000 VNĐ/giao dịch</p>
	<p>Áp dụng cho mỗi lần giao dịch thành công</p>',
	'<p>•   Bankplus eco: 20 triệu đồng/giao dịch và 50 triệu đồng/ngày.</p>
	<p>•   Bankplus Pro: 100 triệu đồng/giao dịch </p>',
	'<p>•   Gói Bankplus Eco: online 11.000﻿ VNĐ</p>
	<p>•   Gói Bankplus Pro: 22.000 VNĐ</p>',
	'<p>•   Bankplus agent: 100 triệu đồng/giao dịch… và 500 triệu đồng/ngày.</p>'	
];
?>
<section class=" sec-tb  ">
  <div class="container"  >
  	<div class="entry-head text-center">
  		<h2 class="ht  ">Hạn mức</h2>
  	</div>			
	<div class="row grid-space-20 list-item ">
		<?php for($i=1;$i<=8;$i++) { ?>
		<div class="col-md-6 col-sm-6 col-lg-3">
			<div class="widget-default">
				<h4 class="widget-title">Hạn mức chuyển tiền <?php echo $i; ?></h4>
				<div class="widget-content entry-content">
					<p>Tối thiểu 10.000 VNĐ/giao dịch</p>
					<p>Áp dụng cho mỗi lần giao dịch thành công</p>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
  </div>
</section>
