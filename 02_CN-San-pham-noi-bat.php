<?php include 'include/index-top.php';?>
<section  class="banner-img-1 next-shadow" >
	<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-1.jpg" src="">
</section>

<main id="main"  class="sec-tb " >
	<div class="container">
		<h1 class="text-center">Hãy để MB hỗ trợ bạn ngay</h1>

			<div class="menuicon  owl-carousel   s-nav nav-2" data-res="8,4,3,2" paramowl="margin=0">
		    <?php
		    for($i=1;$i<=10;$i++) {?>
		    <div class="item <?php if($i==1) echo 'active'; ?>">
	          <a href="#" class="link">
	          	<div class="img">
	          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/svg/other/ask.svg">
	          	</div>
	          	<div class="title">Ngân hàng số <?php echo $i; ?></div>
	          </a>
	        </div>
	    	<?php } ?>
	    	</div>


		<div class="list-5 row list-item" >
		    <?php
		    for($i=1;$i<=9;$i++) {?>
		    	<div class="col-md-4">
		          <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
		          	<div class="img tRes_71">
		          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
		          	</div>
		          	<div class="divtext">
		          		<h4 class="title"><?php echo $i; ?> Thay đổi địa điểm phòng giao dịch Bến Thành chi nhánh Sài Gòn</h4>
		          		<div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
		          	</div>
		          </a>
		    	</div>				          
	    	<?php } ?>
		</div>

		<?php //include '_module/pagination.php';?>






	</div>    	
</main>

<?php include 'include/index-bottom.php';?>