<?php include 'include/index-top.php';?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">MB kết nối</a>
        <span class="item">Tin tức chi tiết</span>
      </div>
    </div>
</div>


<section class="banner-heading-3 next-shadow" >
	<div class="container">
		<div class="divtext pd-0">
			<div class="max750">
				<h1 class="uppercase" >KẾ HOẠCH TÀI CHÍNH CÁ NHÂN – HƯỚNG DẪN CHI TIẾT CHO NGƯỜI MỚI BẮT ĐẦU</h1>
			</div>
		</div>
	</div>
	<!-- <img class="img br lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-10.svg" src=""> -->
</section>

<main id="main"  class="sec-b page-news-detail" >
	<div class="container">
		<div class=" max750">
			<div class="top-heading">
				<div class="date">01/ 12/ 2019</div>
				
				<ul class="blog-item-social ">
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
				</ul>					
			</div>

			<div class="entry-content">
				<p>Thu nhập của người dân đã tăng lên đáng kể trong những năm gần đây. Nhưng các vấn đề về tiền bạc vẫn luôn là nỗi lo với nhiều người do chưa biết cách lập kế hoạch tài chính cá nhân phù hợp. Đặc biệt khái niệm lập kế hoạch chi tiêu và quản lý tài chính cá nhân vẫn còn khá xa lạ ở Việt Nam. Hãy để MB Kết nối đưa đến bạn hướng dẫn chi tiết nhất trong năm 2020.</p>
				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/detail/detail-1.png" src="https://via.placeholder.com/75x30">
					<figcaption> Đánh giá tình hình tài chính hiện tại trước khi lập kế hoạch tài chính cá nhân</figcaption>
				</figure>
				<p class="b">1. Đánh giá tình hình tài chính hiện tại trước khi lập kế hoạch tài chính cá nhân</p>
				<p>Đầu tiên, cần nắm rõ tình hình tài chính hiện tại của bản thân. Hãy dành thời gian và thu thập tất cả thông tin tài chính thích hợp. Bắt đầu bằng cách liệt kê tất cả tài sản của bạn như: nhà, tiền mặt, tài khoản tiết kiệm… Đừng quên bao gồm bất kỳ tài khoản đầu tư và hưu trí nào. Sau đó liệt kê tất cả các khoản nợ phải trả của bạn. Ví dụ danh sách tất cả các thẻ tín dụng, các khoản thế chấp… Điều này có thể khiến bạn cảm thấy áp lực, nhưng đây là bước quan trọng để kiểm soát tình hình tài chính.</p>
				<p>Số tiền còn lại sau khi lấy tài sản trừ đi nợ chính là giá trị tài sản ròng của bạn. Lúc này, bạn sẽ có một bức tranh rõ nét về tình hình hiện tại của mình.Từ đó, xây dựng kế hoạch tài chính cá nhân phù hợp với điều kiện và nhu cầu của bản thân.</p>
				
				<p class="b">2. Biết rõ mình kiếm được bao nhiêu và chi tiêu thế nào mỗi tháng</p>
				<p>Mọi người có thể nhanh chóng đưa ra được câu trả lời mỗi tháng mình kiếm được bao nhiêu tiền. Nhưng thường bối rối khi nghĩ lại bản thân đã tiêu tiền vào những việc gì trong tháng qua. Một vài người bạn của tôi luôn thắc mắc rằng: “Chẳng hiểu tiền đi đâu hết?”. Trong khi thu nhập ở mức tương đối cao nhưng vẫn rơi vào tình trạng “cháy túi” vào cuối tháng.</p>
				<p>Lý do là bởi họ chưa từng nhìn nhận trung thực về vấn đề chi tiêu của bản thân. Luôn chi tiêu theo sở thích thay vì lên kế hoạch một cách rõ ràng.</p>
				<p>Hãy dùng một cuốn sổ, bảng excel hoặc ứng dụng quản lý chi tiêu để thống kê chi tiết toàn bộ các khoản chi tiêu trong tháng. Bạn sẽ biết được mỗi tháng mình dành tiền cho những khoản gì? Khoản nào nào bạn cảm thấy mình đang quá đà? Đánh dấu hoặc đặt nhắc nhở để hạn chế chi tiêu trong tháng sau. Đây là cách quản lý chi tiêu và lập kế hoạch tài chính cá nhân cơ bản.</p>

				<p class="b">3. Lập ngân sách chi tiêu</p>
				<p>Sau khi có cái nhìn khách quan, chính xác về thu nhập và chi tiêu mỗi tháng. Bước tiếp theo, hãy phân loại rạch ròi các khoản chi tiêu của mình. Bạn có thể phân loại chi tiêu của mình thành các nhóm khác nhau:</p>
				<p>•	Nhóm chi tiêu thiết yếu: tiền nhà, tiền ăn, tiền xăng xe, điện thoại…</p>	
				<p>•	Nhóm tiết kiệm: tiền tiết kiệm cho kế hoạch trong tương lai,quỹ khẩn cấp…</p>
				<p>•	Nhóm chi phí giáo dục: tham gia các khoá học, mua sách, tham dự hội thảo …</p>
				<p>•	Nhóm tiền đầu tư: tiền đầu tư cho học hành, phát triển sự nghiệp; tiền đầu tư vào các hoạt động kinh doanh sinh lời…</p>	
				<p>•	Nhóm tiền hưởng thụ: mua sắm quần áo, mỹ phẩm…</p>	
				<p>•	Nhóm tiền dành cho những khoản chi tiêu phát sinh trong tháng: tiền cưới hỏi, ma chay, hỏng xe...</p>

				<p class="b">4. LÁp dụng bí kíp từ những Quy tắc tài chính</p>
				<p class="b">Quy tắc 50 - 20 - 30</p>
				<p>Để lập ngân sách phù hợp, cần theo dõi các khoản thu chi của gia đình trong 3 tháng gần nhất. Từ đó, bạn sẽ rút ra được hạn mức chi tiêu cần thiết cho từng khoản mục trong sinh hoạt hàng ngày. Việc này giúp bạn quản lý và kiểm soát dòng tiền hiệu quả hơn. Các khoản chi được phân chia rõ ràng với hạn mức cụ thể để bạn sử dụng một cách khoa học.</p>
				<p>Để xây dựng ngân sách chi tiêu khoa học và hiệu quả, có thể tham khảo một số cách phân chia ngân sách như sau:</p>
				<p>Bạn có thể cân nhắc việc chia ngân sách chi tiêu theo quy tắc 50/30/20 như sau:</p>
				<p>•	50% cho chi tiêu thiết yếu như tiền thuê nhà, ăn uống, điện nước…</p>
				<p>•	30% cho chi tiêu cá nhân như xem phim, du lịch…</p>
				<p>•	20% cho các mục tiêu tài chính như tiết kiệm, trả nợ...</p>
				<p>Tuy nhiên, các con số này có thể thay đổi linh hoạt phù hợp với hoàn cảnh của từng người. Hãy tăng chi phí thiết yếu lên 60-70% nếu bạn thấy nó cần thiết hơn nhu cầu giải trí của bản thân.</p>

				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/detail/detail-2.png" src="https://via.placeholder.com/75x30">
					<figcaption>Trọn đời miễn phí - Tiền về đầy ví</figcaption>
				</figure>
				
				<p>Ví dụ, với thu nhập 10 triệu đồng/ tháng, bạn có thể chia ngân sách như sau:</p>
				<p>•	5 triệu đồng dùng cho các chi tiêu thiết yếu.</p>
				<p>•	3 triệu đồng dành để chi tiêu cá nhân.</p>
				<p>•	2 triệu còn lại để tiết kiệm cho các mục tiêu tài chính trong tương lai. Tuy nhiên, do chiếc TV cũ bị hỏng nên bạn dự định sẽ mua chiếc mới vào tháng tới. Để duy trì kế hoạch, bạn cần thay đổi ngân sách bằng cách tăng số tiền tiết kiệm lên 3 triệu đồng/ tháng. Đồng thời giảm bớt chi tiêu cá nhân xuống còn 2 triệu đồng, để đảm bảo ngân sách.</p>

				<p class="b"></p>
				<p>Phương pháp Kakeibo của người Nhật cũng là một lựa chọn dễ áp dụng trong cách chia ngân sách. </p>
				<p>Thu nhập hàng tháng của bạn sẽ được chia vào 4 phong bì:</p>
				<p>•	Chi phí cơ bản: ăn uống, đi lại, hóa đơn…</p>
				<p>•	Chi phí mở mang kiến thức: mua sách, xem phim,…</p>
				<p>•	Chi phí không bắt buộc: nhà hàng, mua sắm…</p>
				<p>•	Chi phí phát sinh: sửa xe, ốm đau…</p>
				<p>Nếu tiêu hết tiền trong một danh mục nào đó, bạn có thể lấy tiền từ phong bì khác. Tuy nhiên, điều này đồng nghĩa với việc bạn sẽ còn ít tiền hơn để tiêu cho danh mục đó. Vì vậy, cần tính toán chi tiêu hợp lý để đảm bảo ngân sách đã đặt ra.</p>

				<p class="b">Phương pháp 50 – 50 </p>
				<p>Đơn giản hơn Phương pháp Kakeibo, bạn có thể chia thu nhập thành 2 phần. Một phần dùng để chi tiêu sinh hoạt hàng ngày, phần còn lại dành để tiết kiệm.</p>
				<p>Tóm lại, lập ngân sách càng cụ thể, rõ ràng, việc quản lý tiền bạc sẽ càng trở nên dễ dàng và hiệu quả hơn. Để duy trì ngân sách, cần nghiêm túc thực hiện theo kế hoạch đã đặt ra. Nên chi tiêu ít hơn số tiền kiếm được. Cắt giảm tối đa những chi phí không cần thiết để tiết kiệm tiền bạc, tích lũy nhiều hơn cho tương lai.</p>
				
				<p class="b">5. Theo dõi ngân sách thường xuyên khi thực hiện kế hoạch tài chính cá nhân</p>
				<p>Để quản lý tài chính hiệu quả, cần theo dõi ngân sách thường xuyên. Việc phân bổ chi tiêu cần được duy trì một cách hợp lý và thường xuyên. Tuy nhiên, cần có sự điều chỉnh linh hoạt theo biến động của thị trường.</p>
				<p>Nên ghi chép lại các khoản thu chi để theo dõi và kiểm soát tình hình tài chính trong gia đình dễ dàng hơn. Điều này sẽ giúp bạn biết được ngân sách mình đặt ra đã phù hợp hay chưa? Khoản mục nào cần cắt giảm hoặc tăng thêm?</p>
				<p>Từ đó, điều chỉnh kế hoạch chi tiêu sao cho phù hợp với mục tiêu tài chính của hai vợ chồng.</p>

				<p class="b">6. Đặt ra các mục tiêu tài chính</p>
				<p>Cách tốt nhất để bắt đầu tiết kiệm là có ý tưởng rõ ràng về những gì bạn đang tiết kiệm. Hãy xác định cụ thể những mục tiêu tài chính mà bạn mong muốn đạt được trong tương lai như mua nhà, đổi xe, đi du lịch…</p>
				<p>Đồng thời, ước tính số tiền cần tiết kiệm cùng thời gian thực hiện để hoàn thành mục tiêu. Xác định mục tiêu càng cụ thể, việc lập kế hoạch tiết kiệm càng trở nên dễ dàng.</p>
				<p>Xem xét các bước sau đây có thể giúp bạn xác định, lập kế hoạch và bắt đầu tiết kiệm để đáp ứng các mục tiêu tài chính:</p>
				<p>•	Kiểm tra tình hình tài chính hiện tại: Bạn có chi tiêu ít hơn số tiền bạn kiếm được không? Giá trị thực bạn nhận là bao nhiêu?</p>
				<p>•	Xác định mục tiêu: Bạn muốn thực hiện những gì? Điều gì sẽ xảy ra nếu một số mục tiêu của bạn không được hoàn thành?</p>
				<p>•	Thiết lập một số tiền: Bạn cần bao nhiêu tiền để đạt được từng mục tiêu?</p>
				<p>•	Thiết lập mốc thời gian: Khi nào bạn cần đạt được mục tiêu của mình</p>
				<p>•	Ghi lại mục tiêu: Liệt kê chúng bằng màu đen và trắng có thể giúp bạn đánh giá những gì bạn thực sự cần hoặc muốn</p>
				<p>•	Mục tiêu ưu tiên: Điều này sẽ giúp bạn theo đuổi một kế hoạch tập trung rõ ràng.</p>
				<p>•	Phát triển một kế hoạch: Thiết lập ngân sách bao gồm các mục tiêu tài chính của bạn.</p>
				<p>•	Kiểm tra tiến độ: Xem xét kế hoạch của bạn theo định kỳ và thực hiện các điều chỉnh khi cần thiết.</p>

				<p class="b">7. Tiết kiệm trước khi tiêu – quy tắc vàng khi thực hiện kế hoạch tài chính cá nhân</p>
				<p>Nhiều người có thói quen nhận lương về sẽ tiêu trước, dư ra được bao nhiêu mới tiết kiệm. Thực tế, đây không phải là một ý tưởng tồi.</p>
				<p>Tuy nhiên với cách làm này, sẽ rất lâu bạn mới tiết kiệm được một khoản kha khá dự trù cho những kế hoạch tài chính của mình.</p>
				<p><span class="b">Tiết kiệm trước khi tiêu</span> sẽ khiến bạn tự biết cân đối chi tiêu cho phù hợp với khoản tiền hiện có. Không bị rơi vào tình trạng thâm hụt như trước đây. Còn khoản tiền tiết kiệm hàng tháng, bạn nên cất kỹ vào một chỗ và tạm quên nó đi.</p>
				<p>Có thể gửi vào một tài khoản tiết kiệm của ngân hàng. Vừa đảm bảo an toàn, vừa mang lại một khoản tiền sinh lời hàng tháng. Hiện nay, lãi suất tiền gửi ở các ngân hàng Việt Nam khoảng 7 – 8%/ năm.</p>
				<p>Ngoài ra các ứng dụng ngân hàng hiện nay đều có phần tổng kết rõ ràng về số dư, lịch sử chuyền tiền, lịch sử giao dịch…kèm theo vô vàn ưu đãi, giúp bạn tiết kiệm không nhỏ khi sử dụng. Ví dụ như App MB Bank với ưu đãi miễn phí chuyển tiền trọn đời và gói quà tặng Family Combo hiện đang thu hút khá nhiều người sử dụng.</p>

				<figure class="text-center">
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/ketnoi/detail/detail-3.png" src="https://via.placeholder.com/75x30">
				</figure>

				<p class="b">8. Xây dựng kế hoạch tiết kiệm và đầu tư</p>
				<p>Cần tinh chỉnh cả kế hoạch tiết kiệm và kế hoạch đầu tư để tối ưu hóa quá trình hướng tới các mục tiêu tài chính.</p>
				<p class="b">Đặt mục tiêu tiết kiệm cho kế hoạch tài chính cá nhân</p>
				<p>Tự đặt ra số tiền bạn dự định tiết kiệm và phương pháp bạn sẽ sử dụng để tiết kiệm theo khoảng thời gian nhất định.</p>
				<p>Thực hiện với các phương thức tiết kiệm cụ thể như thiết lập kế hoạch tiết kiệm tự động, cắt giảm chi tiêu không cần thiết…</p>
				<p class="b">Lên kế hoạch đầu tư sinh lời</p>
				<p>Kiểm tra các mục tiêu tài chính để xác định mục tiêu ngắn hạn dưới 3 năm; trung hạn 3 – 7 năm; dài hạn > 7 năm. Điều này sẽ được xác định bởi khoảng thời gian bạn cần để hoàn thành mục tiêu.</p>
				<p>Đối với mỗi mục tiêu, hãy xem xét có bao nhiêu rủi ro đầu tư. Rủi ro đầu tư càng lớn, càng có nhiều biến động về giá trị. Lợi nhuận tiềm năng càng lớn thì rủi ro càng cao.</p>
				<p>Chứng khoán, Bitcoin, bất động sản… là những kênh đầu tư sinh lời hấp dẫn được nhiều người lựa chọn hiện nay.</p>

				<p class="b">9. Luôn lập kế hoạch cho tương lai</p>
				<p>Trước tiên, cần tích lũy một khoản tiền dự phòng cho các tình huống khẩn cấp có thể xảy ra trong cuộc sống. Chẳng hạn như sửa chữa xe, đau ốm, thất nghiệp…. Theo các chuyên gia tài chính, cần chuẩn bị quỹ khẩn cấp bằng 3 – 6 tháng chi phí sinh hoạt thông thường. Do đó, mỗi tháng, nên dành ít nhất 5 – 10% thu nhập để tiết kiệm cho quỹ này.</p>
				<p>Ví dụ: Nếu thu nhập khoảng 10 triệu đồng/ tháng, bạn cần chuẩn bị quỹ khẩn cấp từ 30 đến 60 triệu đồng để đảm bảo duy trì sinh hoạt khi gặp tình huống khó khăn.</p>
				<p>Bên cạnh việc thực hiện các mục tiêu tài chính, cần quan tâm đến việc lên kế hoạch tiết kiệm cho quỹ hưu trí. Chuẩn bị càng sớm, cuộc sống hưu trí sau này sẽ càng an nhàn.</p>
				<p>Nên dành 4 – 5% thu nhập hàng tháng để tiết kiệm cho quỹ hưu trí. Tuy nhiên, con số này có thể tăng giảm linh hoạt tùy thuộc vào điều kiện của mỗi người.</p>
				<p>Ngoài ra, có thể tham khảo việc mua các sản phẩm bảo hiểm hưu trí để đảm bảo cuộc sống khi về già. Tuy nhiên, cần tính toán kỹ lưỡng để đảm bảo ngân sách. Tránh ảnh hưởng đến các chi phí khác.</p>

				<p class="b">10. Thường xuyên cập nhật kế hoạch tài chính cá nhân</p>
				<p>Trước tiên, cần tích lũy một khoản tiền dự phòng cho các tình huống khẩn cấp có thể xảy ra trong cuộc sống. Chẳng hạn như sửa chữa xe, đau ốm, thất nghiệp…. Theo các chuyên gia tài chính, cần chuẩn bị quỹ khẩn cấp bằng 3 – 6 tháng chi phí sinh hoạt thông thường. Do đó, mỗi tháng, nên dành ít nhất 5 – 10% thu nhập để tiết kiệm cho quỹ này.</p>
				<p>Ví dụ: Nếu thu nhập khoảng 10 triệu đồng/ tháng, bạn cần chuẩn bị quỹ khẩn cấp từ 30 đến 60 triệu đồng để đảm bảo duy trì sinh hoạt khi gặp tình huống khó khăn.</p>
				<p>Bên cạnh việc thực hiện các mục tiêu tài chính, cần quan tâm đến việc lên kế hoạch tiết kiệm cho quỹ hưu trí. Chuẩn bị càng sớm, cuộc sống hưu trí sau này sẽ càng an nhàn.</p>
				<p>Nên dành 4 – 5% thu nhập hàng tháng để tiết kiệm cho quỹ hưu trí. Tuy nhiên, con số này có thể tăng giảm linh hoạt tùy thuộc vào điều kiện của mỗi người.</p>
				<p>Ngoài ra, có thể tham khảo việc mua các sản phẩm bảo hiểm hưu trí để đảm bảo cuộc sống khi về già. Tuy nhiên, cần tính toán kỹ lưỡng để đảm bảo ngân sách. Tránh ảnh hưởng đến các chi phí khác.</p>

				<p class="b">Nguồn: Money Lover</p>

				<p>Khách hàng có thể tải App MBBank và trải nghiệm tại:</p>
				<p>- Link tải tại App Store: <a href="#" class="cl1 b">https://apple.co/2AqB7ZM</a></p>
				<p>- Link tải tại Google Play: <a href="#" class="cl1 b">https://bit.ly/2v5ZsyP</a></p>
				<p>Hoặc liên hệ MB247: <span class="cl1 b">1900 545 426 / 024 3767 4050</span> để được tư vấn.
</p>
			</div>
			<br>

			<div class="tags">
				<h2>Tin bài khác</h2>
				<a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
				<a class="tag" href="#">Ngân hàng đầu tư</a>
				<a class="tag" href="#">Quản lý dòng tiền</a>
			</div>					
    	</div>
	</div>    	
</main>
</section>


<section  class="sec-tb" >
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-t">Tin bài liên quan</h2>
		</div>
		<div class="list-7  list-item row" >
		    <?php
		    $img = ['tlq-1.png','tlq-2.png'];
		    $title = 	[
		    				'"Quán quân" công bố thông tin năm 2017 đang thuộc …',
		    				'MB khuyến cáo khách hàng bảo mật thông tin ngân hàng điện tử'
		    			];
		    for($i=1;$i<=2;$i++) {?>
		    	<div class="col-md-6">
		          <a href="#" class="item item-inline-table">
		          	<div class="img">
		          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/ketnoi/detail/<?php echo $img[$i-1] ?>">
		          	</div>
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2"><?php echo $title[$i-1] ?></h4>
		          		<div class="desc line3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh…</div>
		          	</div>
		          </a>
	        	</div>
	    	<?php } ?>
	    </div>
	</div>	
</section>


<?php include 'include/index-bottom.php';?>