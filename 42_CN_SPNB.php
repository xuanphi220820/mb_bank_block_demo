<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Trang chủ</a>
        <a class="item" href="#">Cá nhân</a>
        <a class="item" href="#">Ngân hàng số</a>
        <span class="item">Gói sản phẩm gia đình tôi yêu</span>
      </div>
    </div>
</div>


<section class=" banner-heading-1 lazy-hidden group-ef next-shadow">
  <div class="container">
    <div class="divtext top35">
      <h1 class=" efch-2 ef-img-l">Gia đình tôi yêu</h1>
      <div class="efch-3 ef-img-l desc cl1">Tặng 1 triệu cho mỗi con <br> Miễn phí trọn đời cho bố mẹ</div>
    </div>
  </div>
  <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-5.jpg">
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Hướng dẫn đăng ký</a></li>
        <li><a href="#tab3" class="scrollspy">Quà tặng</a></li>
        <li><a href="#tab4" class="scrollspy">Chia sẽ</a></li>
        <li><a href="#tab5" class="scrollspy">Hỏi đáp</a></li>
        <li><a href="#tab6" class="scrollspy">Ưu đãi</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php'; ?>


<section id="tab2" class="sec-tb sec-img-svg group-ef lazy-hidden">
  <div class="container">
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">Hướng dẫn dăng ký</h2>
    </div>
    <div class="row list-item">
      <?php
      $img = ['meeting.svg','calendar.svg','policy.svg'];
      $title =  [
        'Bước 1 - Bố mẹ chứng minh',
        'Bước 2 - Đăng ký online',
        'Bước 3 - Hoàn tất và kích hoạt'
      ];
      $desc = [
        'Chứng minh thư của bố & mẹ, giấy đăng ký kết hôn, giy khai sinh của con',
        'Truy cập website để cung cấp thông tin và làm theo hướng dẫn',
        'Bố mẹ đến các chi nhánh MB gần nhất để ký hợp đồng gói dịch vụ Gia đình tôi yêu và nhận các quà tặng cho con và bố mẹ'
      ];
      for ($i = 1; $i <= 3; $i++) { ?>
        <div class="col-sm-6 col-md-4 efch-<?php echo $i + 1; ?> ef-img-t ">
          <div class="item">
            <div class="img ">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i-1] ?>" src="https://via.placeholder.com/6x4">
            </div>
            <div class="divtext">
              <h4 class="title text-normal"><?php echo $title[$i - 1]; ?></h4>
              <div class="desc"><?php echo $desc[$i - 1]; ?></div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab3">
  <div class="container">
    <div class="max950">   
      <div class="row list-item">
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Quà tặng cho con</h4>
            <div class="widget-content entry-content">
              <div class="toggleAutoHeight" data-height="200" data-more="+ Xem thêm" data-less="- Thu gọn" data-i="" >
                <div class="tgh-content">
                  <div class="tgh-first">
                    <p class="b">Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                    <ul>
                      <li>Phụ huynh thông minh - Chi càng nhiều, tích lũy càng nhiều cho con</li>
                      <li>Với mỗi chi tiêu 1 triệu trên thẻ tín dụng của bố mẹ, tài khoản tiết kiệm của các con sẽ được cộng thêm 0.3% giá trị giao dịch</li>
                    </ul>
                    <p class="b">1 triệu+++ cho mỗi con - không giới hạn</p>
                  </div>                
                  <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                  <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                  <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                  <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
                  <?php if($i==1) { ?>
                    <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                    <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                    <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                    <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="widget-default">
            <h4 class="widget-title">Quà tặng cho bố mẹ</h4>
            <div class="widget-content entry-content">
              <div class="toggleAutoHeight" data-height="200" data-more="+ Xem thêm" data-less="- Thu gọn" data-i="" >
                <div class="tgh-content">
                  <div class="tgh-first">
                    <p class="b">Miễn phí trọn đời</p>
                    <p>Khách hàng được miễn phí trọn đời cho các giao dịch chuyển tiền trong và ngoài hệ thống của MBBank (bao gồm chi phí chuyển khoản liên ngân hàng) trên App MBBank và cho toàn bộ giao dịch tại các điểm giao dịch của MB.</p>
                    <p class="b">Tặng thẻ tín dụng</p>
                    <p>Tặng ngay thẻ tín dụng với hạn mức tối đa 100 triệu đồng cho</p>
                  </div>                
                  <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                  <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                  <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                  <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
                  <?php if($i==1) { ?>
                    <p>Cho phép khách hàng thực hiện các tính năng phi tài chính:</p>
                    <p>- Quản lý thông tin chung (Truy vấn nhật ký truy cập; thay đổi mật khẩu</p>
                    <p>- Thay đổi thông tin cá nhân..); Hoạt động tài khoản (Truy vấn thông tin số dư tài khoản</p>
                    <p>- Truy vấn các giao dịch trong ngày; tìm kiếm giao dịch; quản lý tài khoản..)</p>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="4" class="sec-b">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht efch-1 ef-img-l">MB kết nối</h2>
      <a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>
    <div class="list-5 row list-item">
      <?php
      $img = ['mbketnoi-1.jpg','mbketnoi-2.jpg','mbketnoi-3.jpg'];
      $title =  [
                  'Tương lai cho con bắt đầu từ sự đầu tư của cha mẹ',
                  'Gia đình - Nền tảng của cuộc sống',
                  'Làm sao để bố mẹ có quỹ tích lũy khi con trưởng thành'
                ];
      $desc =   [
                  'Thay vì phải xếp hàng tại các ngân hàng để làm thủ tục chuyển tiền, giờ đây, tất cả đã trở nên dễ dàng hơn bao giờ hết khi sử dụng App MBBank',
                  'Rút tiền tại ATM qua App MBBank không chỉ tiện lợi khi bạn quên ví, quên thẻ mà còn có thể chuyển tiền cho người thân vô cùng nhanh chóng và bảo mật.',
                  'Chỉ bằng vài cú chạm trên App MBBank, bất kỳ ở đâu và bất kỳ khi nào khách hàng đều có thể thực hiện giao dịch vay vốn một cách nhanh chóng và an toàn mà không cần giấy tờ, không cần tài sản bảo đảm.'
                ];
      for ($i = 1; $i <= 3; $i++) { ?>
        <div class="col-md-4">
          <a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l ">
            <div class="img tRes_71">
              <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/gdty/<?php echo $img[$i-1] ?>">
            </div>
            <div class="divtext">
              <h4 class="title line2"><?php echo $title[$i-1] ?></h4>
              <div class="desc line2"><?php echo $desc[$i-1] ?></div>
            </div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab5" class=" sec-b sec-cauhoi ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht ">Câu hỏi thường gặp</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>      
    <div class="accodion accodion-1">
        <?php
        $ques = [
                  'Gia đình tôi được nhận tối đa bao nhiêu sổ tiết kiệm cho con khi tham gia gói?',
                  'Khi tham gia gói “Gia đình tôi yêu” vợ chồng tôi được miễn phí những dịch vụ gì?',
                  'Sau khi đăng ký bao lâu thì gia đình tôi được hưởng các ưu đãi từ gói “Gia đình tôi yêu”',
                  'Vợ/chồng tôi hiện đang có thẻ tại ngân hàng khác và chưa có tài khoản tại MB có thể tham gói “Gia đình tôi yêu” không và cần những giấy tờ gì?'
                ];
        for($i=1;$i<=4;$i++) {
        ?>
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span><?php echo $ques[$i-1] ?><span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content entry-content" >
                    <div class="inner">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde doloremque soluta ullam modi aliquam totam enim, quasi ex, consequuntur tenetur non tempora illo beatae ad dolor delectus distinctio debitis eos.</p>
                    </div>
                </div>
            </div>
        <?php
        } ?>
    </div>           
  </div>
</section>

<section id="tab6"  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khuyến mãi</h2>
            <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $a_h1 = [
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
                    ];
            $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
            for($i=1;$i<=8;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php'; ?>






<?php include 'include/index-bottom.php'; ?>