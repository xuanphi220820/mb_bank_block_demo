<?php include 'include/index-top.php'; ?>

<div class="entry-breadcrumb">
	<div class="container">
		<div class="breadcrumbs">
			<a class="item" href="#">Home</a>
			<a class="item" href="#">Doanh nghiệp</a>
			<span class="item">Doanh nghiệp lớn CIB</span>
		</div>
	</div>
</div>

<section class="banner-img-1 next-shadow">
	<img class="img loaded loaded" data-lazy-type="image" data-lazy-src="assets/images/heading-1.jpg" src="assets/images/doanhnghiep/dnlon/banner.jpg">
</section>

<section class="sec-tb">
	<div class="container">
		<div class="max750">
			<h1 class="text-center">Nhu cầu của doanh nghiệp</h1>
			<div class="menuicon  owl-carousel s-nav nav-2" data-res="6,4,3,2" paramowl="margin=0">
				<?php
				$img = ['bank.svg','money-2.svg','key.svg','lai-suat.svg','dich-vu-khac.svg','money-1.svg','bank.svg','money-2.svg','key.svg','lai-suat.svg'];
				$a_h1 = [
					'Sản phẩm nổi bật',
					'Ngân hàng đầu tư',
					'Tín dụng',
					'Dịch vụ thu hộ hóa đơn',
					'Thanh toán quốc tế & Tài trợ thương mại',
					'Sản phẩm ngoại hối & phát sinh',
					'Tài trợ & Phát sinh',
					'Sản phẩm nổi bật',
					'Ngân hàng số MBBiz',
					'Tài khoản và dịch vụ',
					'Cho vay & Bảo lãnh'
				];
				$link = ['#tab1','#tab2','#tab3','#tab4','#tab5','#tab6'];
				for ($i = 1; $i <= 10; $i++) { ?>
					<div class="item">
						<a href="<?php echo $link[$i-1]?> " class="link scrollspy">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="title"><?php echo $a_h1[$i - 1] ?></div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<section id="tab1" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Sản phẩm nổi bật</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Ngân hầng đầu tư',
				'Trái phiếu doanh nghiệp G-Bond',
				'Dịch vụ quản lý dòng tiền dành KHDN',
				'Ngân hầng đầu tư',
				'Trái phiếu doanh nghiệp G-Bond',
				'Dịch vụ quản lý dòng tiền dành KHDN'
			];
			$img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg','img-5.jpg','img-6.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab2" class="sec-tb sec-h-3 ">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">Ngân hàng đầu tư</h2>
		</div>
		<div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="3,3,2,1" paramowl="margin=0">
			<?php
			$a_h1 = [
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN',
				'Tư vấn phát hành TPDN',
				'Dịch vụ quản lý tài khoản TPDN',
				'Dịch vụ quản lý TSBĐ cho TPDN'
			];
			$img = ['img-4.jpg','img-5.jpg','img-6.jpg','img-1.jpg','img-2.jpg','img-3.jpg'];
			for ($i = 1; $i <= 10; $i++) { ?>
				<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
					<div class="img tRes_71">
						<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/dnlon/<?php echo $img[$i-1] ?>">
					</div>
					<div class="divtext">
						<h4 class="title line2"><?php echo $a_h1[$i-1] ?></h4>
						<div class="desc line2">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>

<section id="tab3" class="sec-tb sec-img-svg group-ef lazy-hidden">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht  efch-1 ef-img-t">Why MB?</h2>
		</div>
		<div class="max950">
			<div class="row  list-item grid-space-60">
				<?php
				$img = ['giai-thuong.svg', 'meeting.svg', 'thong-bao.svg'];
				$a_5_2 = ['Giải thưởng', 'Sự kiện <br> (Hợp tác toàn diện)', 'Về MB'];
				$link = ['10_ve_mb_thanhtich.php','#','05_Ve_MB_3.php'];
				for ($i = 1; $i <= 3; $i++) { ?>
					<div class="col-sm-4 efch-<?php echo $i + 1; ?> ef-img-t ">
						<a href="./<?php echo $link[$i-1] ?>">
							<div class="item">
								<div class="img ">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/other/<?php echo $img[$i-1] ?>" src="https://via.placeholder.com/6x4">
								</div>
								<div class="divtext">
									<h4 class="title"><?php echo $a_5_2[$i - 1]; ?></h4>
								</div>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<section class="sec-b sec-h-4">
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-l">MB kết nối</h2>
			<a class="viewall" href="./40_mb_ket_noi.php">Xem tất cả <i class="icon-arrow-1"></i></a>
		</div>

		<div class="row list-item">
			<div class="col-lg-8 ">

				<div class="list-5 row ">
					<?php
					$img = ['MBKN-1.jpg','MBKN-2.jpg'];
					$a_h1 =	[
						'MB ra mắt hệ sinh thái số dành cho khách hàng doanh nghiệp',
						'Doanh nghiệp vừa và nhỏ được quảng cáo miễn phí với gói sản phẩm "SWE ..."'
					];
					for ($i = 1; $i <= 2; $i++) { ?>
						<div class="col-md-6">
							<a href="#" class="item efch-<?php echo $i + 1; ?> ef-img-l equal">
								<div class="img tRes_71">
									<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
								</div>
								<div class="divtext">
									<div class="date">01/ 12/ 2019</div>
									<h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
									<div class="desc line2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur </div>
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="list-6">
					<?php
					$img = ['MBKN-3.jpg','MBKN-4.jpg','MBKN-5.jpg'];
					$a_h1 =	[
						'Cho vay mua, xây dựng, sữa chữa nhà, đất',
						'Kinh nghiệm chuẩn bị tài chính và các thủ tục đi du học',
						'Dùng heo đất để tiết kiệm định kì đúng cách với app MBBank'
					];
					for ($i = 1; $i <= 3; $i++) { ?>
						<a href="#" class="item item-inline-table">
							<div class="img">
								<img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/doanhnghiep/mb-ketoi/<?php echo $img[$i - 1] ?>">
							</div>
							<div class="divtext">
								<h4 class="title line4"><?php echo $a_h1[$i - 1] ?></h4>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="sec-b sec-tigia sec-h-2">
	<div class="container">
		<div class="row list-item list-2">
			<div class="col-lg-7">
				<div class="cttab-v3 divtigia">
					<div class="tab-menu">
						<div class="tg-tab active">Tỷ giá</div>
						<div class="tg-tab">Lãi suất <span class="cl5 text-normal fs18">(%/ năm)</span></div>
					</div>
					<div class="tab-content">
						<div class="active">
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Mã NT</th>
										<th>Mua tiền mặt</th>
										<th>Mua chuyển khoản</th>
										<th>Bán ra</th>
									</tr>
									<?php
									for ($i = 1; $i <= 5; $i++) { ?>
										<tr>
											<td><img src="assets/images/flags/us.png" alt=""> USD</td>
											<td>23,180</td>
											<td>23,180</td>
											<td>23,180</td>
										</tr>
									<?php } ?>
								</table>
							</div>
						</div>
						<div>
							<div class="tab-inner">
								<table class="table">
									<tr>
										<th>Kỳ hạn</th>
										<th>USD</th>
										<th>VND</th>
									</tr>
									<tr>
										<td>Không kỳ hạn</td>
										<td>0 %</td>
										<td>0.1 %</td>
									</tr>
									<tr>
										<td>1 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>2 Tháng</td>
										<td>0 %</td>
										<td>4.3 %</td>
									</tr>
									<tr>
										<td>3 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>5 Tháng</td>
										<td>0 %</td>
										<td>4.8 %</td>
									</tr>
									<tr>
										<td>6 Tháng</td>
										<td>0 %</td>
										<td>5.3 %</td>
									</tr>
									<tr>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-5 efch-7 ef-img-t">
					<div class="item item-2 tg-truycapnhanh">
						<h5 class="uppercase fs24 mb-30">Truy cập nhanh</h5>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t10"></i> Mở tài khoản DN online</a>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t8"></i> Đăng ký vay</a>
						<a class="btn btn-3 radius-8" href="#"> <i class="icon-t11"></i> Tra cứu thông tin bảo lãnh</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/index-bottom.php'; ?>