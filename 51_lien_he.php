<?php include 'include/index-top.php';?>

<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Liên hệ</span>
      </div>
    </div>
</div>


<main id="main" class="section page-lien-he">
	<div class="container ">
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="widget widget-contact">
					<h1 class="widget-title  h2">Liên hệ</h1>

					<h3 >Hội sở chính MBBank:</h3>
					<p>Tòa Nhà MB Sunny Tower, Số 259, Trần Hưng Đạo, Phường Cô Giang, Q. 1, HCM</p>
					<h3 >Thời gian phục vụ khách hàng</h3>
					<p>
					Buổi sáng: 7h30 - 11h30<br>
					Buổi chiều: 13h - 17h<br>
					Từ thứ 2 đến thứ 6 hàng tuần
					</p>
					<h3 >Hotline</h3>
					<div class="phone">1900 545426</div>
					<p>(84-24) 3767 4050 (quốc tế gọi về)</p>
					<h3 >Email</h3>
					<p>mb247@mbbank.com.vn</p>

				</div>
			</div>
			<div class="col-lg-8 col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.825755493042!2d106.58953855121688!3d10.74790876261876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752db1e79c75c9%3A0xb96d562d1552b1a3!2sATM%20Joint%20Stock%20Commercial%20Bank%20For%20Investment%20And%20Development%20Of%20Vietnam%20(BIDV)-%20Tan%20Tao%20Industrial!5e0!3m2!1sen!2s!4v1579054788840!5m2!1sen!2s" width="100%" height="565" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</main> <!-- #main -->
<section class=" sec-tb "   >
  <div class="container"  >
		<div class="max750">
			<div class="max600 entry-head text-center">
		      <h2 class="ht efch-1 ef-tx-t">Gửi tin nhắn</h2>
		      <p class="w6">Nếu Quý Khách có yêu cầu, góp ý, khen ngợi hoặc ý kiến phản hồi xin vui lòng gửi email trực tiếp về cho chúng tôi.</p>
		    </div>		
		    <div class="mb-30 text-center">
				<label class="radio "><input type="radio" name="check1" checked=""><span></span>Cá nhân</label> &nbsp; &nbsp; &nbsp;
				<label class="radio "><input type="radio" name="check1" ><span></span>Doanh nghiệp</label>		    	
		    </div>	
			<form class="row list-item form-contact">
				<div class="col-12">
					<label class="block"> 
						<span class="title">Họ và tên  (<span class="require">*</span>)</span>
						<input class="input" placeholder="Tên đầy đủ của bạn">
					</label>
				</div>
				<div class="col-6">
					<label class="block"> 
						<span class="title">Số điện thoại (<span class="require">*</span>)</span>
						<input class="input" placeholder="XXX-XXX-XXX"> 
					</label>
				</div>					
				<div class="col-6">
					<label class="block"> 
						<span class="title">Email  (<span class="require">*</span>)</span>
						<input class="input" placeholder="Email@gmail.com">
					</label>
				</div>

				<div class="col-12">
					<label class="block"> 
						<span class="title">Nội dung (<span class="require">*</span>)</span>
						<textarea class="input"></textarea>
					</label>
				</div>
				<div class="col-12 text-center">
					<button class="btn">Gửi ngay</button>
				</div>			
			</form>
		</div>

  </div>
</section>

<?php include 'include/index-bottom.php';?>





