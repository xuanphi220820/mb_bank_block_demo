<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section   class=" banner-heading-1 lazy-hidden group-ef next-shadow" >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >MB Visa</h1>
        <div class="efch-3 ef-img-l desc cl1 b">Thẻ ghi nợ quốc tế</div>
        </div>
        <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-12.jpg">
    </div>
    
</section>

<section   class=" sec-menu" >
    <div class="container">
    <ul>
        <li class="active"><a href="#tab1" class="scrollspy">Giới thiệu</a></li>
        <li><a href="#tab2" class="scrollspy">Tính năng</a></li>
        <li><a href="#tab3" class="scrollspy">Hồ sơ đăng ký</a></li>
        <li><a href="#tab4" class="scrollspy">Hỏi đáp</a></li>
        <li><a href="#tab5" class="scrollspy">Ưu đãi</a></li>
    </ul>
    </div>
</section>

<?php include '_block/block_4.php';?>


<section id="tab2" class="sec-b sec-img-svg group-ef lazy-hidden">
  <div class="container"  >
    <div class="entry-head text-center">
      <h2 class="ht  efch-1 ef-img-t">Tính Năng</h2>
    </div>    
    <div class="row list-item grid-space-60">
      <?php 
      $img = ['quet-the.svg','money-2.svg','toan-cau.svg','policy.svg','money-4.svg','ung-dung-3.svg'];
      $a_h1 = [
                'Thanh toán mọi điểm',
                'Thanh toán dễ dàng',
                'Rút tiền toàn cầu',
                'Đăng ký thuận tiện',
                'Kiểm soát dễ dàng',
                'Bảo mật an toàn'
              ];
      $desc = [
                'Thanh toán tại mọi điểm chấp nhận thẻ, có biểu tượng Visa trên toàn cầu',
                'Thanh toán dễ dàng và nhanh chóng qua internet',
                'Rút tiền ATM có biểu tượng Visa/Plus trên toàn cầu',
                'Thanh toán tại mọi điểm chấp nhận thẻ, có biểu tượng Visa trên toàn cầu',
                'Thanh toán dễ dàng và nhanh chóng qua internet',
                'Rút tiền ATM có biểu tượng Visa/Plus trên toàn cầu'
              ];
      for($i=1;$i<=6;$i++) { ?>
      <div class="col-sm-6 col-md-4 efch-<?php echo $i+1; ?> ef-img-t ">
        <div class="item">
          <div class="img ">
            <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/svg/bank/<?php echo $img[$i-1] ?>" src="https://via.placeholder.com/6x4">
          </div>
          <div class="divtext">
            <h4 class="title"><?php echo $a_h1[$i - 1] ?></h4>          
            <div class="desc"><?php echo $desc[$i - 1] ?></div>
          </div>   
        </div>     
      </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="tab3" class="sec-b sec-img-text group-ef lazy-hidden">
  <div class="container"  >
    <div class="row center">
      <div class="col-lg-6">
        <div class="img tRes_66 efch-2 ef-img-r ">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/canhan/mb-visa/hosodangky.jpg" src="https://via.placeholder.com/10x6">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="divtext entry-content">
          <h2 class="ht  efch-1 ef-tx-t ">Hồ sơ đăng ký</h2>
           
          <p>Chứng min nhân dân/Hộ chiếu/Chứng minh sĩ quan còn hiệu lực</p>
          <p >Đăng ký nhận thông tin từ đội ngũ chăm sóc khách hàng của MB Bank</p>
          <a class="btn lg" href="#">ĐĂNG KÝ NGAY</a>  </p>

        </div>
      </div>
    </div>
  </div>
</section>

<section id="tab4" class=" sec-b sec-cauhoi ">
  <div class="container">
    <div class="entry-head">
      <h2 class="ht ">Câu hỏi thường gặp</h2>
      <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
    </div>      
    <div class="accodion accodion-1">
        <?php
        $ques = [
                  'Những ai được sử dụng dịch vụ?',
                  'Đăng ký App ngân hàng MB Bank bằng cách nào?',
                  'Tôi có thể chuyển tiền vào các ngày thứ 7, chủ nhật, ngày lễ không?',
                  'Tôi có thể khóa thẻ của tôi tại MB từ xa qua App ngân hàng MB Bank  không?'
                ];
        for($i=1;$i<=4;$i++) {
        ?>
            <div class="accodion-tab ">
                <input type="checkbox" id="chck_1_<?php echo $i; ?>" <?php if($i==1) echo 'checked'; ?> >
                <label class="accodion-title" for="chck_1_<?php echo $i; ?>" ><span><?php echo $ques[$i-1] ?><span class="triangle" ><i class="icon-plus"></i></span> </label>
                <div class="accodion-content entry-content" >
                    <div class="inner">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. Mauris malesuada nisi sit amet augue accumsan tincidunt. Maecenas tincidunt, velit ac porttitor pulvinar.</p>
                </div>
            </div>
        <?php
        } ?>
    </div>           
  </div>
</section>

<?php include '_block/tailieu_1.php' ?>

<section id="tab5"  class="sec-b " >
    <div class="container">
        <div class="entry-head">
            <h2 class="ht efch-1 ef-img-l">Khuyến mãi nổi bật</h2>
            <a class="viewall" href="#">Xem tất cả <i class="icon-arrow-1"></i></a>
        </div>
        <div class="owl-carousel equalHeight s-nav nav-2 list-5" data-res="4,3,2,1" paramowl="margin=0">
            <?php
            $a_h1 = [
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Thông báo danh sách khách hàng trúng thưởng CT',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch',
                      'Tài trợ các doanh nghiệp kinh doanh xăng dầu',
                      'Những lưu ý khi chi tiêu thanh toán khi đi du lịch'
                    ];
            $img = ['khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4','khuyenmai-1','khuyenmai-2','khuyenmai-3','khuyenmai-4'];
            for($i=1;$i<=8;$i++) {?>
              <a href="#" class="item efch-<?php echo $i+1; ?> ef-img-l equal">
                <div class="img tRes_71">
                    <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/khuyenmai/<?php echo $img[$i-1] ?>.jpg">
                </div>
                <div class="divtext">
                    <div class="date">01/ 12/ 2019</div>
                    <h4 class="title line2"><?php echo $a_h1[$i-1]; ?></h4>
                </div>
              </a>
            <?php } ?>
        </div>          
    </div>
</section>

<section  class="sec-tb bg-gray" >
  <div class="container">
    <div class="entry-head">
        <h2 class="ht efch-1 ef-img-l">Sản phẩm liên quan</h2>
    </div>    
    <div class="list-7  list-item row" >
        <?php
        $a_h1 = [
          'Thẻ tín dụng Quốc tế MB JCB',
          'Đặc quyền cho chủ thẻ MB Visa',
          'Vay nhà đất, nhà dự án',
          'Mua siêu nhanh trên App MBBank'
          ];
        $img = ['img-1.jpg','img-2.jpg','img-3.jpg','img-4.jpg'];
        for($i=1;$i<=4;$i++) {?>
          <div class="col-md-6">
              <a href="#" class="item item-inline-table">
                <div class="img">
                  <img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="assets/images/canhan/splq/<?php echo $img[$i-1] ?>">
                </div>
                <div class="divtext">
                  <h4 class="title line2"><?php echo $a_h1[$i - 1] ?></h4>
                  <div class="desc line4">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
                </div>
              </a>
            </div>
        <?php } ?>
      </div>  
        <div class="tags">
            <a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
            <a class="tag" href="#">Ngân hàng đầu tư</a>
            <a class="tag" href="#">Quản lý dòng tiền</a>
        </div>           
    </div>
</section>

<?php include '_block/tu_van.php';?>






<?php include 'include/index-bottom.php';?>