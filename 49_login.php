<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Mbbank PC</title>
<link rel="shortcut icon" href="images/favicon.ico">
<link rel='stylesheet'  href='assets/css/main.css' type='text/css' media='all' />
<link rel='stylesheet'  href='assets/css/bonus.css' type='text/css' media='all' />
<script type="text/javascript" src="assets/js/jquery.js" ></script>



<!-- <link rel="manifest" href="https://tt.mangoads.vn/propzy/pc/_manifest.json" data-pwa-version="set_by_pwa.js"> -->

<!-- META FOR FACEBOOK -->
<meta property="og:site_name" content="Propzy" />
<meta property="og:url" itemprop="url" content="http://localhost"/>
<meta property="og:image" itemprop="thumbnailUrl" content="xxx"/>
<meta property="og:title" content="Home" itemprop="headline" />
<meta property="og:description" content="" itemprop="description" />
<!-- END META FOR FACEBOOK -->

</head>

<body class="page-login">
<div id="wrapper">
	<div id="panel" >
	  <div class="container">
	    <div class="row">
	      <div class="">

	        <ul class="menu line">
	        	<li><a href="#" class="fs14"><i class="icon-search-2"></i>&nbsp;Tìm kiếm</a></li>
	          	<li><a href="#" class="fs14">Về MBBank</a></li>
	          	<li><a href="#" class="fs14">Nhà đầu tư</a></li>
	          	<li><a href="#" class="fs14">Tuyển dụng</a></li>
	          	<li><a href="#" class="fs14">Liên hệ</a></li>
	        </ul>  
	    	</div>	
	    	<div>
	        <ul class="menu line text-right">
	          <li>
	            <div class="dropdown language">
	              <div class="title"> <span><img src="assets/images/flags/vn.png" alt=""></span> <i class="icon-arrow-2 ib"></i> </div>
	              <div class="content">
	                <div class="inner">
	                  <ul class="menu">
	                    <li class="lang-en"><a href="#" hreflang="en" title="English (en)"><img src="assets/images/flags/gb.png" alt=""> <span>English</span></a></li>
	                    <li class="lang-vi active"><a href="#" hreflang="vi" title="Tiếng Việt (vi)"><img src="assets/images/flags/vn.png" alt=""> <span>Tiếng Việt</span></a></li>
	                  </ul>
	                </div>
	              </div>
	            </div>            
	          </li>
	        </ul>  

	      </div>

	    </div>
	   
	  </div>
	</div>

	<main id="main">
		<div class="container clearfix">
			<form class="form-login">
				<div class="heading">
					<div class="logo"><a href="./" > <img src="assets/images/logo-blue.svg" alt=""></a></div>
					<h4 class="title">Chào mừng đến eBanking</h4>
					<div class="desc">Dành cho khách hàng cá nhân</div>
				</div>
				<input class="input" placeholder="Tên đăng nhập" />
				<input class="input" type="password" placeholder="Mật khẩu" />
				<div class="wcaptchar">
					<input class="input" placeholder="Nhập mã kiểm tra" />
					<div class="captchar"><img src="assets/images/captchar.png" alt=""></div>
				</div>
				<button class="btn btn-3">Đăng nhập</button>
				<button class="btn">Đăng ký ngay</button>

				<ul>
					<li><a href="#">Quên mật khẩu</a></li>
					<li><a href="#">Điều khoản và điều kiện</a></li>
					<li><a href="#">Hướng dẫn giao dịch an toàn</a></li>
				</ul>



			</form>
			<div class="img"><img src="assets/images/login-img.jpg" alt="">	</div>
		</div>

		<div id="footer-login" >
			<div class="container">

				<div class="menu row grid-space-20">
					<?php 
					$a_f_1 = ['icon-t8','icon-t9','icon-t10','icon-t11','icon-t12'];
		    		$a_f_2 = ['Tiết kiệm','Ưu đãi','Vay vốn','Thanh toán hoá đơn','Chuyển tiền 24/7'];
					for($i=1;$i<=5;$i++) { ?>
					<div class="col-sm-4 col-6">
						<a href="#" class="item"> <i class="<?php echo $a_f_1[$i-1]; ?>"></i> <span class="text"><?php echo $a_f_2[$i-1]; ?></span></a>
					</div>
					<?php } ?>

				</div>

				<div class="copyright">
				<div>Bản quyền © 2016 thuộc về Ngân hàng Quân đội. Bảo lưu mọi quyền.</div>
				<ul class="blog-item-social ">
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
				</ul>
				</div>
			</div>			
		</div>			
	</main>




</div> <!--End #wrapper-->

<script type='text/javascript' src='assets/js/script.js'></script> 


</body></html>				        