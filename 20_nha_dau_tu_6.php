<?php include 'include/index-top.php';?>
<div class="entry-breadcrumb">
    <div class="container">
      <div class="breadcrumbs">
        <a class="item" href="#">Home</a>
        <span class="item">Biểu phí lãi xuất</span>
      </div>
    </div>
</div>
<section   class="sec banner-heading-1 next-shadow"  >
    <div class="container">
        <div class="divtext top35">
        <h1 class=" efch-2 ef-img-l" >Bạn cần biết</h1>
        </div>
    </div>
    <img class="img br lazy-hidden efch-1 ef-img-r" data-lazy-type="image" data-lazy-src="assets/images/heading-14_6.jpg">
</section>

<main id="main" class="sec-tb">
  <div class="container">
    <div  class="cttab-v3   ">
      <div  class="tab-menu max500">
        <div  class="active"><span>Biểu phí</span></div>
        <div  ><span>Lãi suất</span></div>
      </div>
      <div class="tab-content">
        <div class="active">
          <div class="tab-inner">

            <div class="accodion accodion-2">
                <?php
                $arr_1 =  [
                            'BIỂU PHÍ KHCN',
                            'BIỂU PHÍ DỊCH VỤ ÁP DỤNG ĐỐI VỚI KHÁCH HÀNG SME',
                            'PHÍ THẺ',
                            'PHÍ THANH TOÁN, KIỀU HỐI',
                            'BIỂU PHÍ TÀI KHOẢN, TIỀN GỬI, NGÂN QUỸ',
                            'BIỂU PHÍ DỊCH VỤ NGÂN HÀNG ĐIỆN TỬ',
                            'BIỂU PHÍ DỊCH VỤ DÀNH CHO KHÁCH HÀNG CÁ NHÂN'
                          ]; 
                for($j=1;$j<=7;$j++) {
                ?>
                <div class="accodion-tab ">
                    <input type="radio" id="chck_1_<?php echo $j; ?>" <?php if($j==1) echo 'checked'; ?> name="chck_1" >
                    <label class="accodion-title h2" for="chck_1_<?php echo $j; ?>" ><span> 0<?php echo $j; ?>. <?php echo $arr_1[$j-1] ?></span> <span class="triangle" ><i class="icon-plus"></i></span> </label>
                    <div class="accodion-content entry-content" >
                        <div class="inner">
                          <p>Hiệu lực từ ngày 30/5/2018 - Lưu ý: Biểu phí này chưa bao gồm 10% thuế VAT, ngoại trừ một số loại phí tại mục D.Thẻ\ 5.Thẻ tín dụng dành cho KHCN (bao gồm: Phí phát hành, Phí thường niên, Phạt chậm thanh toán, Phí thay đổi hạn mức tín dụng thẻ theo yêu cầu của KH, Phí thay đổi hình thức đảm bảo sử dụng thẻ) không áp dụng VAT</p>

                          <h3 class="cl7">A. MỞ TÀI KHOẢN</h3>
                          <div class="table-responsive">
                            <table class="table table-full table-bieuphi">
                              <tr>
                                <th>STT</th>
                                <th>Khoản mục phí</th>
                                <th>Mức phí</th>
                                <th>Tối thiểu</th>
                                <th>Tối đa</th>
                              </tr>
                              <?php
                              $cot2 = [
                                        'Mở tài khoản thanh toán thông thường VND và ngoại tệ',
                                        'Mở tài khoản thanh toán Số đẹp (V1): 6 số cuối giống nhau',
                                        'Mở tài khoản thanh toán Số đẹp (V2): 6 số đẹp (6 số lặp, 6 số tiến, các số loại: aaabbb, aabbaa, aabbcc, abcabc)',
                                        'Mở tài khoản thanh toán Số đẹp (V3): 5 số đẹp (5 số giống nhau, 5 số tiến, các số loại: aaaab, aabbb, aaabb, aabaa)',
                                        'Mở tài khoản thanh toán Số đẹp (V4): 4 số đẹp (4 số giống nhau, 4 số lặp abab, 4 số tiến, các số loại: aabb, aaab)',
                                        'Mở tài khoản thanh toán Số đẹp (V5): các loại số chọn còn lại (chọn 3,4,5,6 số bất kỳ)'
                                      ];
                              $cot3 = [
                                        'Miễn phí (Số dư tối thiểu tài khoản VND: 50.000 VND)',
                                        '25.000.000 VND <br> KHDN CC: 12.500.000 VND',
                                        '15.000.000 VND <br> KHDN CC: Miễn phí',
                                        '10.000.000 VND <br> KHDN CC: Miễn phí',
                                        '1.000.000 VND <br> KHDN CC: Miễn phí',
                                        '200.000 VND <br> KHDN CC: Miễn phí'
                                      ];
                              $cot4 = [
                                        ' ',
                                        '25.000.000 VND <br>   KHCN CC: 12.500.000 VND',
                                        '15.000.000 VND <br>   KHCN CC: Miễn phí',
                                        '10.000.000 VND <br>   KHCN CC: Miễn phí',
                                        '1.000.000 VND <br>   KHCN CC: Miễn phí',
                                        '200.000 VND <br>   KHCN CC: Miễn phí'
                                      ];
                              for($i=1;$i<=5;$i++) {
                              ?>                            
                              <tr>
                                <td>0<?php echo $i; ?> </td>
                                <td><span class="b"><?php echo $cot2[$i-1] ?></span></td>
                                <td><?php echo $cot3[$i-1] ?></td>
                                <td><?php echo $cot3[$i-1] ?></td>
                                <td></td>
                              </tr>
                              <?php
                              } ?>
                            </table>
                          </div>


                          <h3 class="cl7">B. QUẢN LÝ TÀI KHOẢN</h3>
                          <div class="table-responsive">
                            <table class="table table-full table-bieuphi">
                              <tr>
                                <th>STT</th>
                                <th>Khoản mục phí</th>
                                <th>Mức phí</th>
                                <th>Tối thiểu</th>
                                <th>Tối đa</th>
                              </tr>
                              <?php
                              $cot2 = [
                                        'Phí quản lý tài khoản thanh toán VND',
                                        'Phí quản lý tài khoản thấu chi/rút vốn nhanh',
                                        'Phí đăng ký/hủy Dịch vụ Tài khoản Nhà đầu tư',
                                        'Phí duy trì Dịch vụ Tài khoản Nhà đầu tư',
                                        'Phí thay đổi thông tin Tài khoản Nhà đầu tư',
                                        'Phí đóng Tài khoản Nhà đầu tư',
                                        'Phí quản lý tài khoản của người lao động đi làm việc ở nước ngoài'
                                      ];
                               $cot3 = [
                                        '8.800 VND/tháng (miễn phí nếu số dư BQ tài khoản > 2 triệu VND/tháng)',
                                        '50.000 VND/tháng',
                                        'Miễn phí',
                                        '5.000 VND/tài khoản/tháng',
                                        'Mức phí thu tương tự Phí thay đổi thông tin/ Phí đóng tài khoản thanh toán (nếu có)',
                                        '200.000 VND KHCN CC: Miễn phí',
                                        'Theo thỏa thuận'
                                      ];
                               $cot4 = [
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '4 USD/tháng/tài khoản',
                                        ''
                                      ];

                              for($i=1;$i<=7;$i++) {
                              ?>                            
                              <tr>
                                <td><?php echo $i+8; ?></td>
                                <td><span class="b"><?php echo $cot2[$i-1] ?></span></td>
                                <td><?php echo $cot3[$i-1] ?></td>
                                <td><?php echo $cot4[$i-1] ?></td>
                                <td></td>
                              </tr>
                              <?php
                              } ?>
                            </table>
                          </div>

                          <h3 class="cl7">C. ĐÓNG TÀI KHOẢN</h3>
                          <div class="table-responsive">
                            <table class="table table-full table-bieuphi">
                              <tr>
                                <th>STT</th>
                                <th>Khoản mục phí</th>
                                <th>Mức phí</th>
                                <th>Tối thiểu</th>
                                <th>Tối đa</th>
                              </tr>
                              <?php
                              $cot2 = [
                                        'Đóng tài khoản thanh toán VND',
                                        'Đóng tài khoản thanh toán ngoại tệ',
                                        'Đóng tài khoản thấu chi/rút vốn nhanh',
                                        'Cung cấp sao kê, tài khoản phụ định kỳ hàng tháng',
                                        'Cung cấp sao kê, tài khoản theo yêu cầu của KH',
                                        'Sao kê/sổ phụ các giao dịch phát sinh bằng hoặc dưới 12 tháng',
                                        'Sao kê/sổ phụ các giao dịch phát sinh trên 12 tháng',
                                        'Phí fax sổ phụ, chứng từ theo yêu cầu của KH',
                                        'Gửi sổ phụ bằng thư bảo đảm cho KH (Khi KH có yêu cầu) bao gồm Phí cung cấp sao kê và Bưu phí'
                                      ];
                              $cot3 = [
                                        '50.000 VND',
                                        '5 USD/ 5 EUR/ ngoại tệ khác quy đổi 5 USD',
                                        '100.000 VND',
                                        '5.000 VND/tháng hoặc 0,5 USD/tháng',
                                        '',
                                        '5.000 VND/trang hoặc 0,25 USD/trang',
                                        '10.000 VND/trang hoặc 0,5 USD/trang',
                                        '5.000 VND/tờ',
                                        'Mức phí thu tương tự Mục 1.4.1.a; 1.4.1.b + Phí chuyển phát nhanh thực tế phát sinh',
                                      ];
                              $cot4 = [
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '
                                        - 50.000 VND/bản sao kê/tài khoản VND
                                        <br>
                                        - 2 USD/bản sao kê/tài khoản ngoại tệ',
                                        '
                                        - 100.000 VND/bản sao kê/tài khoản VND
                                        <br>
                                        - 5 USD/bản sao kê/tài khoản ngoại tệ',
                                        '10.000 VND',
                                        ''
                                      ];
                              for($i=1;$i<=9;$i++) {
                              ?>                            
                              <tr>
                                <td><?php echo $i+15; ?></td>
                                <td><span class="b"><?php echo $cot2[$i-1] ?></span></td>
                                <td><?php echo $cot3[$i-1] ?></td>
                                <td><?php echo $cot4[$i-1] ?></td>
                                <td></td>
                              </tr>
                              <?php
                              } ?>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
                <?php
                } ?>
            </div> 

<h3 class="cl7">LƯU Ý</h3>  
<p>
- Biểu phí dịch vụ chưa bao gồm thuế giá trị gia tăng (VAT)
</p>          
<p>
- Phí dịch vụ và các chi phí khác thu theo Biểu phí quy định không hoàn lại trong trường hợp khách hàng yêu cầu hủy bỏ giao dịch hoặc giao dịch không thực hiện được do sai sót, sự cố không phải do lỗi của MB gây ra.
</p>          
<p>

- Khách hàng có thể trả phí dịch vụ bằng VND (theo tỷ giá quy đổi là giá bán giao ngay của MB công bố tại thời điểm thu phí), hoặc ngoại tệ tương đương (theo tỷ giá quy đổi là tỷ giá bán giao ngay của MB công bố tại thời điểm thu phí).
</p>          
<p>

- Các giao dịch liên quan đến việc sử dụng ngoại tệ tuân thủ theo quy định Quản lý ngoại hối hiện hành của Ngân hàng Nhà nước và các văn bản liên quan.
</p>          
<p>

- Các chi phí thực tế như: Thuế, điện phí, bưu phí và các chi phí khác phải trả cho bên cung cấp dịch vụ (nếu có), thu theo thực tế phát sinh.
</p>          
<p>

- Những dịch vụ khác không được liệt kê tại Biểu phí dịch vụ này sẽ được MB thông báo cho khách hàng khi có nghiệp vụ phát sinh.
</p>          
<p>

- Biểu phí này có thể thay đổi theo chính sách của MB từng thời kỳ.
(*) Tài khoản không hoạt động trên 12 tháng là tài khoản sau 12 tháng liên tục không phát sinh bất kỳ giao dịch nào ngoài giao dịch thu phí và giao dịch trả lãi tài khoản của MB.
</p>          



           </div>
        </div>
        <div >
          <div class="tab-inner">
            2
          </div>
        </div>

      </div>
    </div>
  </div>
</main>


<?php include 'include/index-bottom.php';?>