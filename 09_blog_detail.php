<?php include 'include/index-top.php';?>
<?php include '_module/breadcrumb.php';?>
<section  class="banner-heading-3 next-shadow" >
	<div class="container">
		<div class="divtext">
			<div class="max750">
				<h1 class=" " >MB liên tiếp được vinh danh 2 giải thưởng “Ngân hàng số tiêu biểu” và “Ngân hàng cộng đồng tiêu biểu” năm 2018</h1>
			</div>
		</div>
	</div>
	<!-- <img class="img br lazy-hidden" data-lazy-type="image" data-lazy-src="assets/images/heading-10.svg" src=""> -->
</section>

<main id="main"  class="sec-b page-news-detail" >
	<div class="container">
		<div class=" max750">
			<div class="top-heading">
				<div class="date">01/ 12/ 2019</div>
				
				<ul class="blog-item-social ">
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-instagram"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="item" title="" target="_blank" href="#"><i class="icon-youtube-2"></i></a></li>
				</ul>					
			</div>

			<div class="entry-content">
				<p>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”.</p>
				<figure>
					<img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/750x300" src="https://via.placeholder.com/75x30">
					<figcaption> Ông Lê Xuân Vũ và Ông Nguyễn Đức Huy tại Lễ trao giải</figcaption>
				</figure>
				
				<p>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”.</p>
				<p><img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/750x300" src="https://via.placeholder.com/75x30"></p>
				
				<p>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”.</p>
				<p><img class="img lazy-hidden" data-lazy-type="image" data-lazy-src="https://via.placeholder.com/750x300" src="https://via.placeholder.com/75x30"></p>
				<p>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”.</p>
				<p>Tối 28/11/2018, tại Thành phố Hồ Chí Minh, trong khuôn khổ Diễn đàn Ngân hàng Bán lẻ 2018 (Vietnam Retail Banking Forum 2018) do Hiệp hội Ngân hàng Việt Nam (VNBA) và Tổ chức dữ liệu Quốc tế IDG đồng tổ chức, đã diễn ra lễ trao giải thưởng Ngân hàng Việt Nam tiêu biểu (VOBA) năm 2018. Ngân hàng TMCP Quân Đội (MB) xuất sắc nhận 2 giải thưởng “ngân hàng số tiêu biểu” và “ngân hàng vì cộng đồng tiêu biểu”.</p>	
			</div>
			<br>

			<div class="tags">
				<h2>Nội dung liên quan</h2>
				<a class="tag" href="#">Ngân hàng số dành cho doanh nghiệp</a>
				<a class="tag" href="#">Ngân hàng đầu tư</a>
				<a class="tag" href="#">Quản lý dòng tiền</a>
			</div>					
    	</div>
	</div>    	
</main>
</section>


<section  class="sec-tb" >
	<div class="container">
		<div class="entry-head">
			<h2 class="ht efch-1 ef-img-t">Tin bài liên quan</h2>
		</div>
		<div class="list-7  list-item row" >
		    <?php
		    for($i=1;$i<=2;$i++) {?>
		    	<div class="col-md-6">
		          <a href="#" class="item item-inline-table">
		          	<div class="img">
		          		<img class="lazy-hidden"  data-lazy-type="image" data-lazy-src="https://via.placeholder.com/262x187">
		          	</div>
		          	<div class="divtext">
		          		<div class="date">01/ 12/ 2019</div>
		          		<h4 class="title line2"><?php echo $i; ?> Đặc quyền cho chủ thẻ MB Visa Platinum</h4>
		          		<div class="desc line3">Tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay, tặng sổ tiết kiệm cho con, miễn phí trọn đời cho bố mẹ. Bố mẹ tham gia gói dịch vụ “Gia đình tôi yêu” hôm nay, Nhận ngay sổ tiết kiệm 1 triệu đồng. </div>
		          	</div>
		          </a>
	        	</div>
	    	<?php } ?>
	    </div>
	</div>	
</section>


<?php include 'include/index-bottom.php';?>