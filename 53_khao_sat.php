<?php include 'include/index-top.php'; ?>
<section class="sec-tb page-survey">
	<div class="container">
		<div class=" max750">
			<div class="text-center">
				<h1 class="title">Khảo sát hài lòng khách hàng</h1>
			</div>
			<div class="entry-content">
				<p class="b">Kính gửi: Quý khách hàng,</p>
				<p>Ngân hàng Quân đội (MB) xin cảm ơn Anh/chị đã sử dụng sản phẩm dịch vụ của MB.</p>
				<p>Để nâng cao chất lượng sản phẩm dịch vụ và chất lượng chăm sóc khách hàng, MB trân trọng kính mời Quý khách phản hồi các ý kiến đánh giá khách quan dưới đây:</p>
			</div>
			<form class="form-survey">
				<ul class="survey-l">
					<li><span class="num">1</span><span class="b">Quý khách hàng có hài lòng về kết quả hỗ trợ giải quyết khiếu nại của MB</span>
						<ul class="rate-l">
							<li><label class="radio "><input type="radio" name="rate1" checked=""><span></span>Rất hài lòng</label></li>
							<li><label class="radio "><input type="radio" name="rate1"><span></span>Hài lòng</label></li>
							<li><label class="radio "><input type="radio" name="rate1"><span></span>Bình thường</label></li>
							<li><label class="radio "><input type="radio" name="rate1"><span></span>Không hài lòng</label></li>
							<li><label class="radio "><input type="radio" name="rate1"><span></span>Rất không hài lòng</label></li>
						</ul>
					</li>
					<li><span class="num">2</span><span class="b">Quý khách hàng vui lòng cho biết các yếu tố cần cải thiện với công tác hỗ trợ giải quyết khiếu nại của MB ?</span>
						<ul class="rate-l">
							<li><label class="radio "><input type="radio" name="rate2"><span></span>Phương án và hướng xử lý của MB</label></li>
							<li><label class="radio "><input type="radio" name="rate2"><span></span>Thời gian xử lý của MB</label></li>
							<li><label class="radio "><input type="radio" name="rate2"><span></span>Thái độ, kỹ năng và sự nhiệt tình hỗ trợ của nhân viên chăm sóc khách hàng</label></li>
							<li><label class="radio"><input type="radio" name="rate2"><span></span>Khác</label><textarea class="input other" rows="3"></textarea></li>
						</ul>
					</li>
					<li><span class="num">3</span><span class="b">Để MB phục vụ Quý khách tốt hơn, vui lòng cho biết ý kiến đóng góp</span>
						<div class="rate-l">
							<textarea class="input" rows="3"></textarea>
						</div>
					</li>
				</ul>
				<div class="entry-content">
					<p>Những ý kiến quý báu của Quý khách sẽ là cơ sở để MB nâng cao chất lượng dịch vụ và chăm sóc khách hàng.</p>
					<p>Trân trọng cảm ơn Quý khách hàng!</p>
				</div>

				<div class="text-center">
					<button class="btn lg">Gửi khảo sát</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php include 'include/index-bottom.php'; ?>